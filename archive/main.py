#!/usr/bin/env python3

import flint as f
from math import pi
import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as LA

mu0    = 4*pi*1e-7
mu_air = 1.00000037*mu0
mu_cu  = 0.999994*mu0

if __name__ == "__main__":
    # Create a problem instance
    problem = f.Problem()

    # Create the boundaries in the problem
    boundary_segment_1 = f.CircularBoundarySegment(radius=0.001,
                                                   center=(0,-0.0015),
                                                   num_nodes=10,
                                                   poly_order=2,
                                                   normal_points_to_the_right=True)
    boundary_segment_2 = f.CircularBoundarySegment(radius=0.001,
                                                   center=(0,0.0015),
                                                   num_nodes=10,
                                                   poly_order=2,
                                                   normal_points_to_the_right=True)
    boundary_segment_3 = f.CircularBoundarySegment(radius=0.001,
                                                   center=(-0.003,0),
                                                   num_nodes=10,
                                                   poly_order=2,
                                                   normal_points_to_the_right=True)

    # Create the domains in the problem
    Omega_int_1 = f.Domain(boundary_segments=[[boundary_segment_1,True]],
                           mu=mu_cu,
                           J=1.0/1e-6)
    Omega_int_2 = f.Domain(boundary_segments=[[boundary_segment_2,True]],
                           mu=mu_cu,
                           J=-1.0/1e-6)
    Omega_int_3 = f.Domain(boundary_segments=[[boundary_segment_3,True]],
                           mu=500*mu0,
                           J=0.0)
    Omega_ext = f.ExteriorDomain(boundary_segments=[[boundary_segment_1,False],
                                                    [boundary_segment_2,False],
                                                    [boundary_segment_3,False]],
                                                    mu=mu_air)


    # Solve the problem
    #problem.solve(store_solution=True,solution_name="results/Two_cables_and_iron")
    problem.solve(load_solution=True,solution_name="results/Two_cables_and_iron")

    # Plot the solution
    field_vectors_x = []
    field_vectors_y = []
    field_vectors_u = []
    field_vectors_v = []
    #for radius in np.linspace(0.0015,0.02,10):
    #for radius in [0.0026]:
    for radius in [0.0011]:
        for phi in np.linspace(0,2*np.pi,25)[:-1]:
            x   = radius*np.cos(phi)
            y   = radius*np.sin(phi)+0.0015
            sol = problem.evaluateInteriorSolution(domain_index=Omega_ext.getIndex(),
                                                   x=x,
                                                   y=y)
            #print("*********")
            #print(sol)
            #print(LA.norm(sol))
            field_vectors_x.append(x)
            field_vectors_y.append(y)
            field_vectors_u.append(sol[0])
            field_vectors_v.append(sol[1])
    for radius in [0.0011]:
        for phi in np.linspace(0,2*np.pi,25)[:-1]:
            x   = radius*np.cos(phi)
            y   = radius*np.sin(phi)-0.0015
            sol = problem.evaluateInteriorSolution(domain_index=Omega_ext.getIndex(),
                                                   x=x,
                                                   y=y)
            #print("*********")
            #print(sol)
            #print(LA.norm(sol))
            field_vectors_x.append(x)
            field_vectors_y.append(y)
            field_vectors_u.append(sol[0])
            field_vectors_v.append(sol[1])

    for radius in [0.0011]:
        for phi in np.linspace(0,2*np.pi,25)[:-1]:
            x   = radius*np.cos(phi)-0.003
            y   = radius*np.sin(phi)
            sol = problem.evaluateInteriorSolution(domain_index=Omega_ext.getIndex(),
                                                   x=x,
                                                   y=y)
            #print("*********")
            #print(sol)
            #print(LA.norm(sol))
            field_vectors_x.append(x)
            field_vectors_y.append(y)
            field_vectors_u.append(sol[0])
            field_vectors_v.append(sol[1])

    # Compute statistics for debugging
    #outer_radius = 0.002
    #cable_area   = 0.001*0.001*np.pi
    #current      = cable_area*1.0/1e-6
    #H_field_norm = current/(2.0*outer_radius*np.pi)
    #B_field_norm = H_field_norm*mu_air
    #print("The magnetic flux density should have a norm of {0}T.".format(B_field_norm))

    plt.quiver(np.array(field_vectors_x),np.array(field_vectors_y),field_vectors_u,field_vectors_v)
    plt.axis('equal')
    plt.tight_layout()
    plt.show()
