from os.path import dirname,abspath
from ctypes import CDLL,c_uint,c_double,c_bool,c_char_p
import numpy as np
from math import sin,cos
from matplotlib.colors import hsv_to_rgb

# Load the C++ library
flint = CDLL(dirname(abspath(__file__))+'/build/libflint.so')
flint.init()
flint.solve.argtypes                           = [c_uint,c_uint,c_char_p]
flint.createCircularBoundarySegment.argtypes   = [c_double,c_double,c_double,c_uint,c_uint,c_uint,c_uint]
flint.createCircularBoundarySegment.restype    = c_uint
flint.createArcedBoundarySegment.argtypes      = [c_double,c_double,c_double,c_double,c_double,c_uint,c_uint,c_uint,c_uint]
flint.createArcedBoundarySegment.restype       = c_uint
flint.createStraightBoundarySegment.argtypes   = [c_double,c_double,c_double,c_double,c_uint,c_uint,c_uint,c_uint]
flint.createStraightBoundarySegment.restype    = c_uint
flint.createDomain.argtypes                    = [np.ctypeslib.ndpointer(dtype=c_uint,flags="C_CONTIGUOUS"),
                                                  np.ctypeslib.ndpointer(dtype=c_uint,flags="C_CONTIGUOUS"),
                                                  c_uint,
                                                  c_double,
                                                  c_double]
flint.createDomain.restype                     = c_uint
flint.evaluateInteriorSolution.argtypes        = [c_uint,c_double,c_double,np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS")]
flint.getNumBoundarySegmentDataPoints.argtypes = [c_uint,c_double]
flint.getNumBoundarySegmentDataPoints.restype  = c_uint
flint.getBoundarySegmentDataPoints.argtypes    = [c_uint,np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS"),
                                                         np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS"),c_double]
flint.getNormals.argtypes                      = [c_uint,c_double,np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS"),
                                                                  np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS"),
                                                                  np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS"),
                                                                  np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS")]
flint.getTangentials.argtypes                  = [c_uint,c_double,np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS"),
                                                                  np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS"),
                                                                  np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS"),
                                                                  np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS")]
flint.getNumDataPointsType1.argtypes           = [c_uint,c_uint,c_uint,c_uint]
flint.getNumDataPointsType1.restype            = c_uint
flint.getFieldDataType1.argtypes               = [c_uint,c_uint,c_uint,c_uint,c_uint,np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS"),
                                                                                     np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS"),
                                                                                     np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS"),
                                                                                     np.ctypeslib.ndpointer(dtype=c_double,flags="C_CONTIGUOUS")]

class Problem():
    def __init__(self):
        flint.createProblem()
        self.domains           = []
        self.boundary_segments = []

    def solve(self,load_solution=False,store_solution=False,solution_name=None):
        if store_solution == True and load_solution == True:
            raise ValueError('Invalid arguments provided to Problem.solve. Can\'t load and store a solution at the same time.')
        folder_name = ""
        if store_solution == True:
            store_solution_ = 1
            folder_name     = solution_name
            folder_name    = solution_name
            if not len(solution_name) > 0:
                raise ValueError('Invalid solution_name provided to Problem.solve.')
        else:
            store_solution_ = 0
        if load_solution == True:
            load_solution_ = 1
            folder_name    = solution_name
            if not len(solution_name) > 0:
                raise ValueError('Invalid solution_name provided to Problem.solve.')
        else:
            load_solution_ = 0
        flint.solve(load_solution_,store_solution_,str.encode(folder_name))

    def evaluateInteriorSolution(self,domain_index,x,y):
        sol = np.ndarray((2,),dtype=c_double)
        flint.evaluateInteriorSolution(domain_index,x,y,sol)
        return sol.reshape(2,1)

    def addCircularBoundarySegment(self,radius,center,num_nodes,poly_order,normal_points_to_the_right,is_reference=False):
        self.boundary_segments.append(CircularBoundarySegment(radius,center,num_nodes,poly_order,normal_points_to_the_right,is_reference))
        return self.boundary_segments[-1]

    def addArcedBoundarySegment(self,radius,center,phi_min,phi_max,num_nodes,poly_order,normal_points_to_the_right,is_reference=False):
        self.boundary_segments.append(ArcedBoundarySegment(radius,center,phi_min,phi_max,num_nodes,poly_order,normal_points_to_the_right,is_reference))
        return self.boundary_segments[-1]

    def addStraightBoundarySegment(self,start,end,num_nodes,poly_order,normal_points_to_the_right,is_reference=False):
        self.boundary_segments.append(StraightBoundarySegment(start,end,num_nodes,poly_order,normal_points_to_the_right,is_reference))
        return self.boundary_segments[-1]

    def addDomain(self,boundary_segments,mu,J):
        self.domains.append(Domain(boundary_segments,mu,J))
        return self.domains[-1]

    def addReferenceDomain(self,boundary_segments,domain_type,underlying_domain):
        self.domains.append(ReferenceDomain(boundary_segments,domain_type,underlying_domain))
        return self.domains[-1]

    def addExteriorDomain(self,boundary_segments,mu):
        self.domains.append(ExteriorDomain(boundary_segments,mu))
        return self.domains[-1]

    def getBoundarySegmentsPlottingData(self,max_length_per_segment,show_reference_boundaries):
        boundary_segments_plotting_data = []
        for boundary_segment in self.boundary_segments:
            if not show_reference_boundaries and boundary_segment.isReference():
                continue
            elif boundary_segment.isReference():
                font = '.k'
            else:
                font = 'k'
            num_boundary_segment_data_points = flint.getNumBoundarySegmentDataPoints(boundary_segment.index,max_length_per_segment)
            boundary_segment_data_points_x = np.ndarray((num_boundary_segment_data_points,),dtype=c_double)
            boundary_segment_data_points_y = np.ndarray((num_boundary_segment_data_points,),dtype=c_double)
            flint.getBoundarySegmentDataPoints(boundary_segment.index,boundary_segment_data_points_x,boundary_segment_data_points_y,max_length_per_segment)
            boundary_segments_plotting_data.append({'x':boundary_segment_data_points_x,'y':boundary_segment_data_points_y,'font':font})
        return boundary_segments_plotting_data

    def getNormals(self,max_length_per_segment,arrow_length):
        normals = []
        for domain in self.domains:
            if domain.isReference():
                continue
            color = hsv_to_rgb([domain.getIndex()/len(self.domains),1,1])
            for boundary_segment in domain.getBoundarySegments():
                num_boundary_segment_data_points = flint.getNumBoundarySegmentDataPoints(boundary_segment[0].index,max_length_per_segment)
                normals_x = np.ndarray((num_boundary_segment_data_points,),dtype=c_double)
                normals_y = np.ndarray((num_boundary_segment_data_points,),dtype=c_double)
                normals_u = np.ndarray((num_boundary_segment_data_points,),dtype=c_double)
                normals_v = np.ndarray((num_boundary_segment_data_points,),dtype=c_double)
                flint.getNormals(boundary_segment[0].index,max_length_per_segment,normals_x,normals_y,normals_u,normals_v)
                if boundary_segment[1]:
                    sign = 1
                else:
                    sign = -1
                normals.append({'x':normals_x,'y':normals_y,'u':sign*normals_u*arrow_length,'v':sign*normals_v*arrow_length,'color':color})
        return normals

    def getTangentials(self,max_length_per_segment,arrow_length):
        tangentials = []
        for domain in self.domains:
            if domain.isReference():
                continue
            color = hsv_to_rgb([domain.getIndex()/len(self.domains),1,1])
            for boundary_segment in domain.getBoundarySegments():
                num_boundary_segment_data_points = flint.getNumBoundarySegmentDataPoints(boundary_segment[0].index,max_length_per_segment)
                tangentials_x = np.ndarray((num_boundary_segment_data_points,),dtype=c_double)
                tangentials_y = np.ndarray((num_boundary_segment_data_points,),dtype=c_double)
                tangentials_u = np.ndarray((num_boundary_segment_data_points,),dtype=c_double)
                tangentials_v = np.ndarray((num_boundary_segment_data_points,),dtype=c_double)
                flint.getTangentials(boundary_segment[0].index,max_length_per_segment,tangentials_x,tangentials_y,tangentials_u,tangentials_v)
                if boundary_segment[1]:
                    sign = 1
                else:
                    sign = -1
                tangentials.append({'x':tangentials_x,'y':tangentials_y,'u':sign*tangentials_u*arrow_length,'v':sign*tangentials_v*arrow_length,'color':color})
        return tangentials

    def getFieldData(self):
        field_data = []
        for domain in self.domains:
            domain_type = domain.getType()
            if domain_type == 1:
                boundary_segments = domain.getBoundarySegments()
                num_data_points   = flint.getNumDataPointsType1(boundary_segments[0][0].index,boundary_segments[1][0].index,4,100)
                field_data_x      = np.ndarray((num_data_points,),dtype=c_double)
                field_data_y      = np.ndarray((num_data_points,),dtype=c_double)
                field_data_u      = np.ndarray((num_data_points,),dtype=c_double)
                field_data_v      = np.ndarray((num_data_points,),dtype=c_double)
                flint.getFieldDataType1(domain.getIndex(),boundary_segments[0][0].index,boundary_segments[1][0].index,4,100,field_data_x,field_data_y,field_data_u,field_data_v)
                field_data.append({'x':field_data_x,'y':field_data_y,'u':field_data_u,'v':field_data_v})
        return field_data

class CircularBoundarySegment():
    def __init__(self,radius,center,num_nodes,poly_order,normal_points_to_the_right,is_reference):
        self.is_reference = is_reference
        if normal_points_to_the_right:
            normal_points_to_the_right_ = 1
        else:
            normal_points_to_the_right_ = 0
        if is_reference:
            is_reference_ = 1
        else:
            is_reference_ = 0
        self.index = flint.createCircularBoundarySegment(radius,
                                                         center[0],
                                                         center[1],
                                                         num_nodes,
                                                         poly_order,
                                                         normal_points_to_the_right_,
                                                         is_reference_)

    def isReference(self):
        return self.is_reference

class ArcedBoundarySegment():
    def __init__(self,radius,center,phi_min,phi_max,num_nodes,poly_order,normal_points_to_the_right,is_reference):
        self.radius       = radius
        self.center       = np.array(center).reshape(-1,)
        self.phi_min      = phi_min
        self.phi_max      = phi_max
        self.is_reference = is_reference
        if normal_points_to_the_right:
            normal_points_to_the_right_ = 1
        else:
            normal_points_to_the_right_ = 0
        if is_reference:
            is_reference_ = 1
        else:
            is_reference_ = 0
        self.index = flint.createArcedBoundarySegment(radius,
                                                      center[0],
                                                      center[1],
                                                      phi_min,
                                                      phi_max,
                                                      num_nodes,
                                                      poly_order,
                                                      normal_points_to_the_right_,
                                                      is_reference_)

    def getStartPoint(self):
        return self.center + self.radius*np.array([cos(self.phi_min),sin(self.phi_min)]).reshape(-1,)

    def getEndPoint(self):
        return self.center + self.radius*np.array([cos(self.phi_max),sin(self.phi_max)]).reshape(-1,)

    def isReference(self):
        return self.is_reference

class StraightBoundarySegment():
    def __init__(self,start,end,num_nodes,poly_order,normal_points_to_the_right,is_reference):
        self.start        = start
        self.end          = end
        self.is_reference = is_reference
        if normal_points_to_the_right:
            normal_points_to_the_right_ = 1
        else:
            normal_points_to_the_right_ = 0
        if is_reference:
            is_reference_ = 1
        else:
            is_reference_ = 0
        self.index = flint.createStraightBoundarySegment(start[0],
                                                         start[1],
                                                         end[0],
                                                         end[1],
                                                         num_nodes,
                                                         poly_order,
                                                         normal_points_to_the_right_,
                                                         is_reference_)

    def getStartPoint(self):
        return self.start

    def getEndPoint(self):
        return self.end

    def isReference(self):
        return self.is_reference

class Domain():
    def __init__(self,boundary_segments,mu,J,domain_type=None):
        boundary_segments_           = np.ndarray((len(boundary_segments),),dtype=c_uint)
        boundary_segment_directions_ = np.ndarray((len(boundary_segments),),dtype=c_uint)
        for i in range(len(boundary_segments)):
            boundary_segments_[i] = boundary_segments[i][0].index
            if boundary_segments[i][1]:
                boundary_segment_directions_[i] = 1
            else:
                boundary_segment_directions_[i] = 0
        self.index = flint.createDomain(boundary_segments_,
                                        boundary_segment_directions_,
                                        len(boundary_segments),
                                        mu,
                                        J)
        self.boundary_segments = boundary_segments
        self.domain_type       = domain_type

    def getIndex(self):
        return self.index

    def getBoundarySegments(self):
        return self.boundary_segments

    def getType(self):
        return self.domain_type

    def isReference(self):
        return False

class ExteriorDomain():
    def __init__(self,boundary_segments,mu):
        self.domain = Domain(boundary_segments,mu,0.0)

    def getIndex(self):
        return self.domain.index

    def getBoundarySegments(self):
        return self.domain.getBoundarySegments()

    def getType(self):
        return self.domain.domain_type

    def isReference(self):
        return False

class ReferenceDomain():
    def __init__(self,boundary_segments,domain_type,underlying_domain):
        self.boundary_segments = boundary_segments
        self.domain_type       = domain_type
        self.underlying_domain = underlying_domain

    def getType(self):
        return self.domain_type

    def getBoundarySegments(self):
        return self.boundary_segments

    def isReference(self):
        return True

    def getIndex(self):
        return self.underlying_domain.getIndex()
