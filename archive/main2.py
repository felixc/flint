#!/usr/bin/env python3

import flint as f
from math import sin,cos,pi
import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as LA

mu0                     = 4*pi*1e-7
mu_air                  = 1.00000037*mu0
mu_cu                   = 0.999994*mu0

yoke_outer_radius       = 0.03
yoke_inner_radius       = 0.025
yoke_inner_radius_angle = 161.5862/360.0*2*pi
teeth_angle_width       = 30.4879/360.0*2*pi

if __name__ == "__main__":
    # Create a problem instance
    p = f.Problem()

    # Create the boundaries in the problem
    # Yoke
    outer_rim = p.addCircularBoundarySegment(radius=yoke_outer_radius,
                                             center=(0.0,0.0),
                                             num_nodes=10,
                                             poly_order=2,
                                             normal_points_to_the_right=True)
    upper_inner_rim = p.addArcedBoundarySegment(radius=yoke_inner_radius,
                                                center=(0.0,0.0),
                                                phi_min=pi/2.0-yoke_inner_radius_angle/2.0,
                                                phi_max=pi/2.0+yoke_inner_radius_angle/2.0,
                                                num_nodes=5,
                                                poly_order=2,
                                                normal_points_to_the_right=False)
    lower_inner_rim = p.addArcedBoundarySegment(radius=yoke_inner_radius,
                                                center=(0.0,0.0),
                                                phi_min=3.0*pi/2.0-yoke_inner_radius_angle/2.0,
                                                phi_max=3.0*pi/2.0+yoke_inner_radius_angle/2.0,
                                                num_nodes=5,
                                                poly_order=2,
                                                normal_points_to_the_right=False)
    left_inner_rim = p.addArcedBoundarySegment(radius=0.0155,
                                               center=(0.0,0.0),
                                               phi_min=pi-teeth_angle_width/2.0,
                                               phi_max=pi+teeth_angle_width/2.0,
                                               num_nodes=2,
                                               poly_order=2,
                                               normal_points_to_the_right=False)
    right_inner_rim = p.addArcedBoundarySegment(radius=0.0155,
                                                center=(0.0,0.0),
                                                phi_min=-teeth_angle_width/2.0,
                                                phi_max=teeth_angle_width/2.0,
                                                num_nodes=2,
                                                poly_order=2,
                                                normal_points_to_the_right=False)
    straight_segment_left_upper = p.addStraightBoundarySegment(start=upper_inner_rim.getEndPoint(),
                                                               end=left_inner_rim.getStartPoint(),
                                                               num_nodes=2,
                                                               poly_order=2,
                                                               normal_points_to_the_right=False)
    straight_segment_left_lower = p.addStraightBoundarySegment(start=left_inner_rim.getEndPoint(),
                                                               end=lower_inner_rim.getStartPoint(),
                                                               num_nodes=2,
                                                               poly_order=2,
                                                               normal_points_to_the_right=False)
    straight_segment_right_lower = p.addStraightBoundarySegment(start=lower_inner_rim.getEndPoint(),
                                                                end=right_inner_rim.getStartPoint(),
                                                                num_nodes=2,
                                                                poly_order=2,
                                                                normal_points_to_the_right=False)
    straight_segment_right_upper = p.addStraightBoundarySegment(start=right_inner_rim.getEndPoint(),
                                                                end=upper_inner_rim.getStartPoint(),
                                                                num_nodes=2,
                                                                poly_order=2,
                                                                normal_points_to_the_right=False)
    cable_boundary_left_upper = p.addCircularBoundarySegment(radius=0.0025,
                                                             center=(0.0215*cos(pi-20.5/360.0*2.0*pi),0.0215*sin(pi-20.5/360.0*2.0*pi)),
                                                             num_nodes=6,
                                                             poly_order=2,
                                                             normal_points_to_the_right=True)
    cable_boundary_left_lower = p.addCircularBoundarySegment(radius=0.0025,
                                                             center=(0.0215*cos(pi+20.5/360.0*2.0*pi),0.0215*sin(pi+20.5/360.0*2.0*pi)),
                                                             num_nodes=6,
                                                             poly_order=2,
                                                             normal_points_to_the_right=True)
    cable_boundary_right_lower = p.addCircularBoundarySegment(radius=0.0025,
                                                              center=(0.0215*cos(-20.5/360.0*2.0*pi),0.0215*sin(-20.5/360.0*2.0*pi)),
                                                              num_nodes=6,
                                                              poly_order=2,
                                                              normal_points_to_the_right=True)
    cable_boundary_right_upper = p.addCircularBoundarySegment(radius=0.0025,
                                                              center=(0.0215*cos(20.5/360.0*2.0*pi),0.0215*sin(20.5/360.0*2.0*pi)),
                                                              num_nodes=6,
                                                              poly_order=2,
                                                              normal_points_to_the_right=True)
    rotor_boundary = p.addCircularBoundarySegment(radius=0.0135,
                                                  center=(0.0,0.0),
                                                  num_nodes=12,
                                                  poly_order=2,
                                                  normal_points_to_the_right=True)

    # Create the domains in the problem
    yoke = p.addDomain(boundary_segments=[[outer_rim,True],
                                          [upper_inner_rim,True],
                                          [straight_segment_left_upper,True],
                                          [left_inner_rim,True],
                                          [straight_segment_left_lower,True],
                                          [lower_inner_rim,True],
                                          [straight_segment_right_lower,True],
                                          [right_inner_rim,True],
                                          [straight_segment_right_upper,True]],
                                          mu=500*mu0,
                                          J=0.0)
    rotor = p.addDomain(boundary_segments=[[rotor_boundary,True]],
                        mu=500*mu0,
                        J=0.0)
    cable_left_upper  = p.addDomain(boundary_segments=[[cable_boundary_left_upper,True]],
                                    mu=mu_cu,
                                    J=-1.0/1e-6)
    cable_left_lower  = p.addDomain(boundary_segments=[[cable_boundary_left_lower,True]],
                                    mu=mu_cu,
                                    J=1.0/1e-6)
    cable_right_lower = p.addDomain(boundary_segments=[[cable_boundary_right_lower,True]],
                                    mu=mu_cu,
                                    J=1.0/1e-6)
    air_interior = p.addDomain(boundary_segments=[[upper_inner_rim,False],
                                                  [straight_segment_left_upper,False],
                                                  [left_inner_rim,False],
                                                  [straight_segment_left_lower,False],
                                                  [lower_inner_rim,False],
                                                  [straight_segment_right_lower,False],
                                                  [right_inner_rim,False],
                                                  [straight_segment_right_upper,False],
                                                  [cable_boundary_left_upper,False],
                                                  [cable_boundary_left_lower,False],
                                                  [cable_boundary_right_lower,False],
                                                  [cable_boundary_right_upper,False],
                                                  [rotor_boundary,False]],
                                                  mu=mu_air,
                                                  J=0.0)
    Omega_ext = p.addExteriorDomain(boundary_segments=[[outer_rim,False]],
                                    mu=mu_air)
    cable_right_upper = p.addDomain(boundary_segments=[[cable_boundary_right_upper,True]],
                                    mu=mu_cu,
                                    J=-1.0/1e-6)

    # Solve the problem
    p.solve(store_solution=False,solution_name="results/DC_motor")
    #p.solve(load_solution=True,solution_name="results/DC_motor")

    # Plot the boundaries in the problem
    for boundary_segment_plotting_data in p.getBoundarySegmentsPlottingData(max_length_per_segment=0.001/4,show_reference_boundaries=False):
        plt.plot(boundary_segment_plotting_data['x'],boundary_segment_plotting_data['y'],'r')

    # Plot the normals of the boundaries
    for normals in p.getNormals(max_length_per_segment=0.001,arrow_length=1e-3):
        plt.quiver(normals['x'],normals['y'],normals['u'],normals['v'],angles='xy',scale_units='xy',scale=1,color=normals['color'])

    # Plot the tangentials of the boundaries
    for tangentials in p.getTangentials(max_length_per_segment=0.001,arrow_length=1e-3):
        plt.quiver(tangentials['x'],tangentials['y'],tangentials['u'],tangentials['v'],angles='xy',scale_units='xy',scale=1,color=tangentials['color'])

    #plt.axis('equal')
    #plt.tight_layout()
    #plt.show()

    #exit(0)

    # Plot the solution
    ##for
    #plt.quiver(np.array(field_vectors_x),np.array(field_vectors_y),field_vectors_u,field_vectors_v)
    #plt.axis('equal')
    #plt.tight_layout()
    #plt.show()


    # Plot the solution
    field_vectors_x = []
    field_vectors_y = []
    field_vectors_u = []
    field_vectors_v = []
    #for radius in np.linspace(0.0015,0.02,10):
    #for radius in [0.0026]:
    for radius in [0.013]:
        for phi in np.linspace(0,2*np.pi,37)[:-1]:
            x   = radius*np.cos(phi)
            y   = radius*np.sin(phi)
            sol = p.evaluateInteriorSolution(domain_index=rotor.getIndex(),
                                             x=x,
                                             y=y)
            #print("*********")
            #print(sol)
            #print(LA.norm(sol))
            field_vectors_x.append(x)
            field_vectors_y.append(y)
            field_vectors_u.append(sol[0])
            field_vectors_v.append(sol[1])
    #for radius in [0.0011]:
    #    for phi in np.linspace(0,2*np.pi,25)[:-1]:
    #        x   = radius*np.cos(phi)
    #        y   = radius*np.sin(phi)-0.0015
    #        sol = problem.evaluateInteriorSolution(domain_index=Omega_ext.getIndex(),
    #                                               x=x,
    #                                               y=y)
    #        #print("*********")
    #        #print(sol)
    #        #print(LA.norm(sol))
    #        field_vectors_x.append(x)
    #        field_vectors_y.append(y)
    #        field_vectors_u.append(sol[0])
    #        field_vectors_v.append(sol[1])

    #for radius in [0.0011]:
    #    for phi in np.linspace(0,2*np.pi,25)[:-1]:
    #        x   = radius*np.cos(phi)-0.003
    #        y   = radius*np.sin(phi)
    #        sol = problem.evaluateInteriorSolution(domain_index=Omega_ext.getIndex(),
    #                                               x=x,
    #                                               y=y)
    #        #print("*********")
    #        #print(sol)
    #        #print(LA.norm(sol))
    #        field_vectors_x.append(x)
    #        field_vectors_y.append(y)
    #        field_vectors_u.append(sol[0])
    #        field_vectors_v.append(sol[1])

    # Compute statistics for debugging
    #outer_radius = 0.002
    #cable_area   = 0.001*0.001*np.pi
    #current      = cable_area*1.0/1e-6
    #H_field_norm = current/(2.0*outer_radius*np.pi)
    #B_field_norm = H_field_norm*mu_air
    #print("The magnetic flux density should have a norm of {0}T.".format(B_field_norm))

    plt.quiver(np.array(field_vectors_x),np.array(field_vectors_y),field_vectors_u,field_vectors_v)
    plt.axis('equal')
    plt.tight_layout()
    plt.show()
