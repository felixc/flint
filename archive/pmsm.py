#!/usr/bin/env python3

import flint as f
from math import sin,cos,pi
import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as LA

mu0                     = 4*pi*1e-7
mu_air                  = 1.00000037*mu0
mu_cu                   = 0.999994*mu0
mu_magnet               = 1.05*mu0 # https://en.wikipedia.org/wiki/Permeability_(electromagnetism)
mu_iron                 = 500*mu0

yoke_outer_radius       = 0.03
yoke_inner_radius       = 0.025
yoke_inner_radius_angle = 161.5862/360.0*2*pi
teeth_angle_width       = 30.4879/360.0*2*pi

num_magnets = 4
num_teeth   = 4

dm1 = 0.0035
dm2 = 0.0115
dm3 = 0.004

cm1 = 0.017
cm2 = 0.023
cm3 = 0.005
cm4 = 0.007

dt1 = 0.0155
dt2 = 2*pi*78/360

rotor_radius = 0.013

stator_outer_radius = 0.028

rotor_angle = 20/360*2*pi

if __name__ == "__main__":
    # Create a problem instance
    p = f.Problem()

    # Create the boundaries in the problem
    # Magnets
    all_magnets_boundary_segments = []
    for magnet_index in range(num_magnets):
        corners = np.array([[-(dm2-dm1)/2,(dm2-dm1)/2,(dm2-dm1)/2,-(dm2-dm1)/2],
                            [-dm3/2,-dm3/2,dm3/2,dm3/2]])
        phi     = 2*pi/num_magnets*magnet_index-rotor_angle
        rot_mat = np.array([[cos(phi),-sin(phi)],
                            [sin(phi),cos(phi)]])
        rotated_corners = rot_mat@corners
        shifted_corners = rotated_corners + rot_mat@np.array([[(dm2+dm1)/2],[0]])
        num_nodes = [3,2,3,2]
        all_magnets_boundary_segments.append([])
        for c in range(4):
            all_magnets_boundary_segments[magnet_index].append(p.addStraightBoundarySegment(start=shifted_corners[:,c],
                                                                                            end=shifted_corners[:,(c+1)%4],
                                                                                            num_nodes=num_nodes[c],
                                                                                            poly_order=2,
                                                                                            normal_points_to_the_right=True))

    # Rotor
    rotor_outer_rim = p.addCircularBoundarySegment(radius=rotor_radius,
                                                   center=(0,0),
                                                   num_nodes=12,
                                                   poly_order=2,
                                                   normal_points_to_the_right=True)

    # Coils
    all_coils_boundary_segments = []
    for coil_index in range(num_teeth):
        corners = np.array([[-(cm2-cm1)/2,(cm2-cm1)/2,(cm2-cm1)/2,-(cm2-cm1)/2],
                            [-cm3/2,-cm4/2,cm4/2,cm3/2]])
        phi     = 2*pi/num_teeth*coil_index
        rot_mat = np.array([[cos(phi),-sin(phi)],
                            [sin(phi),cos(phi)]])
        rotated_corners = rot_mat@corners
        shifted_corners = rotated_corners + rot_mat@np.array([[(cm2+cm1)/2],[0]])
        num_nodes = [3,2,3,2]
        all_coils_boundary_segments.append([])
        for c in range(4):
            all_coils_boundary_segments[coil_index].append(p.addStraightBoundarySegment(start=shifted_corners[:,c],
                                                                                        end=shifted_corners[:,(c+1)%4],
                                                                                        num_nodes=num_nodes[c],
                                                                                        poly_order=2,
                                                                                        normal_points_to_the_right=True))

    # Teeth
    all_teeth_boundary_segments = []
    for teeth_index in range(num_teeth):
        all_teeth_boundary_segments.append([])
        all_teeth_boundary_segments[teeth_index].append(p.addArcedBoundarySegment(radius=dt1,
                                                                                  center=(0,0),
                                                                                  phi_min=-dt2/2+2*pi/num_teeth*(teeth_index+0.5),
                                                                                  phi_max=dt2/2+2*pi/num_teeth*(teeth_index+0.5),
                                                                                  num_nodes=3,
                                                                                  poly_order=2,
                                                                                  normal_points_to_the_right=False))
        all_teeth_boundary_segments[teeth_index].append(p.addStraightBoundarySegment(start=all_coils_boundary_segments[teeth_index][2].getEndPoint(),
                                                                                     end=all_teeth_boundary_segments[-1][0].getStartPoint(),
                                                                                     num_nodes=2,
                                                                                     poly_order=2,
                                                                                     normal_points_to_the_right=False))
        all_teeth_boundary_segments[teeth_index].append(p.addStraightBoundarySegment(start=all_teeth_boundary_segments[-1][0].getEndPoint(),
                                                                                     end=all_coils_boundary_segments[(teeth_index+1)%4][0].getStartPoint(),
                                                                                     num_nodes=2,
                                                                                     poly_order=2,
                                                                                     normal_points_to_the_right=False))

    # Stator
    stator_outer_rim = p.addCircularBoundarySegment(radius=stator_outer_radius,
                                                    center=(0,0),
                                                    num_nodes=12,
                                                    poly_order=2,
                                                    normal_points_to_the_right=True)

    # Reference boundaries
    interior_air_outer_rim_r = p.addCircularBoundarySegment(radius=dt1,
                                                            center=(0,0),
                                                            num_nodes=12,
                                                            poly_order=2,
                                                            normal_points_to_the_right=True,
                                                            is_reference=True)

    # Create the domains in the problem
    # Magnets
    magnet_domains = []
    for magnet_index in range(num_magnets):
        magnet_domains.append(p.addDomain(boundary_segments=[[boundary_segment,True] for boundary_segment in all_magnets_boundary_segments[magnet_index]],
                                          mu=mu_magnet,
                                          J=0.0))

    # Rotor
    p.addDomain(boundary_segments=[[boundary_segment,False] for magnet_boundary_segments in all_magnets_boundary_segments for boundary_segment in magnet_boundary_segments]+[[rotor_outer_rim,True]],
                mu=mu_iron,
                J=0.0)

    # Coils
    coil_domains = []
    for coil_index in range(num_teeth):
        coil_domains.append(p.addDomain(boundary_segments=[[boundary_segment,True] for boundary_segment in all_coils_boundary_segments[coil_index]],
                                        mu=mu_cu,
                                        J=2/1e-6*((-1)**coil_index)))

    # Inner air
    inner_air = p.addDomain(boundary_segments=[[boundary_segment,False] for teeth_boundary_segments in all_teeth_boundary_segments for boundary_segment in teeth_boundary_segments]+
                                              [[coil_boundary_segments[3],False] for coil_boundary_segments in all_coils_boundary_segments]+[[rotor_outer_rim,False]],
                                              mu=mu_air,
                                              J=0.0)

    # Yoke
    p.addDomain(boundary_segments=[[boundary_segment,True] for teeth_boundary_segments in all_teeth_boundary_segments for boundary_segment in teeth_boundary_segments]+
                                  [[boundary_segment,False] for coil_boundary_segments in all_coils_boundary_segments for boundary_segment in coil_boundary_segments[0:3]]+
                                  [[stator_outer_rim,True]],
                                  mu=mu_iron,
                                  J=0.0)

    # Exterior
    Omega_ext = p.addExteriorDomain(boundary_segments=[[stator_outer_rim,False]],
                                    mu=mu_air)

    #=========>
    # Reference domains
    p.addReferenceDomain(boundary_segments=[[rotor_outer_rim,False],
                                            [interior_air_outer_rim_r,True]],
                                            domain_type=1,
                                            underlying_domain=inner_air)
    #=========>

    # Solve the problem
    #p.solve(store_solution=True,solution_name="results/PMSM")
    p.solve(load_solution=True,solution_name="results/PMSM")

    # Plot the boundaries in the problem
    for boundary_segment_plotting_data in p.getBoundarySegmentsPlottingData(max_length_per_segment=0.001/4,show_reference_boundaries=True):
        plt.plot(boundary_segment_plotting_data['x'],boundary_segment_plotting_data['y'],boundary_segment_plotting_data['font'],markersize=1)

    # Plot the normals of the boundaries
    for normals in p.getNormals(max_length_per_segment=0.001,arrow_length=1e-3):
        plt.quiver(normals['x'],normals['y'],normals['u'],normals['v'],angles='xy',scale_units='xy',scale=1,color=normals['color'])

    # Plot the tangentials of the boundaries
    for tangentials in p.getTangentials(max_length_per_segment=0.001,arrow_length=1e-3):
        plt.quiver(tangentials['x'],tangentials['y'],tangentials['u'],tangentials['v'],angles='xy',scale_units='xy',scale=1,color=tangentials['color'])
    #=========>

    # Plot the magnetic flux density
    for field_data in p.getFieldData():
        plt.quiver(field_data['x'],field_data['y'],field_data['u'],field_data['v'])

    plt.axis('equal')
    plt.tight_layout()
    plt.show()

    exit(0)

    # Plot the solution
    ##for
    #plt.quiver(np.array(field_vectors_x),np.array(field_vectors_y),field_vectors_u,field_vectors_v)
    #plt.axis('equal')
    #plt.tight_layout()
    #plt.show()


    # Plot the solution
    field_vectors_x = []
    field_vectors_y = []
    field_vectors_u = []
    field_vectors_v = []
    #for radius in np.linspace(0.0015,0.02,10):
    #for radius in [0.0026]:
    for radius in [0.013]:
        for phi in np.linspace(0,2*np.pi,37)[:-1]:
            x   = radius*np.cos(phi)
            y   = radius*np.sin(phi)
            sol = p.evaluateInteriorSolution(domain_index=rotor.getIndex(),
                                             x=x,
                                             y=y)
            #print("*********")
            #print(sol)
            #print(LA.norm(sol))
            field_vectors_x.append(x)
            field_vectors_y.append(y)
            field_vectors_u.append(sol[0])
            field_vectors_v.append(sol[1])
    #for radius in [0.0011]:
    #    for phi in np.linspace(0,2*np.pi,25)[:-1]:
    #        x   = radius*np.cos(phi)
    #        y   = radius*np.sin(phi)-0.0015
    #        sol = problem.evaluateInteriorSolution(domain_index=Omega_ext.getIndex(),
    #                                               x=x,
    #                                               y=y)
    #        #print("*********")
    #        #print(sol)
    #        #print(LA.norm(sol))
    #        field_vectors_x.append(x)
    #        field_vectors_y.append(y)
    #        field_vectors_u.append(sol[0])
    #        field_vectors_v.append(sol[1])

    #for radius in [0.0011]:
    #    for phi in np.linspace(0,2*np.pi,25)[:-1]:
    #        x   = radius*np.cos(phi)-0.003
    #        y   = radius*np.sin(phi)
    #        sol = problem.evaluateInteriorSolution(domain_index=Omega_ext.getIndex(),
    #                                               x=x,
    #                                               y=y)
    #        #print("*********")
    #        #print(sol)
    #        #print(LA.norm(sol))
    #        field_vectors_x.append(x)
    #        field_vectors_y.append(y)
    #        field_vectors_u.append(sol[0])
    #        field_vectors_v.append(sol[1])

    # Compute statistics for debugging
    #outer_radius = 0.002
    #cable_area   = 0.001*0.001*np.pi
    #current      = cable_area*1.0/1e-6
    #H_field_norm = current/(2.0*outer_radius*np.pi)
    #B_field_norm = H_field_norm*mu_air
    #print("The magnetic flux density should have a norm of {0}T.".format(B_field_norm))

    plt.quiver(np.array(field_vectors_x),np.array(field_vectors_y),field_vectors_u,field_vectors_v)
    plt.axis('equal')
    plt.tight_layout()
    plt.show()
