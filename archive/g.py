import cmath
from math import pi,exp
import numpy as np

def g(k):
    return 1-cmath.exp(-1j*k*2*pi/12)-cmath.exp(-1j*k*6*2*pi/12)+cmath.exp(-1j*k*7*2*pi/12)

def f(k):
    return 1+cmath.exp(-1j*k*2*pi/3)*cmath.exp(-2*pi/3)-cmath.exp(-1j*k*pi/3)*cmath.exp(-4*pi/3)

print(np.angle(g(7))/2/pi)
print(np.angle(g(7)*f(7))/2/pi)
