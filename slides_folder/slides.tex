%! TeX program = lualatex
\documentclass[aspectratio=169]{beamer}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{empheq}
\usepackage{dsfont}
\usepackage{esint}
\usepackage{commath}
\usepackage[most]{tcolorbox}
\usepackage{bm}
\usepackage[nodisplayskipstretch]{setspace}
\usepackage{siunitx}
%Information to be included in the title page:
\title{Application of the boundary element method to magnetostatics problems}
\author{Felix Crazzolara}
\date{2021}

\DeclareMathOperator{\SL}{SL}
\DeclareMathOperator{\DL}{DL}
\DeclareMathOperator{\grad}{grad}
\DeclareMathOperator{\Curl}{Curl}

\newcommand{\x}{x}
\newcommand{\y}{y}
\newcommand{\z}{z}
\newcommand{\nt}{\tilde{n}}
\let\tt\relax
\newcommand{\tt}{\tilde{t}}
\newcommand{\dn}{\partial_{\nt}}
\newcommand{\dnk}{\partial_{\nt_k}}
\newcommand{\dnkx}{\partial_{\nt_k(\x)}}
\newcommand{\dnjx}{\partial_{\nt_j(\x)}}
\newcommand{\dt}{\partial_{\tt}}
\newcommand{\dtk}{\partial_{\tt_k}}
\newcommand{\dtkx}{\partial_{\tt_k(\x)}}
\newcommand{\dtjx}{\partial_{\tt_j(\x)}}

\beamertemplatenavigationsymbolsempty

\setbeamertemplate{footline}{}

\begin{document}

\setlength\belowcaptionskip{-20pt}

\frame{\titlepage}

\begin{frame}
\frametitle{Problem formulation}
\fontsize{7pt}{9pt}\selectfont
\begin{columns}[T]
\begin{column}{.59\textwidth}
\vspace{0.5cm}
Project goal:\\[0.5cm]
\hspace{0.7cm}\textit{Simulation of an electric machine in 2D}\\[1.1cm]
Fundamental equations:
\begin{align*}
&\text{Maxwell's equations}\quad\begin{cases}\begin{aligned}%
\nabla\times \bm{H}(\bm{x})&=\bm{J}(\bm{x})\\[2pt]
\nabla\cdot \bm{B}(\bm{x})&=0%
\end{aligned}\end{cases}\\[0.3cm]
&\text{Constitutive relation}\quad\begin{cases}\begin{aligned}%
\bm{B}(\bm{x})&=\mu(\bm{x})\,\bm{H}(\bm{x})%
\end{aligned}\end{cases}\\[0.3cm]
&\text{Boundary conditions}\quad\begin{cases}\begin{aligned}%
\bm{n}(\bm{x})\cdot(\bm{B}(\bm{x}^+)-\bm{B}(\bm{x}^-))&=0\\[2pt]
\bm{n}(\bm{x})\times(\bm{H}(\bm{x}^+)-\bm{H}(\bm{x}^-))&=\bm{J}(\bm{x})%
\end{aligned}\end{cases}
\end{align*}
\end{column}%
\hfill%
\begin{column}{.39\textwidth}
\vspace{0.8cm}
\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{img/DC_Motor.png}
\end{figure}
\fontsize{6pt}{7pt}\selectfont
Assume infinite extent in z-direction (into image plane) [1].
\vfill
\end{column}
\end{columns}
\vskip0pt plus 1filll
\fontsize{5pt}{6pt}\selectfont
\hspace{-0.5cm}[1] 227-0517-10L Fundamentals of Electric Machines, Exercise 5 (Course material)
\end{frame}

\begin{frame}
\frametitle{Arriving at Poisson's equation I}
\vspace{0.3cm}
\begin{columns}[T]
\begin{column}{.48\textwidth}
\fontsize{6pt}{7pt}\selectfont
\textbf{Proposition 1:} Given the conditions stated before, the magnetic flux density $\bm{B}$ can be written as%
\[\bm{B}(\bm{x})=\bm{B}(x,y)\quad\text{with}\quad \bm{B}(\bm{x})\cdot \bm{e}_z=0.\]%
\textbf{Corrolary 1:} The magnetic flux density $\bm{B}$ can be written as%
\[\bm{B}(\bm{x})=\begin{bmatrix}\tilde{\bm{B}}(\bm{x})\\0\end{bmatrix}.\]%
\textbf{Proposition 2:} The space of Lebesgue square-integrable functions $L^2(\Omega)^n$ allows the decomposition%
\[L^2(\Omega)^n=\grad H_0^1(\Omega)\oplus\mathbb{H}_2\oplus\Curl H^1(\Omega)\ [2].\]%
\textbf{Corrolary 2:} The magnetic flux density $\tilde{\bm{B}}$ can be written as%
\[\tilde{\bm{B}}(\bm{x})=\nabla B_{\nabla}(\bm{x})+\Curl B_{\times}(\bm{x}).\]
\end{column}%
\begin{column}{.48\textwidth}
\fontsize{6pt}{7pt}\selectfont
\begin{figure}
\centering
\includegraphics[width=0.3\columnwidth]{img/Domain.png}
\end{figure}
\centerline{Simple domain $\Omega$.}
\vfill
\end{column}
\end{columns}
\vskip0pt plus 1filll
\fontsize{5pt}{6pt}\selectfont
\hspace{-0.5cm}[2] Dautray, 1990, Mathematical Analysis and Numerical Methods for Science and Technology: Volume 3 Spectral Theory and Applications
\end{frame}

\begin{frame}
\frametitle{Arriving at Poisson's equation II}
\vspace{0.3cm}
\begin{columns}[T]
\begin{column}{.48\textwidth}
\fontsize{6pt}{7pt}\selectfont
\textbf{Corrolary 2:} The magnetic flux density $\tilde{\bm{B}}$ can be written as%
\[\tilde{\bm{B}}(\bm{x})=\nabla B_{\nabla}(\bm{x})+\Curl B_{\times}(\bm{x}).\]%
\textbf{Proposition 3:} Applying Maxwell's equations to $\tilde{\bm{B}}$ yields%
\[\Delta B_{\nabla}(x,y)=0\quad\text{and}\quad\Delta B_{\times}(x,y)=-\mu\,|\bm{J}(x,y)|=-\mu\,|J_z(x,y)|.\]%
\textbf{Theorem 1:} Let $f^\pm\in\tilde{H}^{-1}(\Omega^\pm)^m$ and define $f\triangleq f^++f^-\in H^{-1}(\mathbb{R}^n)^m$. If $u\in L_2(\mathbb{R}^n)^m$ with $u^\pm\in H^1(\Omega^\pm)^m$ and $u$ has compact support in $\mathbb{R}^n$ (and thus $f$ has also compact support in $\mathbb{R}^n$) and%
\[\mathcal{P}u^{\pm}=f^{\pm}\quad\text{on}\ \Omega^{\pm},\]%
then%
\[u=\mathcal{G}f+\DL[u]_\Gamma-\SL[\mathcal{B}_\nu u]_\Gamma\quad\text{on}\ \mathbb{R}^n\ [3].\]%
\end{column}%
\begin{column}{.48\textwidth}
\fontsize{6pt}{7pt}\selectfont
Boundary conditions (reminder):%
\begin{align*}%
\bm{n}(\bm{x})\cdot(\bm{B}(\bm{x}^+)-\bm{B}(\bm{x}^-))&=0\\[2pt]
\bm{n}(\bm{x})\times(\bm{H}(\bm{x}^+)-\bm{H}(\bm{x}^-))&=\bm{J}(\bm{x})%
\end{align*}
\begin{figure}
\centering
\includegraphics[width=0.3\columnwidth]{img/Domain.png}
\end{figure}
\centerline{Simple domain $\Omega$.}
\vfill
\end{column}
\end{columns}
\vskip0pt plus 1filll
\fontsize{5pt}{6pt}\selectfont
\hspace{-0.5cm}[3] McLean, 2000, Strongly Elliptic Systems and Boundary Integral Equations
\end{frame}

\begin{frame}
\frametitle{Arriving at Poisson's equation II}
\vspace{0.3cm}
\begin{columns}[T]
\begin{column}{.48\textwidth}
\fontsize{6pt}{7pt}\selectfont
\textbf{Corrolary 2:} The magnetic flux density $\tilde{\bm{B}}$ can be written as%
\[\tilde{\bm{B}}(\bm{x})=\nabla B_{\nabla}(\bm{x})+\Curl B_{\times}(\bm{x}).\]%
\textbf{Proposition 3:} Applying Maxwell's equations to $\tilde{\bm{B}}$ yields%
\[\Delta B_{\nabla}(x,y)=0\quad\text{and}\quad\Delta B_{\times}(x,y)=-\mu\,|\bm{J}(x,y)|=-\mu\,|J_z(x,y)|.\]%
\textbf{Theorem 1:} Let $f^\pm\in\tilde{H}^{-1}(\Omega^\pm)^m$ and define $f\triangleq f^++f^-\in H^{-1}(\mathbb{R}^n)^m$. If $u\in L_2(\mathbb{R}^n)^m$ with $u^\pm\in H^1(\Omega^\pm)^m$ and $u$ has compact support in $\mathbb{R}^n$ (and thus $f$ has also compact support in $\mathbb{R}^n$) and%
\[\mathcal{P}u^{\pm}=f^{\pm}\quad\text{on}\ \Omega^{\pm},\]%
then%
\[u=\mathcal{G}f+\DL[u]_\Gamma-\SL[\mathcal{B}_\nu u]_\Gamma\quad\text{on}\ \mathbb{R}^n\ [3].\]%
\end{column}%
\begin{column}{.48\textwidth}
\fontsize{6pt}{7pt}\selectfont
Boundary conditions (reminder):\\
\begin{center}\tcboxmath[colback=white,colframe=red,left=0pt,right=0pt,bottom=0pt,top=0pt]{\bm{n}(\bm{x})\cdot(\bm{B}(\bm{x}^+)-\bm{B}(\bm{x}^-))=0}\end{center}
\[\bm{n}(\bm{x})\times(\bm{H}(\bm{x}^+)-\bm{H}(\bm{x}^-))=\bm{J}(\bm{x})\]%
\begin{figure}
\centering
\includegraphics[width=0.3\columnwidth]{img/Domain.png}
\end{figure}
\centerline{Simple domain $\Omega$.}
\vfill
\end{column}
\end{columns}
\vskip0pt plus 1filll
\fontsize{5pt}{6pt}\selectfont
\hspace{-0.5cm}[3] McLean, 2000, Strongly Elliptic Systems and Boundary Integral Equations
\end{frame}

\begin{frame}
\frametitle{Formulation as boundary integral equations}
\vspace{0.3cm}
\begin{columns}[T]
\begin{column}{.58\textwidth}
\fontsize{6pt}{7pt}\selectfont
First boundary condition (not rigorous):
\vspace{-0.2cm}
\begin{align*}
-&\lim_{\substack{\y\rightarrow\x\\\y\in\Omega_j}}(\dnjx(-\DL\gamma^{\Omega_j}B_{\nabla,j}+\SL\mathcal{B}_\nu^{\Omega_j}B_{\nabla,j}))(\y)\\[-4pt]
-&\lim_{\substack{\y\rightarrow\x\\\y\in\Omega_j}}\dtjx(-\DL\gamma^{\Omega_j}B_{\times,j}+\SL\mathcal{B}_\nu^{\Omega_j}B_{\times,j}) \\[-4pt]
-&\lim_{\substack{\y\rightarrow\x\\\y\in\Omega_k}}(\dnkx(-\DL\gamma^{\Omega_k}B_{\nabla,k}+\SL\mathcal{B}_\nu^{\Omega_k}B_{\nabla,k}))(\y) \\[-4pt]
-&\lim_{\substack{\y\rightarrow\x\\\y\in\Omega_k}}\dtkx(-\DL\gamma^{\Omega_k}B_{\times,k}+\SL\mathcal{B}_\nu^{\Omega_k}B_{\times,k})=c_{1,k}(\x;j)
\end{align*}
First boundary condition (variational formulation, rigorous):
\vspace{-0.2cm}
\begin{align*}
(W_t^{\Omega_j}(\partial_{t_{\Omega_j}(\cdot)}B_{\nabla,j})\Big\rvert_{\Gamma_j}^*,\xi)_{\Gamma_k}&-(W^{\Omega_j}_n(\partial_{\nu_{\Omega_j}(\cdot)}B_{\nabla,j})\Big\rvert_{\Gamma_j}^*,\xi)_{\Gamma_k} \\
-(W_n^{\Omega_j}(\partial_{t_{\Omega_j}(\cdot)}B_{\times,j}\Big\rvert_{\Gamma_j}^*,\xi)_{\Gamma_k}&-(W^{\Omega_j}_t(\partial_{\nu_{\Omega_j}(\cdot)}B_{\times,j})\Big\rvert_{\Gamma_j}^*,\xi)_{\Gamma_k} \\
+(W_t^{\Omega_k}(\partial_{t_{\Omega_k}(\cdot)}B_{\nabla,k})\Big\rvert_{\Gamma_k}^*,\xi)_{\Gamma_k}&-(W_n^{\Omega_k}(\partial_{\nu_{\Omega_k}(\cdot)}B_{\nabla,k})\Big\rvert_{\Gamma_j}^*,\xi)_{\Gamma_k} \\
-(W_n^{\Omega_k}(\partial_{t_{\Omega_k}(\cdot)}B_{\times,k}\Big\rvert_{\Gamma_k}^*,\xi)_{\Gamma_k}&-(W^{\Omega_k}_t(\partial_{\nu_{\Omega_k}(\cdot)}B_{\times,k})\Big\rvert_{\Gamma_k}^*,\xi)_{\Gamma_k}=(c_{1,k}(\cdot;j),\xi)_{\Gamma_k}
\end{align*}%
\vspace{-0.6cm}
\begin{center}\tcboxmath[colback=white,colframe=red,left=2pt,right=2pt,bottom=2pt,top=2pt]{\forall\xi\in X(\Gamma_k)}\hspace{0.4cm}Suppose $c_1(\cdot;j)\in Y(\Gamma_k)$ then pick $X=Y^*$.\end{center}
\vskip0pt plus 1filll
\end{column}
\begin{column}{.38\textwidth}
\fontsize{6pt}{7pt}\selectfont
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{img/Oj_Ok.png}
\end{figure}
\vspace{-0.3cm}
\centerline{Adjacent domains $\Omega_j$ and $\Omega_k$.}
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{img/nk_defn.png}
\end{figure}
\vspace{-0.3cm}
\centerline{Visualization of the normal and tangential vectors.}
\vskip0pt plus 1filll
\end{column}
\end{columns}
\vskip0pt plus 1filll
\end{frame}

\begin{frame}
\frametitle{Parametrization of the boundary}
\vspace{0.3cm}
\begin{columns}[T]
\begin{column}{.58\textwidth}
\fontsize{6pt}{7pt}\selectfont
Parametrization of the tangential and normal derivatives:
\begin{align*}
\sum\limits_{i=1}^{N_k}\mathds{1}_{\Psi_{i,k}}\,\eta_{i,k}^t\circ\psi_{i,k}^{-1}\approx\partial_{t_{\Omega_k}(\cdot)}B_{\nabla,k}
\end{align*}
First boundary condition (variational formulation, rigorous):
\vspace{-0.2cm}
\begin{align*}
 &(W_t^{\Omega_j}\sum\limits_{i=1}^{N_j}\mathds{1}_{\Psi_{i,j}}\,\eta_{i,j}^t\circ\psi_{i,j}^{-1},\xi)_{\Gamma_k}-(W_n^{\Omega_j}\sum\limits_{i=1}^{N_j}\mathds{1}_{\Psi_{i,j}}\,\eta_{i,j}^n\circ\psi_{i,j}^{-1},\xi)_{\Gamma_k} \\
-&(W_n^{\Omega_j}\sum\limits_{i=1}^{N_j}\mathds{1}_{\Psi_{i,j}}\,\rho_{i,j}^t\circ\psi_{i,j}^{-1},\xi)_{\Gamma_k}-(W_t^{\Omega_j}\sum\limits_{i=1}^{N_j}\mathds{1}_{\Psi_{i,j}}\,\rho_{i,j}^n\circ\psi_{i,j}^{-1},\xi)_{\Gamma_k} \\
+&(W_t^{\Omega_k}\sum\limits_{i=1}^{N_k}\mathds{1}_{\Psi_{i,k}}\,\eta_{i,k}^t\circ\psi_{i,k}^{-1},\xi)_{\Gamma_k}-(W_n^{\Omega_k}\sum\limits_{i=1}^{N_k}\mathds{1}_{\Psi_{i,k}}\,\eta_{i,k}^n\circ\psi_{i,k}^{-1},\xi)_{\Gamma_k} \\
-&(W_n^{\Omega_k}\sum\limits_{i=1}^{N_k}\mathds{1}_{\Psi_{i,k}}\,\rho_{i,k}^t\circ\psi_{i,k}^{-1},\xi)_{\Gamma_k}-(W_t^{\Omega_k}\sum\limits_{i=1}^{N_k}\mathds{1}_{\Psi_{i,k}}\,\rho_{i,k}^n\circ\psi_{i,k}^{-1},\xi)_{\Gamma_k}=(c_{1,k}(\cdot;j),\xi)_{\Gamma_k}
\end{align*}
\begin{center}$\forall\xi\in X(\Gamma_k)$\end{center}
\vskip0pt plus 1filll
\end{column}
\begin{column}{.38\textwidth}
\fontsize{6pt}{7pt}\selectfont
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{img/Parametrization.png}
\end{figure}
\vspace{-0.3cm}
\centerline{Visualization of the boundary segments}
\vskip0pt plus 1filll
\end{column}
\end{columns}
\vskip0pt plus 1filll
\end{frame}

\begin{frame}
\frametitle{Choosing $X(\Gamma_k)$}
\vspace{0.3cm}
\begin{columns}[T]
\begin{column}{.58\textwidth}
\setlength{\abovedisplayskip}{0pt}
\setlength{\belowdisplayskip}{0pt}
\fontsize{7pt}{9pt}\selectfont
$\rightarrow$ There are 
\begin{equation*}
\sum\limits_{k\in\mathcal{K}}\sum\limits_{l=1}^{N_k}f\,(p_{k,l}+1)
\end{equation*}
many unknowns.\\[5pt]
$\rightarrow$ Considering $\partial_{t}B_{\nabla}$, $\partial_{\nu}B_{\nabla}$, $\partial_{t}B_{\times}$ and $\partial_{\nu}B_{\times}$ results in $f=4$.\\[5pt]
$\rightarrow$ The fact that $B_{\nabla}\in \grad H_0^1(\Omega)\oplus\mathbb{H}_2$ implies that $\partial_t B_{\nabla}=0$ and hence $f=3$ is fine too.\\[5pt]
$\rightarrow$ Choosing $X(\Gamma_k)$ as the set of monomials on $\Psi_{k,j},\,j\in\{1,\ldots,N_k\}$ and up to order $p_{k,l}$ yields
\begin{equation*}
\frac{1}{2}\sum\limits_{k\in\mathcal{K}}\sum\limits_{l=1}^{N_k}2\,(p_{k,l}+1)=\sum\limits_{k\in\mathcal{K}}\sum\limits_{l=1}^{N_k}(p_{k,l}+1)
\end{equation*}
many equations.\\[5pt]
$\rightarrow$ Assuming $\partial_{\nu}B_{\nabla}=0$ and enforcing continuity of the approximations of $\partial_{t}B_{\times}$ and $\partial_{\nu}B_{\times}$ results in
\begin{equation*}
\sum\limits_{k\in\mathcal{K}}\sum\limits_{l=1}^{N_k}(p_{k,l}+1)
\end{equation*}
many unknowns.
\vskip0pt plus 1filll
\end{column}
\begin{column}{.38\textwidth}
\fontsize{6pt}{7pt}\selectfont
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{img/Parametrization.png}
\end{figure}
\vspace{-0.3cm}
\centerline{Visualization of the boundary segments}
\vskip0pt plus 1filll
\end{column}
\end{columns}
\vskip0pt plus 1filll
\end{frame}

\begin{frame}
\frametitle{Implementation details}
\fontsize{7pt}{9pt}\selectfont
$\rightarrow$ The method was implemented in C\texttt{++}. The following external libraries were used 
\begin{align*}
\text{Linear algebra} &: \text{Eigen,} \\
\text{Quadrature}     &: \text{GNU Scientific Library,} \\
\text{Visualization}  &: \text{Custom (based on Qt).}
\end{align*}
\end{frame}

\begin{frame}
\frametitle{Validation of the method - Simulating a single wire}
\begin{figure}
\centering
\includegraphics[width=0.6\textwidth]{img/Single_Wire.png}
\end{figure}
\fontsize{6pt}{7pt}\selectfont
\begin{equation*}
|\oint H\cdot\dif s-\oiint J\dif A|/|\oiint J\dif A|\approx\ 10^{-5.2}\ (\text{interior})\ /\ 10^{-7.4}\ (\text{exterior})
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Validation of the method - Simulating many wires}
%\begin{columns}[T]
%\begin{column}{.7\textwidth}
%\begin{figure}
%\centering
%\includegraphics[width=\textwidth]{img/benchmark.png}
%\end{figure}
%\end{column}
%\begin{column}{.29\textwidth}
%\hspace{-0.5cm}\begin{figure}
%\centering
%\includegraphics[width=1.2\textwidth]{img/BenchmarkResults.png}
%\end{figure}
%\end{column}
%\end{columns}
%\fontsize{6pt}{7pt}\selectfont
%\begin{equation*}
%|\oint H\cdot\dif s-\oiint J\dif A|/|\oiint J\dif A|\approx\ 10^{-5.2}\ (\text{interior})\ /\ 10^{-7.4}\ (\text{exterior})
%\end{equation*}
\begin{minipage}[t]{0.6\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{img/benchmark.png}
\end{figure}
\end{minipage}%
\hfill%
\begin{minipage}[t]{0.39\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{img/BenchmarkResults.png}
\end{figure}
\fontsize{6pt}{7pt}\selectfont
The wires and the iron rod have a radius of \SI{3}{\milli\meter} and closed integrals are computed at a radius of \SI{5}{\milli\meter}. The wires conduct currents of $\pm\SI{28.27}{\ampere}$. The x-axis denotes wire indices $\{0,\ldots,7\}$ and the y-axis denotes $s$ with $s$ such that
\begin{equation*}
\frac{|\oint H\cdot\dif s-\oiint J\dif A|}{|\oiint J\dif A|}=10^{s},
\end{equation*}
except for the iron rod which does not conduct current. The integral around the iron was computed as
\begin{equation*}
|\oint H\cdot\dif s|=2.26005\cdot 10^{-12}.
\end{equation*}
$\rightarrow$ Red arrows point to wires.\\[5pt]
$\rightarrow$ The green arrow points to the iron rod.
\end{minipage}
\end{frame}

\begin{frame}
\frametitle{Validation of the method - Simulating a magnet}
\begin{minipage}{0.59\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{img/Magnet.png}
\end{figure}
\end{minipage}%
\hfill%
\begin{minipage}{0.39\textwidth}
\fontsize{6pt}{7pt}\selectfont
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{img/MagnetModel.png}
\end{figure}
\vspace{-0.3cm}
\centerline{Permanent magnet modelled by use of surface currents.}
\vskip0pt plus 1filll
\end{minipage}
\end{frame}

\begin{frame}
\frametitle{Simulation of a DC machine I}
\vspace{0.3cm}
\begin{minipage}{0.39\textwidth}
\begin{figure}
\centering
\includegraphics[width=0.95\textwidth]{img/DC_Motor.png}
\end{figure}
\fontsize{6pt}{7pt}\selectfont
\centerline{DC machine role model [1]}
\vskip0pt plus 1fill
\end{minipage}%
\hfill%
\begin{minipage}{0.59\textwidth}
\begin{figure}
\centering
\includegraphics[width=0.95\textwidth]{img/DC_Motor_Simulation.png}
\end{figure}
\fontsize{6pt}{7pt}\selectfont
\centerline{Simulated DC machine}
\vskip0pt plus 1fill
\end{minipage}
\vskip0pt plus 1filll
\end{frame}

\begin{frame}
\frametitle{Simulation of a DC machine II}
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{img/DC_Machine_Rotor_Field.png}
\end{figure}
\fontsize{6pt}{7pt}\selectfont
\centerline{It can be seen well that the field produced by the excitation windings is twisted due to the rotor field.}
\end{frame}

\begin{frame}
\frametitle{Simulation of a DC machine III}
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{img/DC_Machine_Rotor_Field.png}
\end{figure}
\fontsize{6pt}{7pt}\selectfont
$\rightarrow$ Tried to compute the torque by integrating across the rotor coils
\begin{align*}
F=\sum\limits_{j=1}^{N_{\rm{rotor}}}\int\limits_{\Omega_j}r(\x)\cdot B(\x)\,\dif S(\x).
\end{align*}
$\rightarrow$ Results seemed promising at first, but were not consistent depending on the meshing.\\[5pt]
$\rightarrow$ Conclusion: Very fine mesh required if the field is changing quickly. But in any way, the Lorentz disregards reluctance forces. Hence, it is unsuitable to compute the torque.
\vskip0pt plus 1filll
\end{frame}

\begin{frame}
\setlength{\abovedisplayskip}{5pt}
\setlength{\belowdisplayskip}{5pt}
\frametitle{Computing the forces of a linear actuator}
\begin{minipage}[t]{0.39\textwidth}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{img/Mover.png}
\end{figure}
\fontsize{6pt}{7pt}\selectfont
\centerline{Linear actuator role model [4]}
\vspace{0.2cm}
$\rightarrow$ The torque can be computed by use of Maxwell's stress tensor as
\begin{equation*}
F=\oint\limits_{\Gamma}\left((B\cdot n)H-\frac{\mu}{2}|H|^2n\right)\dif s\,[5].
\end{equation*}
$\rightarrow$ Found forces as follows
\begin{equation*}
F_y=\begin{cases}\begin{aligned}
\SI{60.79}{\newton}&&(\SI{72.75}{\newton})\quad&\text{if}\ \delta_2=\SI{0.003}{\milli\meter},\\
\SI{10.04}{\newton}&&(\SI{9.62}{\newton})\quad&\text{if}\ \delta_2=\SI{0.01}{\milli\meter}.
\end{aligned}\end{cases}
\end{equation*}
\vskip0pt plus 1fill
\end{minipage}%
\hfill%
\begin{minipage}[t]{0.6\textwidth}
\begin{figure}
\centering
\includegraphics[width=0.95\textwidth]{img/Mover_Simulation.png}
\end{figure}
\fontsize{6pt}{7pt}\selectfont
\centerline{Simulation results}
\vskip0pt plus 1fill
\end{minipage}
\vskip0pt plus 1filll
\fontsize{5pt}{6pt}\selectfont
\hspace{-0.5cm}[4] 227-0517-10L Fundamentals of Electric Machines, Exercise 3 (Course material)\\[2pt]
\hspace{-0.5cm}[5] Pile, 2018, Comparison of Main Magnetic Force Computation Methods for Noise and Vibration Assessment in Electrical Machines
\end{frame}

\begin{frame}
\frametitle{Open questions}
\vspace{0.3cm}
\fontsize{7pt}{9pt}\selectfont
$\rightarrow$ Remember: $\tilde{\bm{B}}(\bm{x})=\nabla B_{\nabla}(\bm{x})+\Curl B_{\times}(\bm{x})$\\[5pt]
$\rightarrow$ Assumed $\partial_{\nu}B_{\nabla}\big\rvert_{\Gamma}=0$ which implies $\nabla B_{\nabla}=0$ since $\partial_{t}B_{\nabla}\big\rvert_{\Gamma}=0$ and $\Delta B_{\nabla}=0$. Is it true that the interface conditions (in classical or variational form) can be satisfied with $B_{\nabla}=0$?\\[20pt]
$\rightarrow$ Remember:
\[F=\oint\limits_{\Gamma}\left((B\cdot n)H-\frac{\mu}{2}|H|^2n\right)\dif s\]
$\rightarrow$ To the best of my knowledge, Maxwell's stress tensor must (if taken as surface integral) be applied in the air enclosing an object. Is it possible to use this method to compute the torque? How can the radius, at which the force applies, be taken into consideration?\\[5pt]
$\rightarrow$ Idea: Derive an analytical expression for Maxwell's stress tensor and use it to implement the computation of 2D volume integrals.\\[20pt]
$\rightarrow$ Checked whether 
\[\oint\limits_{\Gamma}\partial_{t}B_{\times}\dif s\approx 0.\]
A similar test could be done for $\partial_{\nu}B_{\times}$.
\vskip0pt plus 1filll
\end{frame}

\end{document}
