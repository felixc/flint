#include <cmath>
#include <iostream>
#include <Eigen/Dense>
#include "Sanji.hpp"
#include "ProblemWrapper.hpp"

using namespace sanji::colors;

/* Type definitions */
template <typename T>
using vector = std::vector<T>;

/* Parameters */
// Visualization
constexpr bool   plot_normals     = true;
constexpr bool   plot_field_data  = true;

// Permeabilities
static constexpr double mu0       = 4.0*M_PI*1.0e-7;
static constexpr double mu_air    = 1.00000037*mu0;
static constexpr double mu_iron   = 4000.0*mu0;
static constexpr double mu_cu     = 0.999994*mu0;

// Geometric parameters
constexpr double rotor_outer_radius = 0.032;       // [m]
constexpr double yoke_inner_radius  = 0.035;       // [m]
constexpr double yoke_outer_radius  = 0.05;        // [m]
constexpr double arc_length_rotor   = 2.0*M_PI/12; // rad
constexpr double coil_width         = 0.005;       // [m]
constexpr double coil_length        = 0.015;       // [m]

int main(int argc, char* argv[]) {
    // Initialize this as a Qt application
    QApplication app(argc, argv);
    sanji::init();

    /* Create a problem instance */
    ProblemWrapper p;

    /* Create the boundary segments */
    const auto rotor_outer_boundary_1 = p.addArcedBoundarySegment(rotor_outer_radius,  /* radius */
                                                                  Vec2D{0.0,0.0},      /* center */
                                                                  -arc_length_rotor/2, /* phi_min */
                                                                   arc_length_rotor/2, /* phi_max */
                                                                  12,                  /* num_nodes */
                                                                  1,                   /* boundary_solution_polynomial_order */
                                                                  true                 /* normal_points_to_the_right */);
    const auto rotor_outer_boundary_2 = p.addArcedBoundarySegment(rotor_outer_radius,      /* radius */
                                                                  Vec2D{0.0,0.0},          /* center */
                                                                  M_PI-arc_length_rotor/2, /* phi_min */
                                                                  M_PI+arc_length_rotor/2, /* phi_max */
                                                                  12,                      /* num_nodes */
                                                                  1,                       /* boundary_solution_polynomial_order */
                                                                  true                     /* normal_points_to_the_right */);
    const auto rotor_outer_boundary_3 = p.addStraightBoundarySegment(rotor_outer_boundary_1->getEndPoint(),   /* start */
                                                                     rotor_outer_boundary_2->getStartPoint(), /* end */
                                                                     4,                                       /* num_nodes */
                                                                     1,                                       /* poly_order */
                                                                     true                                     /* normal_points_to_the_right */);
    const auto rotor_outer_boundary_4 = p.addStraightBoundarySegment(rotor_outer_boundary_2->getEndPoint(),   /* start */
                                                                     rotor_outer_boundary_1->getStartPoint(), /* end */
                                                                     4,                                       /* num_nodes */
                                                                     1,                                       /* poly_order */
                                                                     true                                     /* normal_points_to_the_right */);
    const auto yoke_inner_boundary = p.addCircularBoundarySegment(yoke_inner_radius, /* radius */
                                                                  Vec2D{0.0,0.0},    /* center */
                                                                  12,                /* num_nodes */
                                                                  1,                 /* boundary_solution_polynomial_order */
                                                                  true               /* normal_points_to_the_right */);
    const auto yoke_outer_boundary = p.addCircularBoundarySegment(yoke_outer_radius, /* radius */
                                                                  Vec2D{0.0,0.0},    /* center */
                                                                  12,                /* num_nodes */
                                                                  1,                 /* boundary_solution_polynomial_order */
                                                                  true               /* normal_points_to_the_right */);
    vector<vector<boundary_segment_ptr>> all_coils_dir_bnd_seg_ptrs(2);
    for (uint coil_idx = 0; coil_idx < 2; ++coil_idx) {
        Vec2D center{0.0,(coil_idx == 0 ? 1.0 : -1.0)*(rotor_outer_boundary_1->getEndPoint().y+5.0e-4+coil_width/2.0)};
        all_coils_dir_bnd_seg_ptrs[coil_idx].push_back(p.addStraightBoundarySegment(center+Vec2D{-coil_length/2,-coil_width/2}, /* start */
                                                                                    center+Vec2D{ coil_length/2,-coil_width/2}, /* end */
                                                                                    4,                                          /* num_nodes */
                                                                                    1,                                          /* poly_order */
                                                                                    true                                        /* normal_points_to_the_right */));
        all_coils_dir_bnd_seg_ptrs[coil_idx].push_back(p.addStraightBoundarySegment(center+Vec2D{coil_length/2,-coil_width/2}, /* start */
                                                                                    center+Vec2D{coil_length/2, coil_width/2}, /* end */
                                                                                    4,                                         /* num_nodes */
                                                                                    1,                                         /* poly_order */
                                                                                    true                                       /* normal_points_to_the_right */));
        all_coils_dir_bnd_seg_ptrs[coil_idx].push_back(p.addStraightBoundarySegment(center+Vec2D{ coil_length/2,coil_width/2}, /* start */
                                                                                    center+Vec2D{-coil_length/2,coil_width/2}, /* end */
                                                                                    4,                                         /* num_nodes */
                                                                                    1,                                         /* poly_order */
                                                                                    true                                       /* normal_points_to_the_right */));
        all_coils_dir_bnd_seg_ptrs[coil_idx].push_back(p.addStraightBoundarySegment(center+Vec2D{-coil_length/2, coil_width/2}, /* start */
                                                                                    center+Vec2D{-coil_length/2,-coil_width/2}, /* end */
                                                                                    4,                                          /* num_nodes */
                                                                                    1,                                          /* poly_order */
                                                                                    true                                        /* normal_points_to_the_right */));
    }

    /* Create the domains in the problem */
    // Exterior
    p.addExteriorDomain("outer_air",{{std::make_shared<DirectedBoundarySegment>(yoke_outer_boundary,false)}},mu_air);

    // Yoke
    p.addDomain("yoke",{{std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary,false)},
                        {std::make_shared<DirectedBoundarySegment>(yoke_outer_boundary,true)}},mu_iron,0.0 /* J */, 1/* type */);

    // Rotor
    const auto rotor = p.addDomain("rotor",{{std::make_shared<DirectedBoundarySegment>(rotor_outer_boundary_1,true),
                                             std::make_shared<DirectedBoundarySegment>(rotor_outer_boundary_3,true),
                                             std::make_shared<DirectedBoundarySegment>(rotor_outer_boundary_2,true),
                                             std::make_shared<DirectedBoundarySegment>(rotor_outer_boundary_4,true)}},mu_iron,0.0 /* J */, 3/* type */);
    
    // Coils
    for (uint coil_idx = 0; coil_idx < 2; ++coil_idx) {
        p.addDomain("coil_"+std::to_string(coil_idx),{{std::make_shared<DirectedBoundarySegment>(all_coils_dir_bnd_seg_ptrs[coil_idx][0],true),
                                                       std::make_shared<DirectedBoundarySegment>(all_coils_dir_bnd_seg_ptrs[coil_idx][1],true),
                                                       std::make_shared<DirectedBoundarySegment>(all_coils_dir_bnd_seg_ptrs[coil_idx][2],true),
                                                       std::make_shared<DirectedBoundarySegment>(all_coils_dir_bnd_seg_ptrs[coil_idx][3],true)}},
            mu_cu,(coil_idx==0 ? 1.0 : -1.0)*2.0e6 /* J */, 3/*type*/);
    }

    // Inner air
    const auto inner_air = p.addDomain("inner_air",{{std::make_shared<DirectedBoundarySegment>(all_coils_dir_bnd_seg_ptrs[0][3],false),
                                                     std::make_shared<DirectedBoundarySegment>(all_coils_dir_bnd_seg_ptrs[0][2],false),
                                                     std::make_shared<DirectedBoundarySegment>(all_coils_dir_bnd_seg_ptrs[0][1],false),
                                                     std::make_shared<DirectedBoundarySegment>(all_coils_dir_bnd_seg_ptrs[0][0],false)},
                                                    {std::make_shared<DirectedBoundarySegment>(all_coils_dir_bnd_seg_ptrs[1][3],false),
                                                     std::make_shared<DirectedBoundarySegment>(all_coils_dir_bnd_seg_ptrs[1][2],false),
                                                     std::make_shared<DirectedBoundarySegment>(all_coils_dir_bnd_seg_ptrs[1][1],false),
                                                     std::make_shared<DirectedBoundarySegment>(all_coils_dir_bnd_seg_ptrs[1][0],false)},
                                                    {std::make_shared<DirectedBoundarySegment>(rotor_outer_boundary_4,false),
                                                     std::make_shared<DirectedBoundarySegment>(rotor_outer_boundary_2,false),
                                                     std::make_shared<DirectedBoundarySegment>(rotor_outer_boundary_3,false),
                                                     std::make_shared<DirectedBoundarySegment>(rotor_outer_boundary_1,false)},
                                                    {std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary,true)}},mu_air,0.0 /* J */);

    /* Solve the problem */
    p.solve(true);
    //p.solveAndStore("results/example1");
    //p.loadSolution("results/example1");

    /* Plot the boundaries in the problem */
    sanji::figure("Permanent magnet synchronous machine simulation");
    for (const auto& boundary_segment_plotting_data : p.getBoundarySegmentsPlottingData(0.001/4.0, /* max_length_per_segment */
                                                                                        true       /* show_reference_boundaries */))
        sanji::plot(*boundary_segment_plotting_data.x,*boundary_segment_plotting_data.y,boundary_segment_plotting_data.style);

    /* Plot the normals in the problem */
    if (plot_normals)
        for (const auto& boundary_segment_plotting_vector_data : p.getNormals(0.001, /* max_length_per_segment */
                                                                              1.0e-3 /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);
    /* Plot the magnetic flux density */
    if (plot_field_data) {
        const auto field_data = p.getFieldData();
        double min =  std::numeric_limits<double>::max();
        double max = -std::numeric_limits<double>::max();
        for (const auto& field_data : field_data)
            for (uint i = 0; i < field_data.x->rows(); ++i) {
                const double strength = Vec2D{(*field_data.u)(i),(*field_data.v)(i)}.Norm2();
                if (strength < min) min = strength;
                if (strength > max) max = strength;
            }
        for (const auto& field_data : field_data)
            sanji::quiver(*field_data.x,
                          *field_data.y,
                          *field_data.u,
                          *field_data.v,{{"arrow_length",0.0006},{"colormap",TURBO},{"min",min},{"max",max}},{"use_colormap","center_arrows"});
                          //*field_data.v,{{"arrow_length",0.0006},{"colormap",TURBO},{"min",min},{"max",max}},{"use_colormap","center_arrows","use_logscale"});

        /* Print statistics */
        std::cout << "The minimum is " << min << std::endl;
        std::cout << "The maximum is " << max << std::endl;
    }

    /* Plot cosmetics */
    sanji::setPlotBackgroundColor(GRAY);
    sanji::setAxesRatio("equal");

    // Compute the normal component of the B field along the inner boundary of the rotor
    const auto& domain            = inner_air;
    //const auto& domain            = rotor;
    const uint N                  = 200u;
    const uint Ne                 = 10u;
    Eigen::ArrayXd B_norm_bnd     = VectorXd::LinSpaced(N,0,N-1);
    vector<std::unique_ptr<VectorXd>> B_norm_int(Ne);
    for (uint ei = 0; ei < Ne; ++ei) {
        B_norm_int[ei] = std::make_unique<VectorXd>(N);
    }
    directed_boundary_segment_ptr dir_bnd_seg_ptr;
    for (const auto& dir_bnd_seg_ptrs_ : domain->getDirectedBoundarySegmentPointers())
        for (const auto& dir_bnd_seg_ptr_ : dir_bnd_seg_ptrs_)
            if (rotor_outer_boundary_1.get() == &**dir_bnd_seg_ptr_)
                dir_bnd_seg_ptr = dir_bnd_seg_ptr_;
    std::for_each(B_norm_bnd.cbegin(),B_norm_bnd.cend(),[N,&B_norm_bnd,&B_norm_int,&p,&domain,&dir_bnd_seg_ptr](const int n) {
        // Compute the solution on the boundary
        auto [loop_idx,dir_bnd_seg_idx] = domain->getDirectedBoundarySegmentIndex((**dir_bnd_seg_ptr).getBoundarySegmentIndex());
        auto [B,x,nu]                   = p.evaluateBoundarySolution(domain,loop_idx,dir_bnd_seg_idx,static_cast<double>(n)/N);
        B_norm_bnd(n)                   = nu.dot(B);

        for (uint ei = 0; ei < Ne; ++ei) {
            const double t = static_cast<double>(ei)/(Ne-1);
            (*B_norm_int[ei])(n) = nu.dot(p.evaluateInteriorSolution(domain,x-nu*std::pow(10,-(t*4+(1-t)*9))));
        }
    });
    Eigen::VectorXd error(Ne);
    for (uint ei = 0; ei < Ne; ++ei) {
        Eigen::ArrayXd diff = B_norm_bnd-B_norm_int[ei]->array();
        error(ei)           = (diff*diff).sqrt().mean();
    }
    sanji::figure("Magnetic flux in normal direction along boundary");
    for (uint ei = 0; ei < Ne; ++ei) {
        sanji::plot(VectorXd::LinSpaced(N,0,N-1)*2.0*M_PI/N,*B_norm_int[ei],{{"color",BLACK}});
    }
    sanji::plot(VectorXd::LinSpaced(N,0,N-1)*2.0*M_PI/N,B_norm_bnd,{{"color",RED}});
    sanji::figure("Deviation from boundary value");
    sanji::plot(VectorXd::LinSpaced(Ne,9,4),error);

    // Execute the application
    app.exec();
}
