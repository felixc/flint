#include <cmath>
#include "Sanji.hpp"
#include <Eigen/Dense>
#include <complex>
#include "Colors.hpp"
#include <gsl/gsl_integration.h>
#include <iostream>

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 1e-4
#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-4
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          4000

using namespace sanji::colors;
using namespace std::complex_literals;

/* Type definitions */
using VectorXd  = Eigen::VectorXd;
using VectorXcd = Eigen::VectorXcd;
using MatrixXd  = Eigen::MatrixXd;
using complex   = std::complex<double>;

// Parameters
constexpr uint   p       = 7;
constexpr uint   Ns      = 12;
constexpr uint   Nw      = 50;
constexpr double Ihs     = 3.0;
constexpr double Omega_m = 3000.0/60.0*2.0*M_PI;
constexpr double w_s     = Omega_m*p;

// GSL
gsl_function               gsl_fun;
gsl_integration_workspace* gsl_workspace;

double a(const double phi_s) {
    double phi = phi_s;
    while (phi < -M_PI) phi += 2*M_PI;
    while (phi >= M_PI) phi -= 2*M_PI;
    if (std::abs(phi) <= 2.0*M_PI/(1.5*Ns)/2.0) return 1;
    else                                          return 0;
}

double Br_(const double phi_r) {
    double phi = phi_r;
    while (phi < -M_PI) phi += 2*M_PI;
    while (phi >= M_PI) phi -= 2*M_PI;
    if (std::abs(phi) <= 2.0*M_PI/(1.5*2*p)/2.0) return 1;
    else                                         return 0;
}

double Br(const double phi_r) {
    double phi = phi_r;
    while (phi < -M_PI) phi += 2*M_PI;
    while (phi >= M_PI) phi -= 2*M_PI;
    double result = 0;
    for (uint i = 0; i < 2*p; ++i) {
        if ((i%2) == 0) result += Br_(phi-(2.0*M_PI/(2.0*p))*i);
        else            result -= Br_(phi-(2.0*M_PI/(2.0*p))*i);
    }
    return result;
}

double quad(const double a, const double b) {
    double result,error;
    gsl_integration_qag(&gsl_fun, /* Function to integrate */
                        a,        /* Lower integration limit */
                        b,        /* Upper integration limit */
                        INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                        INTEGRAL_RELATIVE_ERROR_LIMIT,
                        MAX_NUM_SUBINTERVALS,
                        INTEGRATION_KEY,
                        gsl_workspace,&result,&error);
    return result;
};

double A(const double phi_s, const double t) {
    return Nw*( a(phi_s)             - a(phi_s-2*M_PI/12) -  a(phi_s-6*2*M_PI/12) +  a(phi_s-7*2*M_PI/12))*Ihs*std::cos(w_s*t)
          +Nw*( a(phi_s-4*2*M_PI/12) - a(phi_s-5*M_PI/12) - a(phi_s-10*2*M_PI/12) + a(phi_s-11*2*M_PI/12))*Ihs*std::cos(w_s*t-2*M_PI/3)
          +Nw*(-a(phi_s-2*2*M_PI/12) + a(phi_s-3*M_PI/12) +  a(phi_s-8*2*M_PI/12) -  a(phi_s-9*2*M_PI/12))*Ihs*std::cos(w_s*t-4*M_PI/3);
}

complex ak(const int k) {
    bool real = true;
    const auto integrand = [k,&real](const double theta)->double {
        const double  phi = -k*theta;
        const complex c   = std::cos(phi) + 1.0i*std::sin(phi);
        if (real)
            return (a(theta)*c).real();
        else
            return (a(theta)*c).imag();
    };
    gsl_fun.function = [](const double theta, void* params)->double {
        const decltype(integrand)& integrand_ = *static_cast<const decltype(integrand)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
        return integrand_(theta);
    };
    const double** params  = new const double*[1];
    gsl_fun.params         = params;
    params[0]              = reinterpret_cast<const double*>(&integrand);
    const double real_part = quad(-M_PI,M_PI)/(2.0*M_PI);
    real                   = false;
    const double imag_part = quad(-M_PI,M_PI)/(2.0*M_PI);
    delete[] params;
    return real_part + 1.0i*imag_part;
}

complex Ak(const int k, const double t) {
    bool real = true;
    const auto integrand = [k,&real,t](const double theta)->double {
        const double  phi = -k*theta;
        const complex c   = std::cos(phi) + 1.0i*std::sin(phi);
        if (real)
            return (A(theta,t)*c).real();
        else
            return (A(theta,t)*c).imag();
    };
    gsl_fun.function = [](const double theta, void* params)->double {
        const decltype(integrand)& integrand_ = *static_cast<const decltype(integrand)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
        return integrand_(theta);
    };
    const double** params  = new const double*[1];
    gsl_fun.params         = params;
    params[0]              = reinterpret_cast<const double*>(&integrand);
    const double real_part = quad(-M_PI,M_PI)/(2.0*M_PI);
    real                   = false;
    const double imag_part = quad(-M_PI,M_PI)/(2.0*M_PI);
    delete[] params;
    return real_part + 1.0i*imag_part;
}

double torque(const double phi, const double t) {
    const auto integrand  = [t,phi](const double phi_s)->double {
        return Br(phi_s - phi)*A(phi_s,t);
    };
    gsl_fun.function = [](const double theta, void* params)->double {
        const decltype(integrand)& integrand_ = *static_cast<const decltype(integrand)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
        return integrand_(theta);
    };
    const double** params = new const double*[1];
    gsl_fun.params        = params;
    params[0]             = reinterpret_cast<const double*>(&integrand);
    const double result   = quad(0,2*M_PI);
    delete[] params;
    return result;
}

int main(int argc, char* argv[]) {
    // Initialize this as a QT application
    QApplication app(argc, argv);
    sanji::init();

    // GSL
    gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);

    // a(\phi)
    const uint Ntheta = 1000;
    VectorXd theta_vec(Ntheta);
    VectorXd a_vec(Ntheta); 
    for (uint i = 0; i < Ntheta; ++i) {
        theta_vec(i) = -1.0 + i*2.0/(Ntheta-1);
        a_vec(i)     = a(theta_vec(i)*M_PI);
    }
    sanji::figure("a(phi)");
    sanji::plot(theta_vec,a_vec,{{"line_style",'-'},{"color",RED}});

    // ak
    uint Nk = 2*11+1;
    VectorXd  k_vec(Nk);
    VectorXcd ak_vec(Nk);
    for (int i = -11; i <= 11; ++i) {
        k_vec(i+11)  = i;
        ak_vec(i+11) = ak(k_vec(i+11));
    }
    sanji::figure("ak(phi)");
    sanji::plot(k_vec,ak_vec.real(),{{"line_style",'-'},{"color",RED}});
    sanji::plot(k_vec,ak_vec.imag(),{{"line_style",'-'},{"color",BLUE}});

    // A
    VectorXd A_vec(Ntheta); 
    for (uint i = 0; i < Ntheta; ++i) A_vec(i) = A(theta_vec(i)*M_PI,0.0);
    sanji::figure("A(phi)");
    sanji::plot(theta_vec,A_vec,{{"line_style",'-'},{"color",RED}});

    // Ak
    VectorXcd Ak_vec(Nk);
    for (int i = -11; i <= 11; ++i) Ak_vec(i+11) = Ak(k_vec(i+11),0.0);
    sanji::figure("Ak(phi)");
    sanji::plot(k_vec,Ak_vec.real(),{{"line_style",'-'},{"color",RED}});
    sanji::plot(k_vec,Ak_vec.imag(),{{"line_style",'-'},{"color",BLUE}});

    // Br
    VectorXd Br_vec(Ntheta); 
    for (uint i = 0; i < Ntheta; ++i) Br_vec(i) = Br(theta_vec(i)*M_PI);
    sanji::figure("Br(phi)");
    sanji::plot(theta_vec,Br_vec,{{"line_style",'-'},{"color",RED}});

    // Torque
    uint Nt = 360;
    VectorXd time(Nt);
    VectorXd torque_vec(Nt);
    const double phi_os = -std::arg(Ak(7,0.0))/7.0;
    for (uint i = 0; i < Nt; ++i) {
        time(i)       = ((2.0*M_PI/Nt)*i)/Omega_m;
        torque_vec(i) = torque(time(i)*Omega_m+phi_os,time(i));
    }
    sanji::figure("Torque");
    sanji::plot(time,torque_vec,{{"line_style",'-'},{"color",RED}});
    std::cout << "The mean torque is " << torque_vec.mean() << "." << std::endl;

    // GSL
    gsl_integration_workspace_free(gsl_workspace);

    // Execute the application
    app.exec();
}
