#include "ProblemWrapper.hpp"
#include "StraightBoundarySegment.hpp"
#include "ArcedBoundarySegment.hpp"
#include "CircularBoundarySegment.hpp"
#include "flint.hpp"
#include "Sanji.hpp"
#include "ExteriorDomain.hpp"
#include "ReferenceDomain.hpp"
#include <gsl/gsl_integration.h>
#include <iostream>
#include <cmath>
#include <filesystem>
#include <fstream>

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 1e-8
#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-6
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          1000

using namespace sanji::colors;

/* Type definitions */
template <typename T>
using vector = std::vector<T>;

ProblemWrapper::ProblemWrapper() :
    p_ptr_(new Problem())
{}

boundary_segment_ptr ProblemWrapper::addStraightBoundarySegment(const Vec2D& start,
                                                                const Vec2D& end,
                                                                const uint   num_nodes,
                                                                const uint   poly_order,
                                                                const bool   normal_points_to_the_right,
                                                                const bool   is_reference,
                                                                const double surface_current_density) {
    const boundary_segment_ptr boundary_segment_ptr = std::make_shared<StraightBoundarySegment>(start,
                                                                                                end,
                                                                                                num_nodes,
                                                                                                poly_order,
                                                                                                normal_points_to_the_right,
                                                                                                is_reference,
                                                                                                surface_current_density);
    p_ptr_->addBoundarySegment(boundary_segment_ptr);
    return boundary_segment_ptr;
}

boundary_segment_ptr ProblemWrapper::addArcedBoundarySegment(const double radius,
                                                             const Vec2D& center,
                                                             const double phi_min,
                                                             const double phi_max,
                                                             const uint   num_nodes,
                                                             const uint   boundary_solution_polynomial_order,
                                                             const bool   normal_points_to_the_right,
                                                             const bool   is_reference) {
    const boundary_segment_ptr boundary_segment_ptr = std::make_shared<ArcedBoundarySegment>(radius,
                                                                                             center,
                                                                                             phi_min,
                                                                                             phi_max,
                                                                                             num_nodes,
                                                                                             boundary_solution_polynomial_order,
                                                                                             normal_points_to_the_right,
                                                                                             is_reference);
    p_ptr_->addBoundarySegment(boundary_segment_ptr);
    return boundary_segment_ptr;
}

boundary_segment_ptr ProblemWrapper::addCircularBoundarySegment(const double radius,
                                                                const Vec2D& center,
                                                                const uint   num_nodes,
                                                                const uint   boundary_solution_polynomial_order,
                                                                const bool   normal_points_to_the_right,
                                                                const bool   is_reference) {
    const boundary_segment_ptr boundary_segment_ptr = std::make_shared<CircularBoundarySegment>(radius,
                                                                                                center,
                                                                                                num_nodes,
                                                                                                boundary_solution_polynomial_order,
                                                                                                normal_points_to_the_right,
                                                                                                is_reference);
    p_ptr_->addBoundarySegment(boundary_segment_ptr);
    return boundary_segment_ptr;
}

domain_ptr ProblemWrapper::addDomain(const string& name, const vector<vector<directed_boundary_segment_ptr>>& dbsps, const double mu, const double J, const int type) {
    for (uint loop_idx = 0; loop_idx < dbsps.size(); ++loop_idx) {
        const auto& dbsps_ = dbsps[loop_idx];
        bool error         = false;
        double error_mag;
        uint i             = 0;
        for (; i < dbsps_.size()-1; ++i)
            if ((error_mag = (dbsps_[i]->getEndPoint()-dbsps_[i+1]->getStartPoint()).Norm2()) > 1.0e-10) {
                error = true;
                break;
             }
        if ((error_mag = (dbsps_[dbsps_.size()-1]->getEndPoint()-dbsps_[0]->getStartPoint()).Norm2()) > 1.0e-10) error = true;
        if (error) {
            std::cout << "Provided invalid boundary segments to addDomain. Error in loop " << loop_idx << " and at index " << i << ". The magnitude is " << error_mag << "m.";
            std::cout << " The provided name is '" << name << "'." << std::endl;
            abort();
        }
    }
    const domain_ptr domain_ptr = std::make_shared<Domain>(name,dbsps,DomainOptions{DomainOptions::FilterType([](const Vec2D x){ return true; }),false,mu,J,type});
    p_ptr_->addDomain(domain_ptr);
    return domain_ptr;
}

domain_ptr ProblemWrapper::addDomain(const string& name, const initializer_list<vector<directed_boundary_segment_ptr>> dbsps, const double mu, const double J, const int type) {
    return addDomain(name, vector<vector<directed_boundary_segment_ptr>>{dbsps},mu,J,type);
}

domain_ptr ProblemWrapper::addExteriorDomain(const string& name, const vector<vector<directed_boundary_segment_ptr>>& dbsps, const double mu) {
    for (const auto& dbsps_ : dbsps) {
        bool error = false;
        for (uint i = 0; i < dbsps_.size()-1; ++i)
            if ((dbsps_[i]->getEndPoint()-dbsps_[i+1]->getStartPoint()).Norm2() > 1.0e-10) error = true;
        if ((dbsps_[dbsps_.size()-1]->getEndPoint()-dbsps_[0]->getStartPoint()).Norm2() > 1.0e-10) error = true;
        if (error) {
            std::cout << "Provided invalid boundary segments to addExteriorDomain." << std::endl;
            abort();
        }
    }
    const domain_ptr domain_ptr = std::make_shared<ExteriorDomain>(name,dbsps,mu);
    p_ptr_->addDomain(domain_ptr);
    return domain_ptr;
}

domain_ptr ProblemWrapper::addExteriorDomain(const string& name, const initializer_list<vector<directed_boundary_segment_ptr>> dbsps, const double mu) {
    return addExteriorDomain(name, vector<vector<directed_boundary_segment_ptr>>{dbsps},mu);
}

domain_ptr ProblemWrapper::addReferenceDomain(const c_domain_ptr underlying_domain, const vector<vector<directed_boundary_segment_ptr>>& dbsps, const ReferenceDomainOptions options) {
    for (uint loop_idx = 0; loop_idx < dbsps.size(); ++loop_idx) {
        const auto& dbsps_ = dbsps[loop_idx];
        bool error         = false;
        uint i             = 0;
        for (; i < dbsps_.size()-1; ++i)
            if ((dbsps_[i]->getEndPoint()-dbsps_[i+1]->getStartPoint()).Norm2() > 1.0e-10) {
                error = true;
                break;
             }
        if ((dbsps_[dbsps_.size()-1]->getEndPoint()-dbsps_[0]->getStartPoint()).Norm2() > 1.0e-10) error = true;
        if (error) {
            std::cout << "Provided invalid boundary segments to addReferenceDomain. Error in loop " << loop_idx << " and at index " << i << ".";
            std::cout << " The end point is " << dbsps_[i]->getEndPoint().toString() << ", but the start point is " << dbsps_[(i+1)%dbsps_.size()]->getStartPoint().toString() << "." << std::endl;
            abort();
        }
    }
    const domain_ptr domain_ptr = std::make_shared<ReferenceDomain>(underlying_domain,dbsps,options);
    p_ptr_->addDomain(domain_ptr);
    return domain_ptr;
}

domain_ptr ProblemWrapper::addReferenceDomain(const c_domain_ptr underlying_domain, const initializer_list<vector<directed_boundary_segment_ptr>> dbsps, const ReferenceDomainOptions options) {
    return addReferenceDomain(underlying_domain,vector<vector<directed_boundary_segment_ptr>>{dbsps},options);
}

vector<BoundarySegmentPlottingData> ProblemWrapper::getBoundarySegmentsPlottingData(const double max_length_per_segment, const bool show_reference_boundaries) const {
    vector<BoundarySegmentPlottingData> ret;
    for (const c_boundary_segment_ptr& bsp : p_ptr_->boundary_segment_ptrs_) {
        if (!show_reference_boundaries && bsp->isReference()) continue;
        Style style;
        if (bsp->isReference()) style = {{"color",RED},{"line_style",'.'}};
        else                    style = {{"color",RED},{"line_style",'-'}};
        const auto [x,y] = getBoundarySegmentDataPoints(bsp,max_length_per_segment);
        ret.emplace_back(x,y,style);
    }
    return ret;
}

vector<BoundarySegmentPlottingVectorData> ProblemWrapper::getNormals(const double max_length_per_segment, const double arrow_length) const {
    vector<BoundarySegmentPlottingVectorData> ret;
    uint num_non_ref_doms = 0;
    for (const c_domain_ptr& dom_ptr : p_ptr_->domain_ptrs_)
        if (!dom_ptr->isReference()) ++num_non_ref_doms;
    for (const c_domain_ptr& dom_ptr : p_ptr_->domain_ptrs_) {
        if (dom_ptr->isReference()) continue;
        for (const auto& dbsps : dom_ptr->getDirectedBoundarySegmentPointers()) {
            for (const directed_boundary_segment_ptr& dbsp : dbsps) {
                const auto [x,y,u,v] = ::getNormals(**dbsp,max_length_per_segment);
                if (!dbsp->isForward()) {
                    for (uint i = 0; i < u->rows(); ++i) {
                        (*u)(i) *= -1.0;
                        (*v)(i) *= -1.0;
                    }
                }
                for (uint i = 0; i < u->rows(); ++i) {
                    (*u)(i) *= arrow_length;
                    (*v)(i) *= arrow_length;
                }
                Style style;
                style = {{"color",static_cast<uint32_t>(hsv_to_rgb(dom_ptr->getDomainIndex()/static_cast<double>(num_non_ref_doms),1.0,1.0))}};
                ret.emplace_back(x,y,u,v,style);
            }
        }
    }
    return ret;
}

vector<BoundarySegmentPlottingVectorData> ProblemWrapper::getTangentials(const double max_length_per_segment, const double arrow_length) const {
    vector<BoundarySegmentPlottingVectorData> ret;
    uint num_non_ref_doms = 0;
    for (const c_domain_ptr& dom_ptr : p_ptr_->domain_ptrs_)
        if (!dom_ptr->isReference()) ++num_non_ref_doms;
    for (const c_domain_ptr& dom_ptr : p_ptr_->domain_ptrs_) {
        if (dom_ptr->isReference()) continue;
        for (const auto& dbsps : dom_ptr->getDirectedBoundarySegmentPointers()) {
            for (const directed_boundary_segment_ptr& dbsp : dbsps) {
                const auto [x,y,u,v] = ::getTangentials(**dbsp,max_length_per_segment);
                if (!dbsp->isForward()) {
                    for (uint i = 0; i < u->rows(); ++i) {
                        (*u)(i) *= -1.0;
                        (*v)(i) *= -1.0;
                    }
                }
                for (uint i = 0; i < u->rows(); ++i) {
                    (*u)(i) *= arrow_length;
                    (*v)(i) *= arrow_length;
                }
                Style style;
                style = {{"color",static_cast<uint32_t>(hsv_to_rgb(dom_ptr->getDomainIndex()/static_cast<double>(num_non_ref_doms),1.0,1.0))}};
                ret.emplace_back(x,y,u,v,style);
            }
        }
    }
    return ret;
}

vector<BoundarySegmentPlottingVectorData> ProblemWrapper::getFieldData(const double arrow_density, const bool load_field_data, const bool store_field_data, const string folder_name) const {
    vector<BoundarySegmentPlottingVectorData> ret;

    // Possibly load the data
    if (load_field_data) {
        if (!std::filesystem::is_directory(folder_name)) {
            std::cout << "Error in ProblemWrapper::getFieldData. The folder '" << folder_name << "' does not exist." << std::endl;
            abort();
        }
        vector<string> file_names;
        for (const auto& p : std::filesystem::directory_iterator(folder_name)) {
            const string file_name = p.path();
            if (file_name.ends_with(".fd")) file_names.push_back(file_name);
        }
        for (const auto& file_name : file_names) {
            // Open the file and determine its size
            std::ifstream file(file_name,std::ios::in|std::ios::binary);
            file.ignore(std::numeric_limits<std::streamsize>::max());
            std::streamsize file_length = file.gcount();
            file.clear();
            file.seekg(0,std::ios_base::beg);

            // Allocate memory for the solution
            const uint    num_rows = file_length/sizeof(double)/4;
            const vec_ptr x        = std::make_shared<VectorXd>(num_rows);
            const vec_ptr y        = std::make_shared<VectorXd>(num_rows);
            const vec_ptr u        = std::make_shared<VectorXd>(num_rows);
            const vec_ptr v        = std::make_shared<VectorXd>(num_rows);
            ret.emplace_back(x,y,u,v,Style());

            // Load the data
            vector<vec_ptr> vec_ptrs{x,y,u,v};
            vector<uint8_t> row_buffer(sizeof(double));
            for (const auto& v_ptr : vec_ptrs)
                for (uint row = 0; row < num_rows; ++row) {
                    for (uint i = 0; i < sizeof(double); ++i) row_buffer[i] = file.get();
                    double   c;
                    uint8_t* ptr = reinterpret_cast<uint8_t*>(&c);
                    for (uint i = 0; i < sizeof(double); ++i) ptr[i] = row_buffer[i];
                    (*v_ptr)(row) = c;
                }
        }
    } else {
        for (const c_domain_ptr& dom_ptr : p_ptr_->domain_ptrs_) {
            const int    domain_type = dom_ptr->getType();
            if (domain_type == -1) continue;
            const double area        = dom_ptr->getApproximateArea();
            const uint   num_points  = std::round(area*arrow_density);
            const uint   num_levels  = std::round(std::sqrt(num_points/M_PI));
            Style        style;
            if (domain_type == 1) {
                const auto [x,y,u,v] = getFieldDataType1(*p_ptr_,dom_ptr,num_points);
                ret.emplace_back(x,y,u,v,style);
            } else if (domain_type == 2) {
                const auto [x,y,u,v] = getFieldDataType2(*p_ptr_,dom_ptr,num_levels,num_points,dom_ptr->getFilter());
                ret.emplace_back(x,y,u,v,style);
            } else if (domain_type == 3) {
                const auto [x,y,u,v] = getFieldDataType3(*p_ptr_,dom_ptr,num_levels,num_points);
                ret.emplace_back(x,y,u,v,style);
            } else if (domain_type == 4) {
                const auto [x,y,u,v] = getFieldDataType4(*p_ptr_,dom_ptr,num_levels,num_points);
                ret.emplace_back(x,y,u,v,style);
            }
        }

        // Possibly store the data
        if (store_field_data) {
            // Create the result folder
            std::filesystem::create_directories(folder_name);

            // Store the actual data
            uint data_cnt = 0;
            for (const auto& data : ret) {
                std::ofstream file(folder_name+"/"+std::to_string(data_cnt)+".fd",std::ios::out|std::ios::binary);
                ++data_cnt;
                vector<vec_ptr> vec_ptrs{data.x,data.y,data.u,data.v};
                for (const vec_ptr v_ptr : vec_ptrs)
                    for (uint row = 0; row < v_ptr->rows(); ++row) {
                        const double   cd  = (*v_ptr)(row);
                        const uint8_t* ptr = reinterpret_cast<const uint8_t*>(&cd);
                        for (uint i = 0; i < sizeof(double); ++i)
                            file.put(ptr[i]);
                    }
            }
        }
    }

    return ret;
}

MatrixXd ProblemWrapper::getFieldData(const c_domain_ptr underlying_domain, const MatrixXd& positions) const {
    const uint N = positions.rows();

    // Allocate memory for the field data
    MatrixXd ret(N,2u);

    // Compute the field data
    for (uint i = 0u; i < N; ++i) evaluateInteriorSolution(underlying_domain,Vec2D{positions(i,0),positions(i,1)});

    return ret;
}

Vec2D ProblemWrapper::evaluateInteriorSolution(const c_domain_ptr dom_ptr, const Vec2D x) const {
    return p_ptr_->evaluateInteriorSolution(dom_ptr->getDomainIndex(),x);
}

tuple<Vec2D/* B */,Vec2D/* x */,Vec2D/* nu */> ProblemWrapper::evaluateBoundarySolution(const domain_ptr dom_ptr, const uint loop_idx, const uint dir_bnd_seg_idx, const double t) const {
    return p_ptr_->evaluateBoundarySolution(dom_ptr->getDomainIndex(),loop_idx,dir_bnd_seg_idx,t);
}

vector<tuple<string,vector<double>>> ProblemWrapper::getTangentialDerivativesBoundaryIntegrals() const {
    // Create a wrapper to simplify integration
    gsl_function               gsl_fun;
    gsl_integration_workspace* gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
    const auto quad = [&gsl_fun,gsl_workspace]()->double {
        double result,error;
        gsl_integration_qag(&gsl_fun, /* Function to integrate */
                            0.0,      /* Lower integration limit */
                            1.0,      /* Upper integration limit */
                            INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                            INTEGRAL_RELATIVE_ERROR_LIMIT,
                            MAX_NUM_SUBINTERVALS,
                            INTEGRATION_KEY,
                            gsl_workspace,&result,&error);
        return result;
    };

    // A lambda that allows to evaluate a monomial on the unit interval [0,1]
    const auto mono = [](const double t, const uint p)->double {
        if (t == 0.0 && p == 0) return 1.0;
        else if (t == 0.0) return 0.0;
        double c = 1.0;
        for (uint i = 0; i < p; ++i) c *= t;
        return c;
    };

    // A lambda to compute the tangential derivative of the magnetic vector potential along a boundary segment
    const double** params = new const double*[1];
    gsl_fun.params        = params;
    uint sol_row;
    const auto computeBoundaryIntegral = [&gsl_fun,&quad,&params,&mono,&sol_row,this](const directed_boundary_segment_ptr& dbsp, const uint domain_index)->double {
        double integral = 0.0;

        // Compute the integral along each subsegment
        for (uint subseg_idx = 0; subseg_idx < dbsp->getNumSubsegments(); ++subseg_idx) {
            const auto dpsi_i_2Norm = dbsp->getdpsii2Norm(subseg_idx);

            for (uint mon_deg = 0; mon_deg <= (**dbsp).getFunctionApproximationPolynomialOrder(); ++mon_deg) {
                const auto ml = [mon_deg,&mono](const double t)->double {
                    return mono(t,mon_deg);
                };
                const auto integrand = [&dpsi_i_2Norm,&ml](const double t)->double {
                    return dpsi_i_2Norm(t)*ml(t);
                };
                params[0] = reinterpret_cast<const double*>(&integrand);

                gsl_fun.function = [](const double t, void* params)->double {
                    const auto& integrand_ = *static_cast<const decltype(integrand)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0])); 

                    return integrand_(t);
                };
                integral += quad()*(*p_ptr_->sol_)(p_ptr_->domain_system_matrix_column_offsets_[domain_index]+sol_row);

                sol_row += 2;
            }
        }

        return integral;
    };

    // Compute the boundary integrals
    vector<tuple<string,vector<double>>> integrals(p_ptr_->domain_ptrs_.size());
    for (uint dom_idx = 0; dom_idx < p_ptr_->domain_ptrs_.size(); ++dom_idx) {
        // Skip reference domains
        if (p_ptr_->domain_ptrs_[dom_idx]->isReference()) continue;

        std::get<0>(integrals[dom_idx]) = p_ptr_->domain_ptrs_[dom_idx]->getName();

        sol_row = 0;
        for (uint loop_idx = 0; loop_idx < p_ptr_->domain_ptrs_[dom_idx]->getDirectedBoundarySegmentPointers().size(); ++loop_idx) {
            std::get<1>(integrals[dom_idx]).push_back(0.0);
            for (const directed_boundary_segment_ptr& dbsp : p_ptr_->domain_ptrs_[dom_idx]->getDirectedBoundarySegmentPointers()[loop_idx]) {
                std::get<1>(integrals[dom_idx]).back() += computeBoundaryIntegral(dbsp,p_ptr_->domain_ptrs_[dom_idx]->getDomainIndex()); // TODO: This can be right
            }
        }
    }

    // Free allocated memory
    gsl_integration_workspace_free(gsl_workspace);
    delete[] params;

    return integrals;
}

void ProblemWrapper::solve(const bool debug) {
    SolverOptions options;
    options.debug          = debug;
    options.load_solution  = false;
    options.store_solution = false;
    options.folder_name    = "";

    p_ptr_->solve(options);
}

void ProblemWrapper::solveAndStore(const string& folder_name, const bool debug) {
    SolverOptions options;
    options.load_solution  = false;
    options.store_solution = true;
    options.folder_name    = folder_name;
    options.debug          = debug;

    p_ptr_->solve(options);
}

void ProblemWrapper::loadSolution(const string& folder_name) {
    SolverOptions options;
    options.load_solution  = true;
    options.store_solution = false;
    options.folder_name    = folder_name;

    p_ptr_->solve(options);
}
