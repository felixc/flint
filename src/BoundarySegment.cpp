#include <stdexcept>
#include "BoundarySegment.hpp"

/* Type definitions */
template <class... Types>
using tuple = std::tuple<Types...>;

BoundarySegment::BoundarySegment(const bool   is_closed,
                                 const uint   num_nodes,
                                 const uint   boundary_solution_polynomial_order,
                                 const bool   normal_points_to_the_right,
                                 const bool   is_reference,
                                 const double surface_current_density) :
    num_nodes_(num_nodes),
    is_closed_(is_closed),
    boundary_solution_polynomial_order_(boundary_solution_polynomial_order),
    normal_points_to_the_right_(normal_points_to_the_right),
    is_reference_(is_reference),
    surface_current_density_(surface_current_density)
{}

bool BoundarySegment::getNormalPointsToTheRight() const {
    return normal_points_to_the_right_;
}

uint BoundarySegment::getNumNodes() const {
    return num_nodes_;
}

uint BoundarySegment::getNumSubsegments() const {
    if (is_closed_) return num_nodes_;
    else            return num_nodes_-1;
}

uint BoundarySegment::getFunctionApproximationPolynomialOrder() const {
    return boundary_solution_polynomial_order_;
}

const Domain& BoundarySegment::getNeighbouringDomain(const uint i) const {
    return *neighbouring_domain_ptrs_[i];
}

const Domain* BoundarySegment::getOtherDomain(const Domain* domain) const {
    if (neighbouring_domain_ptrs_[0] == domain)
        return neighbouring_domain_ptrs_[1];
    else if (neighbouring_domain_ptrs_[1] == domain)
        return neighbouring_domain_ptrs_[0];
    else
        throw std::runtime_error("Called getOtherDomain with invalid pointer.");
}

uint BoundarySegment::getNumNeighbouringDomains() const {
    return neighbouring_domain_ptrs_.size();
}

uint BoundarySegment::getBoundarySegmentIndex() const {
    return boundary_segment_index_;
}

bool BoundarySegment::isReference() const {
    return is_reference_;
}

const Vec2D BoundarySegment::getStartPoint() const {
    return getBoundaryPoint(0.0);
}

const Vec2D BoundarySegment::getEndPoint() const {
    return getBoundaryPoint(1.0);
}

double BoundarySegment::getSurfaceCurrentDensity() const {
    return surface_current_density_;
}

void BoundarySegment::setBoundarySegmentIndex(const uint boundary_segment_index) {
    boundary_segment_index_ = boundary_segment_index;
}

void BoundarySegment::addNeighbouringDomain(const Domain* dptr) {
    if (neighbouring_domain_ptrs_.size() == 2)
        throw std::runtime_error("Tried to add a third boundary neighbour.");
    else neighbouring_domain_ptrs_.push_back(dptr);
}

uint BoundarySegment::getOtherNeighbourIndex(const uint domain_index) const {
    if (neighbouring_domain_ptrs_.size() != 2)
        throw std::runtime_error("Encountered a boundary which does not have two neighbours.");
    if ((*neighbouring_domain_ptrs_[0]).getDomainIndex() == domain_index) {
        return (*neighbouring_domain_ptrs_[1]).getDomainIndex();
    } else if ((*neighbouring_domain_ptrs_[1]).getDomainIndex() == domain_index) {
        return (*neighbouring_domain_ptrs_[0]).getDomainIndex();
    } else
        throw std::runtime_error("Called getOtherNeighbourIndex with invalid domain_index.");
}

DirectedBoundarySegment::DirectedBoundarySegment(const boundary_segment_ptr bsp,
                                                 const bool is_forward) :
    bsp_(bsp),
    is_forward_(is_forward)
{}

void DirectedBoundarySegment::setAssociatedDomain(const Domain* associated_domain) {
    associated_domain_ = associated_domain;
}

uint DirectedBoundarySegment::getNumSubsegments() const {
    return bsp_->getNumSubsegments();
}

std::function<const Vec2D(const double)> DirectedBoundarySegment::getpsii(const uint i) const {
    if (is_forward_) { return bsp_->getpsii(i); }
    else {
        const uint index_offset = bsp_->is_closed_ ? 1 : 2;
        return [psii=this->bsp_->getpsii(bsp_->num_nodes_-index_offset-i)](const double t)->Vec2D {
            return psii(1.0-t);
        };
    }
}

std::function<double(const double)> DirectedBoundarySegment::getdpsii2Norm(const uint i) const {
    if (is_forward_) return bsp_->getdpsii2Norm(i);
    else {
        const uint index_offset = bsp_->is_closed_ ? 1 : 2;
        return [dpsii=this->bsp_->getdpsii2Norm(bsp_->num_nodes_-index_offset-i)](const double t)->double {
            return dpsii(1.0-t);
        };
    }
}

std::function<const Vec2D(const double)> DirectedBoundarySegment::getnu(const uint i) const {
    if (is_forward_) return bsp_->getnu(i);
    else {
        const uint index_offset = bsp_->is_closed_ ? 1 : 2;
        return [getnu=this->bsp_->getnu(bsp_->num_nodes_-index_offset-i)](const double t)->const Vec2D {
            return -getnu(1.0-t);
        };
    }
}

std::function<const Vec2D(const double)> DirectedBoundarySegment::gett(const uint i) const {
    if (is_forward_) return bsp_->gett(i);
    else {
        const uint index_offset = bsp_->is_closed_ ? 1 : 2;
        return [gett=this->bsp_->gett(bsp_->num_nodes_-index_offset-i)](const double t)->const Vec2D {
            return -gett(1.0-t);
        };
    }
}

bool DirectedBoundarySegment::isForward() const {
    return is_forward_;
}

const Domain& DirectedBoundarySegment::getNeighbouringDomain() const {
    return *bsp_->getOtherDomain(associated_domain_);
}

const DirectedBoundarySegment& DirectedBoundarySegment::getNeighboursDirectedBoundarySegment() const {
    return bsp_->getOtherDomain(associated_domain_)->getDirectedBoundarySegment(bsp_->getBoundarySegmentIndex());
}

tuple<uint,uint> DirectedBoundarySegment::getNeighboursDirectedBoundarySegmentIndex() const {
    return bsp_->getOtherDomain(associated_domain_)->getDirectedBoundarySegmentIndex(bsp_->getBoundarySegmentIndex());
}

uint DirectedBoundarySegment::getTrueSubsegmentIndex(const uint subsegment_index) const {
    // Check the argument
    const uint num_subsegments = bsp_->is_closed_ ? bsp_->num_nodes_ : bsp_->num_nodes_-1;
    if (subsegment_index >= num_subsegments)
        throw new std::runtime_error("Invalid argument provided to getTrueSubsegmentIndex: "+std::to_string(subsegment_index)+">="+std::to_string(num_subsegments));

    if (is_forward_) return subsegment_index;
    else             return num_subsegments-1-subsegment_index;
}

const Vec2D DirectedBoundarySegment::getStartPoint() const {
    if (is_forward_) return bsp_->getStartPoint();
    else             return bsp_->getEndPoint();
}

const Vec2D DirectedBoundarySegment::getEndPoint() const {
    if (is_forward_) return bsp_->getEndPoint();
    else             return bsp_->getStartPoint();
}

const Vec2D DirectedBoundarySegment::getBoundaryPoint(const double t) const {
    if (is_forward_) return bsp_->getBoundaryPoint(t);
    else             return bsp_->getBoundaryPoint(1.0-t);
}

BoundarySegment& DirectedBoundarySegment::operator*() {
    return *bsp_;
}

const BoundarySegment& DirectedBoundarySegment::operator*() const {
    return *bsp_;
}
