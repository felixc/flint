#include <cmath>
#include "Sanji.hpp"
#include "ProblemWrapper.hpp"
#include <iostream>
#include <gsl/gsl_integration.h>
#include <Eigen/Dense>
#include <iomanip>

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 1e-8
#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-6
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          1000

using namespace sanji::colors;

/* Type definitions */
using DBS = DirectedBoundarySegment;
template <typename T>
using vector   = std::vector<T>;
using VectorXd = Eigen::VectorXd;
using MatrixXd = Eigen::MatrixXd;

// Visualization
constexpr bool plot_normals     = true;
constexpr bool plot_tangentials = false;

// Permeabilities
constexpr double mu0    = 4.0*M_PI*1e-7;
constexpr double mu_air = 1.00000037*mu0; // https://www.engineeringtoolbox.com/permeability-d_1923.html
constexpr double mu_cu  = 0.999994*mu0;   // https://www.engineeringtoolbox.com/permeability-d_1923.html

// Geometric parameters
constexpr double cable_radius = 0.001;

// Current densities
constexpr double J = 1.0/1.0e-6;

int main(int argc, char* argv[]) {
    // Initialize this as a Qt application
    QApplication app(argc, argv);
    sanji::init();

    /* Create a problem instance */
    ProblemWrapper p;

    // Set the output precision
    std::cout << std::setprecision(15);

    // Create the boundaries in the problem
    const auto Gamma_1 = p.addCircularBoundarySegment(cable_radius,      /* Radius */
                                                      Vec2D{0.0015,0.0}, /* Center */
                                                      3,                 /* NumNodes */
                                                      1,                 /* boundarySolutionPolynomialOrder */
                                                      true               /* normal_points_to_the_right */);
    const auto Gamma_2 = p.addCircularBoundarySegment(cable_radius,       /* Radius */
                                                      Vec2D{-0.0015,0.0}, /* Center */
                                                      3,                  /* NumNodes */
                                                      1,                  /* boundarySolutionPolynomialOrder */
                                                      true                /* normal_points_to_the_right */);

    // Create the domains in the problem
    const auto cable_1 = p.addDomain({std::make_shared<DBS>(Gamma_1,true)},        /* Boundary information */
                                     mu_cu,                                        /* Magnetic permeability */
                                    -J                                             /* Current density */);
    const auto cable_2 = p.addDomain({std::make_shared<DBS>(Gamma_2,true)},        /* Boundary information */
                                     mu_cu,                                        /* Magnetic permeability */
                                     J                                             /* Current density */);
    const auto air = p.addExteriorDomain({std::make_shared<DBS>(Gamma_1,false),
                                          std::make_shared<DBS>(Gamma_2,false)}, /* Boundary information */
                                         mu_air                                  /* Magnetic permeability */);

    /* Solve the problem */
    p.solve();
    //p.solveAndStore("results/simple");
    //p.loadSolution("results/simple");

    // Reference boundaries and domains
    const auto ring_1 = p.addCircularBoundarySegment(0.002,      /* radius */
                                                     Vec2D{0,0}, /* center */
                                                     12,         /* num_nodes */
                                                     4,          /* boundary_solution_polynomial_order */
                                                     true,       /* normal_points_to_the_right */
                                                     true        /* is_reference */);
    const auto ring_2 = p.addCircularBoundarySegment(0.02,       /* radius */
                                                     Vec2D{0,0}, /* center */
                                                     12,         /* num_nodes */
                                                     4,          /* boundary_solution_polynomial_order */
                                                     true,       /* normal_points_to_the_right */
                                                     true        /* is_reference */);
    const auto reference_domain = p.addReferenceDomain(std::const_pointer_cast<const Domain>(air),
                                                       {std::make_shared<DirectedBoundarySegment>(ring_1,false),
                                                        std::make_shared<DirectedBoundarySegment>(ring_2,true)}, 1 /* Type */);

    /* Plot the boundaries in the problem */
    sanji::figure("Benchmark simulation");
    for (const auto& boundary_segment_plotting_data : p.getBoundarySegmentsPlottingData(0.001/4.0, /* max_length_per_segment */
                                                                                        true       /* show_reference_boundaries */))
        sanji::plot(*boundary_segment_plotting_data.x,*boundary_segment_plotting_data.y,boundary_segment_plotting_data.style);

    /* Plot the normals in the problem */
    if (plot_normals)
        for (const auto& boundary_segment_plotting_vector_data : p.getNormals(0.001, /* max_length_per_segment */
                                                                              1.0e-3 /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);

    /* Plot the magnetic flux density */
    const auto field_data = p.getFieldData();
    double min =  std::numeric_limits<double>::max();
    double max = -std::numeric_limits<double>::max();
    for (const auto& field_data : field_data)
        for (uint i = 0; i < field_data.x->rows(); ++i) {
            const double strength = Vec2D{(*field_data.u)(i),(*field_data.v)(i)}.Norm2();
            if (strength < min) min = strength;
            if (strength > max) max = strength;
        }
    for (const auto& field_data : field_data)
        sanji::quiver(*field_data.x,
                      *field_data.y,
                      *field_data.u,
                      *field_data.v,{{"arrow_length",0.0006},{"use_colormap",1},{"colormap",TURBO},{"min",min},{"max",max}});

    /* Plot cosmetics */
    sanji::setPlotBackgroundColor(GRAY);
    sanji::setAxesRatio("equal");

    /* Compute problem statistics */

    // Create a wrapper to simplify integration
    gsl_function               gsl_fun;
    gsl_integration_workspace* gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
    const auto quad = [&gsl_fun,gsl_workspace]()->double {
        double result,error;
        gsl_integration_qag(&gsl_fun, /* Function to integrate */
                            0.0,      /* Lower integration limit */
                            1.0,      /* Upper integration limit */
                            INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                            INTEGRAL_RELATIVE_ERROR_LIMIT,
                            MAX_NUM_SUBINTERVALS,
                            INTEGRATION_KEY,
                            gsl_workspace,&result,&error);
        return result;
    };

    // Create a lambda to evaluate the solution in the exterior
    const auto eval_sol_ext = [&p,air](const Vec2D x)->const Vec2D {
        return p.evaluateInteriorSolution(air,x);
    };

    // Integrate the magnetic field at different radii
    const uint Nr           = 10;
    const double min_radius = 0.0015;
    const double max_radius = 0.1;
    VectorXd radii(Nr+1);
    VectorXd results(Nr+1);
    for (uint radius_idx = 0; radius_idx < Nr+1; ++radius_idx) {
        const double radius = min_radius + (max_radius-min_radius)*radius_idx/static_cast<double>(Nr);
        radii(radius_idx)   = radius;
        gsl_fun.function = [](const double t, void* params)-> double {
            const decltype(eval_sol_ext)& eval_sol_ext_ = *static_cast<const decltype(eval_sol_ext)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
            const double& radius                        = *reinterpret_cast<const double **>(params)[1];

            const double phi    = t*2.0*M_PI;
            const Vec2D  bnd_pt = radius*Vec2D{std::cos(phi),std::sin(phi)};
            const Vec2D  B      = eval_sol_ext_(bnd_pt);
            const Vec2D  nu     = Vec2D{-std::sin(phi),std::cos(phi)};

            return nu.dot(B);
        };
        const double** params = new const double*[2];
        params[0]             = reinterpret_cast<const double*>(&eval_sol_ext);
        params[1]             = &radius;
        gsl_fun.params        = params;
        results(radius_idx)   = quad()*2.0*M_PI*radius;
        delete[] params;
    }
    gsl_integration_workspace_free(gsl_workspace);

    // Compute the scores
    const double current = cable_radius*cable_radius*M_PI*J;
    double mean_score = 0.0;
    for (uint i = 0; i < Nr+1; ++i) {
        const double c = std::abs(current-results(i)/mu0+1.0e-30)/std::abs(current);
        mean_score -= std::log10(c)*20;
    }
    std::cout << "The mean score is: " << mean_score/(Nr+1) << std::endl;

    // Compute the boundary integrals of the tangential derivatives for all domains
    std::cout << p.getTangentialDerivativesBoundaryIntegrals() << std::endl;

    sanji::figure("Statistic");
    sanji::plot(radii,results);

    // Execute the application
    app.exec();
}
