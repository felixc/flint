#include <algorithm>
#include <tuple>
#include <thread>
#include <mutex>
#include <atomic>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <chrono>
#include <gsl/gsl_integration.h>
#include "Problem.hpp"
#include "Vec2D.hpp"

#include <iomanip>

/* Type definitions */
template <class... Types>
using tuple = std::tuple<Types...>;

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 1e-8
#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-8
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          4000
#define OUTER_INTEGRAL_ADAPTIV // Undefine this to use Gauss-Legendre quadrature for the outer integrals instead of adaptive quadrature
#ifndef OUTER_INTEGRAL_ADAPTIV
#define OUTER_QUADRATURE_ORDER        1000
#endif

// Parameters
static constexpr double mu0 = 4.0*M_PI*1e-7;

// A function that evaluates monomials
static double mono(const double t, const uint p) {
    if (t == 0.0 && p == 0) return 1.0;
    else if (t == 0.0) return 0.0;
    double c = 1.0;
    for (uint i = 0; i < p; ++i) c *= t;
    return c;
};

// Function to compute the fundamental solution and its derivatives
static double E(const Vec2D& x) {
    return std::log(x.Norm2())/2.0/M_PI;
};
static const Vec2D GradE(const Vec2D& x) {
    return Vec2D{x.x,x.y}/(2.0*M_PI*x.Norm2Squared());
};
static const Vec2D CurlE(const Vec2D& x) {
    return Vec2D{x.y,-x.x}/(2.0*M_PI*x.Norm2Squared());
};

void Problem::solve(const bool load_solution_flag, const bool store_solution_flag, const std::string folder_name) {
    // Initialize helper variables
    initializeHelperVariables();

    // Check if the solution shall be loaded from the file system
    if (load_solution_flag) {
        load_solution(folder_name);
        return;
    }

    // Allocate memory for the system matrix
    const unique_mat_ptr sys_mat_ptr = allocateSystemMatrix();

    // Allocate memory for the RHS
    const unique_vec_ptr rhs_ptr = allocateRHS();

    /* Create the tasks
     arg0 : Domain index (test)
     arg1 : Directed boundary segment index (test)
     arg2 : System matrix row to start
    */
    vector<tuple<uint,uint,uint>> tasks;
    for (auto [dom_idx_tst,sys_mat_row] = tuple<uint,uint>{0u,0u}; dom_idx_tst < domain_ptrs_.size(); ++dom_idx_tst) {
        // Define for convencience and clarity 
        const Domain& dom_tst = *domain_ptrs_[dom_idx_tst];
        const vector<directed_boundary_segment_ptr>& dir_bnd_seg_ptrs_tst = dom_tst.getDirectedBoundarySegmentPointers();

        // Skip reference domains
        if (dom_tst.isReference()) continue;

        for (uint dir_bnd_seg_ptr_idx_tst = 0; dir_bnd_seg_ptr_idx_tst < dir_bnd_seg_ptrs_tst.size(); ++dir_bnd_seg_ptr_idx_tst) {
            // Define for convencience and clarity 
            const DirectedBoundarySegment& dir_bnd_seg_tst = *dir_bnd_seg_ptrs_tst[dir_bnd_seg_ptr_idx_tst];

            tasks.emplace_back(dom_idx_tst,dir_bnd_seg_ptr_idx_tst,sys_mat_row);
            sys_mat_row += 2*dir_bnd_seg_tst.getNumSubsegments()*((*dir_bnd_seg_tst).getFunctionApproximationPolynomialOrder()+1);
        }
    }

    // Reverse the order of the tasks such that they are completed in the same order as they were created. For debugging purposes only.
    std::reverse(tasks.begin(),tasks.end());

    // Variable that keeps track of the number of tasks that remain
    std::atomic<uint> remaining_tasks(tasks.size());

    // Create a mutex for the tasks
    std::mutex task_mutex;

    // Create a lambda that creates worker handles
    const auto get_worker_handle = [&task_mutex,
                                    &remaining_tasks,
                                    &tasks,
                                    &sys_mat_ptr,
                                    &rhs_ptr,
                                    &domain_system_matrix_column_offsets=this->domain_system_matrix_column_offsets_,
                                    &domain_ptrs=this->domain_ptrs_](const bool is_main_thread) {
        return [&task_mutex,&remaining_tasks,&tasks,&sys_mat_ptr,&rhs_ptr,&domain_system_matrix_column_offsets,&domain_ptrs,is_main_thread]() {
            // Allocate GSL workspaces and functions
            gsl_function                   gsl_function_inner;
            gsl_function                   gsl_function_outer;
            gsl_integration_workspace*     gsl_workspace_inner = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
            #ifdef OUTER_INTEGRAL_ADAPTIV
            gsl_integration_workspace*     gsl_workspace_outer = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
            #else
            gsl_integration_glfixed_table* quadrature_table    = gsl_integration_glfixed_table_alloc(OUTER_QUADRATURE_ORDER);
            #endif

            // Create a wrappers to simplify integration
            const auto quad_adpt = [](gsl_function& fun, gsl_integration_workspace* workspace, const double a, const double b)->double {
                double result,error;
                gsl_integration_qag(&fun, /* Function to integrate */
                                    a,    /* Lower integration limit */
                                    b,    /* Upper integration limit */
                                    INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                                    INTEGRAL_RELATIVE_ERROR_LIMIT,
                                    MAX_NUM_SUBINTERVALS,
                                    INTEGRATION_KEY,
                                    workspace, &result, &error);
                return result;
            };
            const auto quad_adpt_s = [](gsl_function& fun, gsl_integration_workspace* workspace, const double a, const double b)->double {
                double result,error;
                gsl_integration_qags(&fun, /* Function to integrate */
                                     a,    /* Lower integration limit */
                                     b,    /* Upper integration limit */
                                     INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                                     INTEGRAL_RELATIVE_ERROR_LIMIT,
                                     MAX_NUM_SUBINTERVALS,
                                     workspace, &result, &error);
                return result;
            };
            #ifdef OUTER_INTEGRAL_ADAPTIV
            const auto quad_outer = [&quad_adpt,&gsl_function_outer,gsl_workspace_outer]()->double {
                return quad_adpt(gsl_function_outer,gsl_workspace_outer,0.0,1.0);
            };
            #else
            const auto quad_outer = [&gsl_function_outer,quadrature_table]()->double {
                return gsl_integration_glfixed(&gsl_function_outer,0.0,1.0,quadrature_table);
            };
            #endif
            const auto quad_inner = [&quad_adpt,&gsl_function_inner,gsl_workspace_inner](const double a, const double b)->double {
                return quad_adpt(gsl_function_inner,gsl_workspace_inner,a,b);
            };
            const auto quad_inner_s = [&quad_adpt_s,&gsl_function_inner,gsl_workspace_inner](const double a, const double b)->double {
                return quad_adpt_s(gsl_function_inner,gsl_workspace_inner,a,b);
            };

            // Create a lambda for the operators W_t and W_n which integrate only over \Psi
            const auto WtnPsi = [&quad_inner,&gsl_function_inner,gsl_workspace_inner](const Vec2D                                     tn_vec_jk,
                                                                                      const Vec2D                                     x,
                                                                                      const uint                                      l,
                                                                                      const std::function<const Vec2D(const double)>& psi_i_jk,
                                                                                      const std::function<double(const double)>&      dpsi_i_jk_2Norm,
                                                                                      const bool                                      has_singularity,
                                                                                      const double                                    singularity)->double {
                gsl_function_inner.function = [](const double t, void* params)->double {
                    const auto& m_l             = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
                    const auto& psi_i_jk        = *static_cast<const std::function<const Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[1]));
                    const auto& dpsi_i_jk_2Norm = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[2]));

                    const Vec2D& x   = *static_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[3]));
                    const Vec2D  y   = psi_i_jk(t);

                    const Vec2D& tn_vec_jk = *static_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[4]));

                    return GradE(x-y).dot(tn_vec_jk)*m_l(t)*dpsi_i_jk_2Norm(t);
                };
                std::function<double(const double t)> m_l = [l](const double t)->double {
                    return mono(t,l);
                };

                const double** params = new const double*[5];
                params[0]             = reinterpret_cast<const double*>(&m_l);
                params[1]             = reinterpret_cast<const double*>(&psi_i_jk);
                params[2]             = reinterpret_cast<const double*>(&dpsi_i_jk_2Norm);
                params[3]             = reinterpret_cast<const double*>(&x);
                params[4]             = reinterpret_cast<const double*>(&tn_vec_jk);

                gsl_function_inner.params = params;
                double result;
                if (has_singularity) {
                    result  = quad_inner(0.0,std::max(0.0,singularity-1e-2));
                    result += quad_inner(std::min(singularity+1e-2,1.0),1.0);
                } else result = quad_inner(0.0,1.0);
                delete[] params;
                return   result;
            };

            // Create lambdas for the outer integrals
            const auto outerIntegralLHS = [&quad_outer,
                                           &gsl_function_outer](const uint                                                           lp,
                                                                const std::function<const Vec2D(const double)>&                      nt_vec_jk,
                                                                const std::function<Vec2D(const double)>&                            psi_ip_k,
                                                                const std::function<double(const double)>&                           dpsi_ip_k_2Norm,
                                                                const std::function<double(const Vec2D&,const Vec2D&,const double)>& Wop,
                                                                const bool                                                           has_singularity,
                                                                const bool                                                           directed_boundary_segments_have_same_direction)->double {
                gsl_function_outer.function = [](const double t, void* params)->double {
                    const auto& m_lp                 = *static_cast<std::function<double(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[0]));
                    const auto& nt_vec_jk            = *static_cast<std::function<Vec2D(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[1]));
                    const auto& psi_ip_k             = *static_cast<std::function<Vec2D(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[2]));
                    const auto& dpsi_ip_k_2Norm      = *static_cast<std::function<double(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[3]));
                    const auto& Wop                  = *static_cast<std::function<double(const Vec2D&,const Vec2D&,const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[4]));
                    const bool& has_singularity      = *static_cast<bool*>(static_cast<void*>(reinterpret_cast<double**>(params)[5]));
                    double singularity;
                    if (has_singularity) singularity = *static_cast<bool*>(static_cast<void*>(reinterpret_cast<double**>(params)[6])) ? t : 1-t;
                    return m_lp(t)*Wop(nt_vec_jk(t),psi_ip_k(t),singularity)*dpsi_ip_k_2Norm(t);
                };
                std::function<double(const double t)> m_lp = [lp](const double t)->double {
                    return mono(t,lp);
                };
                const double** params          = new const double*[has_singularity ? 7 : 6];
                params[0]                      = reinterpret_cast<const double*>(&m_lp);
                params[1]                      = reinterpret_cast<const double*>(&nt_vec_jk);
                params[2]                      = reinterpret_cast<const double*>(&psi_ip_k);
                params[3]                      = reinterpret_cast<const double*>(&dpsi_ip_k_2Norm);
                params[4]                      = reinterpret_cast<const double*>(&Wop);
                params[5]                      = reinterpret_cast<const double*>(&has_singularity);
                if (has_singularity) params[6] = reinterpret_cast<const double*>(&directed_boundary_segments_have_same_direction);
                gsl_function_outer.params = params;
                const double result       = quad_outer();
                delete[] params;
                return   result;
            };
            const auto outerIntegralRHS = [&quad_outer,
                                           &gsl_function_outer](const uint                                             lp,
                                                                const std::function<Vec2D(const double)>&              psi_ip_k,
                                                                const std::function<double(const double)>&             dpsi_ip_k_2Norm,
                                                                const std::function<double(const double,const Vec2D)>& c_fun)->double {
                gsl_function_outer.function = [](const double t, void* params)->double {
                    const auto& m_lp                  = *static_cast<std::function<double(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[0]));
                    const auto& psi_ip_k              = *static_cast<std::function<Vec2D(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[1]));
                    const auto& dpsi_ip_k_2Norm       = *static_cast<std::function<double(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[2]));
                    const auto& c_fun                 = *static_cast<std::function<double(const double, const Vec2D)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[3]));

                    return m_lp(t)*c_fun(t,psi_ip_k(t))*dpsi_ip_k_2Norm(t);
                };
                std::function<double(const double t)> m_lp = [lp](const double t)->double {
                    return mono(t,lp);
                };
                const double** params     = new const double*[4];
                params[0]                 = reinterpret_cast<const double*>(&m_lp);
                params[1]                 = reinterpret_cast<const double*>(&psi_ip_k);
                params[2]                 = reinterpret_cast<const double*>(&dpsi_ip_k_2Norm);
                params[3]                 = reinterpret_cast<const double*>(&c_fun);
                gsl_function_outer.params = params;
                const double result       = quad_outer();
                delete[] params;
                return   result;
            };

            // Create a lambda to evaluate the fundamental solution on the boundary of a domain
            const auto getFundamentalSolution = [&quad_inner,
                                                 &quad_inner_s,
                                                 &gsl_function_inner,
                                                 gsl_workspace_inner](const double  tx_jk,
                                                                      const Vec2D&  x,
                                                                      const Domain& domain,
                                                                      const uint    directed_boundary_segment_index,
                                                                      const uint    subsegment_index,
                                                                      const Vec2D   vec_nt)->double {
                // Create a lambda defining the integrand of integrals along boundary subsegments
                const auto boundarySubsegmentIntegral = [](const double t, void* params)-> double {
                    const auto& psi_i        = *static_cast<const std::function<Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
                    const auto& dpsi_i_2Norm = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[1]));
                    const auto& nu_          = *static_cast<const std::function<Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[3]));

                    const Vec2D& x           = *static_cast<const Vec2D*>(static_cast<void*>(reinterpret_cast<double**>(params)[2]));
                    const Vec2D  y           = psi_i(t);
                    const Vec2D  nu          = nu_(t);
                    const Vec2D& vec_nt      = *static_cast<const Vec2D*>(static_cast<void*>(reinterpret_cast<double**>(params)[4]));

                    return vec_nt.dot(nu)*E(y-x)*dpsi_i_2Norm(t);
                };

                // Create a lambda to integrate along boundary subsegments
                const auto getBoundarySubsegmentIntegral = [&boundarySubsegmentIntegral,&quad_inner,&quad_inner_s,&x,&vec_nt,
                                                            &gsl_function_inner,gsl_workspace_inner](const std::function<Vec2D(const double)>  psi_i,
                                                                                                     const std::function<double(const double)> dpsi_i_2Norm,
                                                                                                     const std::function<Vec2D(const double)>  nu,
                                                                                                     const double                              a,
                                                                                                     const double                              b,
                                                                                                     const bool                                has_singularity)->double {
                    gsl_function_inner.function = boundarySubsegmentIntegral;
                    const double** params     = new const double*[5];
                    params[0]                 = reinterpret_cast<const double*>(&psi_i);
                    params[1]                 = reinterpret_cast<const double*>(&dpsi_i_2Norm);
                    params[2]                 = reinterpret_cast<const double*>(&x);
                    params[3]                 = reinterpret_cast<const double*>(&nu);
                    params[4]                 = reinterpret_cast<const double*>(&vec_nt);
                    gsl_function_inner.params = params;
                    double result;
                    //if (has_singularity) result = quad_inner_s(a,b); // qags does worse than qag in terms of accuracy of the solution.
                    if (has_singularity) result = quad_inner(a,b);
                    else                 result = quad_inner(a,b);
                    delete[] params;
                    return result;
                };

                // Get the pointers to the directed boundary segments of the domain
                const vector<directed_boundary_segment_ptr>& directed_boundary_segment_ptrs = domain.getDirectedBoundarySegmentPointers();

                //const double epsilon = 0.0;//1.0e-8;
                const double epsilon = 1.0e-8; // TODO: Check this again
                double integral = 0.0;
                for (uint dir_bnd_seg_idx = directed_boundary_segment_index; dir_bnd_seg_idx < directed_boundary_segment_ptrs.size(); ++dir_bnd_seg_idx) {
                    const DirectedBoundarySegment& dir_bnd_seg = *directed_boundary_segment_ptrs[dir_bnd_seg_idx];
                    const uint subseg_str_idx = dir_bnd_seg_idx == directed_boundary_segment_index ? subsegment_index : 0;
                    for (uint subseg_idx = subseg_str_idx; subseg_idx < directed_boundary_segment_ptrs[dir_bnd_seg_idx]->getNumSubsegments(); ++subseg_idx) {
                        if (dir_bnd_seg_idx == directed_boundary_segment_index && subseg_idx == subsegment_index) {
                            integral += getBoundarySubsegmentIntegral(dir_bnd_seg.getpsii(subseg_idx),
                                                                      dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                                      dir_bnd_seg.getnu(subseg_idx),
                                                                      tx_jk+epsilon,
                                                                      1.0,
                                                                      true /* has singularity */);
                        } else {
                            integral += getBoundarySubsegmentIntegral(dir_bnd_seg.getpsii(subseg_idx),
                                                                      dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                                      dir_bnd_seg.getnu(subseg_idx),
                                                                      0.0,
                                                                      1.0,
                                                                      false /* has_singularity */);
                        }
                    }
                }
                for (uint dir_bnd_seg_idx = 0; dir_bnd_seg_idx <= directed_boundary_segment_index; ++dir_bnd_seg_idx) {
                    const DirectedBoundarySegment& dir_bnd_seg = *directed_boundary_segment_ptrs[dir_bnd_seg_idx];
                    const uint subseg_end_idx = dir_bnd_seg_idx == directed_boundary_segment_index ? subsegment_index : directed_boundary_segment_ptrs[dir_bnd_seg_idx]->getNumSubsegments()-1;
                    for (uint subseg_idx = 0; subseg_idx <= subseg_end_idx; ++subseg_idx) {
                        if (dir_bnd_seg_idx == directed_boundary_segment_index && subseg_idx == subsegment_index) {
                            integral += getBoundarySubsegmentIntegral(dir_bnd_seg.getpsii(subseg_idx),
                                                                      dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                                      dir_bnd_seg.getnu(subseg_idx),
                                                                      0.0,
                                                                      tx_jk-epsilon,
                                                                      true /* has_singularity */);
                        } else {
                            integral += getBoundarySubsegmentIntegral(dir_bnd_seg.getpsii(subseg_idx),
                                                                      dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                                      dir_bnd_seg.getnu(subseg_idx),
                                                                      0.0,
                                                                      1.0,
                                                                      false /* has_singularity */);
                        }
                    }
                }

                return integral;
            };

            // Create a lambda which can be used to complete a task
            const auto do_task = [&outerIntegralLHS,&outerIntegralRHS,&WtnPsi,&getFundamentalSolution,
                                  &gsl_function_inner,gsl_workspace_inner,&gsl_function_outer,
                                  &sys_mat_ptr,&rhs_ptr,&domain_system_matrix_column_offsets,&domain_ptrs](const uint dom_idx_tst,
                                                                                                           const uint dir_bnd_seg_ptr_idx_tst,
                                                                                                                 uint sys_mat_row) {
                // Define variables for convenience
                const Domain&                                dom_tst                  = *domain_ptrs[dom_idx_tst];
                const vector<directed_boundary_segment_ptr>& dir_bnd_seg_ptrs_tst     = dom_tst.getDirectedBoundarySegmentPointers();
                const DirectedBoundarySegment&               dir_bnd_seg_tst          = *dir_bnd_seg_ptrs_tst[dir_bnd_seg_ptr_idx_tst];
                const uint                                   dom_tst_sys_mat_col_ofs  = domain_system_matrix_column_offsets[dom_idx_tst];
                const Domain&                                dom_bie_ext              = dir_bnd_seg_tst.getNeighbouringDomain();
                const vector<directed_boundary_segment_ptr>& dir_bnd_seg_ptrs_bie_ext = dom_bie_ext.getDirectedBoundarySegmentPointers();
                const uint                                   dom_ext_sys_mat_col_ofs  = domain_system_matrix_column_offsets[dom_bie_ext.getDomainIndex()];
                const double                                 mu_int                   = dom_tst.getmu();
                const double                                 mu_ext                   = dom_bie_ext.getmu();

                // sub_seg_idx_tst <==> subsegment index (test)
                for (uint sub_seg_idx_tst = 0; sub_seg_idx_tst < dir_bnd_seg_tst.getNumSubsegments(); ++sub_seg_idx_tst) {

                    // mon_deg_tst <==> monomial degree (test)
                    for (uint mon_deg_tst = 0; mon_deg_tst <= (*dir_bnd_seg_tst).getFunctionApproximationPolynomialOrder(); ++mon_deg_tst) {
                        // At this point, \xi is clearly defined

                        /* Compute the LHS */

                        // dir_bnd_seg_ptr_idx_bie_int <==> directed boundary segment pointer index (boundary integral equation, interior domain)
                        uint var_cnt_int = 0;
                        for (uint dir_bnd_seg_ptr_idx_bie_int = 0; dir_bnd_seg_ptr_idx_bie_int < dir_bnd_seg_ptrs_tst.size(); ++dir_bnd_seg_ptr_idx_bie_int) {
                            const DirectedBoundarySegment& dir_bnd_seg_bie_int = *dir_bnd_seg_ptrs_tst[dir_bnd_seg_ptr_idx_bie_int];

                            // sub_seg_idx_bie_int <==> subsegment index (boundary integral equation, interior domain)
                            for (uint sub_seg_idx_bie_int = 0; sub_seg_idx_bie_int < dir_bnd_seg_bie_int.getNumSubsegments(); ++sub_seg_idx_bie_int) {

                                // mon_deg_bie_int <==> monomial degree (boundary integral equation, interior domain)
                                for (uint mon_deg_bie_int = 0; mon_deg_bie_int <= (*dir_bnd_seg_bie_int).getFunctionApproximationPolynomialOrder(); ++mon_deg_bie_int) {
                                    const bool has_singularity=&*dir_bnd_seg_tst==&*dir_bnd_seg_bie_int && sub_seg_idx_tst==sub_seg_idx_bie_int;
                                    const double ct = outerIntegralLHS(mon_deg_tst,
                                                  dir_bnd_seg_tst.gett(sub_seg_idx_tst),
                                                  dir_bnd_seg_tst.getpsii(sub_seg_idx_tst),
                                                  dir_bnd_seg_tst.getdpsii2Norm(sub_seg_idx_tst),
                                                  [mon_deg_bie_int,
                                                   &WtnPsi,
                                                   psi_i_jk=dir_bnd_seg_bie_int.getpsii(sub_seg_idx_bie_int),
                                                   dpsi_i_jk_2Norm=dir_bnd_seg_bie_int.getdpsii2Norm(sub_seg_idx_bie_int),
                                                   has_singularity,
                                                   &gsl_function_inner,
                                                   gsl_workspace_inner](const Vec2D& t_vec_k, const Vec2D& x, const double singularity)->double {
                                                      return WtnPsi(t_vec_k,
                                                                    x,
                                                                    mon_deg_bie_int,
                                                                    psi_i_jk,
                                                                    dpsi_i_jk_2Norm,
                                                                    has_singularity,
                                                                    singularity);
                                                  },
                                                  has_singularity,
                                                  dir_bnd_seg_tst.isForward()==dir_bnd_seg_bie_int.isForward());
                                    const double cn = outerIntegralLHS(mon_deg_tst,
                                                                       dir_bnd_seg_tst.getnu(sub_seg_idx_tst),
                                                                       dir_bnd_seg_tst.getpsii(sub_seg_idx_tst),
                                                                       dir_bnd_seg_tst.getdpsii2Norm(sub_seg_idx_tst),
                                                                       [mon_deg_bie_int,
                                                                        &WtnPsi,
                                                                        psi_i_jk=dir_bnd_seg_bie_int.getpsii(sub_seg_idx_bie_int),
                                                                        dpsi_i_jk_2Norm=dir_bnd_seg_bie_int.getdpsii2Norm(sub_seg_idx_bie_int),
                                                                        has_singularity,
                                                                        &gsl_function_inner,
                                                                        gsl_workspace_inner](const Vec2D& n_vec_k, const Vec2D& x, const double singularity)->double {
                                                                            double result = WtnPsi(n_vec_k,
                                                                                                   x,
                                                                                                   mon_deg_bie_int,
                                                                                                   psi_i_jk,
                                                                                                   dpsi_i_jk_2Norm,
                                                                                                   has_singularity,
                                                                                                   singularity);
                                                                           if (has_singularity) result -= 0.5*mono(singularity,mon_deg_bie_int);
                                                                           return result;
                                                                       },
                                                                       has_singularity,
                                                                       dir_bnd_seg_tst.isForward()==dir_bnd_seg_bie_int.isForward());
                                    //(*sys_mat_ptr)(sys_mat_row,   dom_tst_sys_mat_col_ofs+var_cnt_int)   =  ct/mu0;
                                    (*sys_mat_ptr)(sys_mat_row,   dom_tst_sys_mat_col_ofs+var_cnt_int+1) = -cn/mu0;
                                    (*sys_mat_ptr)(sys_mat_row,   dom_tst_sys_mat_col_ofs+var_cnt_int+2) = -cn/mu0;
                                    (*sys_mat_ptr)(sys_mat_row,   dom_tst_sys_mat_col_ofs+var_cnt_int+3) = -ct/mu0;
                                    //(*sys_mat_ptr)(sys_mat_row+1, dom_tst_sys_mat_col_ofs+var_cnt_int)   = -cn/mu_int;
                                    (*sys_mat_ptr)(sys_mat_row+1, dom_tst_sys_mat_col_ofs+var_cnt_int+1) = -ct/mu_int;
                                    (*sys_mat_ptr)(sys_mat_row+1, dom_tst_sys_mat_col_ofs+var_cnt_int+2) = -ct/mu_int;
                                    (*sys_mat_ptr)(sys_mat_row+1, dom_tst_sys_mat_col_ofs+var_cnt_int+3) = +cn/mu_int;
                                    var_cnt_int += 4;
                                }
                            }
                        }

                        // dir_bnd_seg_ptr_idx_bie_ext <==> directed boundary segment pointer index (boundary integral equation, exterior domain)
                        uint var_cnt_ext = 0;
                        for (uint dir_bnd_seg_ptr_idx_bie_ext = 0; dir_bnd_seg_ptr_idx_bie_ext < dir_bnd_seg_ptrs_bie_ext.size(); ++dir_bnd_seg_ptr_idx_bie_ext) {
                            const DirectedBoundarySegment& dir_bnd_seg_bie_ext = *dir_bnd_seg_ptrs_bie_ext[dir_bnd_seg_ptr_idx_bie_ext];

                            // sub_seg_idx_bie_ext <==> subsegment index (boundary integral equation, exterior domain)
                            for (uint sub_seg_idx_bie_ext = 0; sub_seg_idx_bie_ext < dir_bnd_seg_bie_ext.getNumSubsegments(); ++sub_seg_idx_bie_ext) {

                                // mon_deg_bie_ext <==> monomial degree (boundary integral equation, exterior domain)
                                for (uint mon_deg_bie_ext = 0; mon_deg_bie_ext <= (*dir_bnd_seg_bie_ext).getFunctionApproximationPolynomialOrder(); ++mon_deg_bie_ext) {
                                    const bool has_singularity = &*dir_bnd_seg_tst==&*dir_bnd_seg_bie_ext &&
                                                       dir_bnd_seg_tst.getTrueSubsegmentIndex(sub_seg_idx_tst)==dir_bnd_seg_bie_ext.getTrueSubsegmentIndex(sub_seg_idx_bie_ext);
                                    const double ct = outerIntegralLHS(mon_deg_tst,
                                                                       [gett=dir_bnd_seg_tst.gett(sub_seg_idx_tst)](const double t)->const Vec2D { return -gett(t); },
                                                                        dir_bnd_seg_tst.getpsii(sub_seg_idx_tst),
                                                                        dir_bnd_seg_tst.getdpsii2Norm(sub_seg_idx_tst),
                                                                       [mon_deg_bie_ext,
                                                                        &WtnPsi,
                                                                        psi_i_jk=dir_bnd_seg_bie_ext.getpsii(sub_seg_idx_bie_ext),
                                                                        dpsi_i_jk_2Norm=dir_bnd_seg_bie_ext.getdpsii2Norm(sub_seg_idx_bie_ext),
                                                                        has_singularity,
                                                                        &gsl_function_inner,
                                                                        gsl_workspace_inner](const Vec2D& t_vec_j, const Vec2D& x, const double singularity)->double {
                                                                           return WtnPsi(t_vec_j,
                                                                                         x,
                                                                                         mon_deg_bie_ext,
                                                                                         psi_i_jk,
                                                                                         dpsi_i_jk_2Norm,
                                                                                         has_singularity,
                                                                                         singularity);
                                                                       },
                                                                       has_singularity,
                                                                       dir_bnd_seg_tst.isForward()==dir_bnd_seg_bie_ext.isForward());
                                    const double cn = outerIntegralLHS(mon_deg_tst,
                                                                       [getnu=dir_bnd_seg_tst.getnu(sub_seg_idx_tst)](const double t)->const Vec2D { return -getnu(t); },
                                                                        dir_bnd_seg_tst.getpsii(sub_seg_idx_tst),
                                                                        dir_bnd_seg_tst.getdpsii2Norm(sub_seg_idx_tst),
                                                                       [mon_deg_bie_ext,
                                                                        &WtnPsi,
                                                                        psi_i_jk=dir_bnd_seg_bie_ext.getpsii(sub_seg_idx_bie_ext),
                                                                        dpsi_i_jk_2Norm=dir_bnd_seg_bie_ext.getdpsii2Norm(sub_seg_idx_bie_ext),
                                                                        has_singularity,
                                                                        &gsl_function_inner,
                                                                        gsl_workspace_inner](const Vec2D& n_vec_k, const Vec2D& x, const double singularity)->double {
                                                                           double result = WtnPsi(n_vec_k,
                                                                                                  x,
                                                                                                  mon_deg_bie_ext,
                                                                                                  psi_i_jk,
                                                                                                  dpsi_i_jk_2Norm,
                                                                                                  has_singularity,
                                                                                                  singularity);
                                                                           if (has_singularity) result -= 0.5*mono(singularity,mon_deg_bie_ext);
                                                                           return result;
                                                                       },
                                                                       has_singularity,
                                                                       dir_bnd_seg_tst.isForward()==dir_bnd_seg_bie_ext.isForward());
                                    //(*sys_mat_ptr)(sys_mat_row,   dom_ext_sys_mat_col_ofs+var_cnt_ext)   = +ct/mu0;
                                    (*sys_mat_ptr)(sys_mat_row,   dom_ext_sys_mat_col_ofs+var_cnt_ext+1) = -cn/mu0;
                                    (*sys_mat_ptr)(sys_mat_row,   dom_ext_sys_mat_col_ofs+var_cnt_ext+2) = -cn/mu0;
                                    (*sys_mat_ptr)(sys_mat_row,   dom_ext_sys_mat_col_ofs+var_cnt_ext+3) = -ct/mu0;
                                    //(*sys_mat_ptr)(sys_mat_row+1, dom_ext_sys_mat_col_ofs+var_cnt_ext)   = -cn/mu_ext;
                                    (*sys_mat_ptr)(sys_mat_row+1, dom_ext_sys_mat_col_ofs+var_cnt_ext+1) = -ct/mu_ext;
                                    (*sys_mat_ptr)(sys_mat_row+1, dom_ext_sys_mat_col_ofs+var_cnt_ext+2) = -ct/mu_ext;
                                    (*sys_mat_ptr)(sys_mat_row+1, dom_ext_sys_mat_col_ofs+var_cnt_ext+3) = +cn/mu_ext;
                                    var_cnt_ext += 4;
                                }
                            }
                        }

                        /* Compute the RHS */
                        // c_{1,k}
                        (*rhs_ptr)(sys_mat_row)  = dom_tst.getJ() == 0.0 ? 0.0 :
                                                   outerIntegralRHS(mon_deg_tst,
                                                                    dir_bnd_seg_tst.getpsii(sub_seg_idx_tst),
                                                                    dir_bnd_seg_tst.getdpsii2Norm(sub_seg_idx_tst),
                                                                    [&getFundamentalSolution,
                                                                     &dom_tst,
                                                                     gett=dir_bnd_seg_tst.gett(sub_seg_idx_tst),
                                                                     dir_bnd_seg_ptr_idx_tst,
                                                                     sub_seg_idx_tst,
                                                                     &gsl_function_inner,
                                                                     gsl_workspace_inner](const double t, const Vec2D x)->double {
                                                                        const Vec2D vec_nt = gett(t);
                                                                        return getFundamentalSolution(t,
                                                                                                      x,
                                                                                                      dom_tst,
                                                                                                      dir_bnd_seg_ptr_idx_tst,
                                                                                                      sub_seg_idx_tst,
                                                                                                      vec_nt)*dom_tst.getmu()*dom_tst.getJ();
                                                                    })/mu0;
                        (*rhs_ptr)(sys_mat_row) -= dom_bie_ext.getJ() == 0.0 ? 0.0 :
                                                   outerIntegralRHS(mon_deg_tst,
                                                                    dir_bnd_seg_tst.getpsii(sub_seg_idx_tst),
                                                                    dir_bnd_seg_tst.getdpsii2Norm(sub_seg_idx_tst),
                                                                    [&getFundamentalSolution,
                                                                     &dom_bie_ext,
                                                                     &dir_bnd_seg_tst,
                                                                     gett=dir_bnd_seg_tst.gett(sub_seg_idx_tst),
                                                                     sub_seg_idx_tst,
                                                                     &gsl_function_inner,
                                                                     gsl_workspace_inner](const double t, const Vec2D x)->double {
                                                                        const Vec2D vec_nt = gett(t);
                                                                        return getFundamentalSolution(dir_bnd_seg_tst.isForward()==dir_bnd_seg_tst.getNeighboursDirectedBoundarySegment().isForward() ?
                                                                                                      t : 1-t,
                                                                                                      x,
                                                                                                      dom_bie_ext,
                                                                                                      dir_bnd_seg_tst.getNeighboursDirectedBoundarySegmentIndex(),
                                                                                                      dir_bnd_seg_tst.isForward()==dir_bnd_seg_tst.getNeighboursDirectedBoundarySegment().isForward() ?
                                                                                                      sub_seg_idx_tst : dir_bnd_seg_tst.getNumSubsegments()-1-sub_seg_idx_tst,
                                                                                                      vec_nt)*dom_bie_ext.getmu()*dom_bie_ext.getJ();
                                                                    })/mu0;
                        // c_{2,k}
                        (*rhs_ptr)(sys_mat_row+1) = outerIntegralRHS(mon_deg_tst,
                                                                     dir_bnd_seg_tst.getpsii(sub_seg_idx_tst),
                                                                     dir_bnd_seg_tst.getdpsii2Norm(sub_seg_idx_tst),
                                                                     [&getFundamentalSolution,
                                                                      &dom_bie_ext,
                                                                      &dir_bnd_seg_tst,
                                                                      getnu=dir_bnd_seg_tst.getnu(sub_seg_idx_tst),
                                                                      sub_seg_idx_tst,
                                                                      &gsl_function_inner,
                                                                      gsl_workspace_inner,
                                                                      surface_current_density=(*dir_bnd_seg_tst).getSurfaceCurrentDensity()](const double t, const Vec2D x)->double {
                                                                          const Vec2D vec_nt = getnu(t);
                                                                          double fs;
                                                                          if (dom_bie_ext.getJ() == 0.0) {
                                                                              fs = 0.0;
                                                                          } else {
                                                                              fs = getFundamentalSolution(dir_bnd_seg_tst.isForward()==
                                                                                                              dir_bnd_seg_tst.getNeighboursDirectedBoundarySegment().isForward() ? t : 1-t,
                                                                                                          x,
                                                                                                          dom_bie_ext,
                                                                                                          dir_bnd_seg_tst.getNeighboursDirectedBoundarySegmentIndex(),
                                                                                                          dir_bnd_seg_tst.isForward()==
                                                                                                              dir_bnd_seg_tst.getNeighboursDirectedBoundarySegment().isForward() ?
                                                                                                              sub_seg_idx_tst : dir_bnd_seg_tst.getNumSubsegments()-1-sub_seg_idx_tst,
                                                                                                          vec_nt)*dom_bie_ext.getJ();
                                                                          }
                                                                          return fs + surface_current_density;
                                                                     });
                        (*rhs_ptr)(sys_mat_row+1) -= dom_tst.getJ() == 0.0 ? 0.0 :
                                                     outerIntegralRHS(mon_deg_tst,
                                                                      dir_bnd_seg_tst.getpsii(sub_seg_idx_tst),
                                                                      dir_bnd_seg_tst.getdpsii2Norm(sub_seg_idx_tst),
                                                                      [&getFundamentalSolution,
                                                                       &dom_tst,
                                                                       getnu=dir_bnd_seg_tst.getnu(sub_seg_idx_tst),
                                                                       dir_bnd_seg_ptr_idx_tst,
                                                                       sub_seg_idx_tst,
                                                                       &gsl_function_inner,
                                                                       gsl_workspace_inner](const double t, const Vec2D x)->double {
                                                                          const Vec2D vec_nt = getnu(t);
                                                                          return getFundamentalSolution(t,
                                                                                                        x,
                                                                                                        dom_tst,
                                                                                                        dir_bnd_seg_ptr_idx_tst,
                                                                                                        sub_seg_idx_tst,
                                                                                                        vec_nt)*dom_tst.getJ();
                                                                      });
                        sys_mat_row += 2;
                    }
                }
            };

            // Actually complete the tasks, i.e. compute the LHS and RHS of the linear system
            int current_completion_percentage = -1;
            while (remaining_tasks.load() > 0) {
                if (task_mutex.try_lock()) {
                    // Get the current task and release the mutex
                    const auto task = tasks.back();
                    tasks.pop_back();
                    remaining_tasks.store(remaining_tasks.load()-1);
                    task_mutex.unlock();

                    // Complete the task
                    do_task(std::get<0>(task),std::get<1>(task),std::get<2>(task));

                    // Visualize the progress
                    if (is_main_thread) {
                        const int completion_percentage = (100.0*(tasks.size()-remaining_tasks.load()))/tasks.size();
                        if (completion_percentage > current_completion_percentage) {
                            std::cout << "\33[2K\rFormulating the linear system: " << completion_percentage << "%" << std::flush;
                            current_completion_percentage = completion_percentage;
                        }
                    }
                }
            }

            // Free the memory of the GSL workspaces and the quadrature weights
            gsl_integration_workspace_free(gsl_workspace_inner);
            #ifdef OUTER_INTEGRAL_ADAPTIV
            gsl_integration_workspace_free(gsl_workspace_outer);
            #else
            gsl_integration_glfixed_table_free(quadrature_table);
            #endif
        };
    };

    // Determine of the number of concurrent threads that are supported
    uint max_num_threads = std::thread::hardware_concurrency();
    if (max_num_threads == 0) max_num_threads = 1;
    // TODO: Fix this
    //max_num_threads = 1;

    // Measure the time it takes to compute the matrix entries
    auto start_time = std::chrono::steady_clock::now();

    // Create worker threads
    vector<std::thread> worker_threads;
    for (uint thread_idx = 0; thread_idx < max_num_threads; ++thread_idx) {
        worker_threads.emplace_back(get_worker_handle(thread_idx==0 /* is_main_thread */));
    }

    // Wait until all workers threads completed execution
    for (uint thread_idx = 0; thread_idx < max_num_threads; ++thread_idx) {
        worker_threads[thread_idx].join();
    }

    /* Allocate memory for E */

    // First, determine the dimensions of \hat{x}_i
    uint E_col_cnt = 0;
    vector<uint> hatxi_dims;
    vector<uint> xi_dims;
    for (const c_domain_ptr& dom_ptr : domain_ptrs_) {
        // Reference domains don't have associated problem variables
        if (dom_ptr->isReference()) continue;

        // Define convenience and clarity
        const auto dir_bnd_seg_ptrs = dom_ptr->getDirectedBoundarySegmentPointers();

        // Count the number of unknowns, equations and linear constraints
        uint unknowns           = 0;
        uint linear_constraints = 0;
        for (uint dir_bnd_seg_idx = 0; dir_bnd_seg_idx < dir_bnd_seg_ptrs.size(); ++dir_bnd_seg_idx) {
            // Define convenience and clarity
            const directed_boundary_segment_ptr& dbsp = dir_bnd_seg_ptrs[dir_bnd_seg_idx];

            for (uint subseg_idx = 0; subseg_idx < dbsp->getNumSubsegments(); ++subseg_idx) {
                // Define convenience and clarity
                const uint poly_order = (**dbsp).getFunctionApproximationPolynomialOrder();

                if (poly_order == 1) {
                    unknowns += 2*(poly_order+1);
                } else {
                    std::cout << "This is not implemented yet." << std::endl;
                    abort();
                }
            }

            // 2*dbsp->getNumSubsegments() many constraints come from the requirement of the tangential and the normal derivative being continuous
            linear_constraints += 2*dbsp->getNumSubsegments();
        }

        // Two constraints come from the conditions that the closed boundary integrals of the tangential and the normal derivative are zero
        linear_constraints += 2;

        // Add columns for this domain
        hatxi_dims.push_back((unknowns - linear_constraints)/2+1);
        xi_dims.push_back(unknowns/2);
        //E_col_cnt += 4*hatxi_dims.back()-2;
        //E_col_cnt += 3*hatxi_dims.back()-1;
        E_col_cnt += 3*hatxi_dims.back();
    }

    // Actually allocate memory for Er
    MatrixXd E = MatrixXd::Zero(sys_mat_ptr->cols(),E_col_cnt);

    /* Compute E */
    /* 
     * x     = Er*\hat{x}    (1)
     * x'    = Ei*x          (2)
     * => x' = Ei*Er*\hat{x} (3)
     * \hat{x} = [x']       
     *           [x_f]       (4)
     *
     * => x' (2)= Ei*x
     *       (1)= Ei*Er*\hat{x}
     *       (4)= Ei*Er*[x']
     *                  [x_f]
     *          = Ei*Er*[0]     + Ei*Er*[1]
     *                  [1]*x_f         [0]*x'
     * => [1-Ei*Er*[1]]*x' = Ei*Er*[0]
     *             [0]             [1]*x_f
     * => x' = [1-Ei*Er*[1]]^{-1}*Ei*Er*[0]
     *                  [0]             [1]*x_f
     * => x' = Et*x_f with Et = [1-Ei*Er*[1]]^{-1}*Ei*Er*[0]
     *                                   [0]             [1] (5)
     *
     * => \hat{x} (4)= [x']
     *                 [x_f]
     *            (5)= [Et*x_f]
     *                 [x_f]
     *               = [Et]
     *                 [ 1]*x_f (6)
     *
     * => x (1)= Er*\hat{x}
     *      (6)= Er*[Et]
     *              [ 1]*x_f
     */

    // Prepare GSL variables and allocate memory
    gsl_function               gsl_fun;
    gsl_integration_workspace* gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
    const double** params                    = new const double*[1];
    gsl_fun.params                           = params;

    // Create a wrapper to simplify integration
    const auto quad = [&gsl_fun,gsl_workspace]()->double {
        double result,error;
        gsl_integration_qag(&gsl_fun, /* Function to integrate */
                            0.0,      /* Lower integration limit */
                            1.0,      /* Upper integration limit */
                            INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                            INTEGRAL_RELATIVE_ERROR_LIMIT,
                            MAX_NUM_SUBINTERVALS,
                            INTEGRATION_KEY,
                            gsl_workspace,&result,&error);
        return result;
    };

    // Actually compute E
    uint col_ofs;
    uint row_offset = 0;
    uint col_offset = 0;
    for (uint domain_index = 0; domain_index < domain_ptrs_.size(); ++domain_index) {
        // Define variables for convenience and clarity
        const c_domain_ptr& dom_ptr             = domain_ptrs_[domain_index];
        const uint          dom_sys_mat_col_ofs = domain_system_matrix_column_offsets_[dom_ptr->getDomainIndex()];
        const auto          dir_bnd_seg_ptrs    = dom_ptr->getDirectedBoundarySegmentPointers();

        // Skip reference domains
        if (dom_ptr->isReference()) continue;

        /* Compute Er */
        // Allocate memory for Er
        MatrixXd Er = MatrixXd::Zero(xi_dims[domain_index],hatxi_dims[domain_index]);

        // Compute the entries of Er
        uint row     = 0;
        uint var_cnt = 0;
        for (uint dir_bnd_seg_idx = 0; dir_bnd_seg_idx < dir_bnd_seg_ptrs.size(); ++dir_bnd_seg_idx) {
            // Define convenience and clarity
            const directed_boundary_segment_ptr& dbsp = dir_bnd_seg_ptrs[dir_bnd_seg_idx];

            for (uint subseg_idx = 0; subseg_idx < dbsp->getNumSubsegments(); ++subseg_idx) {
                for (int mon_deg = 0; mon_deg <= (**dbsp).getFunctionApproximationPolynomialOrder(); ++mon_deg) {
                    if (dir_bnd_seg_idx == 0 && subseg_idx == 0 && mon_deg == 0) {
                        Er(row++,var_cnt++) = 1.0;
                    } else if (dir_bnd_seg_idx == dir_bnd_seg_ptrs.size()-1 && subseg_idx == dbsp->getNumSubsegments()-1 && mon_deg > 0) {
                        if (mon_deg == 1) {
                            uint var_cnt_i = 0;
                            for (int dir_bnd_seg_idx_i = 0; dir_bnd_seg_idx_i <= dir_bnd_seg_idx; ++dir_bnd_seg_idx_i) {
                                const directed_boundary_segment_ptr& dbsp_i     = dir_bnd_seg_ptrs[dir_bnd_seg_idx_i];
                                const int                            max_subseg = dir_bnd_seg_idx==dir_bnd_seg_idx_i ? subseg_idx-1 : dbsp_i->getNumSubsegments()-1;

                                for (int subseg_idx_i = 0; subseg_idx_i <= max_subseg; ++subseg_idx_i) {
                                    for (uint mon_deg_i = 0; mon_deg_i <= (**dbsp_i).getFunctionApproximationPolynomialOrder(); ++mon_deg_i) {
                                        if (dir_bnd_seg_idx_i == 0 && subseg_idx_i == 0 && mon_deg_i == 0) {
                                            ++var_cnt_i;
                                        } else if (mon_deg_i > 0) {
                                            Er(row,var_cnt_i++) = -1.0;
                                        }
                                    }
                                }
                            }
                            ++row;
                        } else {
                            std::cout << "This is not implemented yet." << std::endl;
                            abort();
                        }
                    } else {
                        if (mon_deg == 0) {
                            uint var_cnt_i = 0;
                            for (int dir_bnd_seg_idx_i = 0; dir_bnd_seg_idx_i <= dir_bnd_seg_idx; ++dir_bnd_seg_idx_i) {
                                const directed_boundary_segment_ptr& dbsp_i     = dir_bnd_seg_ptrs[dir_bnd_seg_idx_i];
                                const int                            max_subseg = dir_bnd_seg_idx==dir_bnd_seg_idx_i ? subseg_idx-1 : dbsp_i->getNumSubsegments()-1;

                                for (int subseg_idx_i = 0; subseg_idx_i <= max_subseg; ++subseg_idx_i) {
                                    for (uint mon_deg_i = 0; mon_deg_i <= (**dbsp_i).getFunctionApproximationPolynomialOrder(); ++mon_deg_i) {
                                        if (dir_bnd_seg_idx_i == 0 && subseg_idx_i == 0 && mon_deg_i == 0) {
                                            Er(row,0) = 1.0;
                                            ++var_cnt_i;
                                        } else if (mon_deg_i > 0) {
                                            Er(row,var_cnt_i++) = 1.0;
                                        }
                                    }
                                }
                            }
                            ++row;
                        } else if (mon_deg == 1) {
                            Er(row++,var_cnt++) = 1.0;
                        } else {
                            std::cout << "This is not implemented yet." << std::endl;
                            abort();
                        }
                    }
                }
            }
        }

        /* Compute Ei */
        // Allocate memory for Ei
        MatrixXd Ei = MatrixXd::Zero(1,xi_dims[domain_index]);

        // Compute the entries of Ei
        uint col = 0;
        double self_value;
        for (int dir_bnd_seg_idx = 0; dir_bnd_seg_idx < dir_bnd_seg_ptrs.size(); ++dir_bnd_seg_idx) {
            // Define convenience and clarity
            const directed_boundary_segment_ptr& dbsp = dir_bnd_seg_ptrs[dir_bnd_seg_idx];

            for (int subseg_idx = 0; subseg_idx < dbsp->getNumSubsegments(); ++subseg_idx) {
                for (int mon_deg = 0; mon_deg <= (**dbsp).getFunctionApproximationPolynomialOrder(); ++mon_deg) {
                    // Define the integrand
                    const auto ml = [mon_deg](const double t)->double {
                        return mono(t,mon_deg);
                    };
                    const auto integrand = [dpsi_i_2Norm=dbsp->getdpsii2Norm(subseg_idx),&ml](const double t)->double {
                        return dpsi_i_2Norm(t)*ml(t);
                    };
                    params[0] = reinterpret_cast<const double*>(&integrand);

                    // Define the function to integrate
                    gsl_fun.function = [](const double t, void *params)->double {
                        const auto& integrand_ = *static_cast<const decltype(integrand)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0])); 

                        return integrand_(t);
                    }; 

                    if (dir_bnd_seg_idx == 0 && subseg_idx == 0 && mon_deg == 0) {
                        // This is x'
                        self_value = quad();
                        ++col;
                    } else {
                        Ei(0,col++) = -quad();
                    }
                }
            }
        }
        Ei = Ei.eval()/self_value;

        /* Compute Et */
        MatrixXd Et;
        {
            MatrixXd zero1 = MatrixXd::Zero(hatxi_dims[domain_index],hatxi_dims[domain_index]-1);
            for (uint i = 0; i < hatxi_dims[domain_index]-1; ++i) zero1(i+1,i) = 1.0;
            MatrixXd zero2 = MatrixXd::Zero(hatxi_dims[domain_index],1);
            zero2(0,0)     = 1.0;
            const double c = 1.0 / (1.0 - (Ei*Er*zero2).eval()(0,0));
            Et             = (c*Ei*Er*zero1).eval();
        };

        // Define Ei
        MatrixXd E_i;
        {
            MatrixXd c = MatrixXd::Zero(hatxi_dims[domain_index],hatxi_dims[domain_index]-1);
            c.block(0,0,1,hatxi_dims[domain_index]-1) = Et.eval();
            for (uint i = 0; i < hatxi_dims[domain_index]-1; ++i) c(i+1,i) = 1.0;
            E_i = (Er*c).eval();
        };

        // Fill in E
        //for (uint row = 0; row < Er.rows(); ++row) {
        //    for (uint col = 0; col < E_i.cols(); ++col)
        //        E(row_offset+4*row,  col_offset+col)                        = E_i(row,col);
        //    for (uint col = 0; col < Er.cols(); ++col)
        //        E(row_offset+4*row+1,col_offset+E_i.cols()+col)             = Er(row,col);
        //    for (uint col = 0; col < E_i.cols(); ++col)
        //        E(row_offset+4*row+2,col_offset+E_i.cols()+Er.cols()+col)   = E_i(row,col);
        //    for (uint col = 0; col < Er.cols(); ++col)
        //        E(row_offset+4*row+3,col_offset+2*E_i.cols()+Er.cols()+col) = Er(row,col);
        //}


        for (uint row = 0; row < Er.rows(); ++row) {
            //for (uint col = 0; col < E_i.cols(); ++col)
            //    E(row_offset+4*row,  col_offset+col)                        = E_i(row,col);
            //for (uint col = 0; col < Er.cols(); ++col)
            //    E(row_offset+4*row+1,col_offset+col)             = Er(row,col);
            for (uint col = 0; col < Er.cols(); ++col)
                E(row_offset+4*row+2,col_offset+Er.cols()+col)   = Er(row,col);
            for (uint col = 0; col < Er.cols(); ++col)
                E(row_offset+4*row+3,col_offset+2*Er.cols()+col) = Er(row,col);
        }



        std::cout << Er.cols() << std::endl;

        //for (uint row = 0; row < Er.rows(); ++row) {
        //    for (uint col = 0; col < Er.cols(); ++col) {
        //        for (uint i = 0; i < 4; ++i) {
        //            E(row_offset+4*row+i,col_offset+4*col+i) = Er(row,col);
        //        }
        //    }
        //}

        row_offset += 4*Er.rows();
        //col_offset += E_i.cols()+Er.cols();
        //col_offset += 2*Er.cols()+2*E_i.cols();
        //col_offset += 2*Er.cols()+E_i.cols();
        col_offset += 3*Er.cols();
    }

    // Free allocated memory
    gsl_integration_workspace_free(gsl_workspace);
    delete[] params;

    // Print the time it took to compute the matrix entries
    std::cout << "\33[2K\rFormulating the linear system: 100%" << std::endl;
    std::cout << "Computing the matrix entries took " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-start_time).count()/1000.0 << "s." << std::endl;

    // Measure the time it takes to solve the linear system
    start_time = std::chrono::steady_clock::now();

    // Solve the system of linear equations


    Eigen::JacobiSVD<Eigen::MatrixXd> svd_sol_((*sys_mat_ptr)*E, Eigen::ComputeThinU | Eigen::ComputeThinV);
std::cout << "xxxxxxxxxxxx" << std::endl << sys_mat_ptr->rows() << " " << sys_mat_ptr->cols() << std::endl << svd_sol_.singularValues() << std::endl;




    Eigen::JacobiSVD<Eigen::MatrixXd> svd_sol((*sys_mat_ptr)*E, Eigen::ComputeThinU | Eigen::ComputeThinV);
    //Eigen::JacobiSVD<Eigen::MatrixXd> svd_sol(*sys_mat_ptr, Eigen::ComputeThinU | Eigen::ComputeThinV);
    sol_  = std::make_unique<VectorXd>(rhs_ptr->rows());
    *sol_ = E*svd_sol.solve(*rhs_ptr);
    //*sol_ = E*((*sys_mat_ptr*E).fullPivLu().solve(*rhs_ptr));

    // Debug
    std::cout << std::setprecision(4);
    std::cout << "E:" << std::endl << E << std::endl;
    std::cout << "sol:" << std::endl << *sol_ << std::endl;
    std::cout << "**************" << std::endl;
    std::cout << (*sys_mat_ptr)*(*sol_)-(*rhs_ptr) << std::endl;
    std::cout << std::setprecision(15);


std::cout << "Its singular values are:" << std::endl << svd_sol.singularValues() << std::endl;
//std::cout << "Its left singular vectors are the columns of the thin U matrix:" <<  std::endl << svd_sol.matrixU() << std::endl;
//std::cout << "Its right singular vectors are the columns of the thin V matrix:" << std::endl << svd_sol.matrixV() << std::endl;

//abort();



    /* For debugging purposes
//std::cout << std::setprecision(4);
    std::cout << std::setprecision(4);
    const Eigen::VectorXcd eigenvalues_ = sys_mat_ptr->eigenvalues();
    std::cout << "Eigenvalues:" << std::endl << eigenvalues_ << std::endl;
    std::cout << "E:" << std::endl << E << std::endl;
    *sol_ = svd_sol.solve(*rhs_ptr);
    */




    //*sol_ = sys_mat_ptr->colPivHouseholderQr().solve(*rhs_ptr);             // -17.6309028917237
    //*sol_ = sys_mat_ptr->fullPivHouseholderQr().solve(*rhs_ptr);            // 97.4246433630355
    //*sol_ = sys_mat_ptr->completeOrthogonalDecomposition().solve(*rhs_ptr); // 105.895416982274
    //*sol_ = sys_mat_ptr->bdcSvd(Eigen::ComputeThinU|Eigen::ComputeThinV).solve(*rhs_ptr); // Results in much worse accuracy

    // Print the time it took to solve the linear system
    std::cout << "Solving the linear system took " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-start_time).count()/1000.0 << "s." << std::endl;

    // Possibly store the solution in a file
    if (store_solution_flag) store_solution(folder_name);

    // Compute the condition number of the system
    //Eigen::BDCSVD<Eigen::MatrixXd> svd(*sys_mat_ptr, Eigen::ComputeThinU | Eigen::ComputeThinV); // Has bad accuracy
    //Eigen::JacobiSVD<Eigen::MatrixXd> svd(*sys_mat_ptr, Eigen::ComputeThinU | Eigen::ComputeThinV);
    //const VectorXd singular_values = svd.singularValues();
    //std::cout << "The singular values of the system are:" << std::endl << "--->" << std::endl << singular_values << std::endl << "<---" << std::endl;

    //Eigen::JacobiSVD<Eigen::MatrixXd> svd2((*sys_mat_ptr)*E, Eigen::ComputeThinU | Eigen::ComputeThinV);
    //const VectorXd singular_values2 = svd2.singularValues();
    //std::cout << "The singular values of the system are:" << std::endl << "--->" << std::endl << singular_values2 << std::endl << "<---" << std::endl;
    //MatrixXd c_ = MatrixXd::Zero(E.cols(),sys_mat_ptr->rows());
    //VectorXd d_ = VectorXd(E.cols());
    //{
    //uint row   = 0;
    //uint row_c = 0;
    //uint choice = 0;
    //for (uint domain_index = 0; domain_index < domain_ptrs_.size(); ++domain_index) {
    //    // Define variables for convenience and clarity
    //    const c_domain_ptr& dom_ptr             = domain_ptrs_[domain_index];
    //    const uint          dom_sys_mat_col_ofs = domain_system_matrix_column_offsets_[dom_ptr->getDomainIndex()];
    //    const auto          dir_bnd_seg_ptrs    = dom_ptr->getDirectedBoundarySegmentPointers();
 
    //    // Skip reference domains
    //    if (dom_ptr->isReference()) continue;

    //    for (uint dir_bnd_seg_idx = 0; dir_bnd_seg_idx < dir_bnd_seg_ptrs.size(); ++dir_bnd_seg_idx) {
    //        // Define convenience and clarity
    //        const directed_boundary_segment_ptr& dbsp = dir_bnd_seg_ptrs[dir_bnd_seg_idx];

    //        for (uint subseg_idx = 0; subseg_idx < dbsp->getNumSubsegments()-1; ++subseg_idx) {
    //            for (uint col = 0; col < sys_mat_ptr->cols(); ++col)
    //                c_(row_c,col)   = (*sys_mat_ptr)(row+choice,col);
    //            d_(row_c) = (*rhs_ptr)(row+choice);
    //            for (uint col = 0; col < sys_mat_ptr->cols(); ++col)
    //                c_(row_c+1,col) = (*sys_mat_ptr)(row+((choice+1)%4),col);
    //            d_(row_c+1) = (*rhs_ptr)(row+((choice+1)%4));
    //            choice = choice==3 ? 0 : choice+1;
    //            //choice = choice==3 ? 0 : choice+1;
    //            row_c += 2;

    //            for (int mon_deg = 0; mon_deg <= (**dbsp).getFunctionApproximationPolynomialOrder(); ++mon_deg) {
    //                //for (uint col = 0; col < sys_mat_ptr->cols(); ++col) {
    //                //    //if (domain_index == 0 && mon_deg == 0) {
    //                //    //if (mon_deg == 0) {
    //                //    //    //c_(row_c,col) = (*sys_mat_ptr)(row,col);
    //                //    //} else {
    //                //    //    c_(row_c,col)   = (*sys_mat_ptr)(row,col);
    //                //    //    c_(row_c+1,col) = (*sys_mat_ptr)(row+1,col);
    //                //    //}
    //                //    if (domain_index == 0)
    //                //        c_(row_c,col) = (*sys_mat_ptr)(row,col);
    //                //    else
    //                //        c_(row_c,col) = (*sys_mat_ptr)(row+1,col);
    //                //}
    //                    //if (domain_index == 0 && mon_deg == 0) {
    //                    //if (mon_deg == 0) {
    //                    //    //++row_c;
    //                    //} else {
    //                    //    row_c += 2;
    //                    //}
    //                    //++row_c;
    //                //if (subseg_idx == 0 && domain_index == 0) {
    //                //    ++row;
    //                //} else {
    //                //    for (uint col = 0; col < sys_mat_ptr->cols(); ++col)
    //                //        c_(row_c,col) = (*sys_mat_ptr)(row,col);
    //                //    ++row;
    //                //    ++row_c;
    //                //}
    //                row   += 2;
    //            }
    //        }
    //    }
    //}};
    //std::cout << "c_:" << std::endl << c_ << std::endl;
    //Eigen::JacobiSVD<Eigen::MatrixXd> svd3(c_, Eigen::ComputeThinU | Eigen::ComputeThinV);
    //const VectorXd singular_values3 = svd3.singularValues();
    //std::cout << "The singular values of the system are:" << std::endl << "--->" << std::endl << singular_values3 << std::endl << "<---" << std::endl;

    //std::cout << "@@@@@@@@@@@@@@@@@@@@" << std::endl;
    //for (uint i = 0; i < 8; ++i) {
    //    const auto d = (c_*E).eval();
    //    Eigen::JacobiSVD<Eigen::MatrixXd> svd4(d.block(0,0,i+1,d.cols()), Eigen::ComputeThinU | Eigen::ComputeThinV);
    //    const VectorXd singular_values4 = svd4.singularValues();
    //    std::cout << "The singular values of the system are:" << std::endl << "--->" << std::endl << singular_values4 << std::endl << "<---" << std::endl;
    //}

    //std::cout << std::setprecision(4);
    //*sol_ = E*(c_*E).fullPivLu().solve(d_);
    //std::cout << "sol:" << std::endl << *sol_ << std::endl;
    //std::cout << (*sys_mat_ptr)*(*sol_)-(*rhs_ptr) << std::endl;
    //std::cout << "**************" << std::endl;
    //std::cout << std::setprecision(15);




    //std::cout << std::setprecision(3);
    //std::cout << "Its left singular vectors are the columns of the thin U matrix:" << std::endl << svd2.matrixU() << std::endl;
    //std::cout << "Its right singular vectors are the columns of the thin V matrix:" << std::endl << svd2.matrixV() << std::endl;
    //std::cout << std::setprecision(15);


    //const Eigen::VectorXcd eigenvalues_ = sys_mat_ptr->eigenvalues();
    //vector<double> eigenvalues(eigenvalues_.rows());
    //for (uint i = 0; i < eigenvalues_.rows(); ++i) eigenvalues[i] = std::abs(eigenvalues_(i));
    //std::sort(eigenvalues.begin(),eigenvalues.end());
    //std::cout << "**********" << std::endl;
    //for (uint i = 0; i < eigenvalues_.rows(); ++i) std::cout << eigenvalues[i] << std::endl;
    //std::cout << "**********" << std::endl;
}

void Problem::addDomain(const domain_ptr dptr) {
    dptr->setDomainIndex(domain_ptrs_.size());
    domain_ptrs_.push_back(dptr);
}

void Problem::addBoundarySegment(const boundary_segment_ptr bsptr) {
    bsptr->setBoundarySegmentIndex(boundary_segment_ptrs_.size());
    boundary_segment_ptrs_.push_back(bsptr);
}

Vec2D Problem::evaluateInteriorSolution(const uint domain_index, const Vec2D z) const {
    // Allocate a GSL workspace and a GSL function
    gsl_function gsl_fun;
    gsl_integration_workspace* gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);

    // Create a wrapper to simplify integration
    const auto quad = [&gsl_fun,gsl_workspace]()->double {
        double result,error;
        gsl_integration_qag(&gsl_fun, /* Function to integrate */
                            0.0,      /* Lower integration limit */
                            1.0,      /* Upper integration limit */
                            INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                            INTEGRAL_RELATIVE_ERROR_LIMIT,
                            MAX_NUM_SUBINTERVALS,
                            INTEGRATION_KEY,
                            gsl_workspace, &result, &error);

        //size_t neval;
        //gsl_integration_qng(&gsl_fun, 
        //                    0.0,
        //                    1.0,
        //                    INTEGRAL_ABSOLUTE_ERROR_LIMIT,
        //                    INTEGRAL_RELATIVE_ERROR_LIMIT,
        //                    &result,
        //                    &error,
        //                    &neval);
        //std::cout << "error=" << error << std::endl;

        return result;
    };

    // Create a lambda defining the integrand of integrals along boundary subsegments
    const auto boundarySubsegmentIntegral = [](const double t, void* params)->double {
        const uint& dim_idx      = *static_cast<const uint*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));

        const auto& psi_i        = *static_cast<const std::function<Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[2]));
        const auto& dpsi_i_2Norm = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[3]));
        const bool& is_curl      = *static_cast<const bool*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[4]));

        const auto& m_l          = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[5]));

        const Vec2D& z           = *static_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[1]));
        const Vec2D  y           = psi_i(t);

        if (is_curl)
            return (dim_idx == 0 ? CurlE(z-y).x :
                                   CurlE(z-y).y )*m_l(t)*dpsi_i_2Norm(t);
        else
            return (dim_idx == 0 ? GradE(z-y).x :
                                   GradE(z-y).y )*m_l(t)*dpsi_i_2Norm(t);
    };

    // Create a lambda to integrate along boundary subsegments
    const auto getBoundarySubsegmentIntegral = [&gsl_fun,&boundarySubsegmentIntegral,&quad](const uint                                l,
                                                                                            const Vec2D&                              z,
                                                                                            const std::function<Vec2D(const double)>  psi_i,
                                                                                            const std::function<double(const double)> dpsi_i_2Norm,
                                                                                            const bool                                is_curl)->const Vec2D {
        std::function<double(const double t)> m_l = [l](const double t)->double {
            return mono(t,l);
        };
        Vec2D result;
        for (uint i = 0; i < 2; ++i) {
            gsl_fun.function      = boundarySubsegmentIntegral;
            const double** params = new const double*[6];
            params[0]             = reinterpret_cast<const double*>(&i);
            params[1]             = reinterpret_cast<const double*>(&z);
            params[2]             = reinterpret_cast<const double*>(&psi_i);
            params[3]             = reinterpret_cast<const double*>(&dpsi_i_2Norm);
            params[4]             = reinterpret_cast<const double*>(&is_curl);
            params[5]             = reinterpret_cast<const double*>(&m_l);
            gsl_fun.params        = params;
            i == 0 ? result.x = quad() :
                     result.y = quad();
            delete[] params;
        }
        return result;
    };

    // Find the correct domain
    c_domain_ptr domain_ptr;
    for (const c_domain_ptr& dom_ptr : domain_ptrs_)
        if (dom_ptr->getDomainIndex() == domain_index && !dom_ptr->isReference())
            domain_ptr = dom_ptr;
    if (!domain_ptr) throw new std::runtime_error("Invalid domain_index provided to evaluateInteriorSolution: "+std::to_string(domain_index));

    // Get references to variables of the domain
    const auto& dir_bnd_seg_ptrs = domain_ptr->getDirectedBoundarySegmentPointers();

    Vec2D result = {0.0,0.0};
    uint sol_row = 0;
    for (uint dir_bnd_seg_ptr_idx = 0; dir_bnd_seg_ptr_idx < dir_bnd_seg_ptrs.size(); ++dir_bnd_seg_ptr_idx) {
        const DirectedBoundarySegment& dir_bnd_seg = *dir_bnd_seg_ptrs[dir_bnd_seg_ptr_idx];

        for (uint subseg_idx = 0; subseg_idx < dir_bnd_seg.getNumSubsegments(); ++subseg_idx) {
            for (uint mon_deg = 0; mon_deg <= (*dir_bnd_seg).getFunctionApproximationPolynomialOrder(); ++mon_deg) {
                result -= getBoundarySubsegmentIntegral(mon_deg,
                                                        z,
                                                        dir_bnd_seg.getpsii(subseg_idx),
                                                        dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                        true /* is_curl */)*(*sol_)(domain_system_matrix_column_offsets_[domain_index]+sol_row);
                result += getBoundarySubsegmentIntegral(mon_deg,
                                                        z,
                                                        dir_bnd_seg.getpsii(subseg_idx),
                                                        dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                        false /* is_curl */)*(*sol_)(domain_system_matrix_column_offsets_[domain_index]+sol_row+1);
                sol_row += 2;
            }
        }
    }

    // Free the memory of the GSL workspace
    gsl_integration_workspace_free(gsl_workspace);

    return result;
}

void Problem::initializeHelperVariables() {
    // Allocate memory
    domain_system_matrix_column_offsets_.resize(domain_ptrs_.size());

    // dom_idx <==> domain index
    uint vars = 0;
    for (uint dom_idx = 0; dom_idx < domain_ptrs_.size(); ++dom_idx) {
        domain_system_matrix_column_offsets_[dom_idx] = vars;

        if (dom_idx == domain_ptrs_.size()-1) break;

        const Domain& Omega                                           = *domain_ptrs_[dom_idx];
        const vector<directed_boundary_segment_ptr>& dir_bnd_seg_ptrs = Omega.getDirectedBoundarySegmentPointers();

        // dir_bnd_seg_ptr_idx <==> boundary segment pointer index
        for (uint dir_bnd_seg_ptr_idx = 0; dir_bnd_seg_ptr_idx < dir_bnd_seg_ptrs.size(); ++dir_bnd_seg_ptr_idx) {
            const BoundarySegment& bnd_seg = **dir_bnd_seg_ptrs[dir_bnd_seg_ptr_idx];
            vars += 4*bnd_seg.getNumSubsegments()*(bnd_seg.getFunctionApproximationPolynomialOrder()+1);
        }
    }
}

unique_mat_ptr Problem::allocateSystemMatrix() const {
    // Compute the number of rows
    uint rows = 0;

    for (const c_boundary_segment_ptr& bsp : boundary_segment_ptrs_)
        // There are two times the number of parameters of the polynomial approximition many test functions which
        // will be applied to each boundary segment on both sides.
        if (!bsp->isReference())
            rows += 2*(bsp->getFunctionApproximationPolynomialOrder()+1)*bsp->getNumSubsegments()*2;

    MatrixXd* m_ptr = new MatrixXd(rows,rows*2);
    *m_ptr          = MatrixXd::Zero(rows,rows*2);
    return std::unique_ptr<MatrixXd>(m_ptr);
}

unique_vec_ptr Problem::allocateRHS() const {
    // Compute the number of rows
    uint rows = 0;

    for (const c_boundary_segment_ptr& bsp : boundary_segment_ptrs_)
        // There are two times the number of parameters of the polynomial approximition many test functions which
        // will be applied to each boundary segment on both sides.
        if (!bsp->isReference())
            rows += 2*(bsp->getFunctionApproximationPolynomialOrder()+1)*bsp->getNumSubsegments()*2;

    return std::make_unique<VectorXd>(rows);
}

void Problem::store_solution(const std::string& folder_name) const {
    std::filesystem::create_directories(folder_name);
    std::ofstream file(folder_name+"/sol.b",std::ios::out|std::ios::binary);
    for (uint row = 0; row < sol_->rows(); ++row) {
        const double   cd  = (*sol_)(row);
        const uint8_t* ptr = reinterpret_cast<const uint8_t*>(&cd);
        for (uint i = 0; i < sizeof(double); ++i)
            file.put(ptr[i]);
    }
}

void Problem::load_solution(const std::string& folder_name) {
    // From: https://stackoverflow.com/questions/2409504/using-c-filestreams-fstream-how-can-you-determine-the-size-of-a-file
    std::ifstream file(folder_name+"/sol.b",std::ios::in|std::ios::binary);
    file.ignore(std::numeric_limits<std::streamsize>::max());
    std::streamsize file_length = file.gcount();
    file.clear();
    file.seekg(0,std::ios_base::beg);

    // Allocate memory for the solution
    sol_ = std::make_unique<VectorXd>(static_cast<long>(file_length/sizeof(double)));
    vector<uint8_t> row_buffer(sizeof(double));
    for (uint row = 0; row < sol_->rows(); ++row) {
        for (uint i = 0; i < sizeof(double); ++i) row_buffer[i] = file.get();
        double   c;
        uint8_t* ptr = reinterpret_cast<uint8_t*>(&c);
        for (uint i = 0; i < sizeof(double); ++i) ptr[i] = row_buffer[i];
        (*sol_)(row) = c;
    }
}
