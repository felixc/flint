#include <cmath>
#include <memory>
#include <limits>
#include <Eigen/Dense>
#include "ProblemWrapper.hpp"
#include "Sanji.hpp"
#include <iostream>

using namespace sanji::colors;

/* Type definitions */
template <typename T>
using vector   = std::vector<T>;
using VectorXd = Eigen::VectorXd;
using MatrixXd = Eigen::MatrixXd;

/* Problem parameters */
// Visualization
constexpr bool   plot_normals     = true;
constexpr bool   plot_tangentials = true;

// General
constexpr uint   num_magnets = 12;
constexpr uint   num_teeth   = 14;
constexpr double rotor_angle = 20.0/360.0*2.0*M_PI;

// Permeabilities
constexpr double mu0       = 4.0*M_PI*1.0e-7;
constexpr double mu_air    = 1.00000037*mu0;
constexpr double mu_cu     = 0.999994*mu0;
constexpr double mu_magnet = 500.0*mu0;//1.05*mu0;         // https://en.wikipedia.org/wiki/Permeability_(electromagnetism)
constexpr double mu_iron   = 500.0*mu0;

// Magnets
constexpr double dm1 = 0.0035;
constexpr double dm2 = 0.0115;
constexpr double dm3 = 0.004;

// Coils
constexpr double cm1 = 0.017;
constexpr double cm2 = 0.023;
constexpr double cm3 = 0.004;
constexpr double cm4 = 0.006;

// Teeth
constexpr double dt1 = 0.0155;
constexpr double dt2 = 2.0*M_PI*20.0/360.0;

// Rotor
constexpr double rotor_radius = 0.013;

// Stator
constexpr double stator_outer_radius = 0.028;

int main(int argc, char* argv[]) {
    // Initialize this as a Qt application
    QApplication app(argc, argv);
    sanji::init();

    /* Create a problem instance */
    ProblemWrapper p;

    /* Create the boundaries in the problem */
    // Magnets 
    vector<vector<boundary_segment_ptr>> all_magnets_boundary_segments(num_magnets);
    for (uint magnet_index = 0; magnet_index < num_magnets; ++magnet_index) {
        // Define the corners of an unrotated magnet
        MatrixXd corners(2,4);
        corners << -(dm2-dm1)/2, (dm2-dm1)/2, (dm2-dm1)/2, -(dm2-dm1)/2,
                         -dm3/2,      -dm3/2,       dm3/2,        dm3/2;

        // Compute the corners of the rotated and shifted magnet
        const double phi = 2.0*M_PI/num_magnets*magnet_index-rotor_angle;
        MatrixXd rot_mat(2,2);
        rot_mat << std::cos(phi), -std::sin(phi),
                   std::sin(phi),  std::cos(phi);
        VectorXd shift(2);
        shift << (dm2+dm1)/2.0, 0.0;
        const MatrixXd shifted_corners = (rot_mat*corners).colwise() + rot_mat*shift;

        // Create the boundary segments
        vector<uint> num_nodes{3,2,3,2};
        for (uint c = 0; c < 4; ++c)
            all_magnets_boundary_segments[magnet_index].push_back(p.addStraightBoundarySegment(Vec2D{shifted_corners(0,c),shifted_corners(1,c)},             /* start */
                                                                                               Vec2D{shifted_corners(0,(c+1)%4),shifted_corners(1,(c+1)%4)}, /* end */
                                                                                               num_nodes[c],                                                 /* num_nodes */
                                                                                               2,                                                            /* poly_order */
                                                                                               true                                                          /* normal_points_to_the_right */));
    }

    // Rotor
    const auto rotor_outer_rim = p.addCircularBoundarySegment(rotor_radius,   /* radius */
                                                              Vec2D{0.0,0.0}, /* center */
                                                              12,             /* num_nodes */
                                                              2,              /* boundary_solution_polynomial_order */
                                                              true            /* normal_points_to_the_right */);

    // Coils
    vector<vector<boundary_segment_ptr>> all_coils_boundary_segments(num_teeth);
    for (uint coil_index = 0; coil_index < num_teeth; ++ coil_index) {
        // Define the unrotated corners
        MatrixXd corners(2,4);
        corners << -(cm2-cm1)/2, (cm2-cm1)/2, (cm2-cm1)/2, -(cm2-cm1)/2,
                         -cm3/2,      -cm4/2,       cm4/2,        cm3/2;

        // Compute the corners of the rotated and shifted coil
        const double phi = (2.0*M_PI/num_teeth)*coil_index;
        MatrixXd rot_mat(2,2);
        rot_mat << std::cos(phi), -std::sin(phi),
                   std::sin(phi),  std::cos(phi);
        VectorXd shift(2);
        shift << (cm2+cm1)/2.0, 0.0;
        const MatrixXd shifted_corners = (rot_mat*corners).colwise() + rot_mat*shift;

        // Create the boundary segments
        vector<uint> num_nodes{3,2,3,2};
        for (uint c = 0; c < 4; ++c) {
            all_coils_boundary_segments[coil_index].push_back(p.addStraightBoundarySegment(Vec2D{shifted_corners(0,c),shifted_corners(1,c)},             /* start */
                                                                                           Vec2D{shifted_corners(0,(c+1)%4),shifted_corners(1,(c+1)%4)}, /* end */
                                                                                           num_nodes[c],                                                 /* num_nodes */
                                                                                           2,                                                            /* poly_order */
                                                                                           true                                                          /* normal_points_to_the_right */));
        }
    }

    // Teeth
    vector<vector<boundary_segment_ptr>> all_teeth_boundary_segments(num_teeth);
    for (uint teeth_index = 0; teeth_index < num_teeth; ++ teeth_index) {
        all_teeth_boundary_segments[teeth_index].push_back(p.addArcedBoundarySegment(dt1,                                           /* radius */
                                                                                     Vec2D{0.0,0.0},                                /* center */
                                                                                     -dt2/2.0+2.0*M_PI/num_teeth*(teeth_index+0.5), /* phi_min */
                                                                                      dt2/2.0+2.0*M_PI/num_teeth*(teeth_index+0.5), /* phi_max */
                                                                                     2,                                             /* num_nodes */
                                                                                     2,                                             /* boundary_solution_polynomial_order */
                                                                                     false                                          /* normal_points_to_the_right */));
        all_teeth_boundary_segments[teeth_index].push_back(p.addStraightBoundarySegment(all_coils_boundary_segments[teeth_index][2]->getEndPoint(),   /* start */
                                                                                        all_teeth_boundary_segments[teeth_index][0]->getStartPoint(), /* end */
                                                                                        2,                                                            /* num_nodes */
                                                                                        2,                                                            /* poly_order */
                                                                                        false                                                         /* normal_points_to_the_right */));
        all_teeth_boundary_segments[teeth_index].push_back(p.addStraightBoundarySegment(all_teeth_boundary_segments[teeth_index][0]->getEndPoint(),                 /* start */
                                                                                        all_coils_boundary_segments[(teeth_index+1)%num_teeth][0]->getStartPoint(), /* end */
                                                                                        2,                                                                          /* num_nodes */
                                                                                        2,                                                                          /* poly_order */
                                                                                        false                                                                       /* normal_points_to_the_right */));
    }

    // Stator
    const auto stator_outer_rim = p.addCircularBoundarySegment(stator_outer_radius, /* radius */
                                                               Vec2D{0.0,0.0},      /* center */
                                                               12,                  /* num_nodes */
                                                               2,                   /* boundary_solution_polynomial_order */
                                                               true                 /* normal_points_to_the_right */);

    // Reference boundaries
    const auto interior_air_outer_rim_r = p.addCircularBoundarySegment(dt1,            /* radius */
                                                                       Vec2D{0.0,0.0}, /* center */
                                                                       12,             /* num_nodes */
                                                                       2,              /* boundary_solution_polynomial_order */
                                                                       true,           /* normal_points_to_the_right */
                                                                       true            /* is_reference */);
    vector<boundary_segment_ptr> reference_segments_1;
    vector<boundary_segment_ptr> reference_segments_2;
    vector<boundary_segment_ptr> reference_segments_3;
    vector<boundary_segment_ptr> reference_segments_4;
    vector<boundary_segment_ptr> reference_segments_5;
    for (uint coil_index = 0; coil_index < num_teeth; ++coil_index) {
        Vec2D        corner = all_coils_boundary_segments[coil_index][0]->getEndPoint();
        const double phi1   = std::atan2(corner.y,corner.x);
        reference_segments_1.push_back(p.addStraightBoundarySegment(corner,                                    /* radius */
                                                                    Vec2D{stator_outer_radius*std::cos(phi1),
                                                                          stator_outer_radius*std::sin(phi1)}, /* center */
                                                                    2,                                         /* num_nodes */
                                                                    2,                                         /* boundary_solution_polynomial_order */
                                                                    true,                                      /* normal_points_to_the_right */
                                                                    true                                       /* is_reference */));
        corner        = all_coils_boundary_segments[coil_index][1]->getEndPoint();
        double phi2   = std::atan2(corner.y,corner.x);
        reference_segments_2.push_back(p.addStraightBoundarySegment(corner,                                    /* radius */
                                                                    Vec2D{stator_outer_radius*std::cos(phi2),
                                                                          stator_outer_radius*std::sin(phi2)}, /* center */
                                                                    2,                                         /* num_nodes */
                                                                    2,                                         /* boundary_solution_polynomial_order */
                                                                    true,                                      /* normal_points_to_the_right */
                                                                    true                                       /* is_reference */));
        if (phi2 < phi1) phi2 += 2.0*M_PI;
        reference_segments_3.push_back(p.addArcedBoundarySegment(stator_outer_radius, /* radius */
                                                                 Vec2D{0.0,0.0},      /* center */
                                                                 phi1,                /* phi_min */
                                                                 phi2,                /* phi_max */
                                                                 2,                   /* num_nodes */
                                                                 2,                   /* boundary_solution_polynomial_order */
                                                                 true,                /* normal_points_to_the_right */
                                                                 true                 /* is_reference */));
        reference_segments_4.push_back(p.addStraightBoundarySegment(all_coils_boundary_segments[coil_index][2]->getEndPoint(),                 /* radius */
                                                                    all_coils_boundary_segments[(coil_index+1)%num_teeth][0]->getStartPoint(), /* center */
                                                                    2,                                                                         /* num_nodes */
                                                                    2,                                                                         /* boundary_solution_polynomial_order */
                                                                    true,                                                                      /* normal_points_to_the_right */
                                                                    true                                                                       /* is_reference */));
        reference_segments_5.push_back(p.addArcedBoundarySegment(stator_outer_radius,     /* radius */
                                                                 Vec2D{0.0,0.0},          /* center */
                                                                 phi2-2.0*M_PI/num_teeth, /* phi_min */
                                                                 phi1,                    /* phi_max */
                                                                 2,                       /* num_nodes */
                                                                 2,                       /* boundary_solution_polynomial_order */
                                                                 true,                    /* normal_points_to_the_right */
                                                                 true                     /* is_reference */));
    }

    /* Create the domains in the problem */
    // Magnets
    for (uint magnet_index = 0; magnet_index < num_magnets; ++magnet_index) {
        vector<directed_boundary_segment_ptr> dbsps(4);
        for (uint i = 0; i < 4; ++i)
            dbsps[i] = std::make_shared<DirectedBoundarySegment>(all_magnets_boundary_segments[magnet_index][i],true);
        p.addDomain(dbsps,mu_magnet,0.0 /* J */);
    }

    // Rotor
    vector<directed_boundary_segment_ptr> dbsps;
    for (const auto& magnet_boundary_segments : all_magnets_boundary_segments)
        for (const auto& boundary_segment_ptr : magnet_boundary_segments)
            dbsps.push_back(std::make_shared<DirectedBoundarySegment>(boundary_segment_ptr,false));
    dbsps.push_back(std::make_shared<DirectedBoundarySegment>(rotor_outer_rim,true));
    p.addDomain(dbsps,mu_iron,0.0 /* J */);

    // Coils
    for (uint coil_index = 0; coil_index < num_teeth; ++coil_index) {
        vector<directed_boundary_segment_ptr> dbsps(4);
        for (uint i = 0; i < 4; ++i)
            dbsps[i] = std::make_shared<DirectedBoundarySegment>(all_coils_boundary_segments[coil_index][i],true);
        p.addDomain(dbsps,mu_cu,2.0/1.0e-6*(coil_index % 2 == 0 ? 1.0 : -1.0) /* J */);
    }

    // Inner air
    dbsps.clear();
    for (const auto& teeth_boundary_segments : all_teeth_boundary_segments)
        for (const auto& boundary_segment_ptr : teeth_boundary_segments)
            dbsps.push_back(std::make_shared<DirectedBoundarySegment>(boundary_segment_ptr,false));
    for (const auto& coil_boundary_segments : all_coils_boundary_segments)
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(coil_boundary_segments[3],false));
    dbsps.push_back(std::make_shared<DirectedBoundarySegment>(rotor_outer_rim,false));
    const auto inner_air = p.addDomain(dbsps,mu_air,0.0 /* J */);

    // Yoke
    dbsps.clear();
    for (const auto& teeth_boundary_segments : all_teeth_boundary_segments)
        for (const auto& boundary_segment_ptr : teeth_boundary_segments)
            dbsps.push_back(std::make_shared<DirectedBoundarySegment>(boundary_segment_ptr,true));
    for (const auto& coil_boundary_segments : all_coils_boundary_segments)
        for (uint i = 0; i < 3; ++i)
            dbsps.push_back(std::make_shared<DirectedBoundarySegment>(coil_boundary_segments[i],false));
    dbsps.push_back(std::make_shared<DirectedBoundarySegment>(stator_outer_rim,true));
    p.addDomain(dbsps,mu_iron,0.0 /* J */);

    // Exterior
    p.addExteriorDomain({std::make_shared<DirectedBoundarySegment>(stator_outer_rim,false)},mu_air);

    /* Solve the problem */
    //p.solveAndStore("results/PMSM_");
    //p.loadSolution("results/PMSM_");

    /* Add reference domains */
    p.addReferenceDomain(std::const_pointer_cast<const Domain>(inner_air),
                         {std::make_shared<DirectedBoundarySegment>(rotor_outer_rim,false),
                         std::make_shared<DirectedBoundarySegment>(interior_air_outer_rim_r,true)},
                         1 /* type */);
    for (uint coil_index = 0; coil_index < num_teeth; ++coil_index) {
        dbsps.clear();
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(all_coils_boundary_segments[(coil_index-1+num_teeth)%num_teeth][2],false));
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_2[(coil_index-1+num_teeth)%num_teeth],true));
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_5[coil_index],true));
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_1[coil_index],false));
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(all_coils_boundary_segments[coil_index][0],false));
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_4[(coil_index-1+num_teeth)%num_teeth],false));
        p.addReferenceDomain(yoke,dbsps,3/* type */);
    }
    for (uint coil_index = 0; coil_index < num_teeth; ++coil_index) {
        dbsps.clear();
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_1[coil_index],true));
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_3[coil_index],true));
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_2[coil_index],false));
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(all_coils_boundary_segments[coil_index][1],false));
        p.addReferenceDomain(yoke,dbsps,3/* type */);
    }

    /* Plot the boundaries in the problem */
    sanji::figure("Permanent magnet synchronous machine simulation");
    for (const auto& boundary_segment_plotting_data : p.getBoundarySegmentsPlottingData(0.001/4.0, /* max_length_per_segment */
                                                                                        true       /* show_reference_boundaries */))
        sanji::plot(*boundary_segment_plotting_data.x,*boundary_segment_plotting_data.y,boundary_segment_plotting_data.style);

    /* Plot the normals in the problem */
    if (plot_normals)
        for (const auto& boundary_segment_plotting_vector_data : p.getNormals(0.001,  /* max_length_per_segment */
                                                                              1.0e-3  /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);

    /* Plot the tangentials in the problem */
    if (plot_tangentials)
        for (const auto& boundary_segment_plotting_vector_data : p.getTangentials(0.001,  /* max_length_per_segment */
                                                                                  5.0e-4  /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);

    /* Plot the magnetic flux density */
    const auto field_data = p.getFieldData();
    double min =  std::numeric_limits<double>::max();
    double max = -std::numeric_limits<double>::max();
    for (const auto& field_data : field_data)
        for (uint i = 0; i < field_data.x->rows(); ++i) {
            const double strength = Vec2D{(*field_data.u)(i),(*field_data.v)(i)}.Norm2();
            if (strength < min) min = strength;
            if (strength > max) max = strength;
        }
    for (const auto& field_data : field_data)
        sanji::quiver(*field_data.x,
                      *field_data.y,
                      *field_data.u,
                      *field_data.v,{{"arrow_length",0.0006},{"use_colormap",1},{"colormap",TURBO},{"min",min},{"max",max}});
                      //*field_data.v,{{"arrow_length",0.001},{"use_colormap",1},{"colormap",TURBO},{"min",min},{"max",max},{"use_log_scale",1}});

    /* Print statistics */
    std::cout << "The minimum is " << min << std::endl;
    std::cout << "The maximum is " << max << std::endl;

    /* Plot cosmetics */
    sanji::setAxisRatio("equal");
    //sanji::setxTicksBackgroundColor(BLACK);
    //sanji::setyTicksBackgroundColor(BLACK);
    sanji::setxmin(0);
    sanji::setymin(0);
    sanji::setAxesRatio("equal");

    // Execute the application
    app.exec();
}
