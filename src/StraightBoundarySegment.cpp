#include <iostream>
#include "StraightBoundarySegment.hpp"

StraightBoundarySegment::StraightBoundarySegment(const Vec2D  start,
                                                 const Vec2D  end,
                                                 const uint   num_nodes,
                                                 const uint   boundary_solution_polynomial_order,
                                                 const bool   normal_points_to_the_right,
                                                 const bool   is_reference,
                                                 const double surface_current_density) :
    BoundarySegment(false, /* is_closed */
                    num_nodes,
                    boundary_solution_polynomial_order,
                    normal_points_to_the_right,
                    is_reference,
                    surface_current_density),
    start_(start),
    end_(end)
{}

double StraightBoundarySegment::getLength() const {
    return (end_-start_).Norm2();
}

const Vec2D StraightBoundarySegment::getBoundaryPoint(const double t) const {
    if (t > 1.0 || t < 0.0) {
        std::cout << "Invalid argument provided to StraightBoundarySegment::getBoundaryPoint." << std::endl;
        abort();
    }

    const Vec2D start_to_end = end_ - start_;
    
    return start_ + start_to_end*t;
}

const std::pair<Vec2D,Vec2D> StraightBoundarySegment::getNormal(const double t) const {
    return {getBoundaryPoint(t),getnu(0)(0)};
}

const std::pair<Vec2D,Vec2D> StraightBoundarySegment::getTangential(const double t) const {
    return {getBoundaryPoint(t),gett(0)(0)};
}

std::string StraightBoundarySegment::getClassName() const {
    return "StraightBoundarySegment";
}

std::function<const Vec2D(const double)> StraightBoundarySegment::getnu(const uint i) const {
    // Precompute constants
    const Vec2D start_to_end            = end_ - start_;
    const Vec2D start_to_end_normalized = start_to_end / start_to_end.Norm2();

    if (normal_points_to_the_right_) return [start_to_end_normalized](const double t)->const Vec2D {
                                        return Vec2D{ start_to_end_normalized.y,
                                                     -start_to_end_normalized.x};
                                     };
    else                             return [start_to_end_normalized](const double t)->const Vec2D {
                                        return Vec2D{-start_to_end_normalized.y,
                                                      start_to_end_normalized.x};
                                     };
}

std::function<const Vec2D(const double)> StraightBoundarySegment::gett(const uint i) const {
    return [getnu=this->getnu(i)](const double t)->const Vec2D {
        const Vec2D nu = getnu(t);
        return Vec2D{-nu.y,nu.x};
    };
}

std::function<const Vec2D(const double)> StraightBoundarySegment::getpsii(const uint i) const {
    // Precompute constants
    const Vec2D start_to_end = end_ - start_;
    const Vec2D diff         = start_to_end / (num_nodes_ - 1);
    const Vec2D start        = start_ + diff*i;

    // Return the segment parametrization
    return [start,diff](const double t)->const Vec2D {
        return start + t*diff;
    };
}

std::function<double(const double)> StraightBoundarySegment::getdpsii2Norm(const uint i) const {
    return [ret=(end_-start_).Norm2()/(num_nodes_ - 1)](const double t)->double {
        return ret;
    };
}
