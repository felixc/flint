#include "CircularBoundarySegment.hpp"
#include <cmath>
#include <iostream>

CircularBoundarySegment::CircularBoundarySegment(const double radius,
                                                 const Vec2D& center,
                                                 const uint   num_nodes,
                                                 const uint   boundary_solution_polynomial_order,
                                                 const bool   normal_points_to_the_right,
                                                 const bool   is_reference) :
    BoundarySegment(true, /* is_closed */
                    num_nodes,
                    boundary_solution_polynomial_order,
                    normal_points_to_the_right,
                    is_reference),
    radius_(radius > 0.0 ? radius : throw new std::runtime_error("CircularBoundarySegment: Radius must be greater than zero.")),
    center_(center)
{}

double CircularBoundarySegment::getLength() const {
    return 2.0*radius_*M_PI;
}

double CircularBoundarySegment::getRadius() const {
    return radius_;
}

const Vec2D CircularBoundarySegment::getCenter() const {
    return center_;
}

const Vec2D CircularBoundarySegment::getBoundaryPoint(const double t) const {
    if (t > 1.0 || t < 0.0) {
        std::cout << "Invalid argument provided to StraightBoundarySegment::getBoundaryPoint." << std::endl;
        abort();
    }

    const double phi = t*2.0*M_PI;

    return center_ + radius_*Vec2D{std::cos(phi),std::sin(phi)};
}

const std::pair<Vec2D,Vec2D> CircularBoundarySegment::getNormal(const double t) const {
    const double phi   = t*2.0*M_PI;
    const Vec2D normal = normal_points_to_the_right_ ? Vec2D{std::cos(phi),std::sin(phi)} : Vec2D{-std::cos(phi),-std::sin(phi)};
    return {getBoundaryPoint(t),normal};
};

const std::pair<Vec2D,Vec2D> CircularBoundarySegment::getTangential(const double t) const {
    const double phi       = t*2.0*M_PI;
    const Vec2D tangential = normal_points_to_the_right_ ? Vec2D{-std::sin(phi),std::cos(phi)} : Vec2D{std::sin(phi),-std::cos(phi)};
    return {getBoundaryPoint(t),tangential};
};

std::string CircularBoundarySegment::getClassName() const {
    return "CircularBoundarySegment";
}

std::function<const Vec2D(const double)> CircularBoundarySegment::getnu(const uint i) const {
    // Precompute constants
    const double delta_phi = 2.0*M_PI/num_nodes_;
    const double phi_min   = i*delta_phi;

    if (normal_points_to_the_right_) return [delta_phi,phi_min](const double t)->const Vec2D {
        const double phi = phi_min+t*delta_phi;
        return Vec2D{std::cos(phi),std::sin(phi)}; };
    else                             return [delta_phi,phi_min](const double t)->const Vec2D {
        const double phi = phi_min+t*delta_phi;
        return Vec2D{-std::cos(phi),-std::sin(phi)}; };
}

std::function<const Vec2D(const double)> CircularBoundarySegment::gett(const uint i) const {
    // Precompute constants
    const double delta_phi = 2.0*M_PI/num_nodes_;
    const double phi_min   = i*delta_phi;

    if (normal_points_to_the_right_) return [delta_phi,phi_min](const double t)->const Vec2D {
        const double phi = phi_min+t*delta_phi;
        return Vec2D{-std::sin(phi),std::cos(phi)}; };
    else                             return [delta_phi,phi_min](const double t)->const Vec2D {
        const double phi = phi_min+t*delta_phi;
        return Vec2D{std::sin(phi),-std::cos(phi)}; };
}

std::function<const Vec2D(const double)> CircularBoundarySegment::getpsii(const uint i) const {
    // Check the argument
    if (i >= num_nodes_) throw std::runtime_error("Called CircularBoundarySegment::getpsii with i="+std::to_string(i)+", but num_nodes_="+std::to_string(num_nodes_)+".");

    // Precompute constants
    const double delta_phi = 2.0*M_PI/num_nodes_;
    const double phi_min   = i*delta_phi;

    // Return the segment parametrization
    return [center=this->center_,radius=this->radius_,delta_phi,phi_min](const double t)->const Vec2D {
        const double phi = phi_min+t*delta_phi;
        return center + radius*Vec2D{std::cos(phi),std::sin(phi)};
    };
}

std::function<double(const double)> CircularBoundarySegment::getdpsii2Norm(const uint i) const {
    // \psi(t) = c + r*[cos(phi_min + t*Delta_phi); sin(phi_min + t*Delta_phi)]
    // => d/dt \psi(t)       = r*[-Delta_phi*sin(phi_min + t*Delta_phi); Delta_phi*cos(phi_min + t*Delta_phi)]
    //                       = r*Delta_phi*[-sin(phi_min + t*Delta_phi); cos(phi_min + t*Delta_phi)]
    // => ||d/dt \psi(t)||_2 = r*Delta_phi
    return [ret=radius_*2.0*M_PI/num_nodes_](const double t)->double {
        return ret;
    };
}
