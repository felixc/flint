#include <cmath>
#include <memory>
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <unordered_map>
#include "flint.hpp"
#include "CircularBoundarySegment.hpp"
#include "ArcedBoundarySegment.hpp"
#include "StraightBoundarySegment.hpp"

/* Type definitions */
template <typename T>
using vector = std::vector<T>;

/* Global variables that keep track of the state of the library instantiation */
bool                                          problem_created;
std::shared_ptr<Problem>                      problem_ptr;
std::unordered_map<uint,boundary_segment_ptr> boundary_segment_ptrs_map;

// TODO:
//extern "C" {
//
//void init(void) {
//    // Initialize global variables
//    problem_created = false;
//}
//
//void createProblem(void) {
//    if (problem_created) {
//        std::cout << "Only a single problem instance is currently supported." << std::endl;
//        abort();
//    } else {
//        problem_created = true;
//        problem_ptr     = std::make_shared<Problem>();
//    }
//}
//
//uint createCircularBoundarySegment(const double radius,
//                                   const double center_x,
//                                   const double center_y,
//                                   const uint   num_nodes,
//                                   const uint   poly_order,
//                                   const uint   normal_points_to_the_right,
//                                   const uint   is_reference) {
//    const boundary_segment_ptr boundary_segment_ptr = std::make_shared<CircularBoundarySegment>(radius,
//                                                                                                Vec2D{center_x,center_y},
//                                                                                                num_nodes,
//                                                                                                poly_order,
//                                                                                                normal_points_to_the_right==1,
//                                                                                                is_reference==1);
//    problem_ptr->addBoundarySegment(boundary_segment_ptr);
//    boundary_segment_ptrs_map[boundary_segment_ptr->getBoundarySegmentIndex()] = boundary_segment_ptr;
//    return boundary_segment_ptr->getBoundarySegmentIndex();
//}
//
//uint createArcedBoundarySegment(const double radius,
//                                const double center_x,
//                                const double center_y,
//                                const double phi_min,
//                                const double phi_max,
//                                const uint   num_nodes,
//                                const uint   poly_order,
//                                const uint   normal_points_to_the_right,
//                                const uint   is_reference) {
//    const boundary_segment_ptr boundary_segment_ptr = std::make_shared<ArcedBoundarySegment>(radius,
//                                                                                             Vec2D{center_x,center_y},
//                                                                                             phi_min,
//                                                                                             phi_max,
//                                                                                             num_nodes,
//                                                                                             poly_order,
//                                                                                             normal_points_to_the_right==1,
//                                                                                             is_reference==1);
//    problem_ptr->addBoundarySegment(boundary_segment_ptr);
//    boundary_segment_ptrs_map[boundary_segment_ptr->getBoundarySegmentIndex()] = boundary_segment_ptr;
//    return boundary_segment_ptr->getBoundarySegmentIndex();
//}
//
//uint createStraightBoundarySegment(const double start_x,
//                                   const double start_y,
//                                   const double end_x,
//                                   const double end_y,
//                                   const uint   num_nodes,
//                                   const uint   poly_order,
//                                   const uint   normal_points_to_the_right,
//                                   const uint   is_reference) {
//    const boundary_segment_ptr boundary_segment_ptr = std::make_shared<StraightBoundarySegment>(Vec2D{start_x,start_y},
//                                                                                                Vec2D{end_x,end_y},
//                                                                                                num_nodes,
//                                                                                                poly_order,
//                                                                                                normal_points_to_the_right==1,
//                                                                                                is_reference==1);
//    problem_ptr->addBoundarySegment(boundary_segment_ptr);
//    boundary_segment_ptrs_map[boundary_segment_ptr->getBoundarySegmentIndex()] = boundary_segment_ptr;
//    return boundary_segment_ptr->getBoundarySegmentIndex();
//}
//
//uint createDomain(const uint*  boundary_segment_indices,
//                  const uint*  boundary_segment_direction_flags,
//                  const uint   num_boundary_segments,
//                  const double mu,
//                  const double J) {
//    std::vector<directed_boundary_segment_ptr> directed_boundary_segment_ptrs;
//    for (uint bnd_seg_idx_ptr = 0; bnd_seg_idx_ptr < num_boundary_segments; ++bnd_seg_idx_ptr) {
//        bool boundary_segment_found = false;
//        for (const auto& pair : boundary_segment_ptrs_map)
//            if (std::get<0>(pair) == boundary_segment_indices[bnd_seg_idx_ptr]) {
//                boundary_segment_found = true;
//                directed_boundary_segment_ptrs.emplace_back(new DirectedBoundarySegment(std::get<1>(pair),
//                                                                                        boundary_segment_direction_flags[bnd_seg_idx_ptr]==1));
//                break;
//            }
//        if (!boundary_segment_found) {
//            std::cout << "Invalid boundary segment index encountered in 'createDomain': " << boundary_segment_indices[bnd_seg_idx_ptr] << std::endl;
//            abort();
//        }
//    }
//
//    const domain_ptr domain_ptr = std::make_shared<Domain>(directed_boundary_segment_ptrs,mu,J);
//    problem_ptr->addDomain(domain_ptr);
//    return domain_ptr->getDomainIndex();
//}
//
//void solve(const uint load_solution, const uint store_solution, const char* folder_name) {
//    problem_ptr->solve(load_solution==1,store_solution==1,std::string{folder_name});
//}
//
//void evaluateInteriorSolution(const uint domain_index, const double x, const double y, double* sol) {
//    const Vec2D sol_ = problem_ptr->evaluateInteriorSolution(domain_index, Vec2D{x,y});
//
//    // Store the solution
//    sol[0] = sol_.x;
//    sol[1] = sol_.y;
//}
//
//uint getNumBoundarySegmentDataPoints(const uint boundary_segment_index, const double max_length_per_segment) {
//    return std::ceil(boundary_segment_ptrs_map[boundary_segment_index]->getLength()/max_length_per_segment)+1;
//}
//
//void getBoundarySegmentDataPoints(const uint boundary_segment_index, double* boundary_segment_data_points_x, double* boundary_segment_data_points_y, const double max_length_per_segment) {
//    const uint num_boundary_segment_data_points = getNumBoundarySegmentDataPoints(boundary_segment_index,max_length_per_segment);
//    for (uint pt_idx = 0; pt_idx < num_boundary_segment_data_points; ++pt_idx) {
//        const Vec2D boundary_segment_point = boundary_segment_ptrs_map[boundary_segment_index]->getBoundaryPoint(pt_idx/static_cast<double>(num_boundary_segment_data_points-1));
//        boundary_segment_data_points_x[pt_idx] = boundary_segment_point.x;
//        boundary_segment_data_points_y[pt_idx] = boundary_segment_point.y;
//    }
//}
//
//void getNormals(const uint boundary_segment_index, const double max_length_per_segment, double* normals_x, double* normals_y, double* normals_u, double* normals_v) {
//    const uint num_boundary_segment_data_points = getNumBoundarySegmentDataPoints(boundary_segment_index,max_length_per_segment);
//    for (uint pt_idx = 0; pt_idx < num_boundary_segment_data_points; ++pt_idx) {
//        const pair<Vec2D,Vec2D> normal = boundary_segment_ptrs_map[boundary_segment_index]->getNormal(pt_idx/static_cast<double>(num_boundary_segment_data_points-1));
//        normals_x[pt_idx] = std::get<0>(normal).x;
//        normals_y[pt_idx] = std::get<0>(normal).y;
//        normals_u[pt_idx] = std::get<1>(normal).x;
//        normals_v[pt_idx] = std::get<1>(normal).y;
//    }
//}
//
//void getTangentials(const uint boundary_segment_index, const double max_length_per_segment, double* tangentials_x, double* tangentials_y, double* tangentials_u, double* tangentials_v) {
//    const uint num_boundary_segment_data_points = getNumBoundarySegmentDataPoints(boundary_segment_index,max_length_per_segment);
//    for (uint pt_idx = 0; pt_idx < num_boundary_segment_data_points; ++pt_idx) {
//        const std::pair<Vec2D,Vec2D> tangential = boundary_segment_ptrs_map[boundary_segment_index]->getTangential(pt_idx/static_cast<double>(num_boundary_segment_data_points-1));
//        tangentials_x[pt_idx] = std::get<0>(tangential).x;
//        tangentials_y[pt_idx] = std::get<0>(tangential).y;
//        tangentials_u[pt_idx] = std::get<1>(tangential).x;
//        tangentials_v[pt_idx] = std::get<1>(tangential).y;
//    }
//}
//
//uint getNumDataPointsType1(const uint boundary_segment_index_1, const uint boundary_segment_index_2, const uint num_levels, const uint num_points_suggestion) {
//    // Get the radii of the inner and outer circle
//    const double radius_1 = boundary_segment_ptrs_map[boundary_segment_index_1]->getLength();
//    const double radius_2 = boundary_segment_ptrs_map[boundary_segment_index_2]->getLength();
//
//    // Compute the total length of all subcircles combined (divided by 2*pi)
//    double total_length = 0.0;
//    for (uint i = 0; i < num_levels; ++i) {
//        const double t_i = (i + 1.0) / (num_levels + 1.0);
//        total_length += (1.0-t_i)*radius_1 + t_i*radius_2;
//    }
//
//    // Compute the number of data points
//    const double c = total_length/num_points_suggestion;
//    uint num_points = 0;
//    for (uint i = 0; i < num_levels; ++i) {
//        const double t_i = (i + 1.0) / (num_levels + 1.0);
//        const double r_i = (1.0-t_i)*radius_1 + t_i*radius_2;
//        num_points += static_cast<uint>(std::round(r_i/c));
//    }
//
//    return num_points;
//}
//
//void getFieldDataType1(const uint domain_index, const uint boundary_segment_index_1, const uint boundary_segment_index_2, const uint num_levels, const uint num_points_suggestion, double* field_data_x, double* field_data_y, double* field_data_u, double* field_data_v) {
//    // Get the radii of the inner and outer circle
//    const double radius_1 = boundary_segment_ptrs_map[boundary_segment_index_1]->getLength();
//    const double radius_2 = boundary_segment_ptrs_map[boundary_segment_index_2]->getLength();
//
//    // Compute the total length of all subcircles combined (divided by 2*pi)
//    double total_length = 0.0;
//    for (uint i = 0; i < num_levels; ++i) {
//        const double t_i = (i + 1.0) / (num_levels + 1.0);
//        total_length += (1.0-t_i)*radius_1 + t_i*radius_2;
//    }
//
//    // Compute the number of data points
//    const double c = total_length/num_points_suggestion;
//
//    // Compute the total number of points and the number of points per level
//    //uint num_points = 0;
//    //vector<uint> num_data_points_per_level(num_levels);
//    uint data_pt_cnt = 0;
//    for (uint i = 0; i < num_levels; ++i) {
//        // Determine the number of data points to be computed for this level
//        const double t_i                  = (i + 1.0) / (num_levels + 1.0);
//        const double r_i                  = (1.0-t_i)*radius_1 + t_i*radius_2;
//        const uint   data_pts_in_this_lvl = std::round(r_i/c);
//
//        // Compute the data points for this level
//        for (uint data_pt_idx = 0; data_pt_idx < data_pts_in_this_lvl; ++data_pt_idx) {
//            // Compute the data point to be evaluated in the interior
//            const Vec2D bnd_pt_1 = boundary_segment_ptrs_map[boundary_segment_index_1]->getBoundaryPoint(1.0/data_pts_in_this_lvl*data_pt_idx);
//            const Vec2D bnd_pt_2 = boundary_segment_ptrs_map[boundary_segment_index_2]->getBoundaryPoint(1.0/data_pts_in_this_lvl*data_pt_idx);
//            const Vec2D int_pt   = (1.0-t_i)*bnd_pt_1 + t_i*bnd_pt_2;
//            field_data_x[data_pt_cnt] = int_pt.x;
//            field_data_y[data_pt_cnt] = int_pt.y;
//
//            // Compute the magnetic flux density
//            double* sol = new double[2];
//            evaluateInteriorSolution(domain_index,int_pt.x,int_pt.y,sol);
//            field_data_u[data_pt_cnt] = sol[0];
//            field_data_v[data_pt_cnt] = sol[1];
//            delete[] sol;
//
//            // Increase the data point count
//            ++data_pt_cnt;
//        }
//    }
//}
//
//};

tuple<vec_ptr,vec_ptr> getBoundarySegmentDataPoints(const c_boundary_segment_ptr boundary_segment_ptr, const double max_length_per_segment) {
    // Determine the number of boundary segment data points
    const uint num_boundary_segment_data_points = std::ceil(boundary_segment_ptr->getLength()/max_length_per_segment)+1;

    // Allocate memory
    VectorXd*  x_ptr                            = new VectorXd(num_boundary_segment_data_points);
    VectorXd*  y_ptr                            = new VectorXd(num_boundary_segment_data_points);

    // Compute the boundary segment data points
    for (uint pt_idx = 0; pt_idx < num_boundary_segment_data_points; ++pt_idx) {
        const Vec2D boundary_segment_point = boundary_segment_ptr->getBoundaryPoint(pt_idx/static_cast<double>(num_boundary_segment_data_points-1));
        (*x_ptr)(pt_idx) = boundary_segment_point.x;
        (*y_ptr)(pt_idx) = boundary_segment_point.y;
    }
    return {vec_ptr{x_ptr},vec_ptr{y_ptr}};
}

tuple<vec_ptr,vec_ptr,vec_ptr,vec_ptr> getNormals(const BoundarySegment& bs, const double max_length_per_segment) {
    // Determine the number of boundary segment data points
    const uint num_boundary_segment_data_points = std::ceil(bs.getLength()/max_length_per_segment)+1;

    // Allocate memory
    VectorXd*  x_ptr                            = new VectorXd(num_boundary_segment_data_points);
    VectorXd*  y_ptr                            = new VectorXd(num_boundary_segment_data_points);
    VectorXd*  u_ptr                            = new VectorXd(num_boundary_segment_data_points);
    VectorXd*  v_ptr                            = new VectorXd(num_boundary_segment_data_points);

    // Compute the boundary segment normals
    for (uint pt_idx = 0; pt_idx < num_boundary_segment_data_points; ++pt_idx) {
        const pair<Vec2D,Vec2D> normal = bs.getNormal(pt_idx/static_cast<double>(num_boundary_segment_data_points-1));
        (*x_ptr)(pt_idx) = std::get<0>(normal).x;
        (*y_ptr)(pt_idx) = std::get<0>(normal).y;
        (*u_ptr)(pt_idx) = std::get<1>(normal).x;
        (*v_ptr)(pt_idx) = std::get<1>(normal).y;
    }
    return {vec_ptr{x_ptr},vec_ptr{y_ptr},vec_ptr{u_ptr},vec_ptr{v_ptr}};
}

tuple<vec_ptr,vec_ptr,vec_ptr,vec_ptr> getTangentials(const BoundarySegment& bs, const double max_length_per_segment) {
    // Determine the number of boundary segment data points
    const uint num_boundary_segment_data_points = std::ceil(bs.getLength()/max_length_per_segment)+1;

    // Allocate memory
    VectorXd*  x_ptr                            = new VectorXd(num_boundary_segment_data_points);
    VectorXd*  y_ptr                            = new VectorXd(num_boundary_segment_data_points);
    VectorXd*  u_ptr                            = new VectorXd(num_boundary_segment_data_points);
    VectorXd*  v_ptr                            = new VectorXd(num_boundary_segment_data_points);

    // Compute the boundary segment normals
    for (uint pt_idx = 0; pt_idx < num_boundary_segment_data_points; ++pt_idx) {
        const pair<Vec2D,Vec2D> tangential = bs.getTangential(pt_idx/static_cast<double>(num_boundary_segment_data_points-1));
        (*x_ptr)(pt_idx) = std::get<0>(tangential).x;
        (*y_ptr)(pt_idx) = std::get<0>(tangential).y;
        (*u_ptr)(pt_idx) = std::get<1>(tangential).x;
        (*v_ptr)(pt_idx) = std::get<1>(tangential).y;
    }
    return {vec_ptr{x_ptr},vec_ptr{y_ptr},vec_ptr{u_ptr},vec_ptr{v_ptr}};
}

tuple<vec_ptr,vec_ptr,vec_ptr,vec_ptr> getFieldDataType1(const Problem& p, const c_domain_ptr dom_ptr, const uint num_points_suggestion) {
    // Sanity checks of the arguments
    if (dom_ptr->getDirectedBoundarySegmentPointers().size() != 2) {
        std::cout << "Error in getFieldDataType1: Domain must have exactly two loops, but has " << dom_ptr->getDirectedBoundarySegmentPointers().size() << "." << std::endl;
        abort();
    }
    if (dom_ptr->getDirectedBoundarySegmentPointers()[0].size() != 1) {
        std::cout << "Error in getFieldDataType1: The first loop of the domain must have exactly one boundary segment, but has ";
        std::cout << dom_ptr->getDirectedBoundarySegmentPointers()[0].size() << "." << std::endl;
        abort();
    }
    if (dom_ptr->getDirectedBoundarySegmentPointers()[1].size() != 1) {
        std::cout << "Error in getFieldDataType1: The first loop of the domain must have exactly one boundary segment, but has ";
        std::cout << dom_ptr->getDirectedBoundarySegmentPointers()[1].size() << "." << std::endl;
        abort();
    }

    // Get the radii of the inner and outer circle
    const double radius_1 = (**dom_ptr->getDirectedBoundarySegmentPointers()[0][0]).getLength();
    const double radius_2 = (**dom_ptr->getDirectedBoundarySegmentPointers()[1][0]).getLength();

    // Determine the number of levels
    const uint num_levels = std::round(
        std::sqrt(num_points_suggestion) / std::sqrt(M_PI) / std::sqrt( (radius_1+radius_2) / std::abs(radius_1-radius_2) )
    );

    // Compute the total length of all subcircles combined (divided by 2*pi)
    double total_length = 0.0;
    for (uint i = 0; i < num_levels; ++i) {
        const double t_i = (i + 1.0) / (num_levels + 1.0);
        total_length += (1.0-t_i)*radius_1 + t_i*radius_2;
    }

    // Compute the total number of points
    uint num_points = 0;
    vector<uint> num_points_per_level(num_levels);
    for (uint i = 0; i < num_levels; ++i) {
        const double t_i        = (i + 1.0) / (num_levels + 1.0);
        const double r_i        = (1.0-t_i)*radius_1 + t_i*radius_2;
        num_points_per_level[i] = std::round( 2.0*M_PI*r_i*num_levels/std::abs(radius_1-radius_2) );
        num_points             += num_points_per_level[i];
    }

    // Allocate memory
    VectorXd* x_ptr = new VectorXd(num_points);
    VectorXd* y_ptr = new VectorXd(num_points);
    VectorXd* u_ptr = new VectorXd(num_points);
    VectorXd* v_ptr = new VectorXd(num_points);

    // Compute the total number of points and the number of points per level
    uint data_pt_cnt = 0;
    for (uint i = 0; i < num_levels; ++i) {
        const double t_i = (i + 1.0) / (num_levels + 1.0);

        // Compute the data points for this level
        for (uint data_pt_idx = 0; data_pt_idx < num_points_per_level[i]; ++data_pt_idx) {
            // Compute the data point to be evaluated in the interior
            const Vec2D bnd_pt_1  = (**dom_ptr->getDirectedBoundarySegmentPointers()[0][0]).getBoundaryPoint(1.0/num_points_per_level[i]*data_pt_idx);
            const Vec2D bnd_pt_2  = (**dom_ptr->getDirectedBoundarySegmentPointers()[1][0]).getBoundaryPoint(1.0/num_points_per_level[i]*data_pt_idx);
            const Vec2D int_pt    = (1.0-t_i)*bnd_pt_1 + t_i*bnd_pt_2;
            (*x_ptr)(data_pt_cnt) = int_pt.x;
            (*y_ptr)(data_pt_cnt) = int_pt.y;

            // Compute the magnetic flux density
            const Vec2D sol = p.evaluateInteriorSolution(dom_ptr->getDomainIndex(),int_pt);
            (*u_ptr)(data_pt_cnt) = sol.x;
            (*v_ptr)(data_pt_cnt) = sol.y;

            // Increase the data point count
            ++data_pt_cnt;
        }
    }
    return {vec_ptr{x_ptr},vec_ptr{y_ptr},vec_ptr{u_ptr},vec_ptr{v_ptr}};
}

tuple<vec_ptr,vec_ptr,vec_ptr,vec_ptr> getFieldDataType2(const Problem& p, const c_domain_ptr dom_ptr, const uint num_levels, const uint num_points_suggestion,
    const std::function<bool(const Vec2D)>& filter) {
    // Get the radius of the domain
    const double radius = (**dom_ptr->getDirectedBoundarySegmentPointers()[0][0]).getLength();

    // Compute the total length of all subcircles combined (divided by 2*pi)
    double total_length = 0.0;
    for (uint i = 0; i < num_levels; ++i) {
        const double t_i = (i + 1.0) / (num_levels + 1.0);
        total_length += t_i*radius;
    }

    // Compute the number of data points per unit length (divided by 2*pi)
    const double c = total_length/num_points_suggestion;

    // Compute the total number of points and the number of points per level
    uint num_points = 0;
    vector<uint> num_points_per_level(num_levels);
    for (uint i = 0; i < num_levels; ++i) {
        const double t_i        = (i + 1.0) / (num_levels + 1.0);
        const double r_i        = t_i*radius;
        num_points_per_level[i] = std::round(r_i/c);
        num_points             += num_points_per_level[i];
    }

    // Allocate memory
    vector<double> x_data, y_data, u_data, v_data;

    // Compute the total number of points and the number of points per level
    uint data_pt_cnt = 0u;
    for (uint i = 0u; i < num_levels; ++i) {
        const double t_i = (i + 1.0) / (num_levels + 1.0);

        // Compute the data points for this level
        for (uint data_pt_idx = 0; data_pt_idx < num_points_per_level[i]; ++data_pt_idx) {
            // Compute the data point to be evaluated in the interior
            const Vec2D center    = dynamic_cast<CircularBoundarySegment*>(&**dom_ptr->getDirectedBoundarySegmentPointers()[0][0])->getCenter();
            const Vec2D bnd_pt    = dom_ptr->getDirectedBoundarySegmentPointers()[0][0]->getBoundaryPoint(1.0/num_points_per_level[i]*data_pt_idx);
            const Vec2D int_pt    = (1.0-t_i)*center + t_i*bnd_pt;

            // Filter the points
            if (!filter(int_pt)) continue;

            // Store the current position
            x_data.push_back(int_pt.x);
            y_data.push_back(int_pt.y);

            // Compute the magnetic flux density
            const Vec2D sol = p.evaluateInteriorSolution(dom_ptr->getDomainIndex(),int_pt);
            u_data.push_back(sol.x);
            v_data.push_back(sol.y);

            // Increase the data point count
            ++data_pt_cnt;
        }
    }

    // Allocate memory and copy the data
    VectorXd* x_ptr = new VectorXd(data_pt_cnt);
    VectorXd* y_ptr = new VectorXd(data_pt_cnt);
    VectorXd* u_ptr = new VectorXd(data_pt_cnt);
    VectorXd* v_ptr = new VectorXd(data_pt_cnt);
    for (uint i = 0u; i < data_pt_cnt; ++i) {
        (*x_ptr)(i) = x_data[i];
        (*y_ptr)(i) = y_data[i];
        (*u_ptr)(i) = u_data[i];
        (*v_ptr)(i) = v_data[i];
    }

    return {vec_ptr{x_ptr},vec_ptr{y_ptr},vec_ptr{u_ptr},vec_ptr{v_ptr}};
}

tuple<vec_ptr,vec_ptr,vec_ptr,vec_ptr> getFieldDataType3(const Problem& p, const c_domain_ptr dom_ptr, const uint num_levels, const uint num_points_suggestion) {
    // Define variables for convenience
    const auto& directed_boundary_segment_ptrs = dom_ptr->getDirectedBoundarySegmentPointers()[0];
    const uint  num_boundary_segments          = directed_boundary_segment_ptrs.size();

    // Compute how much the individual boundary segments contribute to the toal length of the boundary
    double boundary_length = 0.0;
    vector<double> contributions(num_boundary_segments);
    for (uint bnd_seg_idx = 0; bnd_seg_idx < num_boundary_segments; ++bnd_seg_idx) {
        contributions[bnd_seg_idx] = (**directed_boundary_segment_ptrs[bnd_seg_idx]).getLength();
        boundary_length           += contributions[bnd_seg_idx];
    }
    for (uint bnd_seg_idx = 0; bnd_seg_idx < num_boundary_segments; ++bnd_seg_idx)
        contributions[bnd_seg_idx] /= boundary_length;

    // Compute the sum of all boundary segments length across all levels
    double total_boundary_length = 0.0;
    for (uint lvl_idx = 0; lvl_idx < num_levels; ++lvl_idx) {
        const double t_i       = (lvl_idx + 1.0) / (num_levels + 1.0);
        total_boundary_length += boundary_length*t_i;
    }

    // Compute the number of data points per unit length
    const double c = (std::max(static_cast<int>(num_points_suggestion)-1,0)) / total_boundary_length;

    // Compute the total number of points as well the number of points per level and boundary segment
    uint num_points = 0;
    vector<vector<uint>> num_points_per_level_and_segment(num_levels);
    for (uint lvl_idx = 0; lvl_idx < num_levels; ++lvl_idx) {
        const double t_i = (lvl_idx + 1.0) / (num_levels + 1.0);
        for (uint bnd_seg_idx = 0; bnd_seg_idx < num_boundary_segments; ++bnd_seg_idx) {
            const double boundary_segment_length = (**directed_boundary_segment_ptrs[bnd_seg_idx]).getLength()*t_i;
            num_points_per_level_and_segment[lvl_idx].push_back(std::ceil(boundary_segment_length*c));
            num_points += num_points_per_level_and_segment[lvl_idx].back();
        }
    }
    if (num_points_suggestion > 0) num_points += 1;

    // Find the center of the domain
    Vec2D center{0.0,0.0};
    uint  add_cnt = 0;
    for (uint bnd_seg_idx = 0; bnd_seg_idx < num_boundary_segments; ++bnd_seg_idx) {
        const uint num_points = contributions[bnd_seg_idx]*100;
        for (uint data_pt_idx = 0; data_pt_idx < num_points; ++data_pt_idx) {
            center  += directed_boundary_segment_ptrs[bnd_seg_idx]->getBoundaryPoint(1.0/num_points*data_pt_idx);
            add_cnt += 1;
        }
    }
    center /= add_cnt;

    // Allocate memory
    VectorXd* x_ptr = new VectorXd(num_points);
    VectorXd* y_ptr = new VectorXd(num_points);
    VectorXd* u_ptr = new VectorXd(num_points);
    VectorXd* v_ptr = new VectorXd(num_points);

    // Compute the total number of points and the number of points per level
    uint data_pt_cnt = 0;
    for (uint lvl_idx = 0; lvl_idx < num_levels; ++lvl_idx) {
        const double t_i = (lvl_idx + 1.0) / (num_levels + 1.0);

        // Compute the data points for this level
        for (uint bnd_seg_idx = 0; bnd_seg_idx < num_boundary_segments; ++bnd_seg_idx) {
            for (uint data_pt_idx = 0; data_pt_idx < num_points_per_level_and_segment[lvl_idx][bnd_seg_idx]; ++data_pt_idx) {
                // Compute the data point to be evaluated in the interior
                const Vec2D bnd_pt    = directed_boundary_segment_ptrs[bnd_seg_idx]->getBoundaryPoint((1.0/num_points_per_level_and_segment[lvl_idx][bnd_seg_idx])*data_pt_idx);
                const Vec2D int_pt    = (1.0-t_i)*center + t_i*bnd_pt;
                (*x_ptr)(data_pt_cnt) = int_pt.x;
                (*y_ptr)(data_pt_cnt) = int_pt.y;

                // Compute the magnetic flux density
                const Vec2D sol = p.evaluateInteriorSolution(dom_ptr->getDomainIndex(),int_pt);
                (*u_ptr)(data_pt_cnt) = sol.x;
                (*v_ptr)(data_pt_cnt) = sol.y;

                // Increase the data point count
                ++data_pt_cnt;
            }
        }
    }
    if (num_points_suggestion > 0) {
        (*x_ptr)(num_points-1) = center.x;
        (*y_ptr)(num_points-1) = center.y;
        const Vec2D sol        = p.evaluateInteriorSolution(dom_ptr->getDomainIndex(),center);
        (*u_ptr)(num_points-1) = sol.x;
        (*v_ptr)(num_points-1) = sol.y;
    }
    return {vec_ptr{x_ptr},vec_ptr{y_ptr},vec_ptr{u_ptr},vec_ptr{v_ptr}};
}

tuple<vec_ptr,vec_ptr,vec_ptr,vec_ptr> getFieldDataType4(const Problem& p, const c_domain_ptr dom_ptr, const uint num_levels, const uint num_points_suggestion) {
    // Sanity checks of the arguments
    if (dom_ptr->getDirectedBoundarySegmentPointers().size() != 1) {
        std::cout << "Error in getFieldDataType4: Domain must have exactly one loop, but has " << dom_ptr->getDirectedBoundarySegmentPointers().size() << "." << std::endl;
        abort();
    }

    // Define variables for convenience
    const auto& directed_boundary_segment_ptrs = dom_ptr->getDirectedBoundarySegmentPointers()[0];
    const uint  num_boundary_segments          = directed_boundary_segment_ptrs.size();

    // Compute how much the individual boundary segments contribute to the toal length of the boundary
    double boundary_length = 0.0;
    vector<double> contributions(num_boundary_segments);
    for (uint bnd_seg_idx = 0; bnd_seg_idx < num_boundary_segments; ++bnd_seg_idx) {
        contributions[bnd_seg_idx] = (**directed_boundary_segment_ptrs[bnd_seg_idx]).getLength();
        boundary_length           += contributions[bnd_seg_idx];
    }
    for (uint bnd_seg_idx = 0; bnd_seg_idx < num_boundary_segments; ++bnd_seg_idx)
        contributions[bnd_seg_idx] /= boundary_length;

    // Compute the sum of all boundary segments length across all levels
    double total_boundary_length = 0.0;
    for (uint lvl_idx = 0; lvl_idx < num_levels; ++lvl_idx) {
        const double t_i       = (lvl_idx + 1.0) / (num_levels + 1.0);
        total_boundary_length += boundary_length*t_i;
    }

    // Compute the number of data points per unit length
    const double c = (std::max(static_cast<int>(num_points_suggestion)-1,0)) / total_boundary_length;

    // Compute the total number of points as well the number of points per level and boundary segment
    uint num_points = 0;
    vector<vector<uint>> num_points_per_level_and_segment(num_levels);
    for (uint lvl_idx = 0; lvl_idx < num_levels; ++lvl_idx) {
        const double t_i = (lvl_idx + 1.0) / (num_levels + 1.0);
        for (uint bnd_seg_idx = 0; bnd_seg_idx < num_boundary_segments; ++bnd_seg_idx) {
            const double boundary_segment_length = (**directed_boundary_segment_ptrs[bnd_seg_idx]).getLength()*t_i;
            num_points_per_level_and_segment[lvl_idx].push_back(std::round(boundary_segment_length*c));
            num_points += num_points_per_level_and_segment[lvl_idx].back();
        }
    }
    if (num_points_suggestion > 0) num_points += 1;

    // Find the center of the domain in radial coordinates
    Vec2D center{0.0,0.0};
    uint  add_cnt = 0;
    for (uint bnd_seg_idx = 0; bnd_seg_idx < num_boundary_segments; ++bnd_seg_idx) {
        const uint num_points = contributions[bnd_seg_idx]*100;
        for (uint data_pt_idx = 0; data_pt_idx < num_points; ++data_pt_idx) {
            center  += directed_boundary_segment_ptrs[bnd_seg_idx]->getBoundaryPoint(1.0/num_points*data_pt_idx).to_polar();
            add_cnt += 1;
        }
    }
    center /= add_cnt;

    // Allocate memory
    VectorXd* x_ptr = new VectorXd(num_points);
    VectorXd* y_ptr = new VectorXd(num_points);
    VectorXd* u_ptr = new VectorXd(num_points);
    VectorXd* v_ptr = new VectorXd(num_points);

    // Compute the total number of points and the number of points per level
    uint data_pt_cnt = 0;
    for (uint lvl_idx = 0; lvl_idx < num_levels; ++lvl_idx) {
        const double t_i = (lvl_idx + 1.0) / (num_levels + 1.0);

        // Compute the data points for this level
        for (uint bnd_seg_idx = 0; bnd_seg_idx < num_boundary_segments; ++bnd_seg_idx) {
            for (uint data_pt_idx = 0; data_pt_idx < num_points_per_level_and_segment[lvl_idx][bnd_seg_idx]; ++data_pt_idx) {
                // Compute the data point to be evaluated in the interior
                const Vec2D bnd_pt    = directed_boundary_segment_ptrs[bnd_seg_idx]->getBoundaryPoint(1.0/num_points_per_level_and_segment[lvl_idx][bnd_seg_idx]*data_pt_idx).to_polar();
                const Vec2D int_pt    = ((1.0-t_i)*center + t_i*bnd_pt).from_polar();
                (*x_ptr)(data_pt_cnt) = int_pt.x;
                (*y_ptr)(data_pt_cnt) = int_pt.y;

                // Compute the magnetic flux density
                const Vec2D sol = p.evaluateInteriorSolution(dom_ptr->getDomainIndex(),int_pt);
                (*u_ptr)(data_pt_cnt) = sol.x;
                (*v_ptr)(data_pt_cnt) = sol.y;

                // Increase the data point count
                ++data_pt_cnt;
            }
        }
    }

    if (num_points_suggestion > 0) {
        (*x_ptr)(num_points-1) = center.x;
        (*y_ptr)(num_points-1) = center.y;
        const Vec2D sol        = p.evaluateInteriorSolution(dom_ptr->getDomainIndex(),center);
        (*u_ptr)(num_points-1) = sol.x;
        (*v_ptr)(num_points-1) = sol.y;
    }
    return {vec_ptr{x_ptr},vec_ptr{y_ptr},vec_ptr{u_ptr},vec_ptr{v_ptr}};
}
