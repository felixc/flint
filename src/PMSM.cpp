#include "PMSM.hpp"

/* Type definitions */
using uint = unsigned int;

// Permeabilities
static constexpr double mu0       = 4.0*M_PI*1.0e-7;
static constexpr double mu_air    = 1.00000037*mu0;
static constexpr double mu_cu     = 0.999994*mu0;
static constexpr double mu_magnet = 1.05*mu0;         // https://en.wikipedia.org/wiki/Permeability_(electromagnetism)
static constexpr double mu_iron   = 4000.0*mu0;

// Magnets
static constexpr double dm1 = 0.0065;
static constexpr double dm2 = 0.0141;
static constexpr double dm3 = 0.003;
static constexpr double Hc  = 1000.0; // [kA/m] https://en.wikipedia.org/wiki/Neodymium_magnet

// Teeth
static constexpr double dt1 = 0.0155;
static constexpr double dt2 = 2.0*M_PI*20.0/360.0;

// Rotor
static constexpr double rotor_radius = 0.0145;

// Stator
static constexpr double stator_outer_radius = 0.028;

PMSM::PMSM(      ProblemWrapper& p,
           const double          rotor_angle,
           const double          max_coil_current_density,
           const VectorXd&       motor_current_factors,
           const uint            num_magnets,
           const uint            num_teeth,
           const double          cm1,
           const double          cm2,
           const double          cm3,
           const double          cm4) :
    num_magnets_(num_magnets),
    num_teeth_(num_teeth),
    cm1_(cm1),
    cm2_(cm2),
    cm3_(cm3),
    cm4_(cm4)
{
    /* Create the boundaries in the problem */
    // Magnets 
    vector<vector<boundary_segment_ptr>> all_magnets_boundary_segments(num_magnets_);
    for (uint magnet_index = 0; magnet_index < num_magnets_; ++magnet_index) {
        // Define the corners of an unrotated magnet
        MatrixXd corners(2,4);
        corners << -(dm2-dm1)/2, (dm2-dm1)/2, (dm2-dm1)/2, -(dm2-dm1)/2,
                         -dm3/2,      -dm3/2,       dm3/2,        dm3/2;

        // Compute the corners of the rotated and shifted magnet
        const double phi = 2.0*M_PI/num_magnets_*magnet_index+rotor_angle;
        MatrixXd rot_mat(2,2);
        rot_mat << std::cos(phi), -std::sin(phi),
                   std::sin(phi),  std::cos(phi);
        VectorXd shift(2);
        shift << (dm2+dm1)/2.0, 0.0;
        const MatrixXd shifted_corners = (rot_mat*corners).colwise() + rot_mat*shift;

        // Create the boundary segments
        vector<uint> num_nodes{30,20,30,20};
        for (uint c = 0; c < 4; ++c) {
            double surface_current_density;
            if (c == 0)
                surface_current_density = Hc*(magnet_index % 2 == 0 ? -1.0 :  1.0);
            else if (c == 2)
                surface_current_density = Hc*(magnet_index % 2 == 0 ?  1.0 : -1.0);
            else
                surface_current_density = 0.0;
            all_magnets_boundary_segments[magnet_index].push_back(p.addStraightBoundarySegment(Vec2D{shifted_corners(0,c),shifted_corners(1,c)},             /* start */
                                                                                               Vec2D{shifted_corners(0,(c+1)%4),shifted_corners(1,(c+1)%4)}, /* end */
                                                                                               num_nodes[c],                                                 /* num_nodes */
                                                                                               1,                                                            /* poly_order */
                                                                                               true,                                                         /* normal_points_to_the_right */
                                                                                               false,                                                        /* is_reference */
                                                                                               surface_current_density));
        }
    }

    // Rotor
    rotor_outer_rim_ = p.addCircularBoundarySegment(rotor_radius,   /* radius */
                                                    Vec2D{0.0,0.0}, /* center */
                                                    120,            /* num_nodes */
                                                    1,              /* boundary_solution_polynomial_order */
                                                    true            /* normal_points_to_the_right */);

    // Coils
    all_coils_boundary_segments_.resize(num_teeth_);
    for (uint coil_index = 0; coil_index < num_teeth_; ++ coil_index) {
        // Define the unrotated corners
        MatrixXd corners(2,4);
        corners << -(cm2-cm1)/2, (cm2-cm1)/2, (cm2-cm1)/2, -(cm2-cm1)/2,
                         -cm3/2,      -cm4/2,       cm4/2,        cm3/2;

        // Compute the corners of the rotated and shifted coil
        const double phi = (2.0*M_PI/num_teeth_)*coil_index;
        MatrixXd rot_mat(2,2);
        rot_mat << std::cos(phi), -std::sin(phi),
                   std::sin(phi),  std::cos(phi);
        VectorXd shift(2);
        shift << (cm2+cm1)/2.0, 0.0;
        const MatrixXd shifted_corners = (rot_mat*corners).colwise() + rot_mat*shift;

        // Create the boundary segments
        vector<uint> num_nodes{30,20,30,20};
        for (uint c = 0; c < 4; ++c) {
            all_coils_boundary_segments_[coil_index].push_back(p.addStraightBoundarySegment(Vec2D{shifted_corners(0,c),shifted_corners(1,c)},             /* start */
                                                                                           Vec2D{shifted_corners(0,(c+1)%4),shifted_corners(1,(c+1)%4)}, /* end */
                                                                                           num_nodes[c],                                                 /* num_nodes */
                                                                                           1,                                                            /* poly_order */
                                                                                           true                                                          /* normal_points_to_the_right */));
        }
    }

    // Teeth
    vector<vector<boundary_segment_ptr>> all_teeth_boundary_segments(num_teeth_);
    for (uint teeth_index = 0; teeth_index < num_teeth_; ++ teeth_index) {
        const auto s = p.addArcedBoundarySegment(dt1,                                            /* radius */
                                                 Vec2D{0.0,0.0},                                 /* center */
                                                 -dt2/2.0+2.0*M_PI/num_teeth_*(teeth_index+0.5), /* phi_min */
                                                  dt2/2.0+2.0*M_PI/num_teeth_*(teeth_index+0.5), /* phi_max */
                                                 20,                                             /* num_nodes */
                                                 1,                                              /* boundary_solution_polynomial_order */
                                                 true                                            /* normal_points_to_the_right */);
        all_teeth_boundary_segments[teeth_index].push_back(p.addStraightBoundarySegment(all_coils_boundary_segments_[teeth_index][2]->getEndPoint(), /* start */
                                                                                        s->getStartPoint(),                                          /* end */
                                                                                        20,                                                          /* num_nodes */
                                                                                        1,                                                           /* poly_order */
                                                                                        true                                                         /* normal_points_to_the_right */));
        all_teeth_boundary_segments[teeth_index].push_back(s);
        all_teeth_boundary_segments[teeth_index].push_back(p.addStraightBoundarySegment(s->getEndPoint(),                                                            /* start */
                                                                                        all_coils_boundary_segments_[(teeth_index+1)%num_teeth_][0]->getStartPoint(), /* end */
                                                                                        20,                                                                          /* num_nodes */
                                                                                        1,                                                                           /* poly_order */
                                                                                        true                                                                         /* normal_points_to_the_right */));
    }

    // Stator
    const auto stator_outer_rim = p.addCircularBoundarySegment(stator_outer_radius, /* radius */
                                                               Vec2D{0.0,0.0},      /* center */
                                                               120,                  /* num_nodes */
                                                               1,                   /* boundary_solution_polynomial_order */
                                                               true                 /* normal_points_to_the_right */);

    /* Create the domains in the problem */
    // Magnets
    for (uint magnet_index = 0; magnet_index < num_magnets_; ++magnet_index) {
        vector<directed_boundary_segment_ptr> dbsps(4);
        for (uint i = 0; i < 4; ++i)
            dbsps[i] = std::make_shared<DirectedBoundarySegment>(all_magnets_boundary_segments[magnet_index][i],true);
        p.addDomain("magnet_"+std::to_string(magnet_index),{dbsps},mu_magnet,0.0 /* J */,3 /* type */);
    }

    // Exterior
    p.addExteriorDomain("outer_air",{{std::make_shared<DirectedBoundarySegment>(stator_outer_rim,false)}},mu_air);

    // Rotor
    vector<vector<directed_boundary_segment_ptr>> dbsps;
    for (const auto& magnet_boundary_segments : all_magnets_boundary_segments) {
        dbsps.emplace_back();
        for (int seg_idx = 3; seg_idx >= 0; --seg_idx)
            dbsps.back().emplace_back(std::make_shared<DirectedBoundarySegment>(magnet_boundary_segments[seg_idx],false));
    }
    dbsps.push_back({{std::make_shared<DirectedBoundarySegment>(rotor_outer_rim_,true)}});
    rotor_ = p.addDomain("rotor",dbsps,mu_iron,0.0 /* J */);

    // Inner air
    dbsps.clear(); dbsps.resize(1);
    for (int teeth_idx = 0; teeth_idx < num_teeth_; ++teeth_idx) {
        dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(all_coils_boundary_segments_[teeth_idx][3],false));
        for (int seg_idx = 0; seg_idx < all_teeth_boundary_segments[teeth_idx].size(); ++seg_idx)
            dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(all_teeth_boundary_segments[teeth_idx][seg_idx],true));
    }
    dbsps.push_back({std::make_shared<DirectedBoundarySegment>(rotor_outer_rim_,false)});
    inner_air_ = p.addDomain("inner_air",dbsps,mu_air,0.0 /* J */);

    // Coils
    current_densities_.resize(num_teeth_);
    coils_.resize(num_teeth_);
    for (uint coil_index = 0; coil_index < num_teeth_; ++coil_index) {
        vector<directed_boundary_segment_ptr> dbsps(4);
        for (uint i = 0; i < 4; ++i)
            dbsps[i] = std::make_shared<DirectedBoundarySegment>(all_coils_boundary_segments_[coil_index][i],true);
        const double current_density   = max_coil_current_density*motor_current_factors(coil_index);
        current_densities_[coil_index] = current_density;
        coils_[coil_index]             = p.addDomain("coil_"+std::to_string(coil_index),{dbsps},mu_cu,current_density);
    }

    // Yoke
    dbsps.clear(); dbsps.resize(1);
    for (int teeth_idx = num_teeth_-1; teeth_idx >= 0; --teeth_idx) {
        for (int seg_idx = all_teeth_boundary_segments[teeth_idx].size()-1; seg_idx >= 0; --seg_idx)
            dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(all_teeth_boundary_segments[teeth_idx][seg_idx],false));
        for (int seg_idx = 2; seg_idx >= 0; --seg_idx)
            dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(all_coils_boundary_segments_[teeth_idx][seg_idx],false));
    }
    dbsps.push_back({std::make_shared<DirectedBoundarySegment>(stator_outer_rim,true)});
    yoke_ = p.addDomain("yoke",dbsps,mu_iron,0.0 /* J */);
}

void PMSM::addReferenceDomains(ProblemWrapper& p) const {
    // Reference boundaries
    const auto interior_air_outer_rim_r = p.addCircularBoundarySegment(dt1,            /* radius */
                                                                       Vec2D{0.0,0.0}, /* center */
                                                                       12,             /* num_nodes */
                                                                       1,              /* boundary_solution_polynomial_order */
                                                                       true,           /* normal_points_to_the_right */
                                                                       true            /* is_reference */);
    vector<boundary_segment_ptr> reference_segments_1;
    vector<boundary_segment_ptr> reference_segments_2;
    vector<boundary_segment_ptr> reference_segments_3;
    vector<boundary_segment_ptr> reference_segments_4;
    vector<boundary_segment_ptr> reference_segments_5;
    for (uint coil_index = 0; coil_index < num_teeth_; ++coil_index) {
        Vec2D        corner = all_coils_boundary_segments_[coil_index][0]->getEndPoint();
        const double phi1   = std::atan2(corner.y,corner.x);
        reference_segments_1.push_back(p.addStraightBoundarySegment(corner,                                    /* radius */
                                                                    Vec2D{stator_outer_radius*std::cos(phi1),
                                                                          stator_outer_radius*std::sin(phi1)}, /* center */
                                                                    2,                                         /* num_nodes */
                                                                    2,                                         /* boundary_solution_polynomial_order */
                                                                    true,                                      /* normal_points_to_the_right */
                                                                    true                                       /* is_reference */));
        corner        = all_coils_boundary_segments_[coil_index][1]->getEndPoint();
        double phi2   = std::atan2(corner.y,corner.x);
        reference_segments_2.push_back(p.addStraightBoundarySegment(corner,                                    /* radius */
                                                                    Vec2D{stator_outer_radius*std::cos(phi2),
                                                                          stator_outer_radius*std::sin(phi2)}, /* center */
                                                                    2,                                         /* num_nodes */
                                                                    2,                                         /* boundary_solution_polynomial_order */
                                                                    true,                                      /* normal_points_to_the_right */
                                                                    true                                       /* is_reference */));
        if (phi2 < phi1) phi2 += 2.0*M_PI;
        reference_segments_3.push_back(p.addArcedBoundarySegment(stator_outer_radius, /* radius */
                                                                 Vec2D{0.0,0.0},      /* center */
                                                                 phi1,                /* phi_min */
                                                                 phi2,                /* phi_max */
                                                                 2,                   /* num_nodes */
                                                                 2,                   /* boundary_solution_polynomial_order */
                                                                 true,                /* normal_points_to_the_right */
                                                                 true                 /* is_reference */));
        reference_segments_4.push_back(p.addStraightBoundarySegment(all_coils_boundary_segments_[coil_index][2]->getEndPoint(),                  /* radius */
                                                                    all_coils_boundary_segments_[(coil_index+1)%num_teeth_][0]->getStartPoint(), /* center */
                                                                    2,                                                                           /* num_nodes */
                                                                    2,                                                                           /* boundary_solution_polynomial_order */
                                                                    true,                                                                        /* normal_points_to_the_right */
                                                                    true                                                                         /* is_reference */));
        reference_segments_5.push_back(p.addArcedBoundarySegment(stator_outer_radius,      /* radius */
                                                                 Vec2D{0.0,0.0},           /* center */
                                                                 phi2-2.0*M_PI/num_teeth_, /* phi_min */
                                                                 phi1,                     /* phi_max */
                                                                 2,                        /* num_nodes */
                                                                 2,                        /* boundary_solution_polynomial_order */
                                                                 true,                     /* normal_points_to_the_right */
                                                                 true                      /* is_reference */));
    }

    /* Add reference domains */
    vector<vector<directed_boundary_segment_ptr>> dbsps;
    p.addReferenceDomain(std::const_pointer_cast<const Domain>(inner_air_),
                         {{std::make_shared<DirectedBoundarySegment>(rotor_outer_rim_,false)},
                          {std::make_shared<DirectedBoundarySegment>(interior_air_outer_rim_r,true)}},
                         {1/*type*/});
    for (uint coil_index = 0; coil_index < num_teeth_; ++coil_index) {
        dbsps.clear(); dbsps.resize(1);
        dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(all_coils_boundary_segments_[(coil_index-1+num_teeth_)%num_teeth_][2],false));
        dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_2[(coil_index-1+num_teeth_)%num_teeth_],true));
        dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_5[coil_index],true));
        dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_1[coil_index],false));
        dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(all_coils_boundary_segments_[coil_index][0],false));
        dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_4[(coil_index-1+num_teeth_)%num_teeth_],false));
        p.addReferenceDomain(yoke_,dbsps,{3/*type*/});
    }
    for (uint coil_index = 0; coil_index < num_teeth_; ++coil_index) {
        dbsps.clear(); dbsps.resize(1);
        dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_1[coil_index],true));
        dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_3[coil_index],true));
        dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(reference_segments_2[coil_index],false));
        dbsps[0].push_back(std::make_shared<DirectedBoundarySegment>(all_coils_boundary_segments_[coil_index][1],false));
        p.addReferenceDomain(yoke_,dbsps,{3/*type*/});
    }
}
