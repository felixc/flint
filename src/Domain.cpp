#include <cmath>
#include <iostream>
#include "Domain.hpp"
#include "CircularBoundarySegment.hpp"

/* Type definitions */
template <typename T>
using vector = std::vector<T>;
template <class... Types>
using tuple  = std::tuple<Types...>;
using string = std::string;

Domain::Domain(const string& name, const initializer_list<vector<directed_boundary_segment_ptr>> dbsps, const DomainOptions options) :
    Domain(name,vector<vector<directed_boundary_segment_ptr>>{dbsps},options)
{}

Domain::Domain(const string& name, const vector<vector<directed_boundary_segment_ptr>>& dbsps, const DomainOptions options) :
    name_(name),
    mu_(options.mu),
    J_(options.J),
    directed_boundary_segment_ptrs_(dbsps),
    is_reference_(options.is_reference),
    type_(options.type),
    filter_(options.filter)
{
    if (!options.is_reference) {
        for (const auto& dbsps_ : dbsps)
            for (const directed_boundary_segment_ptr& dbsp : dbsps_) {
                (**dbsp).addNeighbouringDomain(this);
                dbsp->setAssociatedDomain(this);
            }
    }
}

void Domain::setDomainIndex(const uint domain_index) {
    domain_index_ = domain_index;
}

uint Domain::getDomainIndex() const {
    return domain_index_;
}

const vector<vector<directed_boundary_segment_ptr>>& Domain::getDirectedBoundarySegmentPointers() const {
    return directed_boundary_segment_ptrs_;
}

const DirectedBoundarySegment& Domain::getDirectedBoundarySegment(const uint boundary_segment_index) const {
    for (const auto& dbsps : directed_boundary_segment_ptrs_)
        for (const directed_boundary_segment_ptr dbsp : dbsps)
            if ((**dbsp).getBoundarySegmentIndex() == boundary_segment_index)
                return *dbsp;
    throw std::runtime_error("Invalid boundary_segment_index provided to getDirectedBoundarySegment.");
}

tuple<uint,uint> Domain::getDirectedBoundarySegmentIndex(const uint boundary_segment_index) const {
    for (uint loop_idx = 0; loop_idx < directed_boundary_segment_ptrs_.size(); ++loop_idx)
        for (uint dir_bnd_seg_idx = 0; dir_bnd_seg_idx < directed_boundary_segment_ptrs_[loop_idx].size(); ++dir_bnd_seg_idx)
            if ((**directed_boundary_segment_ptrs_[loop_idx][dir_bnd_seg_idx]).getBoundarySegmentIndex() == boundary_segment_index)
                return {loop_idx,dir_bnd_seg_idx};
    throw std::runtime_error("Invalid boundary_segment_index provided to getDirectedBoundarySegmentIndex.");
}

string Domain::getName() const {
    return name_;
}

double Domain::getmu() const {
    return mu_;
}

double Domain::getJ() const {
    return J_;
}

bool Domain::isReference() const {
    return is_reference_;
}

int Domain::getType() const {
    return type_;
}

uint Domain::getLoopIndex(const directed_boundary_segment_ptr& dir_bnd_seg_ptr) const {
    for (uint loop_idx = 0; loop_idx < directed_boundary_segment_ptrs_.size(); ++loop_idx)
        for (const auto& dir_bnd_seg_ptr_ : directed_boundary_segment_ptrs_[loop_idx])
            if (dir_bnd_seg_ptr_ == dir_bnd_seg_ptr)
                return loop_idx;
    std::cout << "Invalid argument provided to Domain::getLoopIndex." << std::endl;
    abort();
}

double Domain::getApproximateArea() const {
    if (type_ == 1) {
        const double radius_1 = dynamic_cast<CircularBoundarySegment*>(&**getDirectedBoundarySegmentPointers()[0][0])->getRadius();
        const double radius_2 = dynamic_cast<CircularBoundarySegment*>(&**getDirectedBoundarySegmentPointers()[1][0])->getRadius();
        return M_PI*std::abs(radius_1*radius_1-radius_2*radius_2);
    } else if (type_ == 2) {
        const double radius = dynamic_cast<CircularBoundarySegment*>(&**getDirectedBoundarySegmentPointers()[0][0])->getRadius();
        return M_PI*radius*radius;
    } else if (type_ == 3 || type_ == 4) {
        // Define variables for convenience
        const auto& directed_boundary_segment_ptrs = getDirectedBoundarySegmentPointers()[0];
        const uint  num_boundary_segments          = directed_boundary_segment_ptrs.size();

        // Compute the area of the domain using the formula from here:
        // https://mathworld.wolfram.com/PolygonArea.html
        double area = 0.0;
        for (uint bnd_seg_idx = 0; bnd_seg_idx < num_boundary_segments; ++bnd_seg_idx) {
            const auto dir_bnd_seg = *directed_boundary_segment_ptrs[bnd_seg_idx];
            const double t_step    = 0.0005 / (**directed_boundary_segment_ptrs[bnd_seg_idx]).getLength();
                  double t         = 0.0;
            while (t < 1.0) {
                Vec2D v1 = dir_bnd_seg.getBoundaryPoint(t);
                Vec2D v2 = dir_bnd_seg.getBoundaryPoint(std::min(1.0,t+t_step));
                area    += v1.x*v2.y - v2.x*v1.y;
                t       += t_step;
            }
        }
        return 0.5*std::abs(area);
    } else {
        std::cout << "Domain::getApproximateArea is not implemented for domains of type " << type_ << "." << std::endl;
        abort();
    }
}

const DomainOptions::FilterType& Domain::getFilter() const {
    return filter_;
}
