#include "BoundarySegmentInformation.hpp"

BoundarySegmentInformation::BoundarySegmentInformation(const c_boundary_segment_ptr b_seg_ptr, const c_domain_ptr dom_ptr) :
    b_seg_ptr_(b_seg_ptr),
    dom_ptr_(dom_ptr)
{}

double BoundarySegmentInformation::getLength() const {
    return b_seg_ptr_->getLength();
}

uint BoundarySegmentInformation::getNumNodes() const {
    return numNodes_;
}

void BoundarySegmentInformation::setNumNodes(const uint numNodes) {
    numNodes_ = numNodes;
}
