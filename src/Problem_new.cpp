#include <algorithm>
#include <tuple>
#include <utility>
#include <thread>
#include <mutex>
#include <atomic>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <chrono>
#include <gsl/gsl_integration.h>
#include "Problem.hpp"
#include "Vec2D.hpp"

/* Type definitions */
template <class... Types>
using tuple = std::tuple<Types...>;
template <typename U, typename T>
using pair  = std::pair<U,T>;

//#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 1e-6//1e-8
//#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-4//1e-6
#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 1e-8
#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-6
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          4000
#define OUTER_INTEGRAL_ADAPTIV // Undefine this to use Gauss-Legendre quadrature for the outer integrals instead of adaptive quadrature
#ifndef OUTER_INTEGRAL_ADAPTIV
#define OUTER_QUADRATURE_ORDER        1000
#endif

// Parameters
static constexpr double mu0         = 4.0*M_PI*1e-7;
static constexpr bool   use_b_nabla = false;

// A function that evaluates monomials
static double mono(const double t, const uint p) {
    if (t == 0.0 && p == 0) return 1.0;
    else if (t == 0.0) return 0.0;
    double c = 1.0;
    for (uint i = 0; i < p; ++i) c *= t;
    return c;
};

// Functions to compute the fundamental solution and its derivatives
static double E(const Vec2D& x) {
    return std::log(x.Norm2())/2.0/M_PI;
};
static const Vec2D GradE(const Vec2D& x) {
    return Vec2D{x.x,x.y}/(2.0*M_PI*x.Norm2Squared());
};
static const Vec2D CurlE(const Vec2D& x) {
    return Vec2D{x.y,-x.x}/(2.0*M_PI*x.Norm2Squared());
};

double getFundamentalSolution(const double                                           tx_jk,
                              const Vec2D&                                           x,
                              const Domain&                                          domain,
                              const tuple<uint,uint>                                 directed_boundary_segment_index,
                              const uint                                             subsegment_index,
                              const Vec2D                                            xi,
                              const std::function<double(const double,const double)> quad_inner,
                              gsl_function&                                          gsl_function_inner,
                              gsl_integration_workspace*                             gsl_workspace_inner) {
    // Check that the boundary has no holes
    if (domain.getDirectedBoundarySegmentPointers().size() != 1) {
        std::cout << "So far, current carrying domains must have a single boundary loop." << std::endl;
        abort();
    }
    
    // Create a lambda defining the integrand of integrals along boundary subsegments
    const auto boundarySubsegmentIntegral = [](const double t, void* params)-> double {
        const auto& psi_i        = *static_cast<const std::function<Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
        const auto& dpsi_i_2Norm = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[1]));
        const auto& nu_          = *static_cast<const std::function<Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[3]));
    
        const Vec2D& x           = *static_cast<const Vec2D*>(static_cast<void*>(reinterpret_cast<double**>(params)[2]));
        const Vec2D  y           = psi_i(t);
        const Vec2D  nu          = nu_(t);
        const Vec2D& xi          = *static_cast<const Vec2D*>(static_cast<void*>(reinterpret_cast<double**>(params)[4]));
    
        return -xi.dot(nu)*E(y-x)*dpsi_i_2Norm(t);
    };
    
    // Create a lambda to integrate along boundary subsegments
    const auto getBoundarySubsegmentIntegral = [&boundarySubsegmentIntegral,&quad_inner,&x,&xi,
                                                &gsl_function_inner,gsl_workspace_inner](const std::function<Vec2D(const double)>  psi_i,
                                                                                         const std::function<double(const double)> dpsi_i_2Norm,
                                                                                         const std::function<Vec2D(const double)>  nu,
                                                                                         const double                              a,
                                                                                         const double                              b,
                                                                                         const bool                                has_singularity)->double {
        gsl_function_inner.function = boundarySubsegmentIntegral;
        const double** params     = new const double*[5];
        params[0]                 = reinterpret_cast<const double*>(&psi_i);
        params[1]                 = reinterpret_cast<const double*>(&dpsi_i_2Norm);
        params[2]                 = reinterpret_cast<const double*>(&x);
        params[3]                 = reinterpret_cast<const double*>(&nu);
        params[4]                 = reinterpret_cast<const double*>(&xi);
        gsl_function_inner.params = params;
        double result;
        //if (has_singularity) result = quad_inner_s(a,b); // qags does worse than qag in terms of accuracy of the solution.
        if (has_singularity) result = quad_inner(a,b);
        else                 result = quad_inner(a,b);
        delete[] params;
        return result;
    };
    
    // Get the pointers to the directed boundary segments of the domain
    const auto& directed_boundary_segment_ptrs = domain.getDirectedBoundarySegmentPointers()[0];
    
    //const double epsilon = 0.0;//1.0e-8;
    const double epsilon = 1.0e-8; // TODO: Check this again
    double integral = 0.0;
    for (uint dir_bnd_seg_idx = std::get<1>(directed_boundary_segment_index); dir_bnd_seg_idx < directed_boundary_segment_ptrs.size(); ++dir_bnd_seg_idx) {
        const DirectedBoundarySegment& dir_bnd_seg = *directed_boundary_segment_ptrs[dir_bnd_seg_idx];
        const uint subseg_str_idx = dir_bnd_seg_idx == std::get<1>(directed_boundary_segment_index) ? subsegment_index : 0;
        for (uint subseg_idx = subseg_str_idx; subseg_idx < directed_boundary_segment_ptrs[dir_bnd_seg_idx]->getNumSubsegments(); ++subseg_idx) {
            if (dir_bnd_seg_idx == std::get<1>(directed_boundary_segment_index) && subseg_idx == subsegment_index) {
                integral += getBoundarySubsegmentIntegral(dir_bnd_seg.getpsii(subseg_idx),
                                                          dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                          dir_bnd_seg.getnu(subseg_idx),
                                                          tx_jk+epsilon,
                                                          1.0,
                                                          true /* has singularity */);
            } else {
                integral += getBoundarySubsegmentIntegral(dir_bnd_seg.getpsii(subseg_idx),
                                                          dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                          dir_bnd_seg.getnu(subseg_idx),
                                                          0.0,
                                                          1.0,
                                                          false /* has_singularity */);
            }
        }
    }
    for (uint dir_bnd_seg_idx = 0; dir_bnd_seg_idx <= std::get<1>(directed_boundary_segment_index); ++dir_bnd_seg_idx) {
        const DirectedBoundarySegment& dir_bnd_seg = *directed_boundary_segment_ptrs[dir_bnd_seg_idx];
        const uint subseg_end_idx = dir_bnd_seg_idx == std::get<1>(directed_boundary_segment_index) ? subsegment_index : directed_boundary_segment_ptrs[dir_bnd_seg_idx]->getNumSubsegments()-1;
        for (uint subseg_idx = 0; subseg_idx <= subseg_end_idx; ++subseg_idx) {
            if (dir_bnd_seg_idx == std::get<1>(directed_boundary_segment_index) && subseg_idx == subsegment_index) {
                integral += getBoundarySubsegmentIntegral(dir_bnd_seg.getpsii(subseg_idx),
                                                          dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                          dir_bnd_seg.getnu(subseg_idx),
                                                          0.0,
                                                          tx_jk-epsilon,
                                                          true /* has_singularity */);
            } else {
                integral += getBoundarySubsegmentIntegral(dir_bnd_seg.getpsii(subseg_idx),
                                                          dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                          dir_bnd_seg.getnu(subseg_idx),
                                                          0.0,
                                                          1.0,
                                                          false /* has_singularity */);
            }
        }
    }
    
    return integral;
}

void Problem::solve(const SolverOptions options) {
    /* Check that all non-reference boundary segments have two neighbouring domains */
    for (const c_boundary_segment_ptr& bsp : boundary_segment_ptrs_)
        if (!bsp->isReference())
            if (bsp->getNumNeighbouringDomains() != 2u) {
                std::cout << "Encountered a boundary segment which does not have two neighbouring domains." << std::endl;
                abort();
            }

    /* Initialize helper variables */
    initializeHelperVariables();

    /* Check if the solution shall be loaded from the file system */
    if (options.load_solution) {
        load_solution(options.folder_name);
        return;
    }

    /* Allocate memory for the system matrix and the RHS */
    const unique_mat_ptr sys_mat_ptr = allocateSystemMatrix();
    const unique_vec_ptr rhs_ptr     = allocateRHS();

    /* Determine the boundary segments for which boundary conditions must be formulated and create tasks */
    vector<tuple<c_boundary_segment_ptr,uint>> tasks;
    for (auto [bnd_seg_idx,sys_mat_row] = tuple<uint,uint>{0u,0u}; bnd_seg_idx < boundary_segment_ptrs_.size(); ++bnd_seg_idx)
        if (!boundary_segment_ptrs_[bnd_seg_idx]->isReference()) {
            tasks.emplace_back(boundary_segment_ptrs_[bnd_seg_idx],sys_mat_row);
            sys_mat_row += (boundary_segment_ptrs_[bnd_seg_idx]->getFunctionApproximationPolynomialOrder()+1u)*boundary_segment_ptrs_[bnd_seg_idx]->getNumSubsegments()*2u;
        }

    // Reverse the order of the tasks such that they are completed in the same order as they were created. For debugging purposes only.
    std::reverse(tasks.begin(),tasks.end());

    // Variable that keeps track of the number of tasks that remain
    std::atomic<uint> remaining_tasks(tasks.size());

    // Create a mutex for the tasks
    std::mutex task_mutex;

    // Create a lambda that creates worker handles
    const auto get_worker_handle =[&tasks,
                                   &task_mutex,
                                   &remaining_tasks,
                                   &sys_mat_ptr,
                                   &domain_system_matrix_column_offsets=this->domain_system_matrix_column_offsets_,
                                   &domain_ptrs=this->domain_ptrs_,
                                   &rhs_ptr](const bool is_main_thread) {
        return [&remaining_tasks,&task_mutex,&tasks,is_main_thread,&sys_mat_ptr,&domain_system_matrix_column_offsets,&domain_ptrs,&rhs_ptr]() {
            // Allocate GSL workspaces and functions
            gsl_function                   gsl_function_inner;
            gsl_function                   gsl_function_outer;
            gsl_integration_workspace*     gsl_workspace_inner = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
            gsl_integration_workspace*     gsl_workspace_outer = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);

            // Create a wrappers to simplify integration
            const auto quad_adpt = [](gsl_function& fun, gsl_integration_workspace* workspace, const double a, const double b)->double {
                double result,error;
                gsl_integration_qag(&fun, /* Function to integrate */
                                    a,    /* Lower integration limit */
                                    b,    /* Upper integration limit */
                                    INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                                    INTEGRAL_RELATIVE_ERROR_LIMIT,
                                    MAX_NUM_SUBINTERVALS,
                                    INTEGRATION_KEY,
                                    workspace, &result, &error);
                return result;
            };
            const auto quad_outer = [&quad_adpt,&gsl_function_outer,gsl_workspace_outer]()->double {
                return quad_adpt(gsl_function_outer,gsl_workspace_outer,0.0,1.0);
            };
            const auto quad_inner = [&quad_adpt,&gsl_function_inner,gsl_workspace_inner](const double a, const double b)->double {
                return quad_adpt(gsl_function_inner,gsl_workspace_inner,a,b);
            };

            // Create a lambda for the operators W_t and W_n which integrate only over \Psi
            const auto WtnPsi = [&quad_inner,&gsl_function_inner,gsl_workspace_inner](const Vec2D                                     tn_vec_jk,
                                                                                      const Vec2D                                     x,
                                                                                      const uint                                      l,
                                                                                      const std::function<const Vec2D(const double)>& psi_i_jk,
                                                                                      const std::function<double(const double)>&      dpsi_i_jk_2Norm,
                                                                                      const bool                                      has_singularity,
                                                                                      const double                                    singularity)->double {
                gsl_function_inner.function = [](const double t, void* params)->double {
                    const auto& m_l             = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
                    const auto& psi_i_jk        = *static_cast<const std::function<const Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[1]));
                    const auto& dpsi_i_jk_2Norm = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[2]));

                    const Vec2D& x   = *static_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[3]));
                    const Vec2D  y   = psi_i_jk(t);

                    const Vec2D& tn_vec_jk = *static_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[4]));

                    return GradE(x-y).dot(tn_vec_jk)*m_l(t)*dpsi_i_jk_2Norm(t);
                };
                std::function<double(const double t)> m_l = [l](const double t)->double {
                    return mono(t,l);
                };

                const double** params     = new const double*[5];
                params[0]                 = reinterpret_cast<const double*>(&m_l);
                params[1]                 = reinterpret_cast<const double*>(&psi_i_jk);
                params[2]                 = reinterpret_cast<const double*>(&dpsi_i_jk_2Norm);
                params[3]                 = reinterpret_cast<const double*>(&x);
                params[4]                 = reinterpret_cast<const double*>(&tn_vec_jk);
                gsl_function_inner.params = params;

                double result;
                if (has_singularity) {
                    result  = quad_inner(0.0,std::max(0.0,singularity-1e-2));
                    result += quad_inner(std::min(singularity+1e-2,1.0),1.0);
                } else result = quad_inner(0.0,1.0);
                delete[] params;
                return   result;
            };

            // Create lambdas for the outer integrals
            const auto outerIntegralLHS = [&quad_outer,
                                           &gsl_function_outer](const uint                                                           lp,
                                                                const std::function<const Vec2D(const double)>&                      nt_vec_jk,
                                                                const std::function<Vec2D(const double)>&                            psi_ip_k,
                                                                const std::function<double(const double)>&                           dpsi_ip_k_2Norm,
                                                                const std::function<double(const Vec2D&,const Vec2D&,const double)>& Wop,
                                                                const bool                                                           has_singularity,
                                                                const bool                                                           directed_boundary_segments_have_same_direction)->double {
                gsl_function_outer.function = [](const double t, void* params)->double {
                    const auto& m_lp                 = *static_cast<std::function<double(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[0]));
                    const auto& nt_vec_jk            = *static_cast<std::function<Vec2D(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[1]));
                    const auto& psi_ip_k             = *static_cast<std::function<Vec2D(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[2]));
                    const auto& dpsi_ip_k_2Norm      = *static_cast<std::function<double(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[3]));
                    const auto& Wop                  = *static_cast<std::function<double(const Vec2D&,const Vec2D&,const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[4]));
                    const bool& has_singularity      = *static_cast<bool*>(static_cast<void*>(reinterpret_cast<double**>(params)[5]));
                    double singularity;
                    if (has_singularity) singularity = *static_cast<bool*>(static_cast<void*>(reinterpret_cast<double**>(params)[6])) ? t : 1-t;
                    return m_lp(t)*Wop(nt_vec_jk(t),psi_ip_k(t),singularity)*dpsi_ip_k_2Norm(t);
                };
                std::function<double(const double t)> m_lp = [lp](const double t)->double {
                    return mono(t,lp);
                };
                const double** params          = new const double*[has_singularity ? 7 : 6];
                params[0]                      = reinterpret_cast<const double*>(&m_lp);
                params[1]                      = reinterpret_cast<const double*>(&nt_vec_jk);
                params[2]                      = reinterpret_cast<const double*>(&psi_ip_k);
                params[3]                      = reinterpret_cast<const double*>(&dpsi_ip_k_2Norm);
                params[4]                      = reinterpret_cast<const double*>(&Wop);
                params[5]                      = reinterpret_cast<const double*>(&has_singularity);
                if (has_singularity) params[6] = reinterpret_cast<const double*>(&directed_boundary_segments_have_same_direction);
                gsl_function_outer.params = params;
                const double result       = quad_outer();
                delete[] params;
                return   result;
            };
            const auto outerIntegralRHS = [&quad_outer,
                                           &gsl_function_outer](const uint                                             lp,
                                                                const std::function<Vec2D(const double)>&              psi_ip_k,
                                                                const std::function<double(const double)>&             dpsi_ip_k_2Norm,
                                                                const std::function<double(const double,const Vec2D)>& c_fun)->double {
                gsl_function_outer.function = [](const double t, void* params)->double {
                    const auto& m_lp                  = *static_cast<std::function<double(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[0]));
                    const auto& psi_ip_k              = *static_cast<std::function<Vec2D(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[1]));
                    const auto& dpsi_ip_k_2Norm       = *static_cast<std::function<double(const double)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[2]));
                    const auto& c_fun                 = *static_cast<std::function<double(const double, const Vec2D)>*>(static_cast<void*>(reinterpret_cast<double**>(params)[3]));

                    return m_lp(t)*c_fun(t,psi_ip_k(t))*dpsi_ip_k_2Norm(t);
                };
                std::function<double(const double t)> m_lp = [lp](const double t)->double {
                    return mono(t,lp);
                };
                const double** params     = new const double*[4];
                params[0]                 = reinterpret_cast<const double*>(&m_lp);
                params[1]                 = reinterpret_cast<const double*>(&psi_ip_k);
                params[2]                 = reinterpret_cast<const double*>(&dpsi_ip_k_2Norm);
                params[3]                 = reinterpret_cast<const double*>(&c_fun);
                gsl_function_outer.params = params;
                const double result       = quad_outer();
                delete[] params;
                return   result;
            };

            // Create a lambda to evaluate the fundamental solution on the boundary of a domain
            const auto getFundamentalSolution = [&quad_inner,
                                                 &gsl_function_inner,
                                                 gsl_workspace_inner](const double           tx_jk,
                                                                      const Vec2D&           x,
                                                                      const Domain&          domain,
                                                                      const tuple<uint,uint> directed_boundary_segment_index,
                                                                      const uint             subsegment_index,
                                                                      const Vec2D            xi)->double {
                return ::getFundamentalSolution(tx_jk,x,domain,directed_boundary_segment_index,subsegment_index,xi,quad_inner,gsl_function_inner,gsl_workspace_inner);
            };

            // Create a lambda which can be used to complete a task
            const auto do_task = [&outerIntegralLHS,&WtnPsi,&gsl_function_inner,gsl_workspace_inner,
                                  &sys_mat_ptr,&domain_system_matrix_column_offsets,&domain_ptrs,&rhs_ptr,&outerIntegralRHS,
                                  &getFundamentalSolution](const c_boundary_segment_ptr bnd_seg_ptr, uint sys_mat_row) {
                // Define variables for convenience
                const Domain& dom_0         = bnd_seg_ptr->getNeighbouringDomain(0);
                const auto&   dir_bnd_seg_0 = dom_0.getDirectedBoundarySegment(bnd_seg_ptr->getBoundarySegmentIndex());

                // sub_seg_idx_tst <==> subsegment index (test)
                for (uint sub_seg_idx_tst = 0; sub_seg_idx_tst < dir_bnd_seg_0.getNumSubsegments(); ++sub_seg_idx_tst) {
                    // mon_deg_tst <==> monomial degree (test)
                    for (uint mon_deg_tst = 0; mon_deg_tst <= (*dir_bnd_seg_0).getFunctionApproximationPolynomialOrder(); ++mon_deg_tst) {
                        // At this point, \xi is clearly defined

                        // Compute the contributions from both sides
                        for (uint i = 0u; i < 2u; ++i) {
                            // Define variables for convenience
                            const Domain& dom_bie             = bnd_seg_ptr->getNeighbouringDomain(i);
                            const auto&   dir_bnd_seg_ptrs    = dom_bie.getDirectedBoundarySegmentPointers();
                            const auto&   dir_bnd_seg         = dom_bie.getDirectedBoundarySegment(bnd_seg_ptr->getBoundarySegmentIndex());
                            uint          dom_ofs_idx         = 0u;
                            for (uint dom_idx = 0u; dom_idx < domain_ptrs.size(); ++dom_idx) {
                                if (domain_ptrs[dom_idx]->isReference()) continue;
                                if (&*domain_ptrs[dom_idx] == &dom_bie) break;
                                ++dom_ofs_idx;
                            }
                            const uint    dom_sys_mat_col_ofs = domain_system_matrix_column_offsets[dom_ofs_idx];
                            const double  mu_dom_bie          = dom_bie.getmu();

                            /* Compute the LHS */
                            uint var_cnt = 0;
                            for (uint loop_idx_bie = 0; loop_idx_bie < dir_bnd_seg_ptrs.size(); ++loop_idx_bie) {
                                for (uint dir_bnd_seg_ptr_idx_bie = 0; dir_bnd_seg_ptr_idx_bie < dir_bnd_seg_ptrs[loop_idx_bie].size(); ++dir_bnd_seg_ptr_idx_bie) {
                                    // Define vor convenience and clarity
                                    const DirectedBoundarySegment& dir_bnd_seg_bie = *dir_bnd_seg_ptrs[loop_idx_bie][dir_bnd_seg_ptr_idx_bie];

                                    for (uint sub_seg_idx_bie = 0; sub_seg_idx_bie < dir_bnd_seg_bie.getNumSubsegments(); ++sub_seg_idx_bie) {
                                        for (uint mon_deg_bie = 0; mon_deg_bie <= (*dir_bnd_seg_bie).getFunctionApproximationPolynomialOrder(); ++mon_deg_bie) {
                                            const bool has_singularity = &*dir_bnd_seg_0==&*dir_bnd_seg_bie &&
                                               dir_bnd_seg_0.getTrueSubsegmentIndex(sub_seg_idx_tst)==dir_bnd_seg_bie.getTrueSubsegmentIndex(sub_seg_idx_bie);
                                            const double ct = outerIntegralLHS(mon_deg_tst,
                                                                               [gett=dir_bnd_seg_0.gett(sub_seg_idx_tst),is_z=i==0u](const double t)->const Vec2D {
                                                                                   if (is_z) return  gett(t);
                                                                                   else      return -gett(t);
                                                                               },
                                                                               dir_bnd_seg_0.getpsii(sub_seg_idx_tst),
                                                                               dir_bnd_seg_0.getdpsii2Norm(sub_seg_idx_tst),
                                                                               [mon_deg_bie,
                                                                                &WtnPsi,
                                                                                psi_i_jk=dir_bnd_seg_bie.getpsii(sub_seg_idx_bie),
                                                                                dpsi_i_jk_2Norm=dir_bnd_seg_bie.getdpsii2Norm(sub_seg_idx_bie),
                                                                                has_singularity,
                                                                                &gsl_function_inner,
                                                                                gsl_workspace_inner](const Vec2D& t_vec_jk, const Vec2D& x, const double singularity)->double {
                                                                                   return WtnPsi(t_vec_jk,
                                                                                                 x,
                                                                                                 mon_deg_bie,
                                                                                                 psi_i_jk,
                                                                                                 dpsi_i_jk_2Norm,
                                                                                                 has_singularity,
                                                                                                 singularity);
                                                                               },
                                                                               has_singularity,
                                                                               dir_bnd_seg_0.isForward()==dir_bnd_seg_bie.isForward());
                                            const double cn = outerIntegralLHS(mon_deg_tst,
                                                                               [getnu=dir_bnd_seg_0.getnu(sub_seg_idx_tst),is_z=i==0u](const double t)->const Vec2D {
                                                                                   if (is_z) return  getnu(t);
                                                                                   else      return -getnu(t);
                                                                               },
                                                                               dir_bnd_seg_0.getpsii(sub_seg_idx_tst),
                                                                               dir_bnd_seg_0.getdpsii2Norm(sub_seg_idx_tst),
                                                                               [mon_deg_bie,
                                                                                &WtnPsi,
                                                                                psi_i_jk=dir_bnd_seg_bie.getpsii(sub_seg_idx_bie),
                                                                                dpsi_i_jk_2Norm=dir_bnd_seg_bie.getdpsii2Norm(sub_seg_idx_bie),
                                                                                has_singularity,
                                                                                &gsl_function_inner,
                                                                                gsl_workspace_inner](const Vec2D& n_vec_jk, const Vec2D& x, const double singularity)->double {
                                                                                    double result = WtnPsi(n_vec_jk,
                                                                                                           x,
                                                                                                           mon_deg_bie,
                                                                                                           psi_i_jk,
                                                                                                           dpsi_i_jk_2Norm,
                                                                                                           has_singularity,
                                                                                                           singularity);
                                                                                   if (has_singularity) result -= 0.5*mono(singularity,mon_deg_bie);
                                                                                   return result;
                                                                               },
                                                                               has_singularity,
                                                                               dir_bnd_seg_0.isForward()==dir_bnd_seg_bie.isForward());
                                            //std::cout << "cn=" << cn << std::endl;

                                            (*sys_mat_ptr)(sys_mat_row,    dom_sys_mat_col_ofs+var_cnt)    = -cn/mu0;
                                            (*sys_mat_ptr)(sys_mat_row,    dom_sys_mat_col_ofs+var_cnt+1u) = -ct/mu0;
                                            (*sys_mat_ptr)(sys_mat_row+1u, dom_sys_mat_col_ofs+var_cnt)    = -ct/mu_dom_bie; // <--- Here is an error
                                            (*sys_mat_ptr)(sys_mat_row+1u, dom_sys_mat_col_ofs+var_cnt+1u) = +cn/mu_dom_bie;
                                            if (use_b_nabla) {
                                                (*sys_mat_ptr)(sys_mat_row,    dom_sys_mat_col_ofs+var_cnt+2u) = +ct/mu0;
                                                (*sys_mat_ptr)(sys_mat_row,    dom_sys_mat_col_ofs+var_cnt+3u) = -cn/mu0;
                                                (*sys_mat_ptr)(sys_mat_row+1u, dom_sys_mat_col_ofs+var_cnt+2u) = -cn/mu_dom_bie;
                                                (*sys_mat_ptr)(sys_mat_row+1u, dom_sys_mat_col_ofs+var_cnt+3u) = -ct/mu_dom_bie;
                                            }
                                            if (use_b_nabla) var_cnt += 4u;
                                            else             var_cnt += 2u;
                                        }
                                    }
                                }
                            }

                            /* Compute the RHS */
                            // c_{1,k}
                            (*rhs_ptr)(sys_mat_row) += dom_bie.getJ() == 0.0 ? 0.0 :
                                                       outerIntegralRHS(mon_deg_tst,
                                                                        bnd_seg_ptr->getpsii(sub_seg_idx_tst),
                                                                        bnd_seg_ptr->getdpsii2Norm(sub_seg_idx_tst),
                                                                        [&getFundamentalSolution,
                                                                         &dom_bie,
                                                                         &bnd_seg_ptr,
                                                                         &dir_bnd_seg,
                                                                         sub_seg_idx_tst,
                                                                         gett=[&dir_bnd_seg_0,gett=dir_bnd_seg_0.gett(dir_bnd_seg_0.isForward() ? sub_seg_idx_tst :
                                                                         dir_bnd_seg_0.getNumSubsegments()-1u-sub_seg_idx_tst)](const double t)->const Vec2D {
                                                                            if (dir_bnd_seg_0.isForward()) return gett(t);
                                                                            else                           return gett(1.0-t);
                                                                         }](const double t, const Vec2D x)->double {
                                                                            const Vec2D xi = gett(t);
                                                                            return getFundamentalSolution(dir_bnd_seg.isForward() ? t : 1.0-t,
                                                                                                          x,
                                                                                                          dom_bie,
                                                                                                          dom_bie.getDirectedBoundarySegmentIndex(bnd_seg_ptr->getBoundarySegmentIndex()),
                                                                                                          dir_bnd_seg.isForward() ? sub_seg_idx_tst :
                                                                                                                                    dir_bnd_seg.getNumSubsegments()-1u-sub_seg_idx_tst,
                                                                                                          xi)*dom_bie.getmu()*dom_bie.getJ();
                                                                        })/mu0*(i==0u ? -1.0 : 1.0);
                            // c_{2,k}
                            (*rhs_ptr)(sys_mat_row+1) += outerIntegralRHS(mon_deg_tst,
                                                                          bnd_seg_ptr->getpsii(sub_seg_idx_tst),
                                                                          bnd_seg_ptr->getdpsii2Norm(sub_seg_idx_tst),
                                                                          [&getFundamentalSolution,
                                                                           &dom_bie,
                                                                           &bnd_seg_ptr,
                                                                           &dir_bnd_seg,
                                                                           sub_seg_idx_tst,
                                                                           getnu=[&dir_bnd_seg_0,getnu=dir_bnd_seg_0.getnu(dir_bnd_seg_0.isForward() ? sub_seg_idx_tst :
                                                                           dir_bnd_seg_0.getNumSubsegments()-1u-sub_seg_idx_tst)](const double t)->const Vec2D {
                                                                               if (dir_bnd_seg_0.isForward()) return getnu(t);
                                                                               else                           return getnu(1.0-t);
                                                                           },
                                                                           surface_current_density=i==0u ? bnd_seg_ptr->getSurfaceCurrentDensity() : 0.0](const double t, const Vec2D x)->double {
                                                                             const Vec2D xi = getnu(t);
                                                                             double fs = dom_bie.getJ() == 0.0 ? 0.0 :
                                                                                 getFundamentalSolution(dir_bnd_seg.isForward() ? t : 1.0-t,
                                                                                                        x,
                                                                                                        dom_bie,
                                                                                                        dom_bie.getDirectedBoundarySegmentIndex(bnd_seg_ptr->getBoundarySegmentIndex()),
                                                                                                        dir_bnd_seg.isForward() ? sub_seg_idx_tst :
                                                                                                                                  dir_bnd_seg.getNumSubsegments()-1u-sub_seg_idx_tst,
                                                                                                        xi)*dom_bie.getJ();
                                                                             return fs + surface_current_density;
                                                                         })*(i==1u ? -1.0 : 1.0);
                        }
                        sys_mat_row += 2;
                    }
                }
            };

            // Actually complete the tasks, i.e. compute the LHS and RHS of the linear system
            int current_completion_percentage = -1;
            while (remaining_tasks.load() > 0) {
                if (task_mutex.try_lock()) {
                    // Visualize the progress
                    if (is_main_thread) {
                        const int completion_percentage = (100.0*(tasks.size()-remaining_tasks.load()))/tasks.size();
                        if (completion_percentage > current_completion_percentage) {
                            std::cout << "\33[2K\rFormulating the linear system: " << completion_percentage << "%" << std::flush;
                            current_completion_percentage = completion_percentage;
                        }
                    }

                    // Get the current boundary segment and release the mutex
                    const auto task = tasks.back();
                    tasks.pop_back();
                    remaining_tasks.store(remaining_tasks.load()-1);
                    task_mutex.unlock();

                    // Complete the task
                    std::apply(do_task,task);
                }
            }

            // Free the memory of the GSL workspaces and the quadrature weights
            gsl_integration_workspace_free(gsl_workspace_inner);
            gsl_integration_workspace_free(gsl_workspace_outer);
        };
    };

    // Determine of the number of concurrent threads that are supported
    uint max_num_threads = std::thread::hardware_concurrency();
    if (max_num_threads == 0) max_num_threads = 1;
    // TODO: Fix this
    //max_num_threads = 1;

    // Measure the time it takes to compute the matrix entries
    auto start_time = std::chrono::steady_clock::now();

    // Create worker threads
    vector<std::thread> worker_threads;
    for (uint thread_idx = 0; thread_idx < max_num_threads; ++thread_idx) {
        worker_threads.emplace_back(get_worker_handle(thread_idx==0 /* is_main_thread */));
    }

    // Wait until all workers threads completed execution
    for (uint thread_idx = 0; thread_idx < max_num_threads; ++thread_idx) {
        worker_threads[thread_idx].join();
    }

    /* Allocate memory for E */

    // First, determine the dimensions of \hat{x}_i
    uint E_col_cnt = 0;
    vector<vector<uint>> hatxi_dims(domain_ptrs_.size());
    vector<vector<uint>> xi_dims(domain_ptrs_.size());
    for (uint dom_idx = 0; dom_idx < domain_ptrs_.size(); ++dom_idx) {
        // Reference domains don't have associated problem variables
        if (domain_ptrs_[dom_idx]->isReference()) continue;

        // Define for convenience and clarity
        const auto dir_bnd_seg_ptrs = domain_ptrs_[dom_idx]->getDirectedBoundarySegmentPointers();

        for (uint loop_idx = 0; loop_idx < dir_bnd_seg_ptrs.size(); ++loop_idx) {
            // Count the number of unknowns, equations and linear constraints
            uint unknowns           = 0;
            uint linear_constraints = 0;

            for (uint dir_bnd_seg_idx = 0; dir_bnd_seg_idx < dir_bnd_seg_ptrs[loop_idx].size(); ++dir_bnd_seg_idx) {
                // Define for convenience and clarity
                const directed_boundary_segment_ptr& dbsp = dir_bnd_seg_ptrs[loop_idx][dir_bnd_seg_idx];

                for (uint subseg_idx = 0; subseg_idx < dbsp->getNumSubsegments(); ++subseg_idx) {
                    // Define for convenience and clarity
                    const uint poly_order = (**dbsp).getFunctionApproximationPolynomialOrder();

                    if (poly_order == 1) {
                        unknowns += poly_order+1;
                    } else {
                        std::cout << "This is not implemented yet." << std::endl;
                        abort();
                    }
                }

                // dbsp->getNumSubsegments() many constraints come from the requirement of the tangential and the normal derivative being continuous, respectively
                linear_constraints += dbsp->getNumSubsegments();
            }

            // Add columns for this domain
            hatxi_dims[dom_idx].push_back(unknowns-linear_constraints);
            xi_dims[dom_idx].push_back(unknowns);
            if (use_b_nabla) E_col_cnt += 4*hatxi_dims[dom_idx].back();
            else             E_col_cnt += 2*hatxi_dims[dom_idx].back();
        }
    }

    // Actually allocate memory for E
    MatrixXd E = MatrixXd::Zero(sys_mat_ptr->cols(),E_col_cnt);

    /* Compute E */
    /* 
     * x     = Er*\hat{x}    (1)
     * x'    = Ei*x          (2)
     * => x' = Ei*Er*\hat{x} (3)
     * \hat{x} = [x']       
     *           [x_f]       (4)
     *
     * => x' (2)= Ei*x
     *       (1)= Ei*Er*\hat{x}
     *       (4)= Ei*Er*[x']
     *                  [x_f]
     *          = Ei*Er*[0]     + Ei*Er*[1]
     *                  [1]*x_f         [0]*x'
     * => [1-Ei*Er*[1]]*x' = Ei*Er*[0]
     *             [0]             [1]*x_f
     * => x' = [1-Ei*Er*[1]]^{-1}*Ei*Er*[0]
     *                  [0]             [1]*x_f
     * => x' = Et*x_f with Et = [1-Ei*Er*[1]]^{-1}*Ei*Er*[0]
     *                                   [0]             [1] (5)
     *
     * => \hat{x} (4)= [x']
     *                 [x_f]
     *            (5)= [Et*x_f]
     *                 [x_f]
     *               = [Et]
     *                 [ 1]*x_f (6)
     *
     * => x (1)= Er*\hat{x}
     *      (6)= Er*[Et]
     *              [ 1]*x_f
     */

    // Actually compute E
    uint col_ofs;
    uint row_offset = 0;
    uint col_offset = 0;
    for (uint domain_index = 0; domain_index < domain_ptrs_.size(); ++domain_index) {
        // Define variables for convenience and clarity
        const c_domain_ptr& dom_ptr             = domain_ptrs_[domain_index];
        const uint          dom_sys_mat_col_ofs = domain_system_matrix_column_offsets_[dom_ptr->getDomainIndex()];
        const auto          dir_bnd_seg_ptrs    = dom_ptr->getDirectedBoundarySegmentPointers();

        // Skip reference domains
        if (dom_ptr->isReference()) continue;

        for (uint loop_idx = 0; loop_idx < dir_bnd_seg_ptrs.size(); ++loop_idx) {
            /* Compute Er */
            // Allocate memory for Er
            MatrixXd Er = MatrixXd::Zero(xi_dims[domain_index][loop_idx],hatxi_dims[domain_index][loop_idx]);

            // Compute the entries of Er
            uint row     = 0;
            uint var_cnt = 0;
            for (uint dir_bnd_seg_idx = 0; dir_bnd_seg_idx < dir_bnd_seg_ptrs[loop_idx].size(); ++dir_bnd_seg_idx) {
                // Define convenience and clarity
                const directed_boundary_segment_ptr& dbsp = dir_bnd_seg_ptrs[loop_idx][dir_bnd_seg_idx];

                for (uint subseg_idx = 0; subseg_idx < dbsp->getNumSubsegments(); ++subseg_idx) {
                    for (int mon_deg = 0; mon_deg <= (**dbsp).getFunctionApproximationPolynomialOrder(); ++mon_deg) {
                        if (dir_bnd_seg_idx == 0 && subseg_idx == 0 && mon_deg == 0) {
                            Er(row++,var_cnt++) = 1.0;
                        } else if (dir_bnd_seg_idx == dir_bnd_seg_ptrs[loop_idx].size()-1 && subseg_idx == dbsp->getNumSubsegments()-1 && mon_deg > 0) {
                            if (mon_deg == 1) {
                                uint var_cnt_i = 0;
                                for (int dir_bnd_seg_idx_i = 0; dir_bnd_seg_idx_i <= dir_bnd_seg_idx; ++dir_bnd_seg_idx_i) {
                                    const directed_boundary_segment_ptr& dbsp_i     = dir_bnd_seg_ptrs[loop_idx][dir_bnd_seg_idx_i];
                                    const int                            max_subseg = dir_bnd_seg_idx==dir_bnd_seg_idx_i ? subseg_idx-1 : dbsp_i->getNumSubsegments()-1;

                                    for (int subseg_idx_i = 0; subseg_idx_i <= max_subseg; ++subseg_idx_i) {
                                        for (uint mon_deg_i = 0; mon_deg_i <= (**dbsp_i).getFunctionApproximationPolynomialOrder(); ++mon_deg_i) {
                                            if (dir_bnd_seg_idx_i == 0 && subseg_idx_i == 0 && mon_deg_i == 0) {
                                                ++var_cnt_i;
                                            } else if (mon_deg_i > 0) {
                                                Er(row,var_cnt_i++) = -1.0;
                                            }
                                        }
                                    }
                                }
                                ++row;
                            } else {
                                std::cout << "This is not implemented yet." << std::endl;
                                abort();
                            }
                        } else {
                            if (mon_deg == 0) {
                                uint var_cnt_i = 0;
                                for (int dir_bnd_seg_idx_i = 0; dir_bnd_seg_idx_i <= dir_bnd_seg_idx; ++dir_bnd_seg_idx_i) {
                                    const directed_boundary_segment_ptr& dbsp_i     = dir_bnd_seg_ptrs[loop_idx][dir_bnd_seg_idx_i];
                                    const int                            max_subseg = dir_bnd_seg_idx==dir_bnd_seg_idx_i ? subseg_idx-1 : dbsp_i->getNumSubsegments()-1;

                                    for (int subseg_idx_i = 0; subseg_idx_i <= max_subseg; ++subseg_idx_i) {
                                        for (uint mon_deg_i = 0; mon_deg_i <= (**dbsp_i).getFunctionApproximationPolynomialOrder(); ++mon_deg_i) {
                                            if (dir_bnd_seg_idx_i == 0 && subseg_idx_i == 0 && mon_deg_i == 0) {
                                                Er(row,0) = 1.0;
                                                ++var_cnt_i;
                                            } else if (mon_deg_i > 0) {
                                                Er(row,var_cnt_i++) = 1.0;
                                            }
                                        }
                                    }
                                }
                                ++row;
                            } else if (mon_deg == 1) {
                                Er(row++,var_cnt++) = 1.0;
                            } else {
                                std::cout << "This is not implemented yet." << std::endl;
                                abort();
                            }
                        }
                    }
                }
            }

            // Fill in E
            for (uint row = 0; row < Er.rows(); ++row) {
                for (uint col = 0; col < Er.cols(); ++col) {
                    if (use_b_nabla) {
                        E(row_offset+4*row  , col_offset            +col) = Er(row,col);
                        E(row_offset+4*row+1, col_offset+  Er.cols()+col) = Er(row,col);
                        E(row_offset+4*row+2, col_offset+2*Er.cols()+col) = 0.0;//Er(row,col);
                        E(row_offset+4*row+3, col_offset+3*Er.cols()+col) = Er(row,col);
                    } else {
                        E(row_offset+2*row  , col_offset          +col) = Er(row,col);
                        E(row_offset+2*row+1, col_offset+Er.cols()+col) = Er(row,col);
                    }
                }
            }

            // Update the offsets
            if (use_b_nabla) {
                row_offset += 4*Er.rows();
                col_offset += 4*Er.cols();
            } else {
                row_offset += 2*Er.rows();
                col_offset += 2*Er.cols();
            }
        }
    }

    // Print the time it took to compute the matrix entries
    std::cout << "\33[2K\rFormulating the linear system: 100%" << std::endl;
    std::cout << "Computing the matrix entries took " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-start_time).count()/1000.0 << "s." << std::endl;

    // Measure the time it takes to solve the linear system
    start_time = std::chrono::steady_clock::now();

    /* Solve the system of linear equations */
    sol_ = std::make_unique<VectorXd>(rhs_ptr->rows());

    // SVD
    const bool enforce_continuity = true;
    const MatrixXd AE             = ((*sys_mat_ptr)*E).eval();
    Eigen::JacobiSVD<Eigen::MatrixXd> svd_A, svd_AE;
    if (enforce_continuity) {
        if (AE.rows() == AE.cols()) {
            std::cout << "Solving the linear system using full pivot LU decomposition... ";
            *sol_ = E*(AE.fullPivLu().solve(*rhs_ptr));
        } else {
            std::cout << "Solving the linear system using the Jacobi SVD... ";
            svd_AE = Eigen::JacobiSVD<Eigen::MatrixXd>(AE, Eigen::ComputeThinU | Eigen::ComputeThinV);
            *sol_  = E*(svd_AE.solve(*rhs_ptr));
        }
    } else {
        svd_A = Eigen::JacobiSVD<Eigen::MatrixXd>(*sys_mat_ptr, Eigen::ComputeThinU | Eigen::ComputeThinV);
        *sol_ = svd_A.solve(*rhs_ptr);
    }

    //*sol_ = sys_mat_ptr->colPivHouseholderQr().solve(*rhs_ptr);             // -17.6309028917237
    //*sol_ = sys_mat_ptr->fullPivHouseholderQr().solve(*rhs_ptr);            // 97.4246433630355
    //*sol_ = sys_mat_ptr->completeOrthogonalDecomposition().solve(*rhs_ptr); // 105.895416982274
    //*sol_ = sys_mat_ptr->bdcSvd(Eigen::ComputeThinU|Eigen::ComputeThinV).solve(*rhs_ptr); // Results in much worse accuracy

    // Print the time it took to solve the linear system
    std::cout << "Solving the linear system took " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-start_time).count()/1000.0 << "s." << std::endl;

    /* Statistics */
    if (options.debug) {
        for (uint i = 0; i < 30; ++i) std::cout << "*";
        std::cout << std::endl;

        std::cout << "It holds for the matrix A of the original linear system Ax = b that ";
        std::cout << "A ∊ ℝ ^{" << sys_mat_ptr->rows() << " x " << sys_mat_ptr->cols() << "}." << std::endl;
        if (enforce_continuity) {
            std::cout << "After enforcing continuity constraints of the boundary solutions, it holds that" << std::endl;
            std::cout << "AE ∊ ℝ ^{" << sys_mat_ptr->rows() << " x " << E.cols() << "}." << std::endl;
        }
        
        if (enforce_continuity) {
            if (AE.rows() == AE.cols()) {
                const auto eigenvalues_AE = AE.eigenvalues();
                std::cout << "The largest and smallest eigenvalue of the AE matrix are " << eigenvalues_AE(0) << " and ";
                std::cout << eigenvalues_AE(AE.rows()-1) << ", respectively." << std::endl;
            } else {
                const auto singular_values_AE = svd_AE.singularValues();
                std::cout << "The largest and smallest singular value of the AE matrix are " << singular_values_AE(0) << " and ";
                std::cout << singular_values_AE(singular_values_AE.rows()-1) << ", respectively." << std::endl;
            }
        } else {
            const auto singular_values_A = svd_A.singularValues();
            std::cout << "The largest and smallest singular value of the A matrix are " << singular_values_A(0) << " and ";
            std::cout << singular_values_A(singular_values_A.rows()-1) << ", respectively." << std::endl;
        }

        std::cout << "Denoting the solution as x', it holds that ||b||_2 = " << rhs_ptr->norm() << " and ||Ax'-b||_2 = " << ((*sys_mat_ptr)*(*sol_)-(*rhs_ptr)).norm() << "." << std::endl;

        for (uint i = 0; i < 30; ++i) std::cout << "*";
        std::cout << std::endl;
    }

    // Possibly store the solution in a file
    if (options.store_solution) store_solution(options.folder_name);

    /* For debugging purposes
//std::cout << "Its left singular vectors are the columns of the thin U matrix:" <<  std::endl << svd_sol.matrixU() << std::endl;
//std::cout << "Its right singular vectors are the columns of the thin V matrix:" << std::endl << svd_sol.matrixV() << std::endl;
    Eigen::JacobiSVD<Eigen::MatrixXd> svd_sol_((*sys_mat_ptr)*E, Eigen::ComputeThinU | Eigen::ComputeThinV);
std::cout << "xxxxxxxxxxxx" << std::endl << sys_mat_ptr->rows() << " " << sys_mat_ptr->cols() << std::endl << svd_sol_.singularValues() << std::endl;
//std::cout << std::setprecision(4);
    std::cout << std::setprecision(4);
    const Eigen::VectorXcd eigenvalues_ = sys_mat_ptr->eigenvalues();
    std::cout << "Eigenvalues:" << std::endl << eigenvalues_ << std::endl;
    std::cout << "E:" << std::endl << E << std::endl;
    *sol_ = svd_sol.solve(*rhs_ptr);
    */
}

void Problem::addDomain(const domain_ptr dptr) {
    dptr->setDomainIndex(domain_ptrs_.size());
    domain_ptrs_.push_back(dptr);
}

void Problem::addBoundarySegment(const boundary_segment_ptr bsptr) {
    bsptr->setBoundarySegmentIndex(boundary_segment_ptrs_.size());
    boundary_segment_ptrs_.push_back(bsptr);
}

Vec2D Problem::evaluateInteriorSolution(const uint domain_index, const Vec2D z) const {
    // Allocate a GSL workspace and a GSL function
    gsl_function               gsl_fun;
    gsl_integration_workspace* gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);

    // Define for convenience and clarity
    const auto&  domain           = *domain_ptrs_[domain_index];
    const double mu_k             = domain.getmu();
    const double J_k              = domain.getJ();

    // Create a wrapper to simplify integration
    const auto quad = [&gsl_fun,gsl_workspace]()->double {
        double result,error;
        gsl_integration_qag(&gsl_fun, /* Function to integrate */
                            0.0,      /* Lower integration limit */
                            1.0,      /* Upper integration limit */
                            INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                            INTEGRAL_RELATIVE_ERROR_LIMIT,
                            MAX_NUM_SUBINTERVALS,
                            INTEGRATION_KEY,
                            gsl_workspace, &result, &error);
        return result;
    };

    // Create a lambda defining the integrand of integrals along boundary subsegments
    const auto boundarySubsegmentIntegral = [](const double t, void* params)->double {
        const uint& dim_idx      = *static_cast<const uint*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));

        const auto& psi_i        = *static_cast<const std::function<Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[2]));
        const auto& dpsi_i_2Norm = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[3]));
        const bool& is_curl      = *static_cast<const bool*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[4]));

        const auto& m_l          = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[5]));

        const Vec2D& z           = *static_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[1]));
        const Vec2D  y           = psi_i(t);

        if (is_curl)
            return (dim_idx == 0 ? CurlE(z-y).x :
                                   CurlE(z-y).y )*m_l(t)*dpsi_i_2Norm(t);
        else
            return (dim_idx == 0 ? GradE(z-y).x :
                                   GradE(z-y).y )*m_l(t)*dpsi_i_2Norm(t);
    };

    // Create a lambda to integrate along boundary subsegments
    const auto getBoundarySubsegmentIntegral = [&gsl_fun,&boundarySubsegmentIntegral,&quad](const uint                                l,
                                                                                            const Vec2D&                              z,
                                                                                            const std::function<Vec2D(const double)>  psi_i,
                                                                                            const std::function<double(const double)> dpsi_i_2Norm,
                                                                                            const bool                                is_curl)->const Vec2D {
        std::function<double(const double t)> m_l = [l](const double t)->double {
            return mono(t,l);
        };
        Vec2D result;
        for (uint i = 0; i < 2; ++i) {
            gsl_fun.function      = boundarySubsegmentIntegral;
            const double** params = new const double*[6];
            params[0]             = reinterpret_cast<const double*>(&i);
            params[1]             = reinterpret_cast<const double*>(&z);
            params[2]             = reinterpret_cast<const double*>(&psi_i);
            params[3]             = reinterpret_cast<const double*>(&dpsi_i_2Norm);
            params[4]             = reinterpret_cast<const double*>(&is_curl);
            params[5]             = reinterpret_cast<const double*>(&m_l);
            gsl_fun.params        = params;
            i == 0 ? result.x = quad() :
                     result.y = quad();
            delete[] params;
        }
        return result;
    };

    // Find the correct domain
    c_domain_ptr domain_ptr;
    for (const c_domain_ptr& dom_ptr : domain_ptrs_)
        if (dom_ptr->getDomainIndex() == domain_index && !dom_ptr->isReference())
            domain_ptr = dom_ptr;
    if (!domain_ptr) throw new std::runtime_error("Invalid domain_index provided to evaluateInteriorSolution: "+std::to_string(domain_index));

    // Get references to variables of the domain
    const auto& dir_bnd_seg_ptrs = domain_ptr->getDirectedBoundarySegmentPointers();

    /* Evaluate the solution */
    Vec2D result = {0.0,0.0};

    // Compute the contribution from the layer potentials
    uint sol_row = 0;
    for (uint loop_idx = 0; loop_idx < dir_bnd_seg_ptrs.size(); ++loop_idx) {
        for (uint dir_bnd_seg_ptr_idx = 0; dir_bnd_seg_ptr_idx < dir_bnd_seg_ptrs[loop_idx].size(); ++dir_bnd_seg_ptr_idx) {
            const DirectedBoundarySegment& dir_bnd_seg = *dir_bnd_seg_ptrs[loop_idx][dir_bnd_seg_ptr_idx];

            for (uint subseg_idx = 0; subseg_idx < dir_bnd_seg.getNumSubsegments(); ++subseg_idx) {
                for (uint mon_deg = 0; mon_deg <= (*dir_bnd_seg).getFunctionApproximationPolynomialOrder(); ++mon_deg) {
                    result += getBoundarySubsegmentIntegral(mon_deg,
                                                            z,
                                                            dir_bnd_seg.getpsii(subseg_idx),
                                                            dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                            false /* is_curl */)*(*sol_)(domain_system_matrix_column_offsets_[domain_index]+sol_row);
                    result += getBoundarySubsegmentIntegral(mon_deg,
                                                            z,
                                                            dir_bnd_seg.getpsii(subseg_idx),
                                                            dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                            true /* is_curl */)*(*sol_)(domain_system_matrix_column_offsets_[domain_index]+sol_row+1);
                    if (use_b_nabla) {
                        result -= getBoundarySubsegmentIntegral(mon_deg,
                                                                z,
                                                                dir_bnd_seg.getpsii(subseg_idx),
                                                                dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                                true /* is_curl*/)*(*sol_)(domain_system_matrix_column_offsets_[domain_index]+sol_row+2);
                        result += getBoundarySubsegmentIntegral(mon_deg,
                                                                z,
                                                                dir_bnd_seg.getpsii(subseg_idx),
                                                                dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                                false /* is_curl*/)*(*sol_)(domain_system_matrix_column_offsets_[domain_index]+sol_row+3);
                    }
                    if (use_b_nabla) sol_row += 4;
                    else             sol_row += 2;
                }
            }
        }
    }

    // Create a lambda that computes the Curl of the fundamental solution
    const auto curl_of_fund_sol = [&gsl_fun,&quad](const Vec2D                                     x,
                                                   const std::function<const Vec2D(const double)>& psi_i_jk,
                                                   const std::function<double(const double)>&      dpsi_i_jk_2Norm,
                                                   const std::function<Vec2D(const double)>&       getnu) {
        gsl_fun.function = [](const double t, void* params)->double {
            const auto& psi_i_jk        = *static_cast<const std::function<const Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
            const auto& dpsi_i_jk_2Norm = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[1]));
            const auto& nu              = *static_cast<const std::function<Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[2]));
            const uint& dim_idx         = *static_cast<const uint*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[3]));

            const Vec2D& x   = *static_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[4]));
            const Vec2D  y   = psi_i_jk(t);

            return (dim_idx == 0 ? -nu(t).y :
                                    nu(t).x)*E(y-x)*dpsi_i_jk_2Norm(t);
        };

        const double** params = new const double*[5];
        params[0]             = reinterpret_cast<const double*>(&psi_i_jk);
        params[1]             = reinterpret_cast<const double*>(&dpsi_i_jk_2Norm);
        params[2]             = reinterpret_cast<const double*>(&getnu);
        params[4]             = reinterpret_cast<const double*>(&x);
        uint dim_idx;
        params[3]             = reinterpret_cast<const double*>(&dim_idx);
        gsl_fun.params        = params;

        Vec2D result;
        dim_idx  = 0;
        result.x = quad();
        dim_idx  = 1;
        result.y = quad();
        delete[] params;
        return   result;
    };

    // Compute the contribution from the fundamental solution
    if (J_k != 0) {
        const double mu_k_J_k = mu_k*J_k;
        for (uint loop_idx_ = 0; loop_idx_ < dir_bnd_seg_ptrs.size(); ++loop_idx_) {
            for (uint dir_bnd_seg_ptr_idx_ = 0; dir_bnd_seg_ptr_idx_ < dir_bnd_seg_ptrs[loop_idx_].size(); ++dir_bnd_seg_ptr_idx_) {
                const DirectedBoundarySegment& dir_bnd_seg = *dir_bnd_seg_ptrs[loop_idx_][dir_bnd_seg_ptr_idx_];

                for (uint subseg_idx_ = 0; subseg_idx_ < dir_bnd_seg.getNumSubsegments(); ++subseg_idx_) {
                    result -= mu_k_J_k*curl_of_fund_sol(z,
                                                        dir_bnd_seg.getpsii(subseg_idx_),
                                                        dir_bnd_seg.getdpsii2Norm(subseg_idx_),
                                                        dir_bnd_seg.getnu(subseg_idx_));
                }
            }
        }
    }

    // Free the memory of the GSL workspace
    gsl_integration_workspace_free(gsl_workspace);

    return result;
}

tuple<Vec2D/* B */,Vec2D/* x */,Vec2D/* nu */> Problem::evaluateBoundarySolution(const uint dom_idx, const uint outer_loop_idx, const uint dir_bnd_seg_idx, const double t) const {
    // Check the arguments
    if (t < 0.0 || t >= 1.0) {
        std::cout << "Argument 't' given to evaluateBoundarySolution must be in [0,1), but is " << t << "." << std::endl;
        abort();
    }

    // Define for convenience and clarity
    const auto&  domain           = *domain_ptrs_[dom_idx];
    const auto&  dir_bnd_seg_ptr  = domain.getDirectedBoundarySegmentPointers()[outer_loop_idx][dir_bnd_seg_idx];
    const double mu_k             = domain.getmu();
    const double J_k              = domain.getJ();

    // Determine the subsegment index
    const uint outer_subseg_idx = std::floor(t*dir_bnd_seg_ptr->getNumSubsegments());

    // Compute the boundary point
    const Vec2D x = dir_bnd_seg_ptr->getpsii(outer_subseg_idx)(t*dir_bnd_seg_ptr->getNumSubsegments()-outer_subseg_idx);

    // Get the normal and the tangential vector
    // (t - 1.0/N*subseg_idx)/(1/N) = (t - 1.0/N*subseg_idx)*N = (t*N - subseg_idx)
    const Vec2D t_vec  = dir_bnd_seg_ptr->gett(outer_subseg_idx)(t*dir_bnd_seg_ptr->getNumSubsegments()-outer_subseg_idx);
    const Vec2D nu_vec = dir_bnd_seg_ptr->getnu(outer_subseg_idx)(t*dir_bnd_seg_ptr->getNumSubsegments()-outer_subseg_idx);

    // Allocate a GSL workspace and a GSL function
    gsl_function               gsl_fun;
    gsl_integration_workspace* gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);

    // Create a wrapper to simplify integration
    const auto quad = [&gsl_fun,gsl_workspace](const double a, const double b)->double {
        double result,error;
        gsl_integration_qag(&gsl_fun, /* Function to integrate */
                            a,        /* Lower integration limit */
                            b,        /* Upper integration limit */
                            INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                            INTEGRAL_RELATIVE_ERROR_LIMIT,
                            MAX_NUM_SUBINTERVALS,
                            INTEGRATION_KEY,
                            gsl_workspace, &result, &error);
        return result;
    };

    // Create a lambda defining the integrand of integrals along boundary subsegments
    const auto boundarySubsegmentIntegral = [](const double t, void* params)->double {
        const uint& dim_idx      = *static_cast<const uint*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));

        const auto& psi_i        = *static_cast<const std::function<Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[2]));
        const auto& dpsi_i_2Norm = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[3]));
        const bool& is_curl      = *static_cast<const bool*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[4]));

        const auto& m_l          = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[5]));

        const Vec2D& x           = *static_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[1]));
        const Vec2D  y           = psi_i(t);

        if (is_curl)
            return (dim_idx == 0 ? CurlE(x-y).x :
                                   CurlE(x-y).y )*m_l(t)*dpsi_i_2Norm(t);
        else
            return (dim_idx == 0 ? GradE(x-y).x :
                                   GradE(x-y).y )*m_l(t)*dpsi_i_2Norm(t);
    };

    // Create a lambda to integrate along boundary subsegments
    const auto getBoundarySubsegmentIntegral = [&gsl_fun,&boundarySubsegmentIntegral,&quad](const uint                                l,
                                                                                            const Vec2D&                              x,
                                                                                            const std::function<Vec2D(const double)>  psi_i,
                                                                                            const std::function<double(const double)> dpsi_i_2Norm,
                                                                                            const bool                                has_singularity,
                                                                                            const double                              singularity,
                                                                                            const bool                                is_curl)->const Vec2D {
        std::function<double(const double t)> m_l = [l](const double t)->double {
            return mono(t,l);
        };

        // Handle the potential singularity
        const double min_distance = 1.0e-7;
        double       epsilon_l    = 0.0;
        double       epsilon_h    = 0.0;
        if (has_singularity) {
            epsilon_l = 1.0e-8;
            epsilon_h = 1.0e-8;
            while (singularity - epsilon_l > 0.0 && (x-psi_i(singularity-epsilon_l)).Norm2() < min_distance)
                epsilon_l *= 2.0;
            while (singularity + epsilon_h < 1.0 && (x-psi_i(singularity+epsilon_h)).Norm2() < min_distance)
                epsilon_h *= 2.0;
        } else {
            if ((x-psi_i(0.0)).Norm2() < min_distance)
                epsilon_l = 1.0e-8;
            while (epsilon_l < 1.0 && (x-psi_i(epsilon_l)).Norm2() < min_distance)
                epsilon_l *= 2.0;
            if ((x-psi_i(1.0)).Norm2() < min_distance)
                epsilon_h = 1.0e-8;
            while (epsilon_h < 1.0 && (x-psi_i(1.0-epsilon_h)).Norm2() < min_distance)
                epsilon_h *= 2.0;
        }

        // Compute the result
        Vec2D result;
        for (uint i = 0; i < 2; ++i) {
            gsl_fun.function      = boundarySubsegmentIntegral;
            const double** params = new const double*[6];
            params[0]             = reinterpret_cast<const double*>(&i);
            params[1]             = reinterpret_cast<const double*>(&x);
            params[2]             = reinterpret_cast<const double*>(&psi_i);
            params[3]             = reinterpret_cast<const double*>(&dpsi_i_2Norm);
            params[4]             = reinterpret_cast<const double*>(&is_curl);
            params[5]             = reinterpret_cast<const double*>(&m_l);
            gsl_fun.params        = params;
            if (i == 0) {
                if (has_singularity) {
                    result.x  = singularity-epsilon_l > 0.0 ? quad(0.0,singularity-epsilon_l) : 0.0;
                    result.x += singularity+epsilon_h < 1.0 ? quad(singularity+epsilon_h,1.0) : 0.0;
                } else {
                    result.x = quad(epsilon_l,1.0-epsilon_h);
                }
            } else {
                if (has_singularity) {
                    result.y  = singularity-epsilon_l > 0.0 ? quad(0.0,singularity-epsilon_l) : 0.0;
                    result.y += singularity+epsilon_h < 1.0 ? quad(singularity+epsilon_h,1.0) : 0.0;
                } else {
                    result.y = quad(epsilon_l,1.0-epsilon_h);
                }
            }
            delete[] params;
        }
        return result;
    };

    // Find the correct domain
    c_domain_ptr domain_ptr;
    for (const c_domain_ptr& dom_ptr : domain_ptrs_)
        if (dom_ptr->getDomainIndex() == dom_idx && !dom_ptr->isReference())
            domain_ptr = dom_ptr;
    if (!domain_ptr) throw new std::runtime_error("Invalid domain_index provided to evaluateBoundarySolution: "+std::to_string(dom_idx));

    // Get references to variables of the domain
    const auto& dir_bnd_seg_ptrs = domain_ptr->getDirectedBoundarySegmentPointers();

    /* Evaluate the solution */
    Vec2D result = {0.0,0.0};

    // Compute the contribution from the layer potentials
    uint sol_row = 0;
    for (uint loop_idx = 0; loop_idx < dir_bnd_seg_ptrs.size(); ++loop_idx) {
        for (uint dir_bnd_seg_ptr_idx = 0; dir_bnd_seg_ptr_idx < dir_bnd_seg_ptrs[loop_idx].size(); ++dir_bnd_seg_ptr_idx) {
            const DirectedBoundarySegment& dir_bnd_seg = *dir_bnd_seg_ptrs[loop_idx][dir_bnd_seg_ptr_idx];

            for (uint subseg_idx = 0; subseg_idx < dir_bnd_seg.getNumSubsegments(); ++subseg_idx) {
                for (uint mon_deg = 0; mon_deg <= (*dir_bnd_seg).getFunctionApproximationPolynomialOrder(); ++mon_deg) {
                    bool has_singularity = false;
                    if (loop_idx == outer_loop_idx && dir_bnd_seg_ptr_idx == dir_bnd_seg_idx && subseg_idx == outer_subseg_idx) {
                        has_singularity = true;
                        result         += -0.5*nu_vec*(*sol_)(domain_system_matrix_column_offsets_[dom_idx]+sol_row)  *mono(t*dir_bnd_seg_ptr->getNumSubsegments()-outer_subseg_idx,mon_deg);
                        result         +=  0.5*t_vec *(*sol_)(domain_system_matrix_column_offsets_[dom_idx]+sol_row+1)*mono(t*dir_bnd_seg_ptr->getNumSubsegments()-outer_subseg_idx,mon_deg);
                    }
                    result += getBoundarySubsegmentIntegral(mon_deg,
                                                            x,
                                                            dir_bnd_seg.getpsii(subseg_idx),
                                                            dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                            has_singularity,
                                                            t*dir_bnd_seg_ptr->getNumSubsegments()-outer_subseg_idx,
                                                            false /* is_curl */)*(*sol_)(domain_system_matrix_column_offsets_[dom_idx]+sol_row);
                    result += getBoundarySubsegmentIntegral(mon_deg,
                                                            x,
                                                            dir_bnd_seg.getpsii(subseg_idx),
                                                            dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                            has_singularity,
                                                            t*dir_bnd_seg_ptr->getNumSubsegments()-outer_subseg_idx,
                                                            true /* is_curl */)*(*sol_)(domain_system_matrix_column_offsets_[dom_idx]+sol_row+1);
                    if (use_b_nabla) {
                        result -= getBoundarySubsegmentIntegral(mon_deg,
                                                                x,
                                                                dir_bnd_seg.getpsii(subseg_idx),
                                                                dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                                has_singularity,
                                                                t*dir_bnd_seg_ptr->getNumSubsegments()-outer_subseg_idx,
                                                                true /* is_curl*/)*(*sol_)(domain_system_matrix_column_offsets_[dom_idx]+sol_row+2);
                        result += getBoundarySubsegmentIntegral(mon_deg,
                                                                x,
                                                                dir_bnd_seg.getpsii(subseg_idx),
                                                                dir_bnd_seg.getdpsii2Norm(subseg_idx),
                                                                has_singularity,
                                                                t*dir_bnd_seg_ptr->getNumSubsegments()-outer_subseg_idx,
                                                                false /* is_curl*/)*(*sol_)(domain_system_matrix_column_offsets_[dom_idx]+sol_row+3);
                    }
                    if (use_b_nabla) sol_row += 4;
                    else             sol_row += 2;
                }
            }
        }
    }

    //// Create a lambda that computes the Curl of the fundamental solution
    //const auto curl_of_fund_sol = [&gsl_fun,&quad](const Vec2D                                     x,
    //                                               const std::function<const Vec2D(const double)>& psi_i_jk,
    //                                               const std::function<double(const double)>&      dpsi_i_jk_2Norm,
    //                                               const std::function<Vec2D(const double)>&       getnu,
    //                                               const bool                                      has_singularity,
    //                                               const double                                    singularity) {
    //    gsl_fun.function = [](const double t, void* params)->double {
    //        const auto& psi_i_jk        = *static_cast<const std::function<const Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
    //        const auto& dpsi_i_jk_2Norm = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[1]));
    //        const auto& nu              = *static_cast<const std::function<Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[2]));
    //        const uint& dim_idx         = *static_cast<const uint*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[3]));

    //        const Vec2D& x   = *static_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[4]));
    //        const Vec2D  y   = psi_i_jk(t);

    //        return (dim_idx == 0 ? -nu(t).y :
    //                                nu(t).x)*E(y-x)*dpsi_i_jk_2Norm(t);
    //    };

    //// Compute the contribution from the fundamental solution
    //if (J_k != 0) {
    //    const double mu_k_J_k = mu_k*J_k;
    //    for (uint loop_idx_ = 0; loop_idx_ < dir_bnd_seg_ptrs.size(); ++loop_idx_) {
    //        for (uint dir_bnd_seg_ptr_idx_ = 0; dir_bnd_seg_ptr_idx_ < dir_bnd_seg_ptrs[loop_idx_].size(); ++dir_bnd_seg_ptr_idx_) {
    //            const DirectedBoundarySegment& dir_bnd_seg = *dir_bnd_seg_ptrs[loop_idx_][dir_bnd_seg_ptr_idx_];

    //            for (uint subseg_idx_ = 0; subseg_idx_ < dir_bnd_seg.getNumSubsegments(); ++subseg_idx_) {
    //                sol -= mu_k_J_k*curl_of_fund_sol(x,
    //                                                 dir_bnd_seg.getpsii(subseg_idx_),
    //                                                 dir_bnd_seg.getdpsii2Norm(subseg_idx_),
    //                                                 dir_bnd_seg.getnu(subseg_idx_),
    //                                                 loop_idx == loop_idx_ && dir_bnd_seg_idx == dir_bnd_seg_ptr_idx_ && subseg_idx == subseg_idx_ /* has_singularity */,
    //                                                 t*dir_bnd_seg_ptr->getNumSubsegments()-subseg_idx                                             /* singularity */);
    //            }
    //        }
    //    }
    //}

    // Free the memory of the GSL workspace
    gsl_integration_workspace_free(gsl_workspace);

    return {result,x,nu_vec};
}

void Problem::initializeHelperVariables() {
    // Count the number of non-reference domains
    uint non_ref_doms = 0u;
    for (const auto& dom_ptr : domain_ptrs_) if (!dom_ptr->isReference()) non_ref_doms += 1u;

    // Allocate memory
    domain_system_matrix_column_offsets_.resize(non_ref_doms);

    // dom_idx <==> domain index
    for (auto [dom_idx,vars,non_ref_dom_idx] = tuple<int,uint,uint>{0,0u,0u}; dom_idx < domain_ptrs_.size(); ++dom_idx) {
        // Skip reference domains
        if (domain_ptrs_[dom_idx]->isReference()) continue;

        domain_system_matrix_column_offsets_[non_ref_dom_idx++] = vars;

        if (dom_idx == domain_ptrs_.size()-1) break;

        for (uint loop_idx = 0u; loop_idx < domain_ptrs_[dom_idx]->getDirectedBoundarySegmentPointers().size(); ++loop_idx) {
            const vector<directed_boundary_segment_ptr>& dir_bnd_seg_ptrs = domain_ptrs_[dom_idx]->getDirectedBoundarySegmentPointers()[loop_idx];

            // dir_bnd_seg_ptr_idx <==> boundary segment pointer index
            for (uint dir_bnd_seg_ptr_idx = 0u; dir_bnd_seg_ptr_idx < dir_bnd_seg_ptrs.size(); ++dir_bnd_seg_ptr_idx) {
                const BoundarySegment& bnd_seg = **dir_bnd_seg_ptrs[dir_bnd_seg_ptr_idx];

                if (use_b_nabla) vars += 4u*bnd_seg.getNumSubsegments()*(bnd_seg.getFunctionApproximationPolynomialOrder()+1u);
                else             vars += 2u*bnd_seg.getNumSubsegments()*(bnd_seg.getFunctionApproximationPolynomialOrder()+1u);
            }
        }
    }
}

unique_mat_ptr Problem::allocateSystemMatrix() const {
    // Compute the number of rows
    uint rows = 0u;
    for (const c_boundary_segment_ptr& bsp : boundary_segment_ptrs_)
        // There are two times the number of parameters of the polynomial approximition many test functions which
        // will be applied to each boundary segment on both sides.
        if (!bsp->isReference())
            rows += (bsp->getFunctionApproximationPolynomialOrder()+1u)*bsp->getNumSubsegments()*2u;

    // Allocate memory for the system matrix and return it
    MatrixXd* m_ptr = new MatrixXd(rows,use_b_nabla ? rows*4u : rows*2u);
    *m_ptr          = MatrixXd::Zero(rows,use_b_nabla ? rows*4u : rows*2u);
    return std::unique_ptr<MatrixXd>(m_ptr);
}

unique_vec_ptr Problem::allocateRHS() const {
    // Compute the number of rows
    uint rows = 0;
    for (const c_boundary_segment_ptr& bsp : boundary_segment_ptrs_)
        // There are two times the number of parameters of the polynomial approximition many test functions which
        // will be applied to each boundary segment on both sides.
        if (!bsp->isReference())
            rows += (bsp->getFunctionApproximationPolynomialOrder()+1)*bsp->getNumSubsegments()*2;

    // Allocate memory for the RHS and return it
    VectorXd* v_ptr = new VectorXd(rows);
    *v_ptr          = VectorXd::Zero(rows);
    return std::unique_ptr<VectorXd>(v_ptr);
}

void Problem::store_solution(const std::string& folder_name) const {
    std::filesystem::create_directories(folder_name);
    std::ofstream file(folder_name+"/sol.b",std::ios::out|std::ios::binary);
    for (uint row = 0; row < sol_->rows(); ++row) {
        const double   cd  = (*sol_)(row);
        const uint8_t* ptr = reinterpret_cast<const uint8_t*>(&cd);
        for (uint i = 0; i < sizeof(double); ++i)
            file.put(ptr[i]);
    }
}

void Problem::load_solution(const std::string& folder_name) {
    // From: https://stackoverflow.com/questions/2409504/using-c-filestreams-fstream-how-can-you-determine-the-size-of-a-file
    std::ifstream file(folder_name+"/sol.b",std::ios::in|std::ios::binary);
    file.ignore(std::numeric_limits<std::streamsize>::max());
    std::streamsize file_length = file.gcount();
    file.clear();
    file.seekg(0,std::ios_base::beg);

    // Allocate memory for the solution
    sol_ = std::make_unique<VectorXd>(static_cast<long>(file_length/sizeof(double)));
    vector<uint8_t> row_buffer(sizeof(double));
    for (uint row = 0; row < sol_->rows(); ++row) {
        for (uint i = 0; i < sizeof(double); ++i) row_buffer[i] = file.get();
        double   c;
        uint8_t* ptr = reinterpret_cast<uint8_t*>(&c);
        for (uint i = 0; i < sizeof(double); ++i) ptr[i] = row_buffer[i];
        (*sol_)(row) = c;
    }
}
