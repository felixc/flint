#include <cmath>
#include <memory>
#include <limits>
#include <Eigen/Dense>
#include <gsl/gsl_integration.h>
#include "ProblemWrapper.hpp"
#include "Sanji.hpp"
#include "CircularBoundarySegment.hpp"
#include <iostream>
#include <complex>
#include <algorithm>
#include "PMSM.hpp"

using namespace sanji::colors;
using namespace std::complex_literals;

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 1e-8
#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-6
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          1000

/* Type definitions */
template <typename T>
using vector    = std::vector<T>;
using VectorXd  = Eigen::VectorXd;
using VectorXcd = Eigen::VectorXcd;
using MatrixXd  = Eigen::MatrixXd;
using complex   = std::complex<double>;

/* Problem parameters */
// Visualization
constexpr bool plot_normals     = false;
constexpr bool plot_tangentials = false;
constexpr bool plot_field_data  = true;
constexpr bool load_field_data  = false;
constexpr bool store_field_data = true;

// General
static constexpr uint   num_magnets = 12;
static constexpr uint   num_teeth   = 14;

// Coils
static constexpr double cm1 = 0.017;
static constexpr double cm2 = 0.023;
static constexpr double cm3 = 0.004;
static constexpr double cm4 = 0.006;

// Motor control
constexpr double max_coil_current_density = 10.0/1.0e-6; // [A/m^2]
constexpr uint   p       = num_magnets/2;
constexpr double Omega_m = 4000.0/60.0*2.0*M_PI;         // [rad/s]
constexpr double w_s     = Omega_m*p;

double a(const double phi_s) {
    double phi = phi_s;
    while (phi < -M_PI) phi += 2*M_PI;
    while (phi >= M_PI) phi -= 2*M_PI;
    if (std::abs(phi) <= 2.0*M_PI/num_teeth/2.0) return 1;
    else                                         return 0;
}

double A(const double phi_s, const double t) {
    return ( a(phi_s)             - a(phi_s-2*M_PI/12) -  a(phi_s-6*2*M_PI/12) +  a(phi_s-7*2*M_PI/12))*std::cos(w_s*t)
          +( a(phi_s-4*2*M_PI/12) - a(phi_s-5*M_PI/12) - a(phi_s-10*2*M_PI/12) + a(phi_s-11*2*M_PI/12))*std::cos(w_s*t-2*M_PI/3)
          +(-a(phi_s-2*2*M_PI/12) + a(phi_s-3*M_PI/12) +  a(phi_s-8*2*M_PI/12) -  a(phi_s-9*2*M_PI/12))*std::cos(w_s*t-4*M_PI/3);
}

int main(int argc, char* argv[]) {
    // Initialize this as a Qt application
    QApplication app(argc, argv);
    sanji::init();

    /* Create a problem instance */
    ProblemWrapper p;

    /* Motor control */
    // GSL
    VectorXd motor_current_factors(num_teeth);
    double rotor_angle;
    {
    gsl_function               gsl_fun;
    gsl_integration_workspace* gsl_workspace;
    gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);

    const auto quad = [&gsl_fun,gsl_workspace](const double a, const double b)->double {
        double result,error;
        gsl_integration_qag(&gsl_fun, /* Function to integrate */
                            a,        /* Lower integration limit */
                            b,        /* Upper integration limit */
                            INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                            INTEGRAL_RELATIVE_ERROR_LIMIT,
                            MAX_NUM_SUBINTERVALS,
                            INTEGRATION_KEY,
                            gsl_workspace,&result,&error);
        return result;
    };
    const auto ak = [&gsl_fun,&quad](const int k)->complex {
        bool real = true;
        const auto integrand = [k,&real](const double theta)->double {
            const double  phi = -k*theta;
            const complex c   = std::cos(phi) + 1.0i*std::sin(phi);
            if (real)
                return (a(theta)*c).real();
            else
                return (a(theta)*c).imag();
        };
        gsl_fun.function = [](const double theta, void* params)->double {
            const decltype(integrand)& integrand_ = *static_cast<const decltype(integrand)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
            return integrand_(theta);
        };
        const double** params  = new const double*[1];
        gsl_fun.params         = params;
        params[0]              = reinterpret_cast<const double*>(&integrand);
        const double real_part = quad(-M_PI,M_PI)/(2.0*M_PI);
        real                   = false;
        const double imag_part = quad(-M_PI,M_PI)/(2.0*M_PI);
        delete[] params;
        return real_part + 1.0i*imag_part;
    };
    const auto Ak = [&gsl_fun,&quad](const int k, const double t)->complex {
        bool real = true;
        const auto integrand = [k,&real,t](const double theta)->double {
            const double  phi = -k*theta;
            const complex c   = std::cos(phi) + 1.0i*std::sin(phi);
            if (real)
                return (A(theta,t)*c).real();
            else
                return (A(theta,t)*c).imag();
        };
        gsl_fun.function = [](const double theta, void* params)->double {
            const decltype(integrand)& integrand_ = *static_cast<const decltype(integrand)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
            return integrand_(theta);
        };
        const double** params  = new const double*[1];
        gsl_fun.params         = params;
        params[0]              = reinterpret_cast<const double*>(&integrand);
        const double real_part = quad(-M_PI,M_PI)/(2.0*M_PI);
        real                   = false;
        const double imag_part = quad(-M_PI,M_PI)/(2.0*M_PI);
        delete[] params;
        return real_part + 1.0i*imag_part;
    };

    // a(\phi)
    const uint Ntheta = 1000;
    VectorXd theta_vec(Ntheta);
    VectorXd a_vec(Ntheta); 
    for (uint i = 0; i < Ntheta; ++i) {
        theta_vec(i) = -1.0 + i*2.0/(Ntheta-1);
        a_vec(i)     = a(theta_vec(i)*M_PI);
    }
    sanji::figure("a(phi)");
    sanji::plot(theta_vec,a_vec,{{"line_style",'-'},{"color",RED}});

    // ak
    uint Nk = 2*11+1;
    VectorXd  k_vec(Nk);
    VectorXcd ak_vec(Nk);
    for (int i = -11; i <= 11; ++i) {
        k_vec(i+11)  = i;
        ak_vec(i+11) = ak(k_vec(i+11));
    }
    sanji::figure("ak(phi)");
    sanji::plot(k_vec,ak_vec.real(),{{"line_style",'-'},{"color",RED}});
    sanji::plot(k_vec,ak_vec.imag(),{{"line_style",'-'},{"color",BLUE}});

    // A
    VectorXd A_vec(Ntheta); 
    for (uint i = 0; i < Ntheta; ++i) A_vec(i) = A(theta_vec(i)*M_PI,0.0);
    sanji::figure("A(phi)");
    sanji::plot(theta_vec,A_vec,{{"line_style",'-'},{"color",RED}});

    // Ak
    VectorXcd Ak_vec(Nk);
    for (int i = -11; i <= 11; ++i) Ak_vec(i+11) = Ak(k_vec(i+11),0.0);
    sanji::figure("Ak(phi)");
    sanji::plot(k_vec,Ak_vec.real(),{{"line_style",'-'},{"color",RED}});
    sanji::plot(k_vec,Ak_vec.imag(),{{"line_style",'-'},{"color",BLUE}});

    // Computation of the motor current factors
    const double t = 0.0;
    motor_current_factors(0)  = +std::cos(w_s*t);          // A
    motor_current_factors(1)  = -std::cos(w_s*t);          // a
    motor_current_factors(2)  = -std::cos(w_s*t-4*M_PI/3); // c
    motor_current_factors(3)  = +std::cos(w_s*t-4*M_PI/3); // C
    motor_current_factors(4)  = +std::cos(w_s*t-2*M_PI/3); // B
    motor_current_factors(5)  = -std::cos(w_s*t-2*M_PI/3); // b
    motor_current_factors(6)  = -std::cos(w_s*t);          // a
    motor_current_factors(7)  = +std::cos(w_s*t);          // A
    motor_current_factors(8)  = +std::cos(w_s*t-4*M_PI/3); // C
    motor_current_factors(9)  = -std::cos(w_s*t-4*M_PI/3); // c
    motor_current_factors(10) = -std::cos(w_s*t-2*M_PI/3); // b
    motor_current_factors(11) = +std::cos(w_s*t-2*M_PI/3); // B

    // Computation of the rotor angle
    const double phi_os = -std::arg(Ak(num_magnets/2,0.0))/(num_magnets/2);
    rotor_angle         = t*Omega_m+phi_os;
    
    // GSL
    gsl_integration_workspace_free(gsl_workspace);

    app.exec();
    };

    /* Solve the problem with zero coil currents */
    const PMSM pmsm(p,
                    rotor_angle,
                    max_coil_current_density,
                    motor_current_factors,
                    num_magnets,
                    num_teeth,
                    cm1,
                    cm2,
                    cm3,
                    cm4);

    //// Compute the normal component of the B field along the boundary of the rotor
    //const uint     N              = 200;
    //Eigen::ArrayXd B_norm_bnd     = VectorXd::LinSpaced(N,0,N-1);
    //Eigen::ArrayXd B_norm_int_cl  = VectorXd::LinSpaced(N,0,N-1);
    //Eigen::ArrayXd B_norm_int_far = VectorXd::LinSpaced(N,0,N-1);
    //directed_boundary_segment_ptr dir_bnd_seg_ptr;
    //for (const auto& dir_bnd_seg_ptrs_ : pmsm.rotor_->getDirectedBoundarySegmentPointers())
    //    for (const auto& dir_bnd_seg_ptr_ : dir_bnd_seg_ptrs_)
    //        if (pmsm.rotor_outer_rim_.get() == &**dir_bnd_seg_ptr_)
    //            dir_bnd_seg_ptr = dir_bnd_seg_ptr_;
    //std::for_each(B_norm_bnd.cbegin(),B_norm_bnd.cend(),[N,&B_norm_bnd,&B_norm_int_cl,&B_norm_int_far,&p,&pmsm,&dir_bnd_seg_ptr](const int n) {
    //    // Compute the solution on the boundary
    //    auto [loop_idx,dir_bnd_seg_idx] = pmsm.rotor_->getDirectedBoundarySegmentIndex((**dir_bnd_seg_ptr).getBoundarySegmentIndex());
    //    auto [B,x,nu]                   = p.evaluateBoundarySolution(pmsm.rotor_,loop_idx,dir_bnd_seg_idx,static_cast<double>(n)/N);
    //    B_norm_bnd(n)                   = nu.dot(B);

    //    // Compute the solution in the interior
    //    B_norm_int_cl(n)  = nu.dot(p.evaluateInteriorSolution(pmsm.rotor_,x-nu*1.0e-5));
    //    B_norm_int_far(n) = nu.dot(p.evaluateInteriorSolution(pmsm.rotor_,x-nu*1.0e-4));
    //});
    //sanji::figure("");
    //sanji::plot(VectorXd::LinSpaced(N,0,N-1)*2.0*M_PI/N,B_norm_bnd,{{"color",RED}});
    //sanji::plot(VectorXd::LinSpaced(N,0,N-1)*2.0*M_PI/N,B_norm_int_cl,{{"color",BLUE}});
    //sanji::plot(VectorXd::LinSpaced(N,0,N-1)*2.0*M_PI/N,B_norm_int_far,{{"color",BLACK}});
    //app.exec();
    //return 0;

    /* Solve the problem */
    //p.solveAndStore("results/PMSM");
    p.loadSolution("results/PMSM");
    //p.solve();

    // Add reference domains
    pmsm.addReferenceDomains(p);

    /* Plot the boundaries in the problem */
    sanji::figure("Permanent magnet synchronous machine simulation");
    for (const auto& boundary_segment_plotting_data : p.getBoundarySegmentsPlottingData(0.001/4.0, /* max_length_per_segment */
                                                                                        true       /* show_reference_boundaries */))
        sanji::plot(*boundary_segment_plotting_data.x,*boundary_segment_plotting_data.y,boundary_segment_plotting_data.style);

    /* Plot the normals in the problem */
    if (plot_normals)
        for (const auto& boundary_segment_plotting_vector_data : p.getNormals(0.001, /* max_length_per_segment */
                                                                              5.0e-4 /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);

    /* Plot the tangentials in the problem */
    if (plot_tangentials)
        for (const auto& boundary_segment_plotting_vector_data : p.getTangentials(0.001,  /* max_length_per_segment */
                                                                                  5.0e-4  /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);

    /* Plot the magnetic flux density */
    if (plot_field_data) {
        const auto field_data = p.getFieldData(1.0e6,load_field_data,store_field_data,"results/pmsm");
        double min =  std::numeric_limits<double>::max();
        double max = -std::numeric_limits<double>::max();
        for (const auto& field_data : field_data)
            for (uint i = 0; i < field_data.x->rows(); ++i) {
                const double strength = Vec2D{(*field_data.u)(i),(*field_data.v)(i)}.Norm2();
                if (strength < min) min = strength;
                if (strength > max) max = strength;
            }
        for (const auto& field_data : field_data)
            sanji::quiver(*field_data.x,
                          *field_data.y,
                          *field_data.u,
                          *field_data.v,{{"arrow_length",0.0006},{"colormap",TURBO},{"min",min},{"max",max}},{"use_colormap","center_arrows"});
                          //*field_data.v,{{"arrow_length",0.0006},{"colormap",TURBO},{"min",min},{"max",max}},{"use_colormap","center_arrows","use_logscale"});

        /* Print statistics */
        std::cout << "The minimum is " << min << std::endl;
        std::cout << "The maximum is " << max << std::endl;
    }

    ///* Compute the torque */
    //// Define GSL functions and workspaces
    //gsl_function               gsl_fun_x, gsl_fun_y;
    //gsl_integration_workspace* gsl_workspace_x = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
    //gsl_integration_workspace* gsl_workspace_y = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);

    //// Create a wrapper to simplify integration
    //const auto quad = [](gsl_function& gsl_fun, gsl_integration_workspace* workspace)->double {
    //    double result,error;
    //    gsl_integration_qag(&gsl_fun, /* Function to integrate */
    //                        0.0,      /* Lower integration limit */
    //                        1.0,      /* Upper integration limit */
    //                        INTEGRAL_ABSOLUTE_ERROR_LIMIT,
    //                        INTEGRAL_RELATIVE_ERROR_LIMIT,
    //                        MAX_NUM_SUBINTERVALS,
    //                        INTEGRATION_KEY,
    //                        workspace,&result,&error);
    //    return result;
    //};

    //// Create a lambda to evaluate the solution in one of the coils
    //const auto eval_sol_slot = [&p,&coils=pmsm.coils_](const Vec2D x, const uint coil_index)->const Vec2D {
    //    return p.evaluateInteriorSolution(coils[coil_index],x);
    //};

    //// Create a lambda to compute the integral in local y direction
    //const double** params_y = new const double*[1];
    //gsl_fun_y.params        = params_y;
    //const auto integral_y = [&gsl_fun_y,gsl_workspace_y,&quad,&params_y,&eval_sol_slot](const double t, const uint coil_index)->double {
    //    const double x    = (1.0-t)*cm1 + t*cm2;
    //    const double ymax = cm4/2.0*t + (1.0-t)*cm3/2.0;
    //    const auto   sol  = [coil_index,ymax,&eval_sol_slot,x](const double t)->double {
    //        const double  phi   = (2.0*M_PI/num_teeth)*coil_index;
    //        const complex coord = (x+2.0*(t-0.5)*ymax*1.0i)*(std::cos(phi) + 1.0i*std::sin(phi));
    //        const Vec2D   r     = Vec2D{coord.real(),coord.imag()};
    //        const Vec2D   B     = eval_sol_slot(r,coil_index);

    //        return r.dot(B);
    //    };
    //    gsl_fun_y.function = [](const double t, void* params)->double {
    //        const decltype(sol)& sol_ = *static_cast<const decltype(sol)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));

    //        return sol_(t);
    //    }; 
    //    params_y[0] = reinterpret_cast<const double*>(&sol);
    //    return quad(gsl_fun_y,gsl_workspace_y)*2.0*ymax;
    //};

    //// Create a lambda to compute the integral in local x direction
    //const auto integral_x = [&gsl_fun_x,gsl_workspace_x,lx=cm2-cm1,&quad]()->double {
    //    gsl_fun_x.function = [](const double t, void* params)->double {
    //        const decltype(integral_y) int_y      = *reinterpret_cast<const decltype(integral_y)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
    //        const uint                 coil_index = *reinterpret_cast<const uint*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[1]));

    //        return int_y(t,coil_index);
    //    };
    //    return quad(gsl_fun_x,gsl_workspace_x)*lx;
    //};

    //// Compute the torque contributions from the individual slots
    //double torque = 0.0;
    //const double** params_x = new const double*[2];
    //gsl_fun_x.params        = params_x;
    //params_x[0]             = reinterpret_cast<const double*>(&integral_y);
    //for (uint coil_index = 0; coil_index < num_teeth; ++coil_index) {
    //    params_x[1] = reinterpret_cast<const double*>(&coil_index);
    //    torque     += integral_x()*pmsm.current_densities_[coil_index];
    //}
    //delete[] params_x;
    //delete[] params_y;
    //gsl_integration_workspace_free(gsl_workspace_x);
    //gsl_integration_workspace_free(gsl_workspace_y);

    //// Print information
    //std::cout << "The total torque produced by the machine is " << torque << "Nm/m." << std::endl;

    //// Compute the boundary integrals of the tangential derivatives for all domains
    //std::cout << "The boundary integrals of the tangential derivative are:" << std::endl;
    //uint dom_cnt = 0;
    //for (const auto& dom_results : p.getTangentialDerivativesBoundaryIntegrals()) {
    //    if (std::get<1>(dom_results).size() != 0) std::cout << "Domain " << std::get<0>(dom_results) << ":" << std::endl;
    //    for (const double& r : std::get<1>(dom_results))
    //        std::cout << r << std::endl;
    //}

    /* Plot cosmetics */
    sanji::setPlotBackgroundColor(GRAY);
    sanji::setAxesRatio("equal");

    // Execute the application
    app.exec();
}
