#include "ReferenceDomain.hpp"

ReferenceDomain::ReferenceDomain(const c_domain_ptr underlying_domain, const initializer_list<vector<directed_boundary_segment_ptr>> dbsps, const ReferenceDomainOptions options) :
    Domain("___",dbsps,{options.filter,true,0.0,0.0,options.type}),
    underlying_domain_(underlying_domain)
{}

ReferenceDomain::ReferenceDomain(const c_domain_ptr underlying_domain, const vector<vector<directed_boundary_segment_ptr>>& dbsps, const ReferenceDomainOptions options) :
    Domain("___",dbsps,{options.filter,true,0.0,0.0,options.type}),
    underlying_domain_(underlying_domain)
{}

uint ReferenceDomain::getDomainIndex() const {
    return underlying_domain_->getDomainIndex();
}
