#include "ArcedBoundarySegment.hpp"
#include <cmath>
#include <iostream>

ArcedBoundarySegment::ArcedBoundarySegment(const double radius,
                                           const Vec2D& center,
                                           const double phi_min,
                                           const double phi_max,
                                           const uint   num_nodes,
                                           const uint   boundary_solution_polynomial_order,
                                           const bool   normal_points_to_the_right,
                                           const bool   is_reference) :
    BoundarySegment(false, /* is_closed */
                    num_nodes,
                    boundary_solution_polynomial_order,
                    normal_points_to_the_right,
                    is_reference),
    radius_(radius > 0.0 ? radius : throw new std::runtime_error("ArcedBoundarySegment: Radius must be greater than zero.")),
    center_(center),
    phi_min_(phi_min),
    phi_max_(phi_max)
{
    if (phi_min >= phi_max) {
        std::cout << "ArcedBoundarySegment: phi_min must be strictly smaller than phi_max, but phi_min=" << phi_min << " and phi_max=" << phi_max << "." << std::endl;
        abort();
    }
    if (std::abs(phi_max-phi_min) >= 2.0*M_PI) {
        std::cout << "ArcedBoundarySegment: Invalid values provided for phi_min and phi_max." << std::endl;
        abort();
    }
}

double ArcedBoundarySegment::getLength() const {
    return (phi_max_-phi_min_)*radius_;
}

const Vec2D ArcedBoundarySegment::getBoundaryPoint(const double t) const {
    if (t > 1.0 || t < 0.0) {
        std::cout << "Invalid argument provided to StraightBoundarySegment::getBoundaryPoint." << std::endl;
        abort();
    }

    const double phi = phi_min_ + t*(phi_max_ - phi_min_);

    return center_ + radius_*Vec2D{std::cos(phi),std::sin(phi)};
}

const std::pair<Vec2D,Vec2D> ArcedBoundarySegment::getNormal(const double t) const {
    const double phi   = phi_min_ + t*(phi_max_ - phi_min_);
    const Vec2D normal = normal_points_to_the_right_ ? Vec2D{std::cos(phi),std::sin(phi)} : Vec2D{-std::cos(phi),-std::sin(phi)};
    return {getBoundaryPoint(t),normal};
}

const std::pair<Vec2D,Vec2D> ArcedBoundarySegment::getTangential(const double t) const {
    const double phi        = phi_min_ + t*(phi_max_ - phi_min_);
    const Vec2D  tangential = normal_points_to_the_right_ ? Vec2D{-std::sin(phi),std::cos(phi)} : Vec2D{std::sin(phi),-std::cos(phi)};
    return {getBoundaryPoint(t),tangential};
}

std::string ArcedBoundarySegment::getClassName() const {
    return "ArcedBoundarySegment";
}

std::function<const Vec2D(const double)> ArcedBoundarySegment::getnu(const uint i) const {
    // Precompute constants
    const double delta_phi = (phi_max_ - phi_min_) / (num_nodes_ - 1);
    const double phi_min   = phi_min_ + i*delta_phi;

    if (normal_points_to_the_right_) return [delta_phi,phi_min](const double t)->const Vec2D {
        const double phi = phi_min+t*delta_phi;
        return Vec2D{std::cos(phi),std::sin(phi)}; };
    else                             return [delta_phi,phi_min](const double t)->const Vec2D {
        const double phi = phi_min+t*delta_phi;
        return Vec2D{-std::cos(phi),-std::sin(phi)}; };
}

std::function<const Vec2D(const double)> ArcedBoundarySegment::gett(const uint i) const {
    // Precompute constants
    const double delta_phi = (phi_max_ - phi_min_) / (num_nodes_ - 1);
    const double phi_min   = phi_min_ + i*delta_phi;

    if (normal_points_to_the_right_) return [delta_phi,phi_min](const double t)->const Vec2D {
        const double phi = phi_min+t*delta_phi;
        return Vec2D{-std::sin(phi),std::cos(phi)}; };
    else                             return [delta_phi,phi_min](const double t)->const Vec2D {
        const double phi = phi_min+t*delta_phi;
        return Vec2D{std::sin(phi),-std::cos(phi)}; };
}

std::function<const Vec2D(const double)> ArcedBoundarySegment::getpsii(const uint i) const {
    // Check the argument
    if (i >= num_nodes_ - 1) throw std::runtime_error("Called ArcedBoundarySegment::getpsii with i="+std::to_string(i)+", but num_nodes_="+std::to_string(num_nodes_)+".");

    // Precompute constants
    const double delta_phi = (phi_max_ - phi_min_) / (num_nodes_ - 1);
    const double phi_min   = phi_min_ + i*delta_phi;

    // Return the segment parametrization
    return [center=this->center_,radius=this->radius_,delta_phi,phi_min](const double t)->const Vec2D {
        const double phi = phi_min+t*delta_phi;
        return center + radius*Vec2D{std::cos(phi),std::sin(phi)};
    };
}

std::function<double(const double)> ArcedBoundarySegment::getdpsii2Norm(const uint i) const {
    return [ret=radius_*(phi_max_-phi_min_)/(num_nodes_-1)](const double t)->double {
        return ret;
    };
}
