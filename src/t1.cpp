const auto WtPsi = [&quad_inner](const Vec2D                                     t_vec_jk,
                                 const Vec2D                                     x,
                                 const uint                                      l,
                                 const std::function<const Vec2D(const double)>& psi_i_jk,
                                 const std::function<double(const double)>&      dpsi_i_jk_2Norm,
                                 const bool                                      has_singularity,
                                 const double                                    singularity,
                                 gsl_function&                                   gsl_function_inner,
                                 gsl_integration_workspace*                      gsl_workspace_inner)->double {
    gsl_function_inner.function = [](const double t, void* params)->double {
        const auto& m_l             = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
        const auto& psi_i_jk        = *static_cast<const std::function<const Vec2D(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[1]));
        const auto& dpsi_i_jk_2Norm = *static_cast<const std::function<double(const double)>*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[2]));

        const Vec2D& x   = *static_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[3]));
        const Vec2D  y   = psi_i_jk(t);

        const Vec2D& t_vec_jk = *static_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[4]));

        return GradE(x-y).dot(t_vec_jk)*m_l(t)*dpsi_i_jk_2Norm(t);
    };
    std::function<double(const double t)> m_l = [l](const double t)->double {
        return mono(t,l);
    };

    const double** params = new const double*[5];
    params[0]             = reinterpret_cast<const double*>(&m_l);
    params[1]             = reinterpret_cast<const double*>(&psi_i_jk);
    params[2]             = reinterpret_cast<const double*>(&dpsi_i_jk_2Norm);
    params[3]             = reinterpret_cast<const double*>(&x);
    params[4]             = reinterpret_cast<const double*>(&t_vec_jk);

    gsl_function_inner.params = params;
    double result;
    if (has_singularity) {
        result  = quad_inner(0.0,std::max(0.0,singularity-1e-2));
        result += quad_inner(std::min(singularity+1e-2,1.0),1.0);
    } else result = quad_inner(0.0,1.0);
    delete[] params;
    return result;
};
