#include "ExteriorDomain.hpp"

ExteriorDomain::ExteriorDomain(const string& name, const initializer_list<vector<directed_boundary_segment_ptr>> bsps, const double mu) :
    Domain(name,bsps,{DomainOptions::FilterType([](const Vec2D x){ return true; }),false,mu,0.0,-1})
{}

ExteriorDomain::ExteriorDomain(const string& name, const vector<vector<directed_boundary_segment_ptr>>& bsps, const double mu) :
    Domain(name,bsps,{DomainOptions::FilterType([](const Vec2D x){ return true; }),false,mu,0.0,-1})
{}
