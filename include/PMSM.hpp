#pragma once

#include "ProblemWrapper.hpp"
#include <Eigen/Dense>
#include <vector>

class PMSM {
private:

/* Type definitions */
template <typename T>
using vector   = std::vector<T>;
using VectorXd = Eigen::VectorXd;

public:

explicit PMSM(       ProblemWrapper& p,
               const double          rotor_angle,
               const double          max_coil_current_density,
               const VectorXd&       motor_current_factors,
               const uint            num_magnets,
               const uint            num_teeth,
               const double          cm1,
               const double          cm2,
               const double          cm3,
               const double          cm4);

void addReferenceDomains(ProblemWrapper& p) const;

// Boundaries
vector<vector<boundary_segment_ptr>> all_coils_boundary_segments_;
boundary_segment_ptr                 rotor_outer_rim_;

// Domains
vector<domain_ptr> coils_;
domain_ptr         inner_air_;
domain_ptr         yoke_;
domain_ptr         rotor_;

// Miscellaneous
vector<double> current_densities_;

/* Parameters */

// General
const uint num_magnets_;
const uint num_teeth_;

// Coils
const double cm1_;
const double cm2_;
const double cm3_;
const double cm4_;

};
