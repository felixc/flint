#pragma once

#include <memory>
#include "BoundarySegment.hpp"

/* Forward declarations */
class Domain;

/* Type definitions */
using uint                   = unsigned int;
template <typename T>
using c_shared_ptr           = std::shared_ptr<const T>;
using c_domain_ptr           = c_shared_ptr<Domain>;
using c_boundary_segment_ptr = c_shared_ptr<BoundarySegment>;

class BoundarySegmentInformation {
public:

/* Constructor */
BoundarySegmentInformation(const c_boundary_segment_ptr b_seg_ptr, const c_domain_ptr dom_ptr);

/* Getter & Setter */
double getLength() const;

uint getNumNodes() const;

void setNumNodes(const uint numNodes);

uint getBoundarySolutionPolynomialDegree() const;

private:

const c_boundary_segment_ptr b_seg_ptr_;
const c_domain_ptr           dom_ptr_;

uint numNodes_;

};
