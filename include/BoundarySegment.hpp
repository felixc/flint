#pragma once

#include <memory>
#include <vector>
#include <functional>
#include <utility>
#include <tuple>
#include <string>
#include "Vec2D.hpp"

class BoundarySegment;
class DirectedBoundarySegment;

/* Type definitions */
using boundary_segment_ptr          = std::shared_ptr<BoundarySegment>;
using c_boundary_segment_ptr        = std::shared_ptr<const BoundarySegment>;
using directed_boundary_segment_ptr = std::shared_ptr<DirectedBoundarySegment>;

#include "Domain.hpp"

/* Type definitions */
using uint = unsigned int;

class BoundarySegment {
protected:

/* Type definitions */
template <typename T>
using vector = std::vector<T>;
template <typename T,typename U>
using pair   = std::pair<T,U>;
template <class... Types>
using tuple  = std::tuple<Types...>;
using string = std::string;

public:

/* Constructor */
explicit BoundarySegment(const bool   is_closed,
                         const uint   num_nodes,
                         const uint   boundary_solution_polynomial_order,
                         const bool   normal_points_to_the_right,
                         const bool   is_reference,
                         const double surface_curren_density=0.0);

/* Getter */
virtual double                                   getLength()                                     const = 0;
bool                                             getNormalPointsToTheRight()                     const;
uint                                             getNumNodes()                                   const;
uint                                             getNumSubsegments()                             const;
uint                                             getFunctionApproximationPolynomialOrder()       const;
const Domain&                                    getNeighbouringDomain(const uint i)             const;
uint                                             getOtherNeighbourIndex(const uint domain_index) const;
const Domain*                                    getOtherDomain(const Domain* domain)            const;
uint                                             getNumNeighbouringDomains()                     const;
uint                                             getBoundarySegmentIndex()                       const;
virtual const Vec2D                              getBoundaryPoint(const double t)                const = 0;
virtual const pair<Vec2D,Vec2D>                  getNormal(const double t)                       const = 0;
virtual const pair<Vec2D,Vec2D>                  getTangential(const double t)                   const = 0;
bool                                             isReference()                                   const;
const Vec2D                                      getStartPoint()                                 const;
const Vec2D                                      getEndPoint()                                   const;
double                                           getSurfaceCurrentDensity()                      const;
virtual string                                   getClassName()                                  const = 0;
virtual std::function<const Vec2D(const double)> getnu(const uint i)                             const = 0;
virtual std::function<const Vec2D(const double)> gett(const uint i)                              const = 0;
virtual std::function<const Vec2D(const double)> getpsii(const uint i)                           const = 0;
virtual std::function<double(const double)>      getdpsii2Norm(const uint i)                     const = 0;

/* Setter */
void setBoundarySegmentIndex(const uint boundary_segment_index);

/* Miscellanous */
void addNeighbouringDomain(const Domain* dptr);

protected:

const uint num_nodes_;
const bool normal_points_to_the_right_;

private:

friend DirectedBoundarySegment;

const bool            is_closed_;
const uint            boundary_solution_polynomial_order_;
vector<const Domain*> neighbouring_domain_ptrs_;
uint                  boundary_segment_index_;
const bool            is_reference_;
const double          surface_current_density_;

};

class DirectedBoundarySegment {

/* Type definitions */
template <typename T,typename U>
using pair  = std::pair<T,U>;
template <class... Types>
using tuple = std::tuple<Types...>;

public:

/* Constructor */
explicit DirectedBoundarySegment(const boundary_segment_ptr bsp, const bool is_forward);

/* Setter */
void setAssociatedDomain(const Domain* associated_domain);

/* Getter */
uint                                     getNumSubsegments()                                 const;
std::function<const Vec2D(const double)> getpsii(const uint i)                               const;
std::function<double(const double)>      getdpsii2Norm(const uint i)                         const;
std::function<const Vec2D(const double)> getnu(const uint i)                                 const;
std::function<const Vec2D(const double)> gett(const uint i)                                  const;
const Domain&                            getNeighbouringDomain()                             const;
const DirectedBoundarySegment&           getNeighboursDirectedBoundarySegment()              const;
tuple<uint,uint>                         getNeighboursDirectedBoundarySegmentIndex()         const;
uint                                     getTrueSubsegmentIndex(const uint subsegment_index) const;
const Vec2D                              getStartPoint()                                     const;
const Vec2D                              getEndPoint()                                       const;
const Vec2D                              getBoundaryPoint(const double t)                    const;

/* Miscellaneous */
bool isForward() const;

BoundarySegment&       operator*();
const BoundarySegment& operator*() const;

private:

const boundary_segment_ptr bsp_;
const bool                 is_forward_;
const Domain*              associated_domain_;

};
