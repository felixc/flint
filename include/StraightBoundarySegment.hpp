#pragma once

#include "Vec2D.hpp"
#include "BoundarySegment.hpp"

class StraightBoundarySegment : public BoundarySegment {
public:

/* Constructor */
explicit StraightBoundarySegment(const Vec2D  start,
                                 const Vec2D  end,
                                 const uint   num_nodes,
                                 const uint   boundary_solution_polynomial_order,
                                 const bool   normal_points_to_the_right,
                                 const bool   is_reference,
                                 const double surface_current_density=0.0);

/* Getter */
double                                   getLength()                      const override;
const Vec2D                              getBoundaryPoint(const double t) const override;
const pair<Vec2D,Vec2D>                  getNormal(const double t)        const override;
const pair<Vec2D,Vec2D>                  getTangential(const double t)    const override;
string                                   getClassName()                   const override;
std::function<const Vec2D(const double)> getnu(const uint i)              const override;
std::function<const Vec2D(const double)> gett(const uint i)               const override;
std::function<const Vec2D(const double)> getpsii(const uint i)            const override;
std::function<double(const double)>      getdpsii2Norm(const uint i)      const override;

private:

const Vec2D start_;
const Vec2D end_;

};
