#pragma once

#include <vector>
#include <memory>
#include <initializer_list>
#include <tuple>
#include <string>
#include <functional>

class Domain;

/* Type definitions */
using domain_ptr   = std::shared_ptr<Domain>;
using c_domain_ptr = std::shared_ptr<const Domain>;

#include "BoundarySegment.hpp"

struct DomainOptions {
    using FilterType = std::function<bool(const Vec2D)>;

    const FilterType filter       = FilterType([](const Vec2D x){ return true; });
    const bool       is_reference = false;
    const double     mu           = 0.0;
    const double     J            = 0.0;
    const int        type         = -1;
};

class Domain {
protected:

/* Type definitions */
using uint             = unsigned int;
template <typename T>
using initializer_list = std::initializer_list<T>;
using string           = std::string;
template <typename T>
using vector           = std::vector<T>;
template <typename U, typename T>
using pair             = std::pair<U,T>;
template <class... Types>
using tuple            = std::tuple<Types...>;

public:

/* Constructor */
explicit Domain(const string& name, const initializer_list<vector<directed_boundary_segment_ptr>> bsps, const DomainOptions options);
explicit Domain(const string& name, const vector<vector<directed_boundary_segment_ptr>>&          bsps, const DomainOptions options);

/* Setter */
void setDomainIndex(const uint domain_index);

/* Getter */
virtual uint                                         getDomainIndex() const;
const vector<vector<directed_boundary_segment_ptr>>& getDirectedBoundarySegmentPointers() const;
const DirectedBoundarySegment&                       getDirectedBoundarySegment(const uint boundary_segment_index) const;
tuple<uint,uint>                                     getDirectedBoundarySegmentIndex(const uint boundary_segment_index) const;
string                                               getName() const;
double                                               getmu() const;
double                                               getJ() const;
bool                                                 isReference() const;
int                                                  getType() const;
uint                                                 getLoopIndex(const directed_boundary_segment_ptr& dir_bnd_seg_ptr) const;
double                                               getApproximateArea() const;
const DomainOptions::FilterType&                     getFilter() const;

private:

const string                                  name_;
const double                                  mu_;
const double                                  J_;
vector<vector<directed_boundary_segment_ptr>> directed_boundary_segment_ptrs_;
uint                                          domain_index_;
const bool                                    is_reference_;
const int                                     type_;
const DomainOptions::FilterType               filter_;

};
