#pragma once

#include <memory>
#include <vector>
#include <tuple>
#include <Eigen/Dense>
#include "Domain.hpp"
#include "BoundarySegment.hpp"

/* Forward declarations */
class ProblemWrapper;

/* Type definitions */
using unique_mat_ptr = std::unique_ptr<Eigen::MatrixXd>;
using unique_vec_ptr = std::unique_ptr<Eigen::VectorXd>;

struct SolverOptions {
    bool        load_solution;
    bool        store_solution;
    std::string folder_name;
    bool        debug;
};

class Problem {

/* Type definitions */
using uint     = unsigned int;
template <typename T>
using vector   = std::vector<T>;
using MatrixXd = Eigen::MatrixXd;
using VectorXd = Eigen::VectorXd;
template <class... Types>
using tuple    = std::tuple<Types...>;

public:

/* Constructor */
Problem() = default;

void solve(const SolverOptions options);

/* Modifiers */
void addDomain(const domain_ptr dptr);
void addBoundarySegment(const boundary_segment_ptr bptr);

/* Miscellaneous */
Vec2D evaluateInteriorSolution(const uint domain_index, const Vec2D x) const;
tuple<Vec2D/* B */,Vec2D/* x */,Vec2D/* nu */> evaluateBoundarySolution(const uint dom_idx, const uint outer_loop_idx, const uint dir_bnd_seg_idx, const double t) const;

private:

friend ProblemWrapper;

/* Miscellaneous */
void           initializeHelperVariables();
unique_mat_ptr allocateSystemMatrix() const;
unique_vec_ptr allocateRHS() const;
void           store_solution(const std::string& folder_name) const;
void           load_solution(const std::string& folder_name);

vector<c_domain_ptr>           domain_ptrs_;
vector<c_boundary_segment_ptr> boundary_segment_ptrs_;
vector<uint>                   domain_system_matrix_column_offsets_;
unique_vec_ptr                 sol_;
    
};
