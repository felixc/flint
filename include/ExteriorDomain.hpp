#pragma once

#include "Domain.hpp"

class ExteriorDomain : public Domain {
public:

/* Constructor */
explicit ExteriorDomain(const string& name, const initializer_list<vector<directed_boundary_segment_ptr>> bsps, const double mu);
explicit ExteriorDomain(const string& name, const vector<vector<directed_boundary_segment_ptr>>& bsps, const double mu);

};
