#pragma once

#include "Domain.hpp"
#include <functional>

struct ReferenceDomainOptions {
    using FilterType = std::function<bool(const Vec2D)>;

    const int        type   = -1;
    const FilterType filter = FilterType([](const Vec2D x){ return true; });
};

class ReferenceDomain : public Domain {
public:

/* Constructor */
explicit ReferenceDomain(const c_domain_ptr underlying_domain, const initializer_list<vector<directed_boundary_segment_ptr>> dbsps, const ReferenceDomainOptions options);
explicit ReferenceDomain(const c_domain_ptr underlying_domain, const vector<vector<directed_boundary_segment_ptr>>& dbsps, const ReferenceDomainOptions options);

/* Getter */
virtual uint getDomainIndex() const override;

private:

const c_domain_ptr underlying_domain_;

};
