#pragma once

#include <tuple>
#include <memory>
#include <functional>
#include <Eigen/Dense>
#include "BoundarySegment.hpp"
#include "Problem.hpp"

/* Type definitions */
template <class... Types>
using tuple      = std::tuple<Types...>;
using VectorXd   = Eigen::VectorXd;
using MatrixXd   = Eigen::MatrixXd;
template <typename T>
using shared_ptr = std::shared_ptr<T>;
using vec_ptr    = shared_ptr<VectorXd>;
template <typename T,typename U>
using pair       = std::pair<T,U>;

tuple<vec_ptr,vec_ptr>                 getBoundarySegmentDataPoints(const c_boundary_segment_ptr boundary_segment_ptr, const double max_length_per_segment);
tuple<vec_ptr,vec_ptr,vec_ptr,vec_ptr> getNormals(const BoundarySegment& bs, const double max_length_per_segment);
tuple<vec_ptr,vec_ptr,vec_ptr,vec_ptr> getTangentials(const BoundarySegment& bs, const double max_length_per_segment);
tuple<vec_ptr,vec_ptr,vec_ptr,vec_ptr> getFieldDataType1(const Problem& p, const c_domain_ptr dom_ptr, const uint num_points_suggestion);
tuple<vec_ptr,vec_ptr,vec_ptr,vec_ptr> getFieldDataType2(const Problem& p, const c_domain_ptr dom_ptr, const uint num_levels, const uint num_points_suggestion,
    const std::function<bool(const Vec2D)>& filter);
tuple<vec_ptr,vec_ptr,vec_ptr,vec_ptr> getFieldDataType3(const Problem& p, const c_domain_ptr dom_ptr, const uint num_levels, const uint num_points_suggestion);
tuple<vec_ptr,vec_ptr,vec_ptr,vec_ptr> getFieldDataType4(const Problem& p, const c_domain_ptr dom_ptr, const uint num_levels, const uint num_points_suggestion);
