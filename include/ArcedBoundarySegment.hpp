#pragma once

#include "Vec2D.hpp"
#include "BoundarySegment.hpp"

class ArcedBoundarySegment : public BoundarySegment {
public:

/* Constructor */
explicit ArcedBoundarySegment(const double radius,
                              const Vec2D& center,
                              const double phi_min,
                              const double phi_max,
                              const uint   num_nodes,
                              const uint   boundary_solution_polynomial_order,
                              const bool   normal_points_to_the_right,
                              const bool   is_reference);

/* Getter */
double                                   getLength()                      const override;
const Vec2D                              getBoundaryPoint(const double t) const override;
const pair<Vec2D,Vec2D>                  getNormal(const double t)        const override;
const pair<Vec2D,Vec2D>                  getTangential(const double t)    const override;
string                                   getClassName()                   const override;
std::function<const Vec2D(const double)> getnu(const uint i)              const override;
std::function<const Vec2D(const double)> gett(const uint i)               const override;
std::function<const Vec2D(const double)> getpsii(const uint i)            const override;
std::function<double(const double)>      getdpsii2Norm(const uint i)      const override;

private:

const double radius_;
const Vec2D  center_;
const double phi_min_;
const double phi_max_;

};
