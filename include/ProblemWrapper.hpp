#pragma once

#include <memory>
#include <unordered_map>
#include <string>
#include <vector>
#include <Eigen/Dense>
#include <initializer_list>
#include <tuple>
#include <functional>
#include "Problem.hpp"
#include "Vec2D.hpp"
#include "ReferenceDomain.hpp"

/* Type definitions */
template <typename T>
using unique_ptr       = std::unique_ptr<T>;
template <typename T>
using shared_ptr       = std::shared_ptr<T>;
using problem_ptr      = unique_ptr<Problem>;
template <typename K, typename T>
using unordered_map    = std::unordered_map<K,T>;
using string           = std::string;
using VectorXd         = Eigen::VectorXd;
using MatrixXd         = Eigen::MatrixXd;
using vec_ptr          = shared_ptr<VectorXd>;
using Style            = unordered_map<string,double>;
template <typename T>
using initializer_list = std::initializer_list<T>;
template <class... Types>
using tuple            = std::tuple<Types...>;

struct BoundarySegmentPlottingData {
    BoundarySegmentPlottingData(const vec_ptr& x, const vec_ptr& y, const Style& style) :
        x(x),
        y(y),
        style(style)
    {}
    vec_ptr x;
    vec_ptr y;
    Style   style;
};

struct BoundarySegmentPlottingVectorData {
    BoundarySegmentPlottingVectorData(const vec_ptr& x, const vec_ptr& y, const vec_ptr& u, const vec_ptr& v, const Style& style) :
        x(x),
        y(y),
        u(u),
        v(v),
        style(style)
    {}
    vec_ptr x;
    vec_ptr y;
    vec_ptr u;
    vec_ptr v;
    Style   style;
};

class ProblemWrapper {

/* Type definitions */
template <typename T>
using vector = std::vector<T>;

public:

/* Constructor */
explicit ProblemWrapper();

/* Miscellaneous */
boundary_segment_ptr addStraightBoundarySegment(const Vec2D& start,
                                                const Vec2D& end,
                                                const uint   num_nodes,
                                                const uint   poly_order,
                                                const bool   normal_points_to_the_right,
                                                const bool   is_reference=false,
                                                const double surface_current_density=0.0);
boundary_segment_ptr addArcedBoundarySegment(const double radius,
                                             const Vec2D& center,
                                             const double phi_min,
                                             const double phi_max,
                                             const uint   num_nodes,
                                             const uint   boundary_solution_polynomial_order,
                                             const bool   normal_points_to_the_right,
                                             const bool   is_reference=false);
boundary_segment_ptr addCircularBoundarySegment(const double radius,
                                                const Vec2D& center,
                                                const uint   num_nodes,
                                                const uint   boundary_solution_polynomial_order,
                                                const bool   normal_points_to_the_right,
                                                const bool   is_reference=false);
domain_ptr addDomain(const string& name, const vector<vector<directed_boundary_segment_ptr>>& dbsps, const double mu, const double J, const int type=-1);
domain_ptr addDomain(const string& name, const initializer_list<vector<directed_boundary_segment_ptr>> dbsps, const double mu, const double J, const int type=-1);
domain_ptr addExteriorDomain(const string& name, const vector<vector<directed_boundary_segment_ptr>>& dbsps, const double mu);
domain_ptr addExteriorDomain(const string& name, const initializer_list<vector<directed_boundary_segment_ptr>> dbsps, const double mu);
domain_ptr addReferenceDomain(const c_domain_ptr underlying_domain, const vector<vector<directed_boundary_segment_ptr>>& dbsps, const ReferenceDomainOptions options);
domain_ptr addReferenceDomain(const c_domain_ptr underlying_domain, const initializer_list<vector<directed_boundary_segment_ptr>> dbsps, const ReferenceDomainOptions options);

vector<BoundarySegmentPlottingData>       getBoundarySegmentsPlottingData(const double max_length_per_segment, const bool show_reference_boundaries) const;
vector<BoundarySegmentPlottingVectorData> getNormals(const double max_length_per_segment, const double arrow_length) const;
vector<BoundarySegmentPlottingVectorData> getTangentials(const double max_length_per_segment, const double arrow_length) const;
vector<BoundarySegmentPlottingVectorData> getFieldData(const double arrow_density=8.0e5, const bool load_field_data=false, const bool store_field_data=false, const string folder_name="") const;
MatrixXd getFieldData(const c_domain_ptr underlying_domain, const MatrixXd& positions) const;

Vec2D evaluateInteriorSolution(const c_domain_ptr dom_ptr, const Vec2D x) const;
tuple<Vec2D/* B */,Vec2D/* x */,Vec2D/* nu */> evaluateBoundarySolution(const domain_ptr dom_ptr, const uint loop_idx, const uint dir_bnd_seg_idx, const double t) const;

vector<tuple<string,vector<double>>> getTangentialDerivativesBoundaryIntegrals() const;

void solve(const bool debug=false);
void solveAndStore(const string& folder_name, const bool debug=false);
void loadSolution(const string& folder_name);

private:

problem_ptr p_ptr_;

};
