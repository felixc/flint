#include <cmath>
#include <iostream>
#include <gsl/gsl_integration.h>
#include <Eigen/Dense>
#include "ProblemWrapper.hpp"
#include "Sanji.hpp"

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 1e-8
#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-6
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          1000

using namespace sanji::colors;

/* Type definitions */
using DBS = DirectedBoundarySegment;
template <typename T>
using vector   = std::vector<T>;
using VectorXd = Eigen::VectorXd;
using MatrixXd = Eigen::MatrixXd;

// Visualization
constexpr bool plot_normals       = false;
constexpr bool plot_tangentials   = false;
constexpr bool plot_field_data    = true;
constexpr bool compute_statistics = true;

// Permeabilities
constexpr double mu0    = 4.0*M_PI*1e-7;
constexpr double mu_air = 1.00000037*mu0; // https://www.engineeringtoolbox.com/permeability-d_1923.html
constexpr double mu_cu  = 0.999994*mu0;   // https://www.engineeringtoolbox.com/permeability-d_1923.html

// Geometric parameters
constexpr double cable_radius = 0.01;

// Current densities
constexpr double J = 1.0/1.0e-6;

int main(int argc, char* argv[]) {
    // Initialize this as a Qt application
    QApplication app(argc, argv);
    sanji::init();

    /* Create a problem instance */
    ProblemWrapper p;

    // Create the boundaries in the problem
    const auto Gamma_1 = p.addCircularBoundarySegment(cable_radius,  /* Radius */
                                                      Vec2D{0.0,0.0},/* Center */
                                                      5,             /* NumNodes */
                                                      1,             /* boundarySolutionPolynomialOrder */
                                                      true           /* normal_points_to_the_right */);
    const auto Gamma_2 = p.addCircularBoundarySegment(cable_radius,    /* Radius */
                                                      Vec2D{0.03,0.03},/* Center */
                                                      5,               /* NumNodes */
                                                      1,               /* boundarySolutionPolynomialOrder */
                                                      true             /* normal_points_to_the_right */);
    const auto Gamma_3 = p.addCircularBoundarySegment(cable_radius,     /* Radius */
                                                      Vec2D{0.03,-0.04},/* Center */
                                                      5,                /* NumNodes */
                                                      1,                /* boundarySolutionPolynomialOrder */
                                                      true              /* normal_points_to_the_right */);

    // Create the domains in the problem
    vector<domain_ptr> cables(3);
    cables[0] = p.addDomain("wire_0",                               /* name */
                            {{std::make_shared<DBS>(Gamma_1,true)}},/* Boundary information */
                            mu_cu,                                  /* Magnetic permeability */
                            J,                                      /* Current density */ 
                            2                                       /* Type */
                            );
    cables[1] = p.addDomain("wire_1",                               /* name */
                            {{std::make_shared<DBS>(Gamma_2,true)}},/* Boundary information */
                            mu_cu,                                  /* Magnetic permeability */
                            J,                                      /* Current density */
                            2                                       /* Type */
                            );
    cables[2] = p.addDomain("wire_2",                               /* name */
                            {{std::make_shared<DBS>(Gamma_3,true)}},/* Boundary information */
                            mu_cu,                                  /* Magnetic permeability */
                            J,                                      /* Current density */
                            2                                       /* Type */
                            );
    const auto air = p.addExteriorDomain("air",{{std::make_shared<DBS>(Gamma_1,false)},
                                                {std::make_shared<DBS>(Gamma_2,false)},
                                                {std::make_shared<DBS>(Gamma_3,false)}},/* Boundary information */
                                               mu_air                                   /* Magnetic permeability */);

    /* Solve the problem */
    p.solve(true /* debug */);
    //p.solveAndStore("results/three_wires");
    //p.loadSolution("results/three_wires");

    /* Plot the boundaries in the problem */
    sanji::figure("Three wires simulation");
    for (const auto& boundary_segment_plotting_data : p.getBoundarySegmentsPlottingData(0.001/4.0, /* max_length_per_segment */
                                                                                        true       /* show_reference_boundaries */))
        sanji::plot(*boundary_segment_plotting_data.x,*boundary_segment_plotting_data.y,boundary_segment_plotting_data.style);

    /* Plot the normals in the problem */
    if (plot_normals)
        for (const auto& boundary_segment_plotting_vector_data : p.getNormals(0.001, /* max_length_per_segment */
                                                                              1.0e-3 /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);

    /* Plot the magnetic flux density */
    if (plot_field_data) {
        const auto field_data = p.getFieldData();
        double min =  std::numeric_limits<double>::max();
        double max = -std::numeric_limits<double>::max();
        for (const auto& field_data : field_data)
            for (uint i = 0; i < field_data.x->rows(); ++i) {
                const double strength = Vec2D{(*field_data.u)(i),(*field_data.v)(i)}.Norm2();
                if (strength < min) min = strength;
                if (strength > max) max = strength;
            }
        for (const auto& field_data : field_data)
            sanji::quiver(*field_data.x,
                          *field_data.y,
                          *field_data.u,
                          *field_data.v,{{"arrow_length",0.001},{"use_colormap",1},{"colormap",TURBO},{"min",min},{"max",max}});
    }

    /* Plot cosmetics */
    sanji::setPlotBackgroundColor(GRAY);
    sanji::setAxesRatio("equal");

    /* Compute problem statistics */
    if (compute_statistics) {
        // Create a wrapper to simplify integration
        gsl_function               gsl_fun;
        gsl_integration_workspace* gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
        const auto quad = [&gsl_fun,gsl_workspace]()->double {
            double result,error;
            gsl_integration_qag(&gsl_fun, /* Function to integrate */
                                0.0,      /* Lower integration limit */
                                1.0,      /* Upper integration limit */
                                INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                                INTEGRAL_RELATIVE_ERROR_LIMIT,
                                MAX_NUM_SUBINTERVALS,
                                INTEGRATION_KEY,
                                gsl_workspace,&result,&error);
            return result;
        };

        // Create a lambda to evaluate the solution in cable 0
        const auto eval_sol_cable0 = [&p,cable0=cables[0]](const Vec2D x)->const Vec2D {
            return p.evaluateInteriorSolution(cable0,x);
        };

        // Integrate the magnetic field at different radii
        const uint   Nr         = 10u;
        const double min_radius = cable_radius/(Nr+1u);
        const double max_radius = (cable_radius*Nr)/(Nr+1u);
        VectorXd     radii(Nr+1u);
        VectorXd     circular_integrals(Nr+1u);
        for (uint radius_idx = 0u; radius_idx < Nr+1u; ++radius_idx) {
            const double radius = min_radius + (max_radius-min_radius)*radius_idx/static_cast<double>(Nr);
            radii(radius_idx)   = radius;
            gsl_fun.function = [](const double t, void* params)->double {
                const decltype(eval_sol_cable0)& eval_sol_cable0_ = *static_cast<const decltype(eval_sol_cable0)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
                const double& radius                              = *reinterpret_cast<const double**>(params)[1];

                const double phi    = t*2.0*M_PI;
                const Vec2D  bnd_pt = radius*Vec2D{std::cos(phi),std::sin(phi)};
                const Vec2D  B      = eval_sol_cable0_(bnd_pt);
                const Vec2D  tan    = Vec2D{-std::sin(phi),std::cos(phi)};

                return tan.dot(B);
            };
            const double** params          = new const double*[2];
            params[0]                      = reinterpret_cast<const double*>(&eval_sol_cable0);
            params[1]                      = &radius;
            gsl_fun.params                 = params;
            circular_integrals(radius_idx) = quad()*2.0*M_PI*radius;
            delete[] params;
        }

        // Free memory
        gsl_integration_workspace_free(gsl_workspace);

        // Compute the mean negative error exponent
        double       mean_s  = 0.0;
        for (uint radius_idx = 0; radius_idx < Nr+1; ++radius_idx) {
            const double radius = min_radius + (max_radius-min_radius)*radius_idx/static_cast<double>(Nr);
            const double current = radius*radius*M_PI*J;
            const double c       = (std::abs(current-circular_integrals(radius_idx)/mu0)+1.0e-30)/std::abs(current);
            mean_s              -= std::log10(c);
        }

        // Print out information about the accuracy
        std::cout << "An estimate of the accuracy of the simulation results can be obtained by comparing the closed-contour integral of the H field to the enclosed current. ";
        std::cout << "Computing the relative error as (|current - closed_contour_integral/mu| + 1.0e-30)/|current| = 10^-s results in an average s of " << mean_s/(Nr+1u) << "." << std::endl;
    }

    // Compute the boundary integrals of the tangential derivatives for all domains
    std::cout << "The boundary integrals of the tangential derivative are:" << std::endl;
    uint dom_cnt = 0;
    for (const auto& dom_results : p.getTangentialDerivativesBoundaryIntegrals()) {
        if (std::get<1>(dom_results).size() != 0) std::cout << "Domain " << std::get<0>(dom_results) << ":" << std::endl;
        for (const double& r : std::get<1>(dom_results))
            std::cout << r << std::endl;
    }

    // Handle the plots
    app.exec();
}
