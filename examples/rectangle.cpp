#include <cmath>
#include "Sanji.hpp"
#include "ProblemWrapper.hpp"
#include <iostream>
#include <gsl/gsl_integration.h>
#include <Eigen/Dense>
#include <iomanip>

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 1e-8
#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-6
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          1000

using namespace sanji::colors;

/* Type definitions */
using DBS = DirectedBoundarySegment;
template <typename T>
using vector   = std::vector<T>;
using VectorXd = Eigen::VectorXd;
using MatrixXd = Eigen::MatrixXd;

// Visualization
constexpr bool plot_normals       = true;
constexpr bool plot_tangentials   = false;
constexpr bool compute_tang_ders  = true;
constexpr bool compute_statistics = true;

// Permeabilities
constexpr double mu0    = 4.0*M_PI*1e-7;
constexpr double mu_air = 1.00000037*mu0; // https://www.engineeringtoolbox.com/permeability-d_1923.html
constexpr double mu_cu  = 0.999994*mu0;   // https://www.engineeringtoolbox.com/permeability-d_1923.html

// Geometric parameters
constexpr double cable_height  = 0.002; // [m]
constexpr double cable_width   = 0.006; // [m]
constexpr double ring_1_radius = 0.004; // [m]
constexpr double ring_2_radius = 0.02;  // [m]

// Current densities
constexpr double J = 1.0/1.0e-6;

int main(int argc, char* argv[]) {
    // Initialize this as a Qt application
    QApplication app(argc, argv);
    sanji::init();

    /* Create a problem instance */
    ProblemWrapper p;

    // Set the output precision
    std::cout << std::setprecision(15);

    /* Create the boundaries in the problem */
    constexpr uint num_nodes_per_segment = 2;
    const auto seg_1 = p.addStraightBoundarySegment(Vec2D{ cable_width/2.0,cable_height/2.0}, /* start */
                                                    Vec2D{-cable_width/2.0,cable_height/2.0}, /* end */
                                                    num_nodes_per_segment,                    /* num_nodes */
                                                    1,                                        /* poly_order */
                                                    true,                                     /* normal_points_to_the_right */
                                                    false                                     /* is_reference */);
    const auto seg_2 = p.addStraightBoundarySegment(Vec2D{-cable_width/2.0, cable_height/2.0}, /* start */
                                                    Vec2D{-cable_width/2.0,-cable_height/2.0}, /* end */
                                                    num_nodes_per_segment,                     /* num_nodes */
                                                    1,                                         /* poly_order */
                                                    true,                                      /* normal_points_to_the_right */
                                                    false                                      /* is_reference */);
    const auto seg_3 = p.addStraightBoundarySegment(Vec2D{-cable_width/2.0,-cable_height/2.0}, /* start */
                                                    Vec2D{ cable_width/2.0,-cable_height/2.0}, /* end */
                                                    num_nodes_per_segment,                     /* num_nodes */
                                                    1,                                         /* poly_order */
                                                    true,                                      /* normal_points_to_the_right */
                                                    false                                      /* is_reference */);
    const auto seg_4 = p.addStraightBoundarySegment(Vec2D{cable_width/2.0,-cable_height/2.0}, /* start */
                                                    Vec2D{cable_width/2.0, cable_height/2.0}, /* end */
                                                    num_nodes_per_segment,                    /* num_nodes */
                                                    1,                                        /* poly_order */
                                                    true,                                     /* normal_points_to_the_right */
                                                    false                                     /* is_reference */);

    /* Create the domains in the problem */
    const auto box = p.addDomain("box",{{std::make_shared<DBS>(seg_1,true),
                                         std::make_shared<DBS>(seg_2,true),
                                         std::make_shared<DBS>(seg_3,true),
                                         std::make_shared<DBS>(seg_4,true)}}, /* Boundary information */
                                       mu_cu,                                 /* Magnetic permeability */
                                       J                                      /* Current density */);
    const auto air = p.addExteriorDomain("air",{{std::make_shared<DBS>(seg_4,false),
                                                 std::make_shared<DBS>(seg_3,false),
                                                 std::make_shared<DBS>(seg_2,false),
                                                 std::make_shared<DBS>(seg_1,false)}}, /* Boundary information */
                                               mu_air                                  /* Magnetic permeability */);

    /* Solve the problem */
    p.solve(true/*debug*/);
    //p.solveAndStore("results/simple");
    //p.loadSolution("results/simple");

    // Reference boundaries and domains
    const auto ring_1 = p.addCircularBoundarySegment(ring_1_radius, /* radius */
                                                     Vec2D{0,0},    /* center */
                                                     12,            /* num_nodes */
                                                     4,             /* boundary_solution_polynomial_order */
                                                     true,          /* normal_points_to_the_right */
                                                     true           /* is_reference */);
    const auto ring_2 = p.addCircularBoundarySegment(ring_2_radius, /* radius */
                                                     Vec2D{0,0},    /* center */
                                                     12,            /* num_nodes */
                                                     4,             /* boundary_solution_polynomial_order */
                                                     true,          /* normal_points_to_the_right */
                                                     true           /* is_reference */);
    const auto reference_domain = p.addReferenceDomain(std::const_pointer_cast<const Domain>(air),
                                                       {{std::make_shared<DirectedBoundarySegment>(ring_1,false)},
                                                        {std::make_shared<DirectedBoundarySegment>(ring_2,true)}},{1/*Type*/});

    /* Plot the boundaries in the problem */
    sanji::figure("Benchmark simulation");
    for (const auto& boundary_segment_plotting_data : p.getBoundarySegmentsPlottingData(0.001/4.0, /* max_length_per_segment */
                                                                                        true       /* show_reference_boundaries */))
        sanji::plot(*boundary_segment_plotting_data.x,*boundary_segment_plotting_data.y,boundary_segment_plotting_data.style);

    /* Plot the normals in the problem */
    if (plot_normals)
        for (const auto& boundary_segment_plotting_vector_data : p.getNormals(0.001, /* max_length_per_segment */
                                                                              4.0e-4 /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);

    /* Plot the magnetic flux density */
    const auto field_data = p.getFieldData();
    double min =  std::numeric_limits<double>::max();
    double max = -std::numeric_limits<double>::max();
    for (const auto& field_data : field_data)
        for (uint i = 0; i < field_data.x->rows(); ++i) {
            const double strength = Vec2D{(*field_data.u)(i),(*field_data.v)(i)}.Norm2();
            if (strength < min) min = strength;
            if (strength > max) max = strength;
        }
    for (const auto& field_data : field_data)
        sanji::quiver(*field_data.x,
                      *field_data.y,
                      *field_data.u,
                      *field_data.v,{{"arrow_length",0.0006},{"use_colormap",1},{"colormap",TURBO},{"min",min},{"max",max}});

    /* Plot cosmetics */
    sanji::setPlotBackgroundColor(GRAY);
    sanji::setAxesRatio("equal");

    /* Compute problem statistics */
    if (compute_statistics) {
        // Create a wrapper to simplify integration
        gsl_function               gsl_fun;
        gsl_integration_workspace* gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
        const auto quad = [&gsl_fun,gsl_workspace]()->double {
            double result,error;
            gsl_integration_qag(&gsl_fun, /* Function to integrate */
                                0.0,      /* Lower integration limit */
                                1.0,      /* Upper integration limit */
                                INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                                INTEGRAL_RELATIVE_ERROR_LIMIT,
                                MAX_NUM_SUBINTERVALS,
                                INTEGRATION_KEY,
                                gsl_workspace,&result,&error);
            return result;
        };

        // Create a lambda to evaluate the solution in the exterior
        const auto eval_sol_ext = [&p,air](const Vec2D x)->const Vec2D {
            return p.evaluateInteriorSolution(air,x);
        };

        // Integrate the magnetic field at different radii
        const uint Nr           = 10;
        const double min_radius = ring_1_radius;
        const double max_radius = ring_2_radius;
        VectorXd radii(Nr+1);
        VectorXd circular_integrals(Nr+1);
        for (uint radius_idx = 0; radius_idx < Nr+1; ++radius_idx) {
            const double radius = min_radius + (max_radius-min_radius)*radius_idx/static_cast<double>(Nr);
            radii(radius_idx)   = radius;
            gsl_fun.function = [](const double t, void* params)-> double {
                const decltype(eval_sol_ext)& eval_sol_ext_ = *static_cast<const decltype(eval_sol_ext)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
                const double& radius                        = *reinterpret_cast<const double **>(params)[1];

                const double phi    = t*2.0*M_PI;
                const Vec2D  bnd_pt = radius*Vec2D{std::cos(phi),std::sin(phi)};
                const Vec2D  B      = eval_sol_ext_(bnd_pt);
                const Vec2D  nu     = Vec2D{-std::sin(phi),std::cos(phi)};

                return nu.dot(B);
            };
            const double** params          = new const double*[2];
            params[0]                      = reinterpret_cast<const double*>(&eval_sol_ext);
            params[1]                      = &radius;
            gsl_fun.params                 = params;
            circular_integrals(radius_idx) = quad()*2.0*M_PI*radius;
            delete[] params;
        }

        // Free memory
        gsl_integration_workspace_free(gsl_workspace);

        // Compute the mean negative error exponent
        const double current = cable_width*cable_height*J;
              double mean_s  = 0.0;
        for (uint i = 0; i < Nr+1; ++i) {
            const double c = std::abs(current-circular_integrals(i)/mu0+1.0e-30)/std::abs(current);
            std::cout << "current=" << current << " circular_integrals(i)/mu0=" << circular_integrals(i)/mu0 << std::endl;
            mean_s        -= std::log10(c);
        }

        // Print out information about the accuracy
        std::cout << "An estimate of the accuracy of the simulation results can be obtained by comparing the closed-contour integral of the H field to the enclosed current. ";
        std::cout << "Computing the relative error as (|current - closed_contour_integral/mu| + 1.0e-30)/|current| = 10^-s results in an average s of " << mean_s/(Nr+1u) << "." << std::endl;

        sanji::figure("Statistic");
        sanji::plot(radii,circular_integrals);
    }

    // Compute the boundary integrals of the tangential derivatives for all domains
    if (compute_tang_ders) {
        std::cout << "The boundary integrals of the tangential derivative are:" << std::endl;
        uint dom_cnt = 0;
        for (const auto& dom_results : p.getTangentialDerivativesBoundaryIntegrals()) {
            if (std::get<1>(dom_results).size() != 0) std::cout << "Domain " << std::get<0>(dom_results) << ":" << std::endl;
            for (const double& r : std::get<1>(dom_results))
                std::cout << r << std::endl;
        }
    }

    // Execute the application
    app.exec();
}
