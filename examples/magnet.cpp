#include <cmath>
#include "Sanji.hpp"
#include "ProblemWrapper.hpp"
#include <iostream>
#include <gsl/gsl_integration.h>
#include <Eigen/Dense>
#include <iomanip>

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 1e-8
#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-6
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          1000

using namespace sanji::colors;

/* Type definitions */
using DBS = DirectedBoundarySegment;
template <typename T>
using vector   = std::vector<T>;
using VectorXd = Eigen::VectorXd;
using MatrixXd = Eigen::MatrixXd;

// Visualization
constexpr bool plot_normals     = true;
constexpr bool plot_tangentials = true;

// Permeabilities
constexpr double mu0       = 4.0*M_PI*1e-7;
constexpr double mu_air    = 1.00000037*mu0; // https://www.engineeringtoolbox.com/permeability-d_1923.html
constexpr double mu_magnet = 1.05*mu0;       // https://en.wikipedia.org/wiki/Permeability_(electromagnetism)

// Geometric parameters
constexpr double dm1 = 0.0062;
constexpr double dm2 = 0.0135;
constexpr double dm3 = 0.0028;

int main(int argc, char* argv[]) {
    // Initialize this as a Qt application
    QApplication app(argc, argv);
    sanji::init();

    /* Create a problem instance */
    ProblemWrapper p;

    // Define the corners of magnet
    MatrixXd corners(2,4);
    corners << -(dm2-dm1)/2, (dm2-dm1)/2, (dm2-dm1)/2, -(dm2-dm1)/2,
                     -dm3/2,      -dm3/2,       dm3/2,        dm3/2;

    // Create the boundary segments
    vector<boundary_segment_ptr> magnet_boundary_segments;
    vector<uint> num_nodes{3,2,3,2};
    for (uint c = 0; c < 4; ++c) {
        double surface_current_density;
        if (c == 0)
            surface_current_density =  10.0/0.01;
        else if (c == 2)
            surface_current_density = -10.0/0.01;
        else
            surface_current_density = 0.0;
        magnet_boundary_segments.push_back(p.addStraightBoundarySegment(Vec2D{corners(0,c),corners(1,c)},             /* start */
                                                                        Vec2D{corners(0,(c+1)%4),corners(1,(c+1)%4)}, /* end */
                                                                        num_nodes[c],                                 /* num_nodes */
                                                                        1,                                            /* poly_order */
                                                                        true,                                         /* normal_points_to_the_right */
                                                                        false,                                        /* is_reference */
                                                                        surface_current_density));
    }

    // Create the domains in the problem
    vector<directed_boundary_segment_ptr> dbsps(4);
    for (uint i = 0; i < 4; ++i)
        dbsps[i] = std::make_shared<DirectedBoundarySegment>(magnet_boundary_segments[i],true);
    const auto magnet = p.addDomain("magnet",{dbsps},mu_magnet,0.0 /* J */);
    for (uint i = 0; i < 4; ++i)
        dbsps[i] = std::make_shared<DirectedBoundarySegment>(magnet_boundary_segments[3-i],false);
    const auto air = p.addExteriorDomain("outer_air",{dbsps},mu_air);

    /* Solve the problem */
    p.solve(true /* debug */);
    //p.solveAndStore("results/simple");
    //p.loadSolution("results/simple");

    // Reference boundaries and domains
    const auto ring_1 = p.addCircularBoundarySegment(0.005,      /* radius */
                                                     Vec2D{0,0}, /* center */
                                                     12,         /* num_nodes */
                                                     4,          /* boundary_solution_polynomial_order */
                                                     true,       /* normal_points_to_the_right */
                                                     true        /* is_reference */);
    const auto ring_2 = p.addCircularBoundarySegment(0.02,       /* radius */
                                                     Vec2D{0,0}, /* center */
                                                     12,         /* num_nodes */
                                                     4,          /* boundary_solution_polynomial_order */
                                                     true,       /* normal_points_to_the_right */
                                                     true        /* is_reference */);
    const auto reference_domain = p.addReferenceDomain(std::const_pointer_cast<const Domain>(air),
                                                       {{std::make_shared<DirectedBoundarySegment>(ring_1,false)},
                                                        {std::make_shared<DirectedBoundarySegment>(ring_2,true)}},{1/*Type*/});

    /* Plot the boundaries in the problem */
    sanji::figure("Benchmark simulation");
    for (const auto& boundary_segment_plotting_data : p.getBoundarySegmentsPlottingData(0.001/4.0, /* max_length_per_segment */
                                                                                        true       /* show_reference_boundaries */))
        sanji::plot(*boundary_segment_plotting_data.x,*boundary_segment_plotting_data.y,boundary_segment_plotting_data.style);

    /* Plot the normals in the problem */
    if (plot_normals)
        for (const auto& boundary_segment_plotting_vector_data : p.getNormals(0.001, /* max_length_per_segment */
                                                                              1.0e-3 /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);

    /* Plot the magnetic flux density */
    const auto field_data = p.getFieldData();
    double min =  std::numeric_limits<double>::max();
    double max = -std::numeric_limits<double>::max();
    for (const auto& field_data : field_data)
        for (uint i = 0; i < field_data.x->rows(); ++i) {
            const double strength = Vec2D{(*field_data.u)(i),(*field_data.v)(i)}.Norm2();
            if (strength < min) min = strength;
            if (strength > max) max = strength;
        }
    for (const auto& field_data : field_data)
        sanji::quiver(*field_data.x,
                      *field_data.y,
                      *field_data.u,
                      *field_data.v,{{"arrow_length",0.0006},{"use_colormap",1},{"colormap",TURBO},{"min",min},{"max",max}});

    /* Print statistics */
    std::cout << "The minimum flux density is " << min << "T." << std::endl;
    std::cout << "The maximum flux density is " << max << "T." << std::endl;

    /* Plot cosmetics */
    sanji::setPlotBackgroundColor(GRAY);
    sanji::setAxesRatio("equal");

    /* Compute problem statistics */

    // Create a wrapper to simplify integration
    gsl_function               gsl_fun;
    gsl_integration_workspace* gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
    const auto quad = [&gsl_fun,gsl_workspace]()->double {
        double result,error;
        gsl_integration_qag(&gsl_fun, /* Function to integrate */
                            0.0,      /* Lower integration limit */
                            1.0,      /* Upper integration limit */
                            INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                            INTEGRAL_RELATIVE_ERROR_LIMIT,
                            MAX_NUM_SUBINTERVALS,
                            INTEGRATION_KEY,
                            gsl_workspace,&result,&error);
        return result;
    };

    // Create a lambda to evaluate the solution in the exterior
    const auto eval_sol_ext = [&p,air](const Vec2D x)->const Vec2D {
        return p.evaluateInteriorSolution(air,x);
    };

    // Integrate the magnetic field at different radii
    const uint Nr           = 10;
    const double min_radius = 0.005;
    const double max_radius = 0.1;
    VectorXd radii(Nr+1);
    VectorXd results(Nr+1);
    for (uint radius_idx = 0; radius_idx < Nr+1; ++radius_idx) {
        const double radius = min_radius + (max_radius-min_radius)*radius_idx/static_cast<double>(Nr);
        radii(radius_idx)   = radius;
        gsl_fun.function = [](const double t, void* params)-> double {
            const decltype(eval_sol_ext)& eval_sol_ext_ = *static_cast<const decltype(eval_sol_ext)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
            const double& radius                        = *reinterpret_cast<const double **>(params)[1];

            const double phi    = t*2.0*M_PI;
            const Vec2D  bnd_pt = radius*Vec2D{std::cos(phi),std::sin(phi)};
            const Vec2D  B      = eval_sol_ext_(bnd_pt);
            const Vec2D  tan     = Vec2D{-std::sin(phi),std::cos(phi)};

            return tan.dot(B);
        };
        const double** params = new const double*[2];
        params[0]             = reinterpret_cast<const double*>(&eval_sol_ext);
        params[1]             = &radius;
        gsl_fun.params        = params;
        results(radius_idx)   = quad()*2.0*M_PI*radius;
        delete[] params;
    }
    gsl_integration_workspace_free(gsl_workspace);

    // Compute the scores
    double mean_error    = 0.0;
    for (uint i = 0; i < Nr+1; ++i) {
        const double c = std::abs(results(i)/mu0);
        mean_error    += c;
    }
    std::cout << std::setprecision(15);
    std::cout << "The mean error is " << mean_error/(Nr+1) << "A." << std::endl;

    // Execute the application
    app.exec();
}
