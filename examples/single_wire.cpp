#include <cmath>
#include "Sanji.hpp"
#include "ProblemWrapper.hpp"
#include <iostream>
#include <gsl/gsl_integration.h>
#include <Eigen/Dense>

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 1e-8
#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-6
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          1000

using namespace sanji::colors;

/* Type definitions */
using DBS = DirectedBoundarySegment;
template <typename T>
using vector   = std::vector<T>;
using VectorXd = Eigen::VectorXd;
using MatrixXd = Eigen::MatrixXd;

// Visualization
constexpr bool plot_normals     = true;
constexpr bool plot_tangentials = false;

// Permeabilities
constexpr double mu0    = 4.0*M_PI*1e-7;
constexpr double mu_air = 1.00000037*mu0; // https://www.engineeringtoolbox.com/permeability-d_1923.html
constexpr double mu_cu  = 0.999994*mu0;   // https://www.engineeringtoolbox.com/permeability-d_1923.html

// Geometric parameters
constexpr double cable_radius = 0.01;

// Current densities
constexpr double J = 1.0/1.0e-6;

int main(int argc, char* argv[]) {
    // Initialize this as a Qt application
    QApplication app(argc, argv);
    sanji::init();

    /* Create a problem instance */
    ProblemWrapper p;

    // Create the boundaries in the problem
    const auto Gamma = p.addCircularBoundarySegment(cable_radius,   /* Radius */
                                                    Vec2D{0.0,0.0}, /* Center */
                                                    3,              /* NumNodes */
                                                    1,              /* boundarySolutionPolynomialOrder */
                                                    true            /* normal_points_to_the_right */);

    // Create the domains in the problem
    const auto cable = p.addDomain("cable",                               /* name */
                                   {{std::make_shared<DBS>(Gamma,true)}}, /* Boundary information */
                                   mu_cu,                                 /* Magnetic permeability */
                                   J,                                     /* Current density */
                                   2                                      /* type */);
    const auto air = p.addExteriorDomain("air",                                  /* name */
                                         {{std::make_shared<DBS>(Gamma,false)}}, /* Boundary information */
                                         mu_air                                  /* Magnetic permeability */);

    /* Solve the problem */
    p.solve(true /* debug */);
    //p.solveAndStore("results/simple");
    //p.loadSolution("results/simple");

    // Reference boundaries and domains
    const auto ring_1 = p.addCircularBoundarySegment(cable_radius, /* radius */
                                                     Vec2D{0,0},   /* center */
                                                     12,           /* num_nodes */
                                                     4,            /* boundary_solution_polynomial_order */
                                                     true,         /* normal_points_to_the_right */
                                                     true          /* is_reference */);
    const auto ring_2 = p.addCircularBoundarySegment(3*cable_radius, /* radius */
                                                     Vec2D{0,0},     /* center */
                                                     12,             /* num_nodes */
                                                     4,              /* boundary_solution_polynomial_order */
                                                     true,           /* normal_points_to_the_right */
                                                     true            /* is_reference */);
    const auto reference_domain = p.addReferenceDomain(std::const_pointer_cast<const Domain>(air),
                                                       {{std::make_shared<DirectedBoundarySegment>(ring_1,false)},
                                                        {std::make_shared<DirectedBoundarySegment>(ring_2,true)}},{1/*Type*/});

    /* Plot the boundaries in the problem */
    sanji::figure("Single wire simulation");
    for (const auto& boundary_segment_plotting_data : p.getBoundarySegmentsPlottingData(0.001/4.0, /* max_length_per_segment */
                                                                                        true       /* show_reference_boundaries */))
        sanji::plot(*boundary_segment_plotting_data.x,*boundary_segment_plotting_data.y,boundary_segment_plotting_data.style);

    /* Plot the normals in the problem */
    if (plot_normals)
        for (const auto& boundary_segment_plotting_vector_data : p.getNormals(0.001, /* max_length_per_segment */
                                                                              1.0e-5 /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);

    /* Plot the magnetic flux density */
    const auto field_data = p.getFieldData(6.0e5);
    double min =  std::numeric_limits<double>::max();
    double max = -std::numeric_limits<double>::max();
    for (const auto& field_data : field_data)
        for (uint i = 0; i < field_data.x->rows(); ++i) {
            const double strength = Vec2D{(*field_data.u)(i),(*field_data.v)(i)}.Norm2();
            if (strength < min) min = strength;
            if (strength > max) max = strength;
        }
    for (const auto& field_data : field_data)
        sanji::quiver(*field_data.x,
                      *field_data.y,
                      *field_data.u,
                      *field_data.v,{{"arrow_length",0.0012},{"use_colormap",1},{"colormap",TURBO},{"min",min},{"max",max}});

    /* Plot cosmetics */
    sanji::setPlotBackgroundColor(GRAY);
    sanji::setxmin(-1.8e-2);
    sanji::setxmax( 1.8e-2);
    sanji::setymin(-1.8e-2);
    sanji::setymax( 1.8e-2);
    sanji::setAxesRatio("equal");

    /* Compute problem statistics */

    // Create a wrapper to simplify integration
    gsl_function               gsl_fun;
    gsl_integration_workspace* gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
    const auto quad = [&gsl_fun,gsl_workspace]()->double {
        double result,error;
        gsl_integration_qag(&gsl_fun, /* Function to integrate */
                            0.0,      /* Lower integration limit */
                            1.0,      /* Upper integration limit */
                            INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                            INTEGRAL_RELATIVE_ERROR_LIMIT,
                            MAX_NUM_SUBINTERVALS,
                            INTEGRATION_KEY,
                            gsl_workspace,&result,&error);
        return result;
    };

    // Create a lambda to evaluate the solution in the interior and the exteriro
    const auto eval_sol_int = [&p,cable](const Vec2D x)->const Vec2D {
        return p.evaluateInteriorSolution(cable,x);
    };
    const auto eval_sol_ext = [&p,air](const Vec2D x)->const Vec2D {
        return p.evaluateInteriorSolution(air,x);
    };

    // Integrate the magnetic field at different radii
    const uint   Nr             = 10u;
    const double min_radius_int = 0.01*cable_radius;
    const double max_radius_int = 0.95*cable_radius;
    const double min_radius_ext = 1.1*cable_radius;
    const double max_radius_ext = 3.0*cable_radius;
    VectorXd     radii_int(Nr+1u);
    VectorXd     radii_ext(Nr+1u);
    VectorXd     circular_integrals_int(Nr+1u);
    VectorXd     circular_integrals_ext(Nr+1u);
    for (uint radius_idx = 0u; radius_idx < Nr+1u; ++radius_idx) {
        const double radius_int = min_radius_int + (max_radius_int-min_radius_int)*radius_idx/static_cast<double>(Nr);
        const double radius_ext = min_radius_ext + (max_radius_ext-min_radius_ext)*radius_idx/static_cast<double>(Nr);
        radii_int(radius_idx)   = radius_int;
        radii_ext(radius_idx)   = radius_ext;
        gsl_fun.function = [](const double t, void* params)->double {
            const decltype(eval_sol_ext)& eval_sol_ext_ = *static_cast<const decltype(eval_sol_ext)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
            const double& radius                        = *reinterpret_cast<const double **>(params)[1];

            const double phi    = t*2.0*M_PI;
            const Vec2D  bnd_pt = radius*Vec2D{std::cos(phi),std::sin(phi)};
            const Vec2D  B      = eval_sol_ext_(bnd_pt);
            const Vec2D  tan    = Vec2D{-std::sin(phi),std::cos(phi)};

            return tan.dot(B);
        };
        const double** params              = new const double*[2];
        gsl_fun.params                     = params;
        params[0]                          = reinterpret_cast<const double*>(&eval_sol_int);
        params[1]                          = &radius_int;
        circular_integrals_int(radius_idx) = quad()*2.0*M_PI*radius_int;
        params[0]                          = reinterpret_cast<const double*>(&eval_sol_ext);
        params[1]                          = &radius_ext;
        circular_integrals_ext(radius_idx) = quad()*2.0*M_PI*radius_ext;
        delete[] params;
    }
    gsl_integration_workspace_free(gsl_workspace);

    // Compute the mean negative error exponent
    const double current_ext = cable_radius*cable_radius*M_PI*J;
    double       mean_s_int  = 0.0;
    double       mean_s_ext  = 0.0;
    for (uint radius_idx = 0; radius_idx < Nr+1u; ++radius_idx) {
        const double current_int = radii_int(radius_idx)*radii_int(radius_idx)*M_PI*J;
        const double c_int       = (std::abs(current_int-circular_integrals_int(radius_idx)/mu0)+1.0e-30)/std::abs(current_int);
        mean_s_int    -= std::log10(c_int);
        const double c_ext       = (std::abs(current_ext-circular_integrals_ext(radius_idx)/mu0)+1.0e-30)/std::abs(current_ext);
        mean_s_ext    -= std::log10(c_ext);
    }

    // Set the output precision and print the score
    std::cout << "An estimate of the accuracy of the simulation results can be obtained by comparing the closed-contour integral of the H field to the enclosed current. ";
    std::cout << "Computing the relative error as (|current_int - closed_contour_integral_int/mu| + 1.0e-30)/|current_int| = 10^-s results in an average s of " << mean_s_int/(Nr+1u) << "." << std::endl;
    std::cout << "Computing the relative error as (|current_ext - closed_contour_integral_ext/mu| + 1.0e-30)/|current_ext| = 10^-s results in an average s of " << mean_s_ext/(Nr+1u) << "." << std::endl;

    // Compute the boundary integrals of the tangential derivatives for all domains
    std::cout << "The boundary integrals of the tangential derivative are:" << std::endl;
    uint dom_cnt = 0;
    for (const auto& dom_results : p.getTangentialDerivativesBoundaryIntegrals()) {
        if (std::get<1>(dom_results).size() != 0) std::cout << "Domain " << std::get<0>(dom_results) << ":" << std::endl;
        for (const double& r : std::get<1>(dom_results))
            std::cout << r << std::endl;
    }

    // Handle the plots
    app.exec();
}
