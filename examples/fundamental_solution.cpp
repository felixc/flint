#include <gsl/gsl_integration.h>
#include <Eigen/Dense>
#include <cmath>
#include "Sanji.hpp"
#include "Vec2D.hpp"
#include <iostream>

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 1e-8
#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-6
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          1000

using VectorXd = Eigen::VectorXd;

// Functions to compute the fundamental solution and its derivatives
static double E(const Vec2D& x) {
    return std::log(x.Norm2())/2.0/M_PI;
};

int main(int argc, char* argv[]) {
    // Initialize this as a Qt application
    QApplication app(argc, argv);
    sanji::init();

    // Create a wrapper to simplify integration
    gsl_function               gsl_fun;
    gsl_integration_workspace* gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
    const auto quad = [&gsl_fun,gsl_workspace]()->double {
        double result,error;
        gsl_integration_qag(&gsl_fun, /* Function to integrate */
                            0.0,      /* Lower integration limit */
                            1.0,      /* Upper integration limit */
                            INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                            INTEGRAL_RELATIVE_ERROR_LIMIT,
                            MAX_NUM_SUBINTERVALS,
                            INTEGRATION_KEY,
                            gsl_workspace,&result,&error);
        return result;
    };

    // Create a lambda to compute the fundamental solution
    const auto get_fundamental_solution = [&gsl_fun,&quad](const Vec2D x, const double R_k)->const Vec2D {
        uint dim_idx;
        const auto integrand = [&R_k,&x,&dim_idx](const double t)->double {
            const double phi_y = 2.0*M_PI*t;
            const Vec2D y      = R_k*Vec2D{std::cos(phi_y),std::sin(phi_y)};
            const Vec2D nu{std::cos(phi_y),std::sin(phi_y)};

            return dim_idx == 0u ? nu.y*E(y-x)*2.0*M_PI*R_k :
                                  -nu.x*E(y-x)*2.0*M_PI*R_k;
        };

        gsl_fun.function = [](const double t, void* params)->double {
            const decltype(integrand)& integrand_ = *static_cast<const decltype(integrand)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
            const double c = integrand_(t);

            return integrand_(t);
        };
        const double** params = new const double*[1];
        params[0]             = reinterpret_cast<const double*>(&integrand);
        gsl_fun.params        = params;

        const double mu_k     = 4.0*M_PI*1e-7;
        const double J_k      = 1.0/1.0e-6;
        const double fp       = -mu_k*J_k;
    
        Vec2D B; 
        dim_idx = 0u;
        B.x     = -quad()*fp;
        dim_idx = 1u;
        B.y     = -quad()*fp;

        delete[] params;
        return B;
    };

    const uint N     = 50;
    const double R_k = 10*0.001;
    const double Rx  =  5*0.001;
    VectorXd x(N);
    VectorXd y(N);
    VectorXd u(N);
    VectorXd v(N);
    for (uint i = 0; i < N; ++i) {
        const double phi_x = (2.0*M_PI*i)/N;
        x(i)               = Rx*std::cos(phi_x);
        y(i)               = Rx*std::sin(phi_x);
        const Vec2D B      = get_fundamental_solution(Vec2D{x(i),y(i)},R_k);
        u(i)               = B.x;
        v(i)               = B.y;
    }
    sanji::figure("Benchmark simulation");
    sanji::quiver(x,y,u,v,{{"arrow_length",0.0005}},{"center_arrows"});
    //sanji::setPlotBackgroundColor(GRAY);
    sanji::setAxesRatio("equal");

    // Free memory
    gsl_integration_workspace_free(gsl_workspace);

    // Handle the plots
    app.exec();
}
