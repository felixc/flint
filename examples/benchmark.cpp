#include <cmath>
#include "ProblemWrapper.hpp"
#include <gsl/gsl_integration.h>
#include <Eigen/Dense>
#include <iostream>
#include "Sanji.hpp"

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 1e-8
#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-6
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          1000

using namespace sanji::colors;

/* Type definitions */
template <typename T>
using vector   = std::vector<T>;
using VectorXd = Eigen::VectorXd;
using MatrixXd = Eigen::MatrixXd;

// Visualization
constexpr bool plot_normals      = false;
constexpr bool plot_tangentials  = false;
constexpr bool plot_field_data   = false;
constexpr bool compute_tang_ders = false;

// Permeabilities
constexpr double mu0       = 4.0*M_PI*1.0e-7;
constexpr double mu_air    = 1.00000037*mu0;
constexpr double mu_cu     = 0.999994*mu0;
constexpr double mu_iron   = 500.0*mu0;

// Geometric parameters
constexpr double wire_radius = 0.003; // [m]

int main(int argc, char* argv[]) {
    // Initialize this as a Qt application
    QApplication app(argc, argv);
    sanji::init();

    /* Create a problem instance */
    ProblemWrapper p;

    /* Create the boundaries and the domains in the problem */
    vector<boundary_segment_ptr> boundary_segments;
    for (uint star_leg_idx = 0; star_leg_idx < 3; ++star_leg_idx) {
        for (uint i = 0; i < 2; ++i) {
            boundary_segments.push_back(p.addCircularBoundarySegment(wire_radius,                                                    /* radius */
                                                                     (i+1)*0.02*Vec2D{std::cos(2.0*M_PI/3.0*star_leg_idx+M_PI/2.0),
                                                                                      std::sin(2.0*M_PI/3.0*star_leg_idx+M_PI/2.0)}, /* center */
                                                                     12,                                                             /* num_nodes */
                                                                     1,                                                              /* boundary_solution_polynomial_order */
                                                                     true                                                            /* normal_points_to_the_right */));
            p.addDomain("wire_"+std::to_string(star_leg_idx)+"_"+std::to_string(i),{{std::make_shared<DirectedBoundarySegment>(boundary_segments.back(),true)}},
                mu_cu,star_leg_idx==0?1.0/1.0e-6:-1.0/1e-6,2/*type*/);
        }
    }
    boundary_segments.push_back(p.addCircularBoundarySegment(wire_radius,    /* radius */
                                                             Vec2D{0.0,0.0}, /* center */
                                                             12,             /* num_nodes */
                                                             1,              /* boundary_solution_polynomial_order */
                                                             true            /* normal_points_to_the_right */));
    p.addDomain("iron",{{std::make_shared<DirectedBoundarySegment>(boundary_segments.back(),true)}},mu_iron,0.0,2/*type*/);
    boundary_segments.push_back(p.addCircularBoundarySegment(wire_radius,       /* radius */
                                                             Vec2D{0.04,0.032}, /* center */
                                                             12,                /* num_nodes */
                                                             1,                 /* boundary_solution_polynomial_order */
                                                             true               /* normal_points_to_the_right */));
    p.addDomain("wire_offset",{{std::make_shared<DirectedBoundarySegment>(boundary_segments.back(),true)}},mu_cu,1.0/1.0e-6,2/*type*/);
    vector<vector<directed_boundary_segment_ptr>> dbsps;
    for (const auto& boundary_segment : boundary_segments)
        dbsps.push_back({std::make_shared<DirectedBoundarySegment>(boundary_segment,false)});
    const auto air = p.addExteriorDomain("air",dbsps,mu_air);

    // Reference boundaries
    const auto r1 = p.addCircularBoundarySegment(0.055,              /* radius */
                                                 Vec2D{0.005,0.003}, /* center */
                                                 12,                 /* num_nodes */
                                                 1,                  /* boundary_solution_polynomial_order */
                                                 true,               /* normal_points_to_the_right */
                                                 true                /* is_reference */);

    /* Solve the problem */
    //p.solveAndStore("results/benchmark",true /* debug */);
    p.loadSolution("results/benchmark");

    /* Add reference domains */
    p.addReferenceDomain(std::const_pointer_cast<const Domain>(air),
                         {{std::make_shared<DirectedBoundarySegment>(r1,true)}},{2/*Type*/,[](const Vec2D x) {
        for (uint star_leg_idx = 0; star_leg_idx < 3; ++star_leg_idx) {
            for (uint i = 0; i < 2; ++i) {
                const double radius = wire_radius;
                const Vec2D  center = (i+1)*0.02*Vec2D{std::cos(2.0*M_PI/3.0*star_leg_idx+M_PI/2.0),std::sin(2.0*M_PI/3.0*star_leg_idx+M_PI/2.0)};
                if ((x-center).Norm2() <= radius) return false;
            }
        }
        if (x.Norm2() <= wire_radius) return false;
        if ((x-Vec2D{0.04,0.032}).Norm2() <= wire_radius) return false;
        return true;
    }});

    /* Plot the boundaries in the problem */
    sanji::figure("Benchmark simulation");
    for (const auto& boundary_segment_plotting_data : p.getBoundarySegmentsPlottingData(0.001/4.0, /* max_length_per_segment */
                                                                                        true       /* show_reference_boundaries */))
        sanji::plot(*boundary_segment_plotting_data.x,*boundary_segment_plotting_data.y,boundary_segment_plotting_data.style);

    /* Plot the normals in the problem */
    if (plot_normals)
        for (const auto& boundary_segment_plotting_vector_data : p.getNormals(0.001,  /* max_length_per_segment */
                                                                              1.0e-3  /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);

    /* Plot the magnetic flux density */
    if (plot_field_data) {
        const auto field_data = p.getFieldData(8.0e5);
        double min =  std::numeric_limits<double>::max();
        double max = -std::numeric_limits<double>::max();
        for (const auto& field_data : field_data)
            for (uint i = 0; i < field_data.x->rows(); ++i) {
                const double strength = Vec2D{(*field_data.u)(i),(*field_data.v)(i)}.Norm2();
                if (strength < min) min = strength;
                if (strength > max) max = strength;
            }
        for (const auto& field_data : field_data)
            sanji::quiver(*field_data.x,
                          *field_data.y,
                          *field_data.u,
                          *field_data.v,{{"arrow_length",0.0012},{"use_colormap",1},{"colormap",TURBO},{"min",min},{"max",max}});
    }

    /* Plot cosmetics */
    sanji::setPlotBackgroundColor(GRAY);
    sanji::setAxesRatio("equal");
    sanji::setxmin(-0.05);
    sanji::setxmax( 0.05);
    sanji::setymin(-0.03);
    sanji::setymax( 0.044);

    /* Compute problem statistics */

    // Create a wrapper to simplify integration
    gsl_function               gsl_fun;
    gsl_integration_workspace* gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
    const auto quad = [&gsl_fun,gsl_workspace]()->double {
        double result,error;
        gsl_integration_qag(&gsl_fun, /* Function to integrate */
                            0.0,      /* Lower integration limit */
                            1.0,      /* Upper integration limit */
                            INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                            INTEGRAL_RELATIVE_ERROR_LIMIT,
                            MAX_NUM_SUBINTERVALS,
                            INTEGRATION_KEY,
                            gsl_workspace,&result,&error);
        return result;
    };

    // Create a lambda to evaluate the solution in the exterior
    const auto eval_sol_ext = [&p,air](const Vec2D x)->const Vec2D {
        return p.evaluateInteriorSolution(air,x);
    };

    /* Closed-contour integrals of the magnetic field */
    gsl_fun.function = [](const double t, void* params)-> double {
        const decltype(eval_sol_ext)& eval_sol_ext_ = *static_cast<const decltype(eval_sol_ext)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
        const double& radius                        = *reinterpret_cast<const double **>(params)[1];
        const Vec2D&  center                        = *reinterpret_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[2]));

        const double phi    = t*2.0*M_PI;
        const Vec2D  bnd_pt = center+radius*Vec2D{std::cos(phi),std::sin(phi)};
        const Vec2D  B      = eval_sol_ext_(bnd_pt);
        const Vec2D  tan    = Vec2D{-std::sin(phi),std::cos(phi)};

        return tan.dot(B);
    };
    double radius         = 0.005;
    const double** params = new const double*[3];
    params[1]             = &radius;
    gsl_fun.params        = params;
    params[0]             = reinterpret_cast<const double*>(&eval_sol_ext);
    VectorXd scores(8);
    for (uint star_leg_idx = 0; star_leg_idx < 3; ++star_leg_idx) {
        for (uint i = 0; i < 2; ++i) {
            const Vec2D  center         = (i+1)*0.02*Vec2D{std::cos(2.0*M_PI/3.0*star_leg_idx+M_PI/2.0),
                                                           std::sin(2.0*M_PI/3.0*star_leg_idx+M_PI/2.0)};
            params[2]                   = reinterpret_cast<const double*>(&center);
            const double actual_current = (star_leg_idx==0?1.0/1.0e-6:-1.0/1e-6)*wire_radius*wire_radius*M_PI;
            std::cout << "The actual current is " << actual_current << std::endl;
            scores(2*star_leg_idx+i)    = std::log10(std::abs(quad()*2.0*M_PI*radius/mu0-actual_current)/std::abs(actual_current));
        }
    }
    Vec2D  center         = Vec2D{0.0,0.0};
    params[2]             = reinterpret_cast<const double*>(&center);
    double actual_current = 0.0;
    const double iron_int = std::abs(quad()*2.0*M_PI*radius/mu0);
    center                = Vec2D{0.04,0.032};
    params[2]             = reinterpret_cast<const double*>(&center);
    actual_current        = 1.0/1.0e-6*wire_radius*wire_radius*M_PI;
    scores(6)             = std::log10(std::abs(quad()*2.0*M_PI*radius/mu0-actual_current)/std::abs(actual_current));
    radius                = 0.05;
    params[1]             = &radius;
    center                = Vec2D{0.005,0.003};
    params[2]             = reinterpret_cast<const double*>(&center);
    actual_current        = -1.0/1.0e-6*wire_radius*wire_radius*M_PI;
    scores(7)             = std::log10(std::abs(quad()*2.0*M_PI*radius/mu0-actual_current)/std::abs(actual_current));

    // Plot the data
    sanji::figure("Results");
    sanji::plot(VectorXd::LinSpaced(scores.rows(),0,scores.rows()-1),scores,{{"line_style",'o'}});
    std::cout << "iron_int=" << iron_int << std::endl;
    sanji::setxmin(-0.5);
    sanji::setxmax(7.5);
    sanji::setymin(-7);
    sanji::setymax(-5);

    delete[] params;
    gsl_integration_workspace_free(gsl_workspace);
    std::cout << "The scores are" << std::endl << scores << std::endl;

    // Compute the boundary integrals of the tangential derivatives for all domains
    if (compute_tang_ders) {
        std::cout << "The boundary integrals of the tangential derivative are:" << std::endl;
        uint dom_cnt = 0;
        for (const auto& dom_results : p.getTangentialDerivativesBoundaryIntegrals()) {
            if (std::get<1>(dom_results).size() != 0) std::cout << "Domain " << std::get<0>(dom_results) << ":" << std::endl;
            for (const double& r : std::get<1>(dom_results))
                std::cout << r << std::endl;
        }
    }

    // Execute the application
    app.exec();
}
