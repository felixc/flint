#include <gsl/gsl_integration.h>
#include <iostream>
#include <iomanip>
#include <unordered_set>
#include <tuple>
#include "Sanji.hpp"
#include "ProblemWrapper.hpp"

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 0
#define INTEGRAL_RELATIVE_ERROR_LIMIT 1e-2
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          2000
#define OUTER_QUADRATURE_ORDER        100

using namespace sanji::colors;

/* Type definitions */
template <typename T>
using vector = std::vector<T>;

// Visualization
constexpr bool plot_normals     = false;
constexpr bool compute_force    = false;
constexpr bool plot_field_data  = false;
constexpr bool load_field_data  = false;
constexpr bool store_field_data = false;

// Geometric parameters
constexpr double coil_width      = 0.03;   // [m]
constexpr double coil_height     = 0.01;   // [m]
constexpr double yoke_width      = 0.7;    // [m]
constexpr double teeth_width     = 0.15;   // [m]
constexpr double yoke_height     = 0.3;    // [m]
constexpr double yoke_thickness  = 0.05;   // [m]
constexpr double cr              = 0.0008; // [m]
constexpr double delta           = 0.005;  // [m]
constexpr double lever_height    = 0.3;    // [m]
constexpr double lever_thickness = 0.05;   // [m]

// Permeabilities
constexpr double mu0     = 4.0*M_PI*1e-7;
constexpr double mu_air  = 1.00000037*mu0; // https://www.engineeringtoolbox.com/permeability-d_1923.html
constexpr double mu_iron = 25000.0*mu0;
constexpr double mu_cu   = 0.999994*mu0;   // https://www.engineeringtoolbox.com/permeability-d_1923.html

int main(int argc, char* argv[]) {
    // Initialize this as a Qt application
    QApplication app(argc, argv);
    sanji::init();

    /* Create a problem instance */
    ProblemWrapper p;

    /* Create the boundaries in the problem */
    vector<boundary_segment_ptr> yoke_bnd_seg_ptrs; 
    yoke_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{ coil_width/2.0,0.0}, /* start */
                                                             Vec2D{-coil_width/2.0,0.0}, /* end */
                                                             30,                         /* num_nodes */
                                                             1,                          /* poly_order */
                                                             true,                       /* normal_points_to_the_right */
                                                             false                       /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{-coil_width/2.0,0.0},    /* start */
                                                             Vec2D{-yoke_width/2.0+cr,0.0}, /* end */
                                                             30,                            /* num_nodes */
                                                             1,                             /* poly_order */
                                                             true,                          /* normal_points_to_the_right */
                                                             false                          /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                            /* radius */
                                                          Vec2D{-yoke_width/2.0+cr,-cr}, /* center */
                                                          M_PI/2.0,                      /* phi_min */
                                                          M_PI,                          /* phi_max */
                                                          12,                            /* num_nodes */
                                                          1,                             /* poly_order */
                                                          true,                          /* normal_points_to_the_right */
                                                          false                          /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{-yoke_width/2.0,-cr},             /* start */
                                                             Vec2D{-yoke_width/2.0,-yoke_height+cr}, /* end */
                                                             30,                                     /* num_nodes */
                                                             1,                                      /* poly_order */
                                                             true,                                   /* normal_points_to_the_right */
                                                             false                                   /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                        /* radius */
                                                          Vec2D{-yoke_width/2.0+cr,-yoke_height+cr}, /* center */
                                                          -M_PI,                                     /* phi_min */
                                                          -M_PI/2.0,                                 /* phi_max */
                                                          12,                                        /* num_nodes */
                                                          1,                                         /* poly_order */
                                                          true,                                      /* normal_points_to_the_right */
                                                          false                                      /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{-yoke_width/2.0+cr,-yoke_height},             /* start */
                                                             Vec2D{-yoke_width/2.0+teeth_width-cr,-yoke_height}, /* end */
                                                             30,                                                 /* num_nodes */
                                                             1,                                                  /* poly_order */
                                                             true,                                               /* normal_points_to_the_right */
                                                             false                                               /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                                    /* radius */
                                                          Vec2D{-yoke_width/2.0+teeth_width-cr,-yoke_height+cr}, /* center */
                                                          -M_PI/2.0,                                             /* phi_min */
                                                          0.0,                                                   /* phi_max */
                                                          12,                                                    /* num_nodes */
                                                          1,                                                     /* poly_order */
                                                          true,                                                  /* normal_points_to_the_right */
                                                          false                                                  /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{-yoke_width/2.0+teeth_width,-yoke_height+cr},    /* start */
                                                             Vec2D{-yoke_width/2.0+teeth_width,-yoke_thickness-cr}, /* end */
                                                             20,                                                    /* num_nodes */
                                                             1,                                                     /* poly_order */
                                                             true,                                                  /* normal_points_to_the_right */
                                                             false                                                  /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                                       /* radius */
                                                          Vec2D{-yoke_width/2.0+teeth_width+cr,-yoke_thickness-cr}, /* center */
                                                          M_PI/2.0,                                                 /* phi_min */
                                                          M_PI,                                                     /* phi_max */
                                                          12,                                                       /* num_nodes */
                                                          1,                                                        /* poly_order */
                                                          true,                                                     /* normal_points_to_the_right */
                                                          false                                                     /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{-yoke_width/2.0+teeth_width+cr,-yoke_thickness}, /* start */
                                                             Vec2D{-coil_width/2.0,-yoke_thickness},                /* end */
                                                             20,                                                    /* num_nodes */
                                                             1,                                                     /* poly_order */
                                                             true,                                                  /* normal_points_to_the_right */
                                                             false                                                  /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{-coil_width/2.0,-yoke_thickness}, /* start */
                                                             Vec2D{ coil_width/2.0,-yoke_thickness}, /* end */
                                                             30,                                     /* num_nodes */
                                                             1,                                      /* poly_order */
                                                             true,                                   /* normal_points_to_the_right */
                                                             false                                   /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{coil_width/2.0,-yoke_thickness},                /* start */
                                                             Vec2D{yoke_width/2.0-teeth_width-cr,-yoke_thickness}, /* end */
                                                             30,                                                   /* num_nodes */
                                                             1,                                                    /* poly_order */
                                                             true,                                                 /* normal_points_to_the_right */
                                                             false                                                 /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                                      /* radius */
                                                          Vec2D{yoke_width/2.0-teeth_width-cr,-yoke_thickness-cr}, /* center */
                                                          0.0,                                                     /* phi_min */
                                                          M_PI/2.0,                                                /* phi_max */
                                                          12,                                                      /* num_nodes */
                                                          1,                                                       /* poly_order */
                                                          true,                                                    /* normal_points_to_the_right */
                                                          false                                                    /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{yoke_width/2.0-teeth_width,-yoke_thickness-cr}, /* start */
                                                             Vec2D{yoke_width/2.0-teeth_width,-yoke_height+cr},    /* end */
                                                             20,                                                   /* num_nodes */
                                                             1,                                                    /* poly_order */
                                                             true,                                                 /* normal_points_to_the_right */
                                                             false                                                 /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                                   /* radius */
                                                          Vec2D{yoke_width/2.0-teeth_width+cr,-yoke_height+cr}, /* center */
                                                          -M_PI,                                                /* phi_min */
                                                          -M_PI/2.0,                                            /* phi_max */
                                                          12,                                                   /* num_nodes */
                                                          1,                                                    /* poly_order */
                                                          true,                                                 /* normal_points_to_the_right */
                                                          false                                                 /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{yoke_width/2.0-teeth_width+cr,-yoke_height}, /* start */
                                                             Vec2D{yoke_width/2.0-cr,-yoke_height},             /* end */
                                                             30,                                                /* num_nodes */
                                                             1,                                                 /* poly_order */
                                                             true,                                              /* normal_points_to_the_right */
                                                             false                                              /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                       /* radius */
                                                          Vec2D{yoke_width/2.0-cr,-yoke_height+cr}, /* center */
                                                          -M_PI/2.0,                                /* phi_min */
                                                          0.0,                                      /* phi_max */
                                                          12,                                       /* num_nodes */
                                                          1,                                        /* poly_order */
                                                          true,                                     /* normal_points_to_the_right */
                                                          false                                     /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{yoke_width/2.0,-yoke_height+cr}, /* start */
                                                             Vec2D{yoke_width/2.0,-cr},             /* end */
                                                             30,                                    /* num_nodes */
                                                             1,                                     /* poly_order */
                                                             true,                                  /* normal_points_to_the_right */
                                                             false                                  /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                           /* radius */
                                                          Vec2D{yoke_width/2.0-cr,-cr}, /* center */
                                                          0.0,                          /* phi_min */
                                                          M_PI/2.0,                     /* phi_max */
                                                          12,                           /* num_nodes */
                                                          1,                            /* poly_order */
                                                          true,                         /* normal_points_to_the_right */
                                                          false                         /* is_reference */));
    yoke_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{yoke_width/2.0-cr,0.0}, /* start */
                                                             Vec2D{coil_width/2.0,0.0},    /* end */
                                                             30,                           /* num_nodes */
                                                             1,                            /* poly_order */
                                                             true,                         /* normal_points_to_the_right */
                                                             false                         /* is_reference */));

    vector<boundary_segment_ptr> lever_bnd_seg_ptrs; 
    lever_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{-yoke_width/2.0+teeth_width-cr,-yoke_height-delta}, /* start */
                                                              Vec2D{-yoke_width/2.0+cr,-yoke_height-delta},             /* end */
                                                              30,                                                       /* num_nodes */
                                                              1,                                                        /* poly_order */
                                                              true,                                                     /* normal_points_to_the_right */
                                                              false                                                     /* is_reference */));
    lever_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                              /* radius */
                                                           Vec2D{-yoke_width/2.0+cr,-yoke_height-delta-cr}, /* center */
                                                           M_PI/2.0,                                        /* phi_min */
                                                           M_PI,                                            /* phi_max */
                                                           12,                                              /* num_nodes */
                                                           1,                                               /* poly_order */
                                                           true,                                            /* normal_points_to_the_right */
                                                           false                                            /* is_reference */));
    lever_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{-yoke_width/2.0,-yoke_height-delta-cr},              /* start */
                                                              Vec2D{-yoke_width/2.0,-yoke_height-delta-lever_height+cr}, /* end */
                                                              30,                                                        /* num_nodes */
                                                              1,                                                         /* poly_order */
                                                              true,                                                      /* normal_points_to_the_right */
                                                              false                                                      /* is_reference */));
    lever_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                                           /* radius */
                                                           Vec2D{-yoke_width/2.0+cr,-yoke_height-delta-lever_height+cr}, /* center */
                                                           -M_PI,                                                        /* phi_min */
                                                           -M_PI/2.0,                                                    /* phi_max */
                                                           12,                                                           /* num_nodes */
                                                           1,                                                            /* poly_order */
                                                           true,                                                         /* normal_points_to_the_right */
                                                           false                                                         /* is_reference */));
    lever_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{-yoke_width/2.0+cr,-yoke_height-delta-lever_height}, /* start */
                                                              Vec2D{ yoke_width/2.0-cr,-yoke_height-delta-lever_height}, /* end */
                                                              30,                                                        /* num_nodes */
                                                              1,                                                         /* poly_order */
                                                              true,                                                      /* normal_points_to_the_right */
                                                              false                                                      /* is_reference */));
    lever_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                                          /* radius */
                                                           Vec2D{yoke_width/2.0-cr,-yoke_height-delta-lever_height+cr}, /* center */
                                                           -M_PI/2.0,                                                   /* phi_min */
                                                           0.0,                                                         /* phi_max */
                                                           12,                                                          /* num_nodes */
                                                           1,                                                           /* poly_order */
                                                           true,                                                        /* normal_points_to_the_right */
                                                           false                                                        /* is_reference */));
    lever_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{yoke_width/2.0,-yoke_height-delta-lever_height+cr}, /* start */
                                                              Vec2D{yoke_width/2.0,-yoke_height-delta-cr},              /* end */
                                                              30,                                                       /* num_nodes */
                                                              1,                                                        /* poly_order */
                                                              true,                                                     /* normal_points_to_the_right */
                                                              false                                                     /* is_reference */));
    lever_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                             /* radius */
                                                           Vec2D{yoke_width/2.0-cr,-yoke_height-delta-cr}, /* center */
                                                           0.0,                                            /* phi_min */
                                                           M_PI/2.0,                                       /* phi_max */
                                                           12,                                             /* num_nodes */
                                                           1,                                              /* poly_order */
                                                           true,                                           /* normal_points_to_the_right */
                                                           false                                           /* is_reference */));
    lever_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{yoke_width/2.0-cr,-yoke_height-delta},             /* start */
                                                              Vec2D{yoke_width/2.0-teeth_width+cr,-yoke_height-delta}, /* end */
                                                              30,                                                      /* num_nodes */
                                                              1,                                                       /* poly_order */
                                                              true,                                                    /* normal_points_to_the_right */
                                                              false                                                    /* is_reference */));
    lever_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                                         /* radius */
                                                           Vec2D{yoke_width/2.0-teeth_width+cr,-yoke_height-delta-cr}, /* center */
                                                           M_PI/2.0,                                                   /* phi_min */
                                                           M_PI,                                                       /* phi_max */
                                                           12,                                                         /* num_nodes */
                                                           1,                                                          /* poly_order */
                                                           true,                                                       /* normal_points_to_the_right */
                                                           false                                                       /* is_reference */));
    lever_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{yoke_width/2.0-teeth_width,-yoke_height-delta-cr},                           /* start */
                                                              Vec2D{yoke_width/2.0-teeth_width,-yoke_height-delta-lever_height+lever_thickness}, /* end */
                                                              30,                                                                                /* num_nodes */
                                                              1,                                                                                 /* poly_order */
                                                              true,                                                                              /* normal_points_to_the_right */
                                                              false                                                                              /* is_reference */));
    lever_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{ yoke_width/2.0-teeth_width,-yoke_height-delta-lever_height+lever_thickness}, /* start */
                                                              Vec2D{-yoke_width/2.0+teeth_width,-yoke_height-delta-lever_height+lever_thickness}, /* end */
                                                              30,                                                                                 /* num_nodes */
                                                              1,                                                                                  /* poly_order */
                                                              true,                                                                               /* normal_points_to_the_right */
                                                              false                                                                               /* is_reference */));
    lever_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{-yoke_width/2.0+teeth_width,-yoke_height-delta-lever_height+lever_thickness}, /* start */
                                                              Vec2D{-yoke_width/2.0+teeth_width,-yoke_height-delta-cr},                           /* end */
                                                              30,                                                                                 /* num_nodes */
                                                              1,                                                                                  /* poly_order */
                                                              true,                                                                               /* normal_points_to_the_right */
                                                              false                                                                               /* is_reference */));
    lever_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                                          /* radius */
                                                           Vec2D{-yoke_width/2.0+teeth_width-cr,-yoke_height-delta-cr}, /* center */
                                                           0.0,                                                         /* phi_min */
                                                           M_PI/2.0,                                                    /* phi_max */
                                                           12,                                                          /* num_nodes */
                                                           1,                                                           /* poly_order */
                                                           true,                                                        /* normal_points_to_the_right */
                                                           false                                                        /* is_reference */));

    vector<boundary_segment_ptr> coil1_bnd_seg_ptrs; 
    coil1_bnd_seg_ptrs.push_back(yoke_bnd_seg_ptrs[0]);
    coil1_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{coil_width/2.0,0.0},            /* start */
                                                              Vec2D{coil_width/2.0,coil_height-cr}, /* end */
                                                              30,                                   /* num_nodes */
                                                              1,                                    /* poly_order */
                                                              true,                                 /* normal_points_to_the_right */
                                                              false                                 /* is_reference */));
    coil1_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                      /* radius */
                                                           Vec2D{coil_width/2.0-cr,coil_height-cr}, /* center */
                                                           0.0,                                     /* phi_min */
                                                           M_PI/2.0,                                /* phi_max */
                                                           12,                                      /* num_nodes */
                                                           1,                                       /* poly_order */
                                                           true,                                    /* normal_points_to_the_right */
                                                           false                                    /* is_reference */));
    coil1_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{ coil_width/2.0-cr,coil_height}, /* start */
                                                              Vec2D{-coil_width/2.0+cr,coil_height}, /* end */
                                                              30,                                    /* num_nodes */
                                                              1,                                     /* poly_order */
                                                              true,                                  /* normal_points_to_the_right */
                                                              false                                  /* is_reference */));
    coil1_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                       /* radius */
                                                           Vec2D{-coil_width/2.0+cr,coil_height-cr}, /* center */
                                                           M_PI/2.0,                                 /* phi_min */
                                                           M_PI,                                     /* phi_max */
                                                           12,                                       /* num_nodes */
                                                           1,                                        /* poly_order */
                                                           true,                                     /* normal_points_to_the_right */
                                                           false                                     /* is_reference */));
    coil1_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{-coil_width/2.0,coil_height-cr}, /* start */
                                                              Vec2D{-coil_width/2.0,0.0},            /* end */
                                                              30,                                    /* num_nodes */
                                                              1,                                     /* poly_order */
                                                              true,                                  /* normal_points_to_the_right */
                                                              false                                  /* is_reference */));

    vector<boundary_segment_ptr> coil2_bnd_seg_ptrs; 
    coil2_bnd_seg_ptrs.push_back(yoke_bnd_seg_ptrs[10]);
    coil2_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{-coil_width/2.0,-yoke_thickness},                /* start */
                                                              Vec2D{-coil_width/2.0,-coil_height-yoke_thickness+cr}, /* end */
                                                              30,                                                    /* num_nodes */
                                                              1,                                                     /* poly_order */
                                                              true,                                                  /* normal_points_to_the_right */
                                                              false                                                  /* is_reference */));
    coil2_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                                       /* radius */
                                                           Vec2D{-coil_width/2.0+cr,-coil_height-yoke_thickness+cr}, /* center */
                                                           -M_PI,                                                    /* phi_min */
                                                           -M_PI/2.0,                                                /* phi_max */
                                                           12,                                                       /* num_nodes */
                                                           1,                                                        /* poly_order */
                                                           true,                                                     /* normal_points_to_the_right */
                                                           false                                                     /* is_reference */));
    coil2_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{-coil_width/2.0+cr,-coil_height-yoke_thickness}, /* start */
                                                              Vec2D{ coil_width/2.0-cr,-coil_height-yoke_thickness}, /* end */
                                                              30,                                                    /* num_nodes */
                                                              1,                                                     /* poly_order */
                                                              true,                                                  /* normal_points_to_the_right */
                                                              false                                                  /* is_reference */));
    coil2_bnd_seg_ptrs.push_back(p.addArcedBoundarySegment(cr,                                                      /* radius */
                                                           Vec2D{coil_width/2.0-cr,-coil_height-yoke_thickness+cr}, /* center */
                                                           -M_PI/2.0,                                               /* phi_min */
                                                           0.0,                                                     /* phi_max */
                                                           12,                                                      /* num_nodes */
                                                           1,                                                       /* poly_order */
                                                           true,                                                    /* normal_points_to_the_right */
                                                           false                                                    /* is_reference */));
    coil2_bnd_seg_ptrs.push_back(p.addStraightBoundarySegment(Vec2D{coil_width/2.0,-coil_height-yoke_thickness+cr}, /* start */
                                                              Vec2D{coil_width/2.0,            -yoke_thickness},    /* end */
                                                              30,                                                   /* num_nodes */
                                                              1,                                                    /* poly_order */
                                                              true,                                                 /* normal_points_to_the_right */
                                                              false                                                 /* is_reference */));

    /* Create the domains in the problem */
    vector<directed_boundary_segment_ptr> dbsps;
    for (auto [i,reverse] = std::tuple{0,std::unordered_set<int>({8,12})}; i < yoke_bnd_seg_ptrs.size(); ++i)
        if (reverse.contains(i)) dbsps.push_back(std::make_shared<DirectedBoundarySegment>(yoke_bnd_seg_ptrs[i],false));
        else                     dbsps.push_back(std::make_shared<DirectedBoundarySegment>(yoke_bnd_seg_ptrs[i],true));
    const auto yoke = p.addDomain("yoke",{dbsps},mu_iron,0.0);

    dbsps.clear();
    for (auto [i,reverse] = std::tuple{0,std::unordered_set<int>({0})}; i < coil1_bnd_seg_ptrs.size(); ++i)
        if (reverse.contains(i)) dbsps.push_back(std::make_shared<DirectedBoundarySegment>(coil1_bnd_seg_ptrs[i],false));
        else                     dbsps.push_back(std::make_shared<DirectedBoundarySegment>(coil1_bnd_seg_ptrs[i],true));
    const auto coil1 = p.addDomain("coil1",{dbsps},mu_cu,1.0e6);

    dbsps.clear();
    for (auto [i,reverse] = std::tuple{0,std::unordered_set<int>({0})}; i < coil2_bnd_seg_ptrs.size(); ++i)
        if (reverse.contains(i)) dbsps.push_back(std::make_shared<DirectedBoundarySegment>(coil2_bnd_seg_ptrs[i],false));
        else                     dbsps.push_back(std::make_shared<DirectedBoundarySegment>(coil2_bnd_seg_ptrs[i],true));
    const auto coil2 = p.addDomain("coil2",{dbsps},mu_cu,-1.0e6);

    dbsps.clear();
    for (const auto& bnd_seg_ptr : lever_bnd_seg_ptrs)
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(bnd_seg_ptr,true));
    const auto lever = p.addDomain("lever",{dbsps},mu_iron,0.0,3);

    vector<vector<directed_boundary_segment_ptr>> vdbsps;
    dbsps.clear();
    for (int i = coil1_bnd_seg_ptrs.size()-1; i >= 1; --i)
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(coil1_bnd_seg_ptrs[i],false));
    for (int i = yoke_bnd_seg_ptrs.size()-1; i >= 11; --i)
        if (i == 12) dbsps.push_back(std::make_shared<DirectedBoundarySegment>(yoke_bnd_seg_ptrs[i],true));
        else         dbsps.push_back(std::make_shared<DirectedBoundarySegment>(yoke_bnd_seg_ptrs[i],false));
    for (int i = coil2_bnd_seg_ptrs.size()-1; i >= 1; --i)
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(coil2_bnd_seg_ptrs[i],false));
    for (int i = 9; i >= 1; --i)
        if (i == 8) dbsps.push_back(std::make_shared<DirectedBoundarySegment>(yoke_bnd_seg_ptrs[i],true));
        else        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(yoke_bnd_seg_ptrs[i],false));
    vdbsps.push_back(dbsps);
    dbsps.clear();
    for (int i = lever_bnd_seg_ptrs.size()-1; i >= 0; --i)
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(lever_bnd_seg_ptrs[i],false));
    vdbsps.push_back(dbsps);
    const auto air = p.addDomain("air",vdbsps,mu_air,0.0);

    /* Solve the problem */
    //p.solveAndStore("results/lever25000");
    p.loadSolution("results/lever25000");

    /* Plot the boundaries in the problem */
    sanji::figure("Mover simulation");
    for (const auto& boundary_segment_plotting_data : p.getBoundarySegmentsPlottingData(0.001/4.0, /* max_length_per_segment */
                                                                                        true       /* show_reference_boundaries */))
        sanji::plot(*boundary_segment_plotting_data.x,*boundary_segment_plotting_data.y,boundary_segment_plotting_data.style);

    /* Plot the normals in the problem */
    if (plot_normals)
        for (const auto& boundary_segment_plotting_vector_data : p.getNormals(1.0e-3, /* max_length_per_segment */
                                                                              1.0e-3  /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);

    //Plot the magnetic flux density */
    if (plot_field_data) {
        // Compute the field data
        const auto field_data = p.getFieldData(1.0e5,load_field_data,store_field_data,"results/mover");

        double min =  std::numeric_limits<double>::max();
        double max = -std::numeric_limits<double>::max();
        for (const auto& field_data : field_data)
            for (uint i = 0; i < field_data.x->rows(); ++i) {
                const double strength = Vec2D{(*field_data.u)(i),(*field_data.v)(i)}.Norm2();
                if (strength < min) min = strength;
                if (strength > max) max = strength;
            }
        for (const auto& field_data : field_data)
            sanji::quiver(*field_data.x,
                          *field_data.y,
                          *field_data.u,
                          //*field_data.v,{{"arrow_length",0.0006},{"colormap",TURBO},{"min",min},{"max",max}},{"use_colormap","center_arrows"});
                          *field_data.v,{{"arrow_length",0.006},{"colormap",TURBO},{"min",min},{"max",max}},{"use_colormap","center_arrows","use_logscale"});

        /* Print statistics */
        std::cout << "The minimum is " << min << std::endl;
        std::cout << "The maximum is " << max << std::endl;
    }

    /* Plot cosmetics */
    //sanji::setPlotBackgroundColor(GRAY);
    sanji::setAxesRatio("equal");

    /* Compute the force applied onto the mover */
    std::cout << std::setprecision(15);
    if (compute_force) {
        Vec2D force{0.0,0.0};

        // Define a GSL function and a GSL workspace
        gsl_function                   gsl_fun;
        gsl_integration_workspace*     gsl_workspace    = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
        gsl_integration_glfixed_table* quadrature_table = gsl_integration_glfixed_table_alloc(OUTER_QUADRATURE_ORDER);

        // Create a wrapper to simplify integration and allocate memory for the integrand parameters
        const auto quad = [&gsl_fun,gsl_workspace,quadrature_table]()->double {
            //double result,error;
            //gsl_integration_qag(&gsl_fun, /* Function to integrate */
            //                    0.0,      /* Lower integration limit */
            //                    1.0,      /* Upper integration limit */
            //                    INTEGRAL_ABSOLUTE_ERROR_LIMIT,
            //                    INTEGRAL_RELATIVE_ERROR_LIMIT,
            //                    MAX_NUM_SUBINTERVALS,
            //                    INTEGRATION_KEY,
            //                    gsl_workspace,&result,&error);
            //size_t neval;
            //gsl_integration_qng(&gsl_fun,
            //                    0.0,
            //                    1.0,
            //                    INTEGRAL_ABSOLUTE_ERROR_LIMIT,
            //                    INTEGRAL_RELATIVE_ERROR_LIMIT,
            //                    &result,
            //                    &error,
            //                    &neval);
            //return result;
            return gsl_integration_glfixed(&gsl_fun,0.0,1.0,quadrature_table);
        };
        const double** params = new const double*[1];
        gsl_fun.params        = params;

        // Create a lambda to integrate along a boundary segment
        const auto getBoundaryIntegral = [&quad,&gsl_fun,&params,&air,&p](const uint dir_bnd_seg_idx)->const Vec2D {
            // Preliminary definitions
            const auto& dir_bnd_seg_ptr = air->getDirectedBoundarySegmentPointers()[1][dir_bnd_seg_idx];
            Vec2D result{0.0,0.0};

            // Compute the contributions from all subsegments
            for (uint subseg_idx = 0u; subseg_idx < dir_bnd_seg_ptr->getNumSubsegments(); ++subseg_idx) {
                // Define the integrand
                uint dim_idx;
                const auto integrand = [dpsi=dir_bnd_seg_ptr->getdpsii2Norm(subseg_idx),&air,&dir_bnd_seg_ptr,dir_bnd_seg_idx,subseg_idx,&p,&dim_idx](const double t)->double {
                    // Compute the flux density and the normal
                    auto [B,x,nu] = p.evaluateBoundarySolution(air,1/*loop_idx*/,dir_bnd_seg_idx,(subseg_idx+t)/dir_bnd_seg_ptr->getNumSubsegments());

                    // The normal should point to the outside of the mover
                    nu = -nu;

                    // \mu = \mu_0 since we are in the air
                    const double mu = mu0;

                    // Compute the H field
                    const Vec2D  H  = B/mu;

                    // Compute the differential force
                    // From: Comparison of main magnetic force computation methods for noise
                    // and vibration assessment in electrical machines, Eq. (9)
                    const Vec2D dF = (B.dot(nu))*H - mu/2.0*H.Norm2Squared()*nu;

                    return (dim_idx==0u ? dF.x : dF.y)*dpsi(t);
                };
                params[0] = reinterpret_cast<const double*>(&integrand);
                gsl_fun.function = [](const double t, void* params)->double {
                    const decltype(integrand)& integrand_ = *reinterpret_cast<const decltype(integrand)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));

                    return integrand_(t);
                };

                // Integrate along the subsegment
                //dim_idx   = 0u;
                //result.x += quad();
                dim_idx   = 1u;
                result.y += quad();
            }

            return result;
        };

        // Integrate along the four boundary segments
        for (uint dir_bnd_seg_idx = 0u; dir_bnd_seg_idx < air->getDirectedBoundarySegmentPointers()[1].size(); ++dir_bnd_seg_idx) {
            const Vec2D c = getBoundaryIntegral(dir_bnd_seg_idx);
            std::cout << "c=" << c.toString() << std::endl;
            force += c;
        }

        std::cout << "The total force acting on the mover is " << force.toString() << "." << std::endl;

        // Free memory
        delete[] params;
        gsl_integration_workspace_free(gsl_workspace);
        gsl_integration_glfixed_table_free(quadrature_table);
    }

//    // Visualize the flux coming from the yoke into the mover
//    const auto&    domain     = air;
//    const uint     N          = 200u;
//    Eigen::ArrayXd B_norm_bnd = VectorXd::LinSpaced(N,0u,N-1u);
//    directed_boundary_segment_ptr dir_bnd_seg_ptr;
//    for (const auto& dir_bnd_seg_ptrs_ : domain->getDirectedBoundarySegmentPointers())
//        for (const auto& dir_bnd_seg_ptr_ : dir_bnd_seg_ptrs_)
//            //if (s_m_5c.get() == &**dir_bnd_seg_ptr_)
//            if (s_m_2.get() == &**dir_bnd_seg_ptr_)
//                dir_bnd_seg_ptr = dir_bnd_seg_ptr_;
//    std::for_each(B_norm_bnd.cbegin(),B_norm_bnd.cend(),[N,&B_norm_bnd,&p,&domain,&dir_bnd_seg_ptr](const int n) {
//        // Compute the solution on the boundary
//        auto [loop_idx,dir_bnd_seg_idx] = domain->getDirectedBoundarySegmentIndex((**dir_bnd_seg_ptr).getBoundarySegmentIndex());
//        auto [B,x,nu]                   = p.evaluateBoundarySolution(domain,loop_idx,dir_bnd_seg_idx,static_cast<double>(n)/N);
//        //B = p.evaluateInteriorSolution(domain,x-1.0e-5*nu);
//        //B_norm_bnd(n)                   = Vec2D{nu.y,-nu.x}.dot(B);
//        //B_norm_bnd(n)                   = nu.dot(B);
//        const double mu = mu0;
//        const Vec2D  H  = B/mu;
//        const Vec2D  T  = (B.dot(nu))*H - mu/2.0*H.Norm2Squared()*nu;
//        B_norm_bnd(n)   = T.y;
//    });
//    sanji::figure("Magnetic flux in normal direction along boundary");
//    sanji::plot(VectorXd::LinSpaced(N,0,N-1)*2.0*M_PI/N,B_norm_bnd,{{"color",RED}});

    // Print the expected force
    /*
     * F_{\rm{mech}} = (\frac{dW_m}{d\delta}_{\Psi=\rm{const.}}
     *               = \frac{d}{d\delta}2*teeth_width*delta*1*0.5*B_L^2/\mu_0 (*)
     *               -> B_L*1*teeth_width = \phi
     *               -> \phi*R_m          = \Theta
     *               -> \Theta            = coil_width*coil_height*1.0e6
     *               -> R_m               = 2*delta/\mu_0/teeth_width/1
     *                                    = 2*delta/\mu_0/teeth_width
     *               => B_L   = \phi/1/teeth_width
     *                        = \phi/teeth_width
     *               => \phi  = \Theta/R_m
     *               => B_L   = \Theta/R_m/teeth_width
     *                        = coil_width*coil_height*1.0e6/R_m/teeth_width
     *                        = coil_width*coil_height*1.0e6/teeth_width/R_m
     *               => 1/R_m = \mu_0*teeth_width/2/delta
     *               => B_L   = coil_width*coil_height*1.0e6/teeth_width*\mu_0*teeth_width/2/delta
     *                        = coil_width*coil_height*1.0e6*\mu_0/2/delta
     *               => (*) = \frac{d}{d\delta}teeth_width*delta/\mu_0*B_L^2
     *                      = \frac{d}{d\delta}teeth_width*delta/\mu_0*coil_width^2*coil_height^2*1.0e12*\mu_0^2/4.0/delta^2
     *                      = teeth_width/delta*\mu_0*coil_width^2*coil_height^2*1.0e12/4.0*\frac{d}{d\delta}1/delta^2
     *                      = -teeth_width/delta*\mu_0*coil_width^2*coil_height^2*1.0e12/2.0/delta^3
     */
    std::cout << "The expected force is " << 2.5e11*coil_height*coil_height*coil_width*coil_width*teeth_width*mu0/delta/delta << std::endl;

    // Visualize the flux coming from the yoke into the mover
    const auto&    domain     = air;
    const uint     N          = 200u;
    Eigen::ArrayXd B_norm_bnd = VectorXd::LinSpaced(N,0u,N-1u);
    directed_boundary_segment_ptr dir_bnd_seg_ptr;
    for (const auto& dir_bnd_seg_ptrs_ : domain->getDirectedBoundarySegmentPointers())
        for (const auto& dir_bnd_seg_ptr_ : dir_bnd_seg_ptrs_)
            if (lever_bnd_seg_ptrs[0].get() == &**dir_bnd_seg_ptr_)
            //if (s_m_6.get() == &**dir_bnd_seg_ptr_)
                dir_bnd_seg_ptr = dir_bnd_seg_ptr_;
    std::for_each(B_norm_bnd.cbegin(),B_norm_bnd.cend(),[N,&B_norm_bnd,&p,&domain,&dir_bnd_seg_ptr](const int n) {
        // Compute the solution on the boundary
        auto [loop_idx,dir_bnd_seg_idx] = domain->getDirectedBoundarySegmentIndex((**dir_bnd_seg_ptr).getBoundarySegmentIndex());
        auto [B,x,nu]                   = p.evaluateBoundarySolution(domain,loop_idx,dir_bnd_seg_idx,static_cast<double>(n)/N);
        //B_norm_bnd(n)                   = Vec2D{nu.y,-nu.x}.dot(B);
        B_norm_bnd(n)                   = nu.dot(B);
    });
    sanji::figure("Magnetic flux in normal direction along boundary");
    sanji::plot(VectorXd::LinSpaced(N,0,N-1)*2.0*M_PI/N,B_norm_bnd,{{"color",RED}});

    // Execute the application
    app.exec();
}
