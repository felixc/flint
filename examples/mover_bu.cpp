#include <gsl/gsl_integration.h>
#include <iostream>
#include <iomanip>
#include "Sanji.hpp"
#include "ProblemWrapper.hpp"

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 0
#define INTEGRAL_RELATIVE_ERROR_LIMIT 5e-3
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          2000

using namespace sanji::colors;

/* Type definitions */
template <typename T>
using vector = std::vector<T>;

// Visualization
constexpr bool plot_normals     = true;
constexpr bool compute_force    = true;
constexpr bool plot_field_data  = false;
constexpr bool load_field_data  = false;
constexpr bool store_field_data = false;

// Geometric parameters
constexpr double w    = 0.01;   // [m]
constexpr double lm   = 0.025;  // [m]
constexpr double wTht = 0.03;   // [m]
constexpr double lTht = 0.005;  // [m]
constexpr double d1   = 0.001;  // [m]
constexpr double d2   = 0.003;  // [m]
//constexpr double d2   = 0.01;  // [m]
constexpr double lw   = 0.0497; // [m]
constexpr double ww   = 0.01;   // [m]
constexpr double d    = 0.01;   // [m]
constexpr double cr   = 0.0008; // [m]

// Permeabilities
constexpr double mu0     = 4.0*M_PI*1e-7;
constexpr double mu_air  = 1.00000037*mu0; // https://www.engineeringtoolbox.com/permeability-d_1923.html
constexpr double mu_iron = 2500.0*mu0;
constexpr double mu_cu   = 0.999994*mu0;   // https://www.engineeringtoolbox.com/permeability-d_1923.html

int main(int argc, char* argv[]) {

    // Initialize this as a Qt application
    QApplication app(argc, argv);
    sanji::init();

    /* Create a problem instance */
    ProblemWrapper p;

    /* Create the boundaries in the problem */
    const auto s_m_1 = p.addStraightBoundarySegment(Vec2D{cr,0.0},   /* start */
                                                    Vec2D{w-cr,0.0}, /* end */
                                                    30,              /* num_nodes */
                                                    1,               /* poly_order */
                                                    true,            /* normal_points_to_the_right */
                                                    false            /* is_reference */);
    const auto s_m_1c = p.addArcedBoundarySegment(cr,             /* radius */
                                                  Vec2D{w-cr,cr}, /* center */
                                                  -M_PI/2.0,      /* phi_min */
                                                  0.0,            /* phi_max */
                                                  12,              /* num_nodes */
                                                  1,              /* poly_order */
                                                  true,           /* normal_points_to_the_right */
                                                  false           /* is_reference */);
    const auto s_m_2 = p.addStraightBoundarySegment(Vec2D{w,cr},        /* start */
                                                    Vec2D{w,1.4*lm-cr}, /* end */
                                                    30,                 /* num_nodes */
                                                    1,                  /* poly_order */
                                                    true,               /* normal_points_to_the_right */
                                                    false               /* is_reference */);
    const auto s_m_2c = p.addArcedBoundarySegment(cr,                    /* radius */
                                                  Vec2D{w+cr,1.4*lm-cr}, /* center */
                                                  M_PI/2.0,              /* phi_min */
                                                  M_PI,                  /* phi_max */
                                                  12,                     /* num_nodes */
                                                  1,                     /* poly_order */
                                                  true,                  /* normal_points_to_the_right */
                                                  false                  /* is_reference */);
    const auto s_m_3 = p.addStraightBoundarySegment(Vec2D{w+cr,1.4*lm},              /* start */
                                                    Vec2D{w+(wTht-w)/2.0-cr,1.4*lm}, /* end */
                                                    10,                               /* num_nodes */
                                                    1,                               /* poly_order */
                                                    true,                            /* normal_points_to_the_right */
                                                    false                            /* is_reference */);
    const auto s_m_3c = p.addArcedBoundarySegment(cr,                                 /* radius */
                                                  Vec2D{w+(wTht-w)/2.0-cr,1.4*lm+cr}, /* center */
                                                  -M_PI/2.0,                          /* phi_min */
                                                  0.0,                                /* phi_max */
                                                  12,                                  /* num_nodes */
                                                  1,                                  /* poly_order */
                                                  true,                               /* normal_points_to_the_right */
                                                  false                               /* is_reference */);
    const auto s_m_4 = p.addStraightBoundarySegment(Vec2D{w+(wTht-w)/2.0,1.4*lm+cr},      /* start */
                                                    Vec2D{w+(wTht-w)/2.0,1.4*lm+lTht-cr}, /* end */
                                                    10,                                    /* num_nodes */
                                                    1,                                    /* poly_order */
                                                    true,                                 /* normal_points_to_the_right */
                                                    false                                 /* is_reference */);
    const auto s_m_4c = p.addArcedBoundarySegment(cr,                                      /* radius */
                                                  Vec2D{w+(wTht-w)/2.0-cr,1.4*lm+lTht-cr}, /* center */
                                                  0.0,                                     /* phi_min */
                                                  M_PI/2.0,                                /* phi_max */
                                                  12,                                       /* num_nodes */
                                                  1,                                       /* poly_order */
                                                  true,                                    /* normal_points_to_the_right */
                                                  false                                    /* is_reference */);
    const auto s_m_5 = p.addStraightBoundarySegment(Vec2D{w+(wTht-w)/2.0-cr,1.4*lm+lTht}, /* start */
                                                    Vec2D{-(wTht-w)/2.0+cr,1.4*lm+lTht},  /* end */
                                                    30,                                    /* num_nodes */
                                                    1,                                    /* poly_order */
                                                    true,                                 /* normal_points_to_the_right */
                                                    false                                 /* is_reference */);
    const auto s_m_5c = p.addArcedBoundarySegment(cr,                                     /* radius */
                                                  Vec2D{-(wTht-w)/2.0+cr,1.4*lm+lTht-cr}, /* center */
                                                  M_PI/2.0,                               /* phi_min */
                                                  M_PI,                                   /* phi_max */
                                                  12,                                      /* num_nodes */
                                                  1,                                      /* poly_order */
                                                  true,                                   /* normal_points_to_the_right */
                                                  false                                   /* is_reference */);
    const auto s_m_6 = p.addStraightBoundarySegment(Vec2D{-(wTht-w)/2.0,1.4*lm+lTht-cr}, /* start */
                                                    Vec2D{-(wTht-w)/2.0,1.4*lm+cr},      /* end */
                                                    10,                                   /* num_nodes */
                                                    1,                                   /* poly_order */
                                                    true,                                /* normal_points_to_the_right */
                                                    false                                /* is_reference */);
    const auto s_m_6c = p.addArcedBoundarySegment(cr,                                /* radius */
                                                  Vec2D{-(wTht-w)/2.0+cr,1.4*lm+cr}, /* center */
                                                  -M_PI,                             /* phi_min */
                                                  -M_PI/2.0,                         /* phi_max */
                                                  12,                                 /* num_nodes */
                                                  1,                                 /* poly_order */
                                                  true,                              /* normal_points_to_the_right */
                                                  false                              /* is_reference */);
    const auto s_m_7 = p.addStraightBoundarySegment(Vec2D{-(wTht-w)/2.0+cr,1.4*lm}, /* start */
                                                    Vec2D{-cr,1.4*lm},              /* end */
                                                    10,                              /* num_nodes */
                                                    1,                              /* poly_order */
                                                    true,                           /* normal_points_to_the_right */
                                                    false                           /* is_reference */);
    const auto s_m_7c = p.addArcedBoundarySegment(cr,                   /* radius */
                                                  Vec2D{-cr,1.4*lm-cr}, /* center */
                                                  0.0,                  /* phi_min */
                                                  M_PI/2.0,             /* phi_max */
                                                  12,                    /* num_nodes */
                                                  1,                    /* poly_order */
                                                  true,                 /* normal_points_to_the_right */
                                                  false                 /* is_reference */);
    const auto s_m_8 = p.addStraightBoundarySegment(Vec2D{0.0,1.4*lm-cr}, /* start */
                                                    Vec2D{0.0,cr},        /* end */
                                                    30,                   /* num_nodes */
                                                    1,                    /* poly_order */
                                                    true,                 /* normal_points_to_the_right */
                                                    false                 /* is_reference */);
    const auto s_m_8c = p.addArcedBoundarySegment(cr,           /* radius */
                                                  Vec2D{cr,cr}, /* center */
                                                  -M_PI,        /* phi_min */
                                                  -M_PI/2.0,    /* phi_max */
                                                  12,            /* num_nodes */
                                                  1,            /* poly_order */
                                                  true,         /* normal_points_to_the_right */
                                                  false         /* is_reference */);
    const auto s_c_1 = p.addStraightBoundarySegment(Vec2D{w+d1+lTht,1.4*lm-lm-(wTht-w)/2.0},    /* start */
                                                    Vec2D{w+d1+lTht+lw,1.4*lm-lm-(wTht-w)/2.0}, /* end */
                                                    12,                                          /* num_nodes */
                                                    1,                                          /* poly_order */
                                                    true,                                       /* normal_points_to_the_right */
                                                    false                                       /* is_reference */);
    const auto s_c_2 = p.addStraightBoundarySegment(Vec2D{w+d1+lTht+lw,1.4*lm-lm-(wTht-w)/2.0}, /* start */
                                                    Vec2D{w+d1+lTht+lw,1.4*lm-lm},              /* end */
                                                    12,                                          /* num_nodes */
                                                    1,                                          /* poly_order */
                                                    true,                                       /* normal_points_to_the_right */
                                                    false                                       /* is_reference */);
    const auto s_c_5 = p.addStraightBoundarySegment(Vec2D{w+d1+lTht+lw,1.4*lm-lm+w+(wTht-w)/2.0}, /* start */
                                                    Vec2D{w+d1+lTht,1.4*lm-lm+w+(wTht-w)/2.0},    /* end */
                                                    12,                                            /* num_nodes */
                                                    1,                                            /* poly_order */
                                                    true,                                         /* normal_points_to_the_right */
                                                    false                                         /* is_reference */);
    const auto s_y_1 = p.addStraightBoundarySegment(Vec2D{-(wTht-w)/2.0+cr,1.4*lm+lTht+d2},  /* start */
                                                    Vec2D{w+(wTht-w)/2.0-cr,1.4*lm+lTht+d2}, /* end */
                                                    12,                                       /* num_nodes */
                                                    1,                                       /* poly_order */
                                                    true,                                    /* normal_points_to_the_right */
                                                    false                                    /* is_reference */);
    const auto s_y_1c = p.addArcedBoundarySegment(cr,                                         /* radius */
                                                  Vec2D{w+(wTht-w)/2.0-cr,1.4*lm+lTht+d2+cr}, /* center */
                                                  -M_PI/2.0,                                  /* phi_min */
                                                  0.0,                                        /* phi_max */
                                                  6,                                          /* num_nodes */
                                                  1,                                          /* poly_order */
                                                  true,                                       /* normal_points_to_the_right */
                                                  false                                       /* is_reference */);
    const auto s_y_2 = p.addStraightBoundarySegment(Vec2D{w+(wTht-w)/2.0,1.4*lm+lTht+d2+cr},   /* start */
                                                    Vec2D{w+(wTht-w)/2.0,1.4*lm+lTht+d2+lTht}, /* end */
                                                    8,                                         /* num_nodes */
                                                    1,                                         /* poly_order */
                                                    true,                                      /* normal_points_to_the_right */
                                                    false                                      /* is_reference */);
    const auto s_y_3 = p.addStraightBoundarySegment(Vec2D{w+(wTht-w)/2.0,1.4*lm+lTht+d2+lTht}, /* start */
                                                    Vec2D{w+d1+lTht+lw,1.4*lm+lTht+d2+lTht},   /* end */
                                                    12,                                         /* num_nodes */
                                                    1,                                         /* poly_order */
                                                    true,                                      /* normal_points_to_the_right */
                                                    false                                      /* is_reference */);
    const auto s_y_4a = p.addStraightBoundarySegment(Vec2D{w+d1+lTht+lw,1.4*lm+lTht+d2+lTht}, /* start */
                                                     s_c_5->getStartPoint(),                  /* end */
                                                     12,                                       /* num_nodes */
                                                     1,                                       /* poly_order */
                                                     true,                                    /* normal_points_to_the_right */
                                                     false                                    /* is_reference */);
    const auto s_y_4b = p.addStraightBoundarySegment(s_c_5->getStartPoint(),          /* start */
                                                     Vec2D{w+d1+lTht+lw,1.4*lm-lm+w}, /* end */
                                                     8,                               /* num_nodes */
                                                     1,                               /* poly_order */
                                                     true,                            /* normal_points_to_the_right */
                                                     false                            /* is_reference */);
    const auto s_y_5 = p.addStraightBoundarySegment(Vec2D{w+d1+lTht+lw,1.4*lm-lm+w}, /* start */
                                                    Vec2D{w+d1+lTht,1.4*lm-lm+w},    /* end */
                                                    12,                               /* num_nodes */
                                                    1,                               /* poly_order */
                                                    true,                            /* normal_points_to_the_right */
                                                    false                            /* is_reference */);
    const auto s_y_6 = p.addStraightBoundarySegment(Vec2D{w+d1+lTht,1.4*lm-lm+w},              /* start */
                                                    Vec2D{w+d1+lTht,1.4*lm-lm+w+(wTht-w)/2.0}, /* end */
                                                    12,                                         /* num_nodes */
                                                    1,                                         /* poly_order */
                                                    true,                                      /* normal_points_to_the_right */
                                                    false                                      /* is_reference */);
    const auto s_y_7 = p.addStraightBoundarySegment(Vec2D{w+d1+lTht,1.4*lm-lm+w+(wTht-w)/2.0}, /* start */
                                                    Vec2D{w+d1+cr,1.4*lm-lm+w+(wTht-w)/2.0},   /* end */
                                                    12,                                         /* num_nodes */
                                                    1,                                         /* poly_order */
                                                    true,                                      /* normal_points_to_the_right */
                                                    false                                      /* is_reference */);

    const auto s_y_7c = p.addArcedBoundarySegment(cr,                                         /* radius */
                                                  Vec2D{w+d1+cr,1.4*lm-lm+w+(wTht-w)/2.0-cr}, /* center */
                                                  M_PI/2.0,                                   /* phi_min */
                                                  M_PI,                                       /* phi_max */
                                                  12,                                          /* num_nodes */
                                                  1,                                          /* poly_order */
                                                  true,                                       /* normal_points_to_the_right */
                                                  false                                       /* is_reference */);
    const auto s_y_8 = p.addStraightBoundarySegment(Vec2D{w+d1,1.4*lm-lm+w+(wTht-w)/2.0-cr}, /* start */
                                                    Vec2D{w+d1,1.4*lm-lm-(wTht-w)/2.0+cr},   /* end */
                                                    12,                                       /* num_nodes */
                                                    1,                                       /* poly_order */
                                                    true,                                    /* normal_points_to_the_right */
                                                    false                                    /* is_reference */);
    const auto s_y_8c = p.addArcedBoundarySegment(cr,                                       /* radius */
                                                  Vec2D{w+d1+cr,1.4*lm-lm-(wTht-w)/2.0+cr}, /* center */
                                                  -M_PI,                                    /* phi_min */
                                                  -M_PI/2.0,                                /* phi_max */
                                                  12,                                        /* num_nodes */
                                                  1,                                        /* poly_order */
                                                  true,                                     /* normal_points_to_the_right */
                                                  false                                     /* is_reference */);
    const auto s_y_9 = p.addStraightBoundarySegment(Vec2D{w+d1+cr,1.4*lm-lm-(wTht-w)/2.0},   /* start */
                                                    Vec2D{w+d1+lTht,1.4*lm-lm-(wTht-w)/2.0}, /* end */
                                                    12,                                       /* num_nodes */
                                                    1,                                       /* poly_order */
                                                    true,                                    /* normal_points_to_the_right */
                                                    false                                    /* is_reference */);
    const auto s_y_10 = p.addStraightBoundarySegment(Vec2D{w+d1+lTht,1.4*lm-lm-(wTht-w)/2.0}, /* start */
                                                     Vec2D{w+d1+lTht,1.4*lm-lm},              /* end */
                                                     12,                                       /* num_nodes */
                                                     1,                                       /* poly_order */
                                                     true,                                    /* normal_points_to_the_right */
                                                     false                                    /* is_reference */);
    const auto s_y_11a = p.addStraightBoundarySegment(Vec2D{w+d1+lTht,1.4*lm-lm}, /* start */
                                                      s_c_2->getEndPoint(),       /* end */
                                                      12,                          /* num_nodes */
                                                      1,                          /* poly_order */
                                                      true,                       /* normal_points_to_the_right */
                                                      false                       /* is_reference */);
    const auto s_y_12 = p.addStraightBoundarySegment(Vec2D{w+d1+lTht+lw+w,1.4*lm-lm},             /* start */
                                                     Vec2D{w+d1+lTht+lw+w,1.4*lm+lTht+d2+lTht+w}, /* end */
                                                     12,                               /* num_nodes */
                                                     1,                               /* poly_order */
                                                     true,                            /* normal_points_to_the_right */
                                                     false                            /* is_reference */);
    const auto s_y_11b = p.addStraightBoundarySegment(s_c_2->getEndPoint(),    /* start */
                                                      s_y_12->getStartPoint(), /* end */
                                                      12,                       /* num_nodes */
                                                      1,                       /* poly_order */
                                                      true,                    /* normal_point_to_the_right */
                                                      false                    /* is_reference */);
    const auto s_y_13 = p.addStraightBoundarySegment(Vec2D{w+d1+lTht+lw+w,1.4*lm+lTht+d2+lTht+w}, /* start */
                                                     Vec2D{0.0,1.4*lm+lTht+d2+lTht+w},            /* end */
                                                     12,                                           /* num_nodes */
                                                     1,                                           /* poly_order */
                                                     true,                                        /* normal_points_to_the_right */
                                                     false                                        /* is_reference */);
    const auto s_y_14 = p.addStraightBoundarySegment(Vec2D{0.0,1.4*lm+lTht+d2+lTht+w}, /* start */
                                                     Vec2D{0.0,1.4*lm+lTht+d2+lTht},   /* end */
                                                     12,                                /* num_nodes */
                                                     1,                                /* poly_order */
                                                     true,                             /* normal_points_to_the_right */
                                                     false                             /* is_reference */);
    const auto s_y_15 = p.addStraightBoundarySegment(Vec2D{0.0,1.4*lm+lTht+d2+lTht},           /* start */
                                                     Vec2D{-(wTht-w)/2.0,1.4*lm+lTht+d2+lTht}, /* end */
                                                     12,                                        /* num_nodes */
                                                     1,                                        /* poly_order */
                                                     true,                                     /* normal_points_to_the_right */
                                                     false                                     /* is_reference */);
    const auto s_y_16 = p.addStraightBoundarySegment(Vec2D{-(wTht-w)/2.0,1.4*lm+lTht+d2+lTht}, /* start */
                                                     Vec2D{-(wTht-w)/2.0,1.4*lm+lTht+d2+cr},   /* end */
                                                     12,                                        /* num_nodes */
                                                     1,                                        /* poly_order */
                                                     true,                                     /* normal_points_to_the_right */
                                                     false                                     /* is_reference */);
    const auto s_y_16c = p.addArcedBoundarySegment(cr,                                        /* radius */
                                                   Vec2D{-(wTht-w)/2.0+cr,1.4*lm+lTht+d2+cr}, /* center */
                                                   -M_PI,                                     /* phi_min */
                                                   -M_PI/2.0,                                 /* phi_max */
                                                   12,                                         /* num_nodes */
                                                   1,                                         /* poly_order */
                                                   true,                                      /* normal_points_to_the_right */
                                                   false                                      /* is_reference */);

    /* Create reference boundaries */
    const auto r1 = p.addStraightBoundarySegment(s_m_2->getEndPoint(),   /* start */
                                                 s_m_8->getStartPoint(), /* end */
                                                 3,                      /* num_nodes */
                                                 1,                      /* poly_order */
                                                 true,                   /* normal_points_to_the_right */
                                                 true                    /* is_reference */);

    /* Create the domains in the problem */
    const auto mover = p.addDomain("mover",{{std::make_shared<DirectedBoundarySegment>(s_m_1,true),
                                             std::make_shared<DirectedBoundarySegment>(s_m_1c,true),
                                             std::make_shared<DirectedBoundarySegment>(s_m_2,true),
                                             std::make_shared<DirectedBoundarySegment>(s_m_2c,false),
                                             std::make_shared<DirectedBoundarySegment>(s_m_3,true),
                                             std::make_shared<DirectedBoundarySegment>(s_m_3c,true),
                                             std::make_shared<DirectedBoundarySegment>(s_m_4,true),
                                             std::make_shared<DirectedBoundarySegment>(s_m_4c,true),
                                             std::make_shared<DirectedBoundarySegment>(s_m_5,true),
                                             std::make_shared<DirectedBoundarySegment>(s_m_5c,true),
                                             std::make_shared<DirectedBoundarySegment>(s_m_6,true),
                                             std::make_shared<DirectedBoundarySegment>(s_m_6c,true),
                                             std::make_shared<DirectedBoundarySegment>(s_m_7,true),
                                             std::make_shared<DirectedBoundarySegment>(s_m_7c,false),
                                             std::make_shared<DirectedBoundarySegment>(s_m_8,true),
                                             std::make_shared<DirectedBoundarySegment>(s_m_8c,true)}},mu_iron,0.0);
    const auto yoke = p.addDomain("yoke",{{std::make_shared<DirectedBoundarySegment>(s_y_1,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_1c,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_2,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_3,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_4a,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_4b,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_5,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_6,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_7,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_7c,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_8,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_8c,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_9,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_10,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_11a,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_11b,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_12,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_13,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_14,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_15,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_16,true),
                                           std::make_shared<DirectedBoundarySegment>(s_y_16c,true)}},mu_iron,0.0);
    const auto coil1 = p.addDomain("coil1",{{std::make_shared<DirectedBoundarySegment>(s_y_4b,false),
                                             std::make_shared<DirectedBoundarySegment>(s_c_5,true),
                                             std::make_shared<DirectedBoundarySegment>(s_y_6,false),
                                             std::make_shared<DirectedBoundarySegment>(s_y_5,false)}},mu_cu,497.0*5.0/ww/lw);
    const auto coil2 = p.addDomain("coil2",{{std::make_shared<DirectedBoundarySegment>(s_c_1,true),
                                             std::make_shared<DirectedBoundarySegment>(s_c_2,true),
                                             std::make_shared<DirectedBoundarySegment>(s_y_11a,false),
                                             std::make_shared<DirectedBoundarySegment>(s_y_10,false)}},mu_cu,-497.0*5.0/ww/lw);
    const auto air = p.addExteriorDomain("air",{{std::make_shared<DirectedBoundarySegment>(s_m_8c,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_8,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_7c,true),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_7,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_6c,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_6,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_5c,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_5,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_4c,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_4,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_3c,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_3,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_2c,true),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_2,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_1c,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_m_1,false)},
                                                {std::make_shared<DirectedBoundarySegment>(s_y_1,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_16c,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_16,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_15,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_14,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_13,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_12,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_11b,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_c_2,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_c_1,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_9,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_8c,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_8,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_7c,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_7,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_c_5,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_4a,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_3,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_2,false),
                                                 std::make_shared<DirectedBoundarySegment>(s_y_1c,false)}},mu_air);

    /* Create reference domains */
    p.addReferenceDomain(mover,{{std::make_shared<DirectedBoundarySegment>(s_m_1,true),
                                 std::make_shared<DirectedBoundarySegment>(s_m_1c,true),
                                 std::make_shared<DirectedBoundarySegment>(s_m_2,true),
                                 std::make_shared<DirectedBoundarySegment>(r1,true),
                                 std::make_shared<DirectedBoundarySegment>(s_m_8,true),
                                 std::make_shared<DirectedBoundarySegment>(s_m_8c,true)}},{3}/*type*/);
    p.addReferenceDomain(mover,{{std::make_shared<DirectedBoundarySegment>(s_m_2c,false),
                                 std::make_shared<DirectedBoundarySegment>(s_m_3,true),
                                 std::make_shared<DirectedBoundarySegment>(s_m_3c,true),
                                 std::make_shared<DirectedBoundarySegment>(s_m_4,true),
                                 std::make_shared<DirectedBoundarySegment>(s_m_4c,true),
                                 std::make_shared<DirectedBoundarySegment>(s_m_5,true),
                                 std::make_shared<DirectedBoundarySegment>(s_m_5c,true),
                                 std::make_shared<DirectedBoundarySegment>(s_m_6,true),
                                 std::make_shared<DirectedBoundarySegment>(s_m_6c,true),
                                 std::make_shared<DirectedBoundarySegment>(s_m_7,true),
                                 std::make_shared<DirectedBoundarySegment>(s_m_7c,false),
                                 std::make_shared<DirectedBoundarySegment>(r1,false)}},{3}/*type*/);

    /* Solve the problem */
    //p.solveAndStore("results/mover_acc");
    p.solveAndStore("results/mover_v_acc");
    //p.loadSolution("results/mover_acc");
    //p.solveAndStore("results/mover_v_acc_max");

    /* Plot the boundaries in the problem */
    sanji::figure("Mover simulation");
    for (const auto& boundary_segment_plotting_data : p.getBoundarySegmentsPlottingData(0.001/4.0, /* max_length_per_segment */
                                                                                        true       /* show_reference_boundaries */))
        sanji::plot(*boundary_segment_plotting_data.x,*boundary_segment_plotting_data.y,boundary_segment_plotting_data.style);

    /* Plot the normals in the problem */
    if (plot_normals)
        for (const auto& boundary_segment_plotting_vector_data : p.getNormals(1.0e-4, /* max_length_per_segment */
                                                                              1.0e-4 /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);

    //Plot the magnetic flux density */
    if (plot_field_data) {
        // Compute the field data
        //const auto field_data = p.getFieldData(2.0e6,load_field_data,store_field_data,"results/dc_motor");
        const auto field_data = p.getFieldData(2.0e6,load_field_data,store_field_data,"results/mover");

        double min =  std::numeric_limits<double>::max();
        double max = -std::numeric_limits<double>::max();
        for (const auto& field_data : field_data)
            for (uint i = 0; i < field_data.x->rows(); ++i) {
                const double strength = Vec2D{(*field_data.u)(i),(*field_data.v)(i)}.Norm2();
                if (strength < min) min = strength;
                if (strength > max) max = strength;
            }
        for (const auto& field_data : field_data)
            sanji::quiver(*field_data.x,
                          *field_data.y,
                          *field_data.u,
                          //*field_data.v,{{"arrow_length",0.0006},{"colormap",TURBO},{"min",min},{"max",max}},{"use_colormap","center_arrows"});
                          *field_data.v,{{"arrow_length",0.0006},{"colormap",TURBO},{"min",min},{"max",max}},{"use_colormap","center_arrows","use_logscale"});

        /* Print statistics */
        std::cout << "The minimum is " << min << std::endl;
        std::cout << "The maximum is " << max << std::endl;
    }

    /* Plot cosmetics */
    sanji::setPlotBackgroundColor(GRAY);
    sanji::setAxesRatio("equal");

    /* Compute the force applied onto the mover */
    std::cout << std::setprecision(15);
    if (compute_force) {
        Vec2D force{0.0,0.0};

        // Define a GSL function and a GSL workspace
        gsl_function               gsl_fun;
        gsl_integration_workspace* gsl_workspace = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);

        // Create a wrapper to simplify integration and allocate memory for the integrand parameters
        const auto quad = [&gsl_fun,gsl_workspace]()->double {
            double result,error;
            gsl_integration_qag(&gsl_fun, /* Function to integrate */
                                0.0,      /* Lower integration limit */
                                1.0,      /* Upper integration limit */
                                INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                                INTEGRAL_RELATIVE_ERROR_LIMIT,
                                MAX_NUM_SUBINTERVALS,
                                INTEGRATION_KEY,
                                gsl_workspace,&result,&error);
            return result;
        };
        const double** params = new const double*[1];
        gsl_fun.params        = params;

        // Create a lambda to integrate along a boundary segment
        const auto getBoundaryIntegral = [&quad,&gsl_fun,&params,&mover,&p](const uint dir_bnd_seg_idx)->const Vec2D {
            // Preliminary definitions
            const auto& dir_bnd_seg_ptr = mover->getDirectedBoundarySegmentPointers()[0][dir_bnd_seg_idx];
            Vec2D result{0.0,0.0};

            // Compute the contributions from all subsegments
            for (uint subseg_idx = 0u; subseg_idx < dir_bnd_seg_ptr->getNumSubsegments(); ++subseg_idx) {
                // Define the integrand
                uint dim_idx;
                const auto integrand = [dpsi=dir_bnd_seg_ptr->getdpsii2Norm(subseg_idx),&mover,&dir_bnd_seg_ptr,dir_bnd_seg_idx,subseg_idx,&p,&dim_idx](const double t)->double {
                    // Compute the flux density and the normal
                    const auto  [B,x,nu] = p.evaluateInteriorSolution(mover,0/*loop_idx*/,dir_bnd_seg_idx,(subseg_idx+t)/dir_bnd_seg_ptr->getNumSubsegments());
                    const double mu      = mover->getmu();
                    const Vec2D  H       = B/mu;

                    // Compute the differential force
                    // From: Comparison of main magnetic force computation methods for noise
                    // and vibration assessment in electrical machines, Eq. (9)
                    const Vec2D dF = (B.dot(nu))*H - mu/2.0*H.Norm2Squared()*nu;

                    return (dim_idx==0u ? dF.x : dF.y)*dpsi(t);
                };
                params[0] = reinterpret_cast<const double*>(&integrand);
                gsl_fun.function = [](const double t, void* params)->double {
                    const decltype(integrand)& integrand_ = *reinterpret_cast<const decltype(integrand)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));

                    return integrand_(t);
                };

                // Integrate along the subsegment
                dim_idx   = 0u;
                result.x += quad();
                dim_idx   = 1u;
                result.y += quad();
            }

            return result;
        };

        // Integrate along the four boundary segments
        for (uint dir_bnd_seg_idx = 0u; dir_bnd_seg_idx < mover->getDirectedBoundarySegmentPointers()[0].size(); ++dir_bnd_seg_idx) {
            const Vec2D c = getBoundaryIntegral(dir_bnd_seg_idx);
            std::cout << "c=" << c.toString() << std::endl;
            force += c;
        }

        std::cout << "The total force acting on the mover is " << (force*d).toString() << "." << std::endl;

        // Free memory
        delete[] params;
        gsl_integration_workspace_free(gsl_workspace);
    }

    // Visualize the flux coming from the yoke into the mover
    const auto&    domain     = air;
    const uint     N          = 200u;
    Eigen::ArrayXd B_norm_bnd = VectorXd::LinSpaced(N,0u,N-1u);
    directed_boundary_segment_ptr dir_bnd_seg_ptr;
    for (const auto& dir_bnd_seg_ptrs_ : domain->getDirectedBoundarySegmentPointers())
        for (const auto& dir_bnd_seg_ptr_ : dir_bnd_seg_ptrs_)
            if (s_m_5.get() == &**dir_bnd_seg_ptr_)
            //if (s_m_6.get() == &**dir_bnd_seg_ptr_)
                dir_bnd_seg_ptr = dir_bnd_seg_ptr_;
    std::for_each(B_norm_bnd.cbegin(),B_norm_bnd.cend(),[N,&B_norm_bnd,&p,&domain,&dir_bnd_seg_ptr](const int n) {
        // Compute the solution on the boundary
        auto [loop_idx,dir_bnd_seg_idx] = domain->getDirectedBoundarySegmentIndex((**dir_bnd_seg_ptr).getBoundarySegmentIndex());
        auto [B,x,nu]                   = p.evaluateBoundarySolution(domain,loop_idx,dir_bnd_seg_idx,static_cast<double>(n)/N);
        B_norm_bnd(n)                   = Vec2D{nu.y,-nu.x}.dot(B);
    });
    sanji::figure("Magnetic flux in normal direction along boundary");
    sanji::plot(VectorXd::LinSpaced(N,0,N-1)*2.0*M_PI/N,B_norm_bnd,{{"color",RED}});

    // Execute the application
    app.exec();
}
