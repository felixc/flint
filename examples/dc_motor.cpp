#include <cmath>
#include <iostream>
#include <Eigen/Dense>
#include <chrono>
#include <functional>
#include <gsl/gsl_integration.h>
#include <thread>
#include "cubature.h"
#include "Sanji.hpp"
#include "ProblemWrapper.hpp"

#define INTEGRAL_ABSOLUTE_ERROR_LIMIT 0
#define INTEGRAL_RELATIVE_ERROR_LIMIT 5e-3
#define INTEGRATION_KEY               GSL_INTEG_GAUSS61
#define MAX_NUM_SUBINTERVALS          2000

using namespace sanji::colors;

/* Type definitions */
template <typename T>
using vector = std::vector<T>;

/* Parameters */
// Program parameters
constexpr bool plot_normals          = false;
constexpr bool plot_field_data       = true;
constexpr bool load_field_data       = false;
constexpr bool store_field_data      = false;
constexpr bool compute_lorentz_force = true;
constexpr bool compute_tang_ders     = false;
constexpr bool compute_bnd_sol       = false;

// Permeabilities
static constexpr double mu0       = 4.0*M_PI*1.0e-7;
static constexpr double mu_air    = 1.00000037*mu0;
static constexpr double mu_iron   = 2500.0*mu0;
static constexpr double mu_cu     = 0.999994*mu0;

// Geometric parameters
constexpr double da                 = 0.07;                          // [m]
constexpr double rotor_outer_radius = da/2.0;                        // [m]
constexpr double yoke_inner_radius  = (da*(1007-310))/(613-252)/2.0; // [m]
constexpr double yoke_outer_radius  = (da*(824-43))/(613-252)/2.0;   // [m]
constexpr double t1                 = (da*(715-599))/(613-252);      // [m]
constexpr double t2                 = (da*(842-476))/(613-252);      // [m]
constexpr double t3                 = (da*242.639)/(613-252);        // [m]
constexpr double delta              = 0.0025;                        // [m]
constexpr double arc_length_rotor   = 2.0*M_PI/12;                   // rad
constexpr double coil_width         = 0.005;                         // [m]
constexpr double coil_length        = 0.015;                         // [m]
constexpr double alpha              = 0.64;
constexpr double tau_p              = M_PI*da/2.0;                   // [m]
constexpr double b_p                = alpha*tau_p;                   // [m]
constexpr uint   num_rotor_slots    = 12;
constexpr double slot_depth         = 0.003;                         // [m]
constexpr uint   Nsp                = 10;
constexpr double Ia                 = 50.0;                          // [A]
constexpr uint   Nf                 = 300;
constexpr double If                 = 5.57;                          // [A]

int cubature_integrand(unsigned ndim, const double *x, void *fdata,
    unsigned fdim, double *fval) {
    const auto& fun = *reinterpret_cast<std::function<double(const double,const double)>*>(fdata);
    fval[0]         = fun(x[0],x[1]);
    return 0;
}

int main(int argc, char* argv[]) {
    // Initialize this as a Qt application
    QApplication app(argc, argv);
    sanji::init();

    /* Create a problem instance */
    ProblemWrapper p;

    /* Create the boundary segments */
    const double phi1 = std::asin(t1/2.0/yoke_inner_radius);
    const double phi2 = alpha*M_PI/2.0;
    const double phi3 = std::asin(t1/2.0/t3);
    const auto yoke_inner_boundary_1_1 = p.addArcedBoundarySegment(yoke_inner_radius, /* radius */
                                                                   Vec2D{0.0,0.0},    /* center */
                                                                   -M_PI/2.0+phi1,    /* phi_min */
                                                                   -M_PI/2.0+phi2,    /* phi_max */
                                                                   12,                /* num_nodes */
                                                                   1,                 /* boundary_solution_polynomial_order */
                                                                   true               /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_1_2 = p.addArcedBoundarySegment(yoke_inner_radius, /* radius */
                                                                   Vec2D{0.0,0.0},    /* center */
                                                                   -M_PI/2.0+phi2,    /* phi_min */
                                                                    M_PI/2.0-phi2,    /* phi_max */
                                                                   4,                 /* num_nodes */
                                                                   1,                 /* boundary_solution_polynomial_order */
                                                                   true               /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_1_3 = p.addArcedBoundarySegment(yoke_inner_radius, /* radius */
                                                                   Vec2D{0.0,0.0},    /* center */
                                                                   M_PI/2.0-phi2,     /* phi_min */
                                                                   M_PI/2.0-phi1,     /* phi_max */
                                                                   4,                 /* num_nodes */
                                                                   1,                 /* boundary_solution_polynomial_order */
                                                                   true               /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_2_1 = p.addArcedBoundarySegment(yoke_inner_radius, /* radius */
                                                                   Vec2D{0.0,0.0},    /* center */
                                                                   M_PI/2.0+phi1,     /* phi_min */
                                                                   M_PI/2.0+phi2,     /* phi_max */
                                                                   4,                 /* num_nodes */
                                                                   1,                 /* boundary_solution_polynomial_order */
                                                                   true               /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_2_2 = p.addArcedBoundarySegment(yoke_inner_radius, /* radius */
                                                                   Vec2D{0.0,0.0},    /* center */
                                                                       M_PI/2.0+phi2, /* phi_min */
                                                                   3.0*M_PI/2.0-phi2, /* phi_max */
                                                                   4,                 /* num_nodes */
                                                                   1,                 /* boundary_solution_polynomial_order */
                                                                   true               /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_2_3 = p.addArcedBoundarySegment(yoke_inner_radius, /* radius */
                                                                   Vec2D{0.0,0.0},    /* center */
                                                                   3.0*M_PI/2.0-phi2, /* phi_min */
                                                                   3.0*M_PI/2.0-phi1, /* phi_max */
                                                                   12,                /* num_nodes */
                                                                   1,                 /* boundary_solution_polynomial_order */
                                                                   true               /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_3 = p.addArcedBoundarySegment(da/2.0+delta,   /* radius */
                                                                 Vec2D{0.0,0.0}, /* center */
                                                                 M_PI/2.0-phi2,  /* phi_min */
                                                                 M_PI/2.0+phi2,  /* phi_max */
                                                                 12,             /* num_nodes */
                                                                 1,              /* boundary_solution_polynomial_order */
                                                                 true            /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_3_1 = p.addArcedBoundarySegment(da/2.0+delta,   /* radius */
                                                                   Vec2D{0.0,0.0}, /* center */
                                                                   M_PI/2.0-phi2,  /* phi_min */
                                                                   M_PI/2.0-phi3,  /* phi_max */
                                                                   12,             /* num_nodes */
                                                                   1,              /* boundary_solution_polynomial_order */
                                                                   true,           /* normal_points_to_the_right */
                                                                   true            /* is_reference */);
    const auto yoke_inner_boundary_3_2 = p.addArcedBoundarySegment(da/2.0+delta,   /* radius */
                                                                   Vec2D{0.0,0.0}, /* center */
                                                                   M_PI/2.0-phi3,  /* phi_min */
                                                                   M_PI/2.0+phi3,  /* phi_max */
                                                                   12,             /* num_nodes */
                                                                   1,              /* boundary_solution_polynomial_order */
                                                                   true,           /* normal_points_to_the_right */
                                                                   true            /* is_reference */);
    const auto yoke_inner_boundary_3_3 = p.addArcedBoundarySegment(da/2.0+delta,   /* radius */
                                                                   Vec2D{0.0,0.0}, /* center */
                                                                   M_PI/2.0+phi3,  /* phi_min */
                                                                   M_PI/2.0+phi2,  /* phi_max */
                                                                   12,             /* num_nodes */
                                                                   1,              /* boundary_solution_polynomial_order */
                                                                   true,           /* normal_points_to_the_right */
                                                                   true            /* is_reference */);
    const auto yoke_inner_boundary_4 = p.addArcedBoundarySegment(da/2.0+delta,      /* radius */
                                                                 Vec2D{0.0,0.0},    /* center */
                                                                 3.0*M_PI/2.0-phi2, /* phi_min */
                                                                 3.0*M_PI/2.0+phi2, /* phi_max */
                                                                 12,                /* num_nodes */
                                                                 1,                 /* boundary_solution_polynomial_order */
                                                                 true               /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_4_1 = p.addArcedBoundarySegment(da/2.0+delta,      /* radius */
                                                                   Vec2D{0.0,0.0},    /* center */
                                                                   3.0*M_PI/2.0-phi2, /* phi_min */
                                                                   3.0*M_PI/2.0-phi3, /* phi_max */
                                                                   12,                /* num_nodes */
                                                                   1,                 /* boundary_solution_polynomial_order */
                                                                   true,              /* normal_points_to_the_right */
                                                                   true               /* is_reference */);
    const auto yoke_inner_boundary_4_2 = p.addArcedBoundarySegment(da/2.0+delta,      /* radius */
                                                                   Vec2D{0.0,0.0},    /* center */
                                                                   3.0*M_PI/2.0-phi3, /* phi_min */
                                                                   3.0*M_PI/2.0+phi3, /* phi_max */
                                                                   12,                /* num_nodes */
                                                                   1,                 /* boundary_solution_polynomial_order */
                                                                   true,              /* normal_points_to_the_right */
                                                                   true               /* is_reference */);
    const auto yoke_inner_boundary_4_3 = p.addArcedBoundarySegment(da/2.0+delta,      /* radius */
                                                                   Vec2D{0.0,0.0},    /* center */
                                                                   3.0*M_PI/2.0+phi3, /* phi_min */
                                                                   3.0*M_PI/2.0+phi2, /* phi_max */
                                                                   12,                /* num_nodes */
                                                                   1,                 /* boundary_solution_polynomial_order */
                                                                   true,              /* normal_points_to_the_right */
                                                                   true               /* is_reference */);
    const auto yoke_inner_boundary_5 = p.addArcedBoundarySegment(t3,             /* radius */
                                                                 Vec2D{0.0,0.0}, /* center */
                                                                 M_PI/2.0-phi2,  /* phi_min */
                                                                 M_PI/2.0-phi3,  /* phi_max */
                                                                 6,              /* num_nodes */
                                                                 1,              /* boundary_solution_polynomial_order */
                                                                 true            /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_6 = p.addArcedBoundarySegment(t3,             /* radius */
                                                                 Vec2D{0.0,0.0}, /* center */
                                                                 M_PI/2.0+phi3,  /* phi_min */
                                                                 M_PI/2.0+phi2,  /* phi_max */
                                                                 6,              /* num_nodes */
                                                                 1,              /* boundary_solution_polynomial_order */
                                                                 true            /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_7 = p.addArcedBoundarySegment(t3,                /* radius */
                                                                 Vec2D{0.0,0.0},    /* center */
                                                                 3.0*M_PI/2.0-phi2, /* phi_min */
                                                                 3.0*M_PI/2.0-phi3, /* phi_max */
                                                                 6,                 /* num_nodes */
                                                                 1,                 /* boundary_solution_polynomial_order */
                                                                 true               /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_8 = p.addArcedBoundarySegment(t3,                /* radius */
                                                                 Vec2D{0.0,0.0},    /* center */
                                                                 3.0*M_PI/2.0+phi3, /* phi_min */
                                                                 3.0*M_PI/2.0+phi2, /* phi_max */
                                                                 6,                 /* num_nodes */
                                                                 1,                 /* boundary_solution_polynomial_order */
                                                                 true               /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_9 = p.addStraightBoundarySegment(yoke_inner_boundary_1_3->getEndPoint(), /* start */
                                                                    yoke_inner_boundary_5->getEndPoint(),   /* end */
                                                                    4,                                      /* num_nodes */
                                                                    1,                                      /* poly_order */
                                                                    true                                    /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_10 = p.addStraightBoundarySegment(yoke_inner_boundary_6->getStartPoint(),   /* start */
                                                                     yoke_inner_boundary_2_1->getStartPoint(), /* end */
                                                                     4,                                        /* num_nodes */
                                                                     1,                                        /* poly_order */
                                                                     true                                      /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_11 = p.addStraightBoundarySegment(yoke_inner_boundary_2_3->getEndPoint(), /* start */
                                                                     yoke_inner_boundary_7->getEndPoint(),   /* end */
                                                                     4,                                      /* num_nodes */
                                                                     1,                                      /* poly_order */
                                                                     true                                    /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_12 = p.addStraightBoundarySegment(yoke_inner_boundary_8->getStartPoint(),   /* start */
                                                                     yoke_inner_boundary_1_1->getStartPoint(), /* end */
                                                                     4,                                         /* num_nodes */
                                                                     1,                                         /* poly_order */
                                                                     true                                       /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_13 = p.addStraightBoundarySegment(yoke_inner_boundary_5->getStartPoint(), /* start */
                                                                     yoke_inner_boundary_3->getStartPoint(), /* end */
                                                                     4,                                      /* num_nodes */
                                                                     1,                                      /* poly_order */
                                                                     true                                    /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_14 = p.addStraightBoundarySegment(yoke_inner_boundary_3->getEndPoint(), /* start */
                                                                     yoke_inner_boundary_6->getEndPoint(), /* end */
                                                                     4,                                    /* num_nodes */
                                                                     1,                                    /* poly_order */
                                                                     true                                  /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_15 = p.addStraightBoundarySegment(yoke_inner_boundary_7->getStartPoint(), /* start */
                                                                     yoke_inner_boundary_4->getStartPoint(), /* end */
                                                                     4,                                      /* num_nodes */
                                                                     1,                                      /* poly_order */
                                                                     true                                    /* normal_points_to_the_right */);
    const auto yoke_inner_boundary_16 = p.addStraightBoundarySegment(yoke_inner_boundary_4->getEndPoint(), /* start */
                                                                     yoke_inner_boundary_8->getEndPoint(), /* end */
                                                                     4,                                    /* num_nodes */
                                                                     1,                                    /* poly_order */
                                                                     true                                  /* normal_points_to_the_right */);
    const auto yoke_outer_boundary = p.addCircularBoundarySegment(yoke_outer_radius, /* radius */
                                                                  Vec2D{0.0,0.0},    /* center */
                                                                  12,                /* num_nodes */
                                                                  1,                 /* boundary_solution_polynomial_order */
                                                                  true               /* normal_points_to_the_right */);
    const auto coil_boundary_1 = p.addStraightBoundarySegment(yoke_inner_boundary_1_2->getEndPoint(), /* start */
                                                              yoke_inner_boundary_5->getStartPoint(), /* end */
                                                              4,                                      /* num_nodes */
                                                              1,                                      /* poly_order */
                                                              true                                    /* normal_points_to_the_right */);
    const auto coil_boundary_2 = p.addStraightBoundarySegment(yoke_inner_boundary_2_1->getEndPoint(), /* start */
                                                              yoke_inner_boundary_6->getEndPoint(),   /* end */
                                                              4,                                      /* num_nodes */
                                                              1,                                      /* poly_order */
                                                              true                                    /* normal_points_to_the_right */);
    const auto coil_boundary_3 = p.addStraightBoundarySegment(yoke_inner_boundary_2_2->getEndPoint(), /* start */
                                                              yoke_inner_boundary_7->getStartPoint(), /* end */
                                                              4,                                      /* num_nodes */
                                                              1,                                      /* poly_order */
                                                              true                                    /* normal_points_to_the_right */);
    const auto coil_boundary_4 = p.addStraightBoundarySegment(yoke_inner_boundary_1_1->getEndPoint(), /* start */
                                                              yoke_inner_boundary_8->getEndPoint(),   /* end */
                                                              4,                                      /* num_nodes */
                                                              1,                                      /* poly_order */
                                                              true                                    /* normal_points_to_the_right */);
    vector<boundary_segment_ptr> all_slots_boundary_segments_1(num_rotor_slots);
    vector<boundary_segment_ptr> all_slots_boundary_segments_2(num_rotor_slots);
    vector<boundary_segment_ptr> all_slots_boundary_segments_3(num_rotor_slots);
    vector<boundary_segment_ptr> all_slots_boundary_segments_4(num_rotor_slots);
    for (uint slot_idx = 0; slot_idx < num_rotor_slots; ++slot_idx) {
        all_slots_boundary_segments_1[slot_idx] = p.addArcedBoundarySegment(rotor_outer_radius,                        /* radius */
                                                                            Vec2D{0.0,0.0},                            /* center */
                                                                            2.0*M_PI/num_rotor_slots*(slot_idx-7.0/8), /* phi_min */
                                                                            2.0*M_PI/num_rotor_slots*(slot_idx-1.0/8), /* phi_max */
                                                                            8,                                         /* num_nodes */
                                                                            1,                                         /* boundary_solution_polynomial_order */
                                                                            true                                       /* normal_points_to_the_right */);
        all_slots_boundary_segments_3[slot_idx] = p.addArcedBoundarySegment(rotor_outer_radius-slot_depth,                                                                /* radius */
                                                                            Vec2D{0.0,0.0},                                                                               /* center */
                                                                            2.0*M_PI/num_rotor_slots*(slot_idx-rotor_outer_radius/(rotor_outer_radius-slot_depth)*1.0/8), /* phi_min */
                                                                            2.0*M_PI/num_rotor_slots*(slot_idx+rotor_outer_radius/(rotor_outer_radius-slot_depth)*1.0/8), /* phi_max */
                                                                            8,                   /* num_nodes */
                                                                            1,                   /* boundary_solution_polynomial_order */
                                                                            true                 /* normal_points_to_the_right */);
    }
    for (uint slot_idx = 0; slot_idx < num_rotor_slots; ++slot_idx) {
        all_slots_boundary_segments_2[slot_idx] = p.addStraightBoundarySegment(all_slots_boundary_segments_1[slot_idx]->getEndPoint(),   /* start */
                                                                               all_slots_boundary_segments_3[slot_idx]->getStartPoint(), /* end */
                                                                               8,                                                        /* num_nodes */
                                                                               1,                                                        /* boundary_solution_polynomial_order */
                                                                               true                                                      /* normal_points_to_the_right */);
        all_slots_boundary_segments_4[slot_idx] = p.addStraightBoundarySegment(all_slots_boundary_segments_3[slot_idx]->getEndPoint(),                       /* start */
                                                                               all_slots_boundary_segments_1[(slot_idx+1)%num_rotor_slots]->getStartPoint(), /* end */
                                                                               8,                                                                            /* num_nodes */
                                                                               1,                                                                            /* boundary_solution_polynomial_order */
                                                                               true                                                                          /* normal_points_to_the_right */);
    }
    vector<boundary_segment_ptr> all_slots_boundary_segments_5(num_rotor_slots);
    for (uint slot_idx = 0; slot_idx < num_rotor_slots; ++slot_idx) {
        all_slots_boundary_segments_5[slot_idx] = p.addStraightBoundarySegment(all_slots_boundary_segments_1[slot_idx]->getEndPoint(),                       /* start */
                                                                               all_slots_boundary_segments_1[(slot_idx+1)%num_rotor_slots]->getStartPoint(), /* end */
                                                                               8,                                                                            /* num_nodes */
                                                                               1,                                                                            /* boundary_solution_polynomial_order */
                                                                               true                                                                          /* normal_points_to_the_right */);
    }

    /* Define the reference boundaries */
    const auto r1 = p.addCircularBoundarySegment(yoke_inner_radius, /* radius */
                                                 Vec2D{0.0,0.0},    /* center */
                                                 12,                /* num_nodes */
                                                 1,                 /* boundary_solution_polynomial_order */
                                                 true,              /* normal_points_to_the_right */
                                                 true               /* is_reference */);
    const auto r2 = p.addArcedBoundarySegment(yoke_inner_radius, /* radius */
                                              Vec2D{0.0,0.0},    /* center */
                                              M_PI/2.0-phi1,     /* phi_min */
                                              M_PI/2.0+phi1,     /* phi_max */
                                              12,                /* num_nodes */
                                              1,                 /* boundary_solution_polynomial_order */
                                              true,              /* normal_points_to_the_right */
                                              true               /* is_reference */);
    const auto r3 = p.addArcedBoundarySegment(yoke_inner_radius, /* radius */
                                              Vec2D{0.0,0.0},    /* center */
                                              -M_PI/2.0-phi1,    /* phi_min */
                                              -M_PI/2.0+phi1,    /* phi_max */
                                              12,                /* num_nodes */
                                              1,                 /* boundary_solution_polynomial_order */
                                              true,              /* normal_points_to_the_right */
                                              true               /* is_reference */);
    const auto r4 = p.addArcedBoundarySegment(t3,             /* radius */
                                              Vec2D{0.0,0.0}, /* center */
                                              M_PI/2.0-phi3,  /* phi_min */
                                              M_PI/2.0+phi3,  /* phi_max */
                                              12,             /* num_nodes */
                                              1,              /* boundary_solution_polynomial_order */
                                              true,           /* normal_points_to_the_right */
                                              true            /* is_reference */);
    const auto r5 = p.addArcedBoundarySegment(t3,                /* radius */
                                              Vec2D{0.0,0.0},    /* center */
                                              3.0*M_PI/2.0-phi3, /* phi_min */
                                              3.0*M_PI/2.0+phi3, /* phi_max */
                                              12,                /* num_nodes */
                                              1,                 /* boundary_solution_polynomial_order */
                                              true,              /* normal_points_to_the_right */
                                              true               /* is_reference */);
    const auto r6 = p.addCircularBoundarySegment(da/2.0,         /* radius */
                                                 Vec2D{0.0,0.0}, /* center */
                                                 12,             /* num_nodes */
                                                 1,              /* boundary_solution_polynomial_order */
                                                 true,           /* normal_points_to_the_right */
                                                 true            /* is_reference */);
    const auto r7 = p.addCircularBoundarySegment(da/2.0+delta,   /* radius */
                                                 Vec2D{0.0,0.0}, /* center */
                                                 12,             /* num_nodes */
                                                 1,              /* boundary_solution_polynomial_order */
                                                 true,           /* normal_points_to_the_right */
                                                 true            /* is_reference */);
    const auto r8 = p.addStraightBoundarySegment(yoke_inner_boundary_5->getEndPoint(),   /* start */
                                                 yoke_inner_boundary_3_1->getEndPoint(), /* end */
                                                 4,                                      /* num_nodes */
                                                 1,                                      /* boundary_solution_polynomial_order */
                                                 true,                                   /* normal_points_to_the_right */
                                                 true                                    /* is_reference */);
    const auto r9 = p.addStraightBoundarySegment(yoke_inner_boundary_6->getStartPoint(),   /* start */
                                                 yoke_inner_boundary_3_3->getStartPoint(), /* end */
                                                 4,                                        /* num_nodes */
                                                 1,                                        /* boundary_solution_polynomial_order */
                                                 true,                                     /* normal_points_to_the_right */
                                                 true                                      /* is_reference */);


    const auto r10 = p.addStraightBoundarySegment(yoke_inner_boundary_7->getEndPoint(),   /* start */
                                                  yoke_inner_boundary_4_1->getEndPoint(), /* end */
                                                  4,                                      /* num_nodes */
                                                  1,                                      /* boundary_solution_polynomial_order */
                                                  true,                                   /* normal_points_to_the_right */
                                                  true                                    /* is_reference */);
    const auto r11 = p.addStraightBoundarySegment(yoke_inner_boundary_8->getStartPoint(), /* start */
                                                  yoke_inner_boundary_4_2->getEndPoint(), /* end */
                                                  4,                                      /* num_nodes */
                                                  1,                                      /* boundary_solution_polynomial_order */
                                                  true,                                   /* normal_points_to_the_right */
                                                  true                                    /* is_reference */);

    vector<boundary_segment_ptr> reference_boundaries_1(num_rotor_slots);
    for (uint slot_idx = 0; slot_idx < num_rotor_slots; ++slot_idx) {
        reference_boundaries_1[slot_idx] = p.addStraightBoundarySegment(all_slots_boundary_segments_3[(slot_idx-1+num_rotor_slots)%num_rotor_slots]->getEndPoint(),                       /* start */
                                                                        all_slots_boundary_segments_3[slot_idx]->getStartPoint(), /* end */
                                                                        4,                                                                           /* num_nodes */
                                                                        1,                                                                           /* poly_order */
                                                                        true,                                                                        /* normal_points_to_the_right */
                                                                        true                                                                         /* is_reference */);

                                                                        
    }

    /* Create the domains in the problem */
    // Exterior
    p.addExteriorDomain("outer_air",{{std::make_shared<DirectedBoundarySegment>(yoke_outer_boundary,false)}},mu_air);

    // Yoke
    const auto yoke = p.addDomain("yoke",{{std::make_shared<DirectedBoundarySegment>(yoke_outer_boundary,true)},
                                          {std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_1_1,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_12,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_8,true),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_16,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_4,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_15,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_7,true),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_11,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_2_3,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_2_2,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_2_1,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_10,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_6,true),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_14,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_3,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_13,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_5,true),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_9,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_1_3,false),
                                           std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_1_2,false)}},mu_iron,0.0 /* J */);

    // Inner air
    vector<directed_boundary_segment_ptr> dbsps;
    for (int slot_idx = num_rotor_slots-1; slot_idx >= 0; --slot_idx) {
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_5[slot_idx],false));
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_1[slot_idx],false));
    }
    const auto inner_air = p.addDomain("inner_air",{dbsps,
                                                   {std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_1_2,true),
                                                    std::make_shared<DirectedBoundarySegment>(coil_boundary_1,true),
                                                    std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_13,true),
                                                    std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_3,true),
                                                    std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_14,true),
                                                    std::make_shared<DirectedBoundarySegment>(coil_boundary_2,false),
                                                    std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_2_2,true),
                                                    std::make_shared<DirectedBoundarySegment>(coil_boundary_3,true),
                                                    std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_15,true),
                                                    std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_4,true),
                                                    std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_16,true),
                                                    std::make_shared<DirectedBoundarySegment>(coil_boundary_4,false)}},mu_air,0.0 /* J */);

    // Coils
    double J_f;
    {
        const double a1 = (phi2/2.0/M_PI)*M_PI*yoke_inner_radius*yoke_inner_radius;
        const double a2 = (phi2/2.0/M_PI)*M_PI*t3*t3;
        J_f = Nf*If/(a1-a2-(yoke_inner_radius-t3)*t1/2);
    };
    p.addDomain("excitation_coil_0",{{std::make_shared<DirectedBoundarySegment>(coil_boundary_1,false),
                                      std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_1_3,true),
                                      std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_9,true),
                                      std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_5,false)}},mu_cu, J_f /* J */, 3/* type */);
    p.addDomain("excitation_coil_1",{{std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_2_1,true),
                                      std::make_shared<DirectedBoundarySegment>(coil_boundary_2,true),
                                                        std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_6,false),
                                      std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_10,true)}},mu_cu, -J_f /* J */, 3/* type */);
    p.addDomain("excitation_coil_2",{{std::make_shared<DirectedBoundarySegment>(coil_boundary_3,false),
                                      std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_2_3,true),
                                      std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_11,true),
                                      std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_7,false)}},mu_cu, -J_f /* J */, 3/* type */);
    p.addDomain("excitation_coil_3",{{std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_1_1,true),
                                      std::make_shared<DirectedBoundarySegment>(coil_boundary_4,true),
                                      std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_8,false),
                                      std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_12,true)}},mu_cu, J_f /* J */, 3/* type */);

    // Rotor coils
    vector<domain_ptr> rotor_slots;
    for (uint slot_idx = 0; slot_idx < num_rotor_slots; ++slot_idx) {
        const double slot_width  = (all_slots_boundary_segments_3[slot_idx]->getEndPoint()-all_slots_boundary_segments_3[slot_idx]->getStartPoint()).Norm2();
        const double slot_length = (all_slots_boundary_segments_2[slot_idx]->getEndPoint()-all_slots_boundary_segments_2[slot_idx]->getStartPoint()).Norm2();
        const double slot_angle  = 2.0*M_PI/num_rotor_slots*slot_idx;
        double J;
        if (slot_angle > 0 && slot_angle < M_PI) {
            J =  2*static_cast<int>(Nsp)*Ia/slot_width/slot_length;
        } else if (slot_angle > M_PI) {
            J = -2*static_cast<int>(Nsp)*Ia/slot_width/slot_length;
        } else {
            J = 0.0;
        }
        rotor_slots.push_back(p.addDomain("rotor_slot_"+std::to_string(slot_idx),{{std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_5[slot_idx],true),
                                                                                   std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_4[slot_idx],false),
                                                                                   std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_3[slot_idx],false),
                                                                                   std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_2[slot_idx],false)}},
                                                                                  mu_cu, J/* J */ ,3/*type */));
    }

    // Rotor
    dbsps.clear();
    for (uint slot_idx = 0; slot_idx < num_rotor_slots; ++slot_idx) {
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_1[slot_idx],true));
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_2[slot_idx],true));
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_3[slot_idx],true));
        dbsps.push_back(std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_4[slot_idx],true));
    }
    const auto rotor = p.addDomain("rotor",{dbsps}, mu_iron, 0.0/* J */);

    /* Create reference domains */
    p.addReferenceDomain(yoke, {{std::make_shared<DirectedBoundarySegment>(yoke_outer_boundary,true)},
                                {std::make_shared<DirectedBoundarySegment>(r1,false)}},{1/*type*/});
    p.addReferenceDomain(yoke, {{std::make_shared<DirectedBoundarySegment>(r2,true),
                                 std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_10,false),
                                 std::make_shared<DirectedBoundarySegment>(r4,false),
                                 std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_9,false)}},{3/*type*/});
    p.addReferenceDomain(yoke, {{std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_12,false),
                                 std::make_shared<DirectedBoundarySegment>(r5,false),
                                 std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_11,false),
                                 std::make_shared<DirectedBoundarySegment>(r3,true)}},{3/*type*/});
    p.addReferenceDomain(yoke, {{std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_5,true),
                                 std::make_shared<DirectedBoundarySegment>(r8,true),
                                 std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_3_1,false),
                                 std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_13,false)}},{3/*type*/});
    p.addReferenceDomain(yoke, {{std::make_shared<DirectedBoundarySegment>(r4,true),
                                 std::make_shared<DirectedBoundarySegment>(r9,true),
                                 std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_3_2,false),
                                 std::make_shared<DirectedBoundarySegment>(r8,false)}},{3/*type*/});
    p.addReferenceDomain(yoke, {{std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_6,true),
                                 std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_14,false),
                                 std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_3_3,false),
                                 std::make_shared<DirectedBoundarySegment>(r9,false)}},{3/*type*/});
    p.addReferenceDomain(yoke, {{std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_7,true),
                                 std::make_shared<DirectedBoundarySegment>(r10,true),
                                 std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_4_1,false),
                                 std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_15,false)}},{3/*type*/});
    p.addReferenceDomain(yoke, {{std::make_shared<DirectedBoundarySegment>(r5,true),
                                 std::make_shared<DirectedBoundarySegment>(r11,true),
                                 std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_4_2,false),
                                 std::make_shared<DirectedBoundarySegment>(r10,false)}},{3/*type*/});
    p.addReferenceDomain(yoke, {{std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_8,true),
                                 std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_16,false),
                                 std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_4_3,false),
                                 std::make_shared<DirectedBoundarySegment>(r11,false)}},{3/*type*/});
    p.addReferenceDomain(inner_air, {{std::make_shared<DirectedBoundarySegment>(r6,false)},
                                     {std::make_shared<DirectedBoundarySegment>(r7,true)}},{1/*type*/});
    dbsps.resize(num_rotor_slots*2);
    for (uint slot_idx = 0; slot_idx < num_rotor_slots; ++slot_idx) {
        dbsps[2*slot_idx]   = std::make_shared<DirectedBoundarySegment>(reference_boundaries_1[slot_idx],true);
        dbsps[2*slot_idx+1] = std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_3[slot_idx],true);
    }
    p.addReferenceDomain(rotor, {dbsps},{3/*type*/});
    for (uint slot_idx = 0; slot_idx < num_rotor_slots; ++slot_idx) {
        p.addReferenceDomain(rotor, {{std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_4[slot_idx],true),
                                      std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_1[(slot_idx+1u)%num_rotor_slots],true),
                                      std::make_shared<DirectedBoundarySegment>(all_slots_boundary_segments_2[(slot_idx+1u)%num_rotor_slots],true),
                                      std::make_shared<DirectedBoundarySegment>(reference_boundaries_1[(slot_idx+1u)%num_rotor_slots],false)}},{3/*type*/});
    }
    p.addReferenceDomain(inner_air, {{std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_1_2,true),
                                      std::make_shared<DirectedBoundarySegment>(coil_boundary_1,true),
                                      std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_13,true),
                                      std::make_shared<DirectedBoundarySegment>(p.addArcedBoundarySegment(da/2.0+delta,   /* radius */
                                                                                                          Vec2D{0.0,0.0}, /* center */
                                                                                                          -M_PI/2.0+phi2, /* phi_min */
                                                                                                           M_PI/2.0-phi2, /* phi_max */
                                                                                                          4,              /* num_nodes */
                                                                                                          1,              /* boundary_solution_polynomial_order */
                                                                                                          true,           /* normal_points_to_the_right */
                                                                                                          true            /* is_reference */),false),
                                      std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_16,true),
                                      std::make_shared<DirectedBoundarySegment>(coil_boundary_4,false)}},{3/*type*/});
    p.addReferenceDomain(inner_air, {{std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_2_2,true),
                                      std::make_shared<DirectedBoundarySegment>(coil_boundary_3,true),
                                      std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_15,true),
                                      std::make_shared<DirectedBoundarySegment>(p.addArcedBoundarySegment(da/2.0+delta,      /* radius */
                                                                                                          Vec2D{0.0,0.0}, /* center */
                                                                                                              M_PI/2.0+phi2, /* phi_min */
                                                                                                          3.0*M_PI/2.0-phi2, /* phi_max */
                                                                                                          4,                 /* num_nodes */
                                                                                                          1,                 /* boundary_solution_polynomial_order */
                                                                                                          true,              /* normal_points_to_the_right */
                                                                                                          true               /* is_reference */),false),
                                      std::make_shared<DirectedBoundarySegment>(yoke_inner_boundary_14,true),
                                      std::make_shared<DirectedBoundarySegment>(coil_boundary_2,false)}},{3/*type*/});

    /* Solve the problem */
    p.solve(true);
    //p.solveAndStore("results/dc_motor_nabla",true);
    //p.solveAndStore("results/dc_motor",true);
    //p.loadSolution("results/dc_motor");

    /* Plot the boundaries in the problem */
    sanji::figure("Permanent magnet synchronous machine simulation");
    for (const auto& boundary_segment_plotting_data : p.getBoundarySegmentsPlottingData(0.001/4.0, /* max_length_per_segment */
                                                                                        true       /* show_reference_boundaries */))
        sanji::plot(*boundary_segment_plotting_data.x,*boundary_segment_plotting_data.y,boundary_segment_plotting_data.style);

    /* Plot the normals in the problem */
    if (plot_normals)
        for (const auto& boundary_segment_plotting_vector_data : p.getNormals(0.001, /* max_length_per_segment */
                                                                              1.0e-3 /* arrow_length */))
            sanji::quiver(*boundary_segment_plotting_vector_data.x,
                          *boundary_segment_plotting_vector_data.y,
                          *boundary_segment_plotting_vector_data.u,
                          *boundary_segment_plotting_vector_data.v,boundary_segment_plotting_vector_data.style);
    
    /* Plot cosmetics */
    sanji::setPlotBackgroundColor(GRAY);
    sanji::setAxesRatio("equal");

    ///* Compute the air gap flux density */
    //// Compute the normal component of the B field along the boundary of the rotor
    //const uint     N              = 200;
    //Eigen::ArrayXd B_norm_bnd     = VectorXd::LinSpaced(N,0,N-1);
    //Eigen::ArrayXd B_norm_int_cl  = VectorXd::LinSpaced(N,0,N-1);
    //Eigen::ArrayXd B_norm_int_far = VectorXd::LinSpaced(N,0,N-1);
    //directed_boundary_segment_ptr dir_bnd_seg_ptr;
    //for (const auto& dir_bnd_seg_ptrs_ : inner_air->getDirectedBoundarySegmentPointers())
    //    for (const auto& dir_bnd_seg_ptr_ : dir_bnd_seg_ptrs_)
    //        if (yoke_inner_boundary_3.get() == &**dir_bnd_seg_ptr_)
    //            dir_bnd_seg_ptr = dir_bnd_seg_ptr_;
    //std::for_each(B_norm_bnd.cbegin(),B_norm_bnd.cend(),[N,&B_norm_bnd,&B_norm_int_cl,&B_norm_int_far,&p,&inner_air,&dir_bnd_seg_ptr](const int n) {
    //    // Compute the solution on the boundary
    //    auto [loop_idx,dir_bnd_seg_idx] = inner_air->getDirectedBoundarySegmentIndex((**dir_bnd_seg_ptr).getBoundarySegmentIndex());
    //    auto [B,x,nu]                   = p.evaluateBoundarySolution(inner_air,loop_idx,dir_bnd_seg_idx,static_cast<double>(n)/N);
    //    B_norm_bnd(n)                   = -nu.dot(B);

    //    // Compute the solution in the interior
    //    B_norm_int_cl(n)  = -nu.dot(p.evaluateInteriorSolution(inner_air,x-nu*1.0e-5));
    //    B_norm_int_far(n) = -nu.dot(p.evaluateInteriorSolution(inner_air,x-nu*1.0e-4));
    //});
    //sanji::figure("Another figure");
    //sanji::plot(VectorXd::LinSpaced(N,0,N-1)*2.0*M_PI/N,B_norm_bnd,{{"color",RED}});
    //sanji::plot(VectorXd::LinSpaced(N,0,N-1)*2.0*M_PI/N,B_norm_int_cl,{{"color",BLUE}});
    //sanji::plot(VectorXd::LinSpaced(N,0,N-1)*2.0*M_PI/N,B_norm_int_far,{{"color",BLACK}});

    /* Compute the Lorentz force acting on the rotor */
    if (compute_lorentz_force) {
        // Create a wrapper to simplify integration
        const auto quad = [](gsl_function& gsl_fun, gsl_integration_workspace* workspace)->double {
            double result,error;
            gsl_integration_qag(&gsl_fun, /* Function to integrate */
                                0.0,      /* Lower integration limit */
                                1.0,      /* Upper integration limit */
                                INTEGRAL_ABSOLUTE_ERROR_LIMIT,
                                INTEGRAL_RELATIVE_ERROR_LIMIT,
                                MAX_NUM_SUBINTERVALS,
                                INTEGRATION_KEY,
                                workspace,&result,&error);
            return result;
        };

        // Create a lambda to evaluate the solution in one of the coils
        const auto eval_sol_slot = [&p,&rotor_slots](const Vec2D x, const uint slot_idx)->const Vec2D {
            return p.evaluateInteriorSolution(rotor_slots[slot_idx],x);
        };

        // Compute the torque contributions from the individual slots
        vector<double> torques(num_rotor_slots);

        // Define the task for the worker threads
        const auto task = [&torques,&all_slots_boundary_segments_2,&all_slots_boundary_segments_3,&quad,&eval_sol_slot,&rotor_slots](const uint slot_idx) {
            // Define GSL functions and workspaces
            gsl_function               gsl_fun_x, gsl_fun_y;
            gsl_integration_workspace* gsl_workspace_x = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);
            gsl_integration_workspace* gsl_workspace_y = gsl_integration_workspace_alloc(MAX_NUM_SUBINTERVALS);

            // Create a lambda to compute the integral in local y direction (tangential direction)
            const double** params_y = new const double*[4];
            gsl_fun_y.params        = params_y;
            const auto integral_y = [&gsl_fun_y,gsl_workspace_y,&quad,&params_y,&eval_sol_slot](const double tx, const uint slot_index)->double {
                const Vec2D  origin = *reinterpret_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params_y)[1]));
                const Vec2D  diff_x = *reinterpret_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params_y)[2]));
                const Vec2D  diff_y = *reinterpret_cast<const Vec2D*>(static_cast<const void*>(reinterpret_cast<const double**>(params_y)[3]));

                const auto   integrand  = [slot_index,&eval_sol_slot,origin_y=origin+tx*diff_x,&diff_y](const double t)->double {
                    const Vec2D   r     = origin_y + t*diff_y;
                    const Vec2D   B     = eval_sol_slot(r,slot_index);

                    return r.dot(B);
                };
                gsl_fun_y.function = [](const double t, void* params)->double {
                    const decltype(integrand)& integrand_ = *static_cast<const decltype(integrand)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));

                    return integrand_(t);
                }; 
                params_y[0] = reinterpret_cast<const double*>(&integrand);

                return quad(gsl_fun_y,gsl_workspace_y);
            };

            // Create a lambda to compute the integral in local x direction (radial direction)
            const double** params_x = new const double*[2];
            gsl_fun_x.params        = params_x;
            const auto integral_x = [&gsl_fun_x,gsl_workspace_x,&quad](/*const double slot_length*/)->double {
                gsl_fun_x.function = [](const double t, void* params)->double {
                    const decltype(integral_y)& int_y      = *reinterpret_cast<const decltype(integral_y)*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[0]));
                    const uint                  slot_index = *reinterpret_cast<const uint*>(static_cast<const void*>(reinterpret_cast<const double**>(params)[1]));

                    return int_y(t,slot_index);
                };

                return quad(gsl_fun_x,gsl_workspace_x);
            };

            // Compute the length of rotor slot
            const double slot_length = (all_slots_boundary_segments_2[slot_idx]->getEndPoint()-all_slots_boundary_segments_2[slot_idx]->getStartPoint()).Norm2();
            const double slot_width  = (all_slots_boundary_segments_3[slot_idx]->getEndPoint()-all_slots_boundary_segments_3[slot_idx]->getStartPoint()).Norm2();
            const Vec2D  origin      = all_slots_boundary_segments_2[slot_idx]->getEndPoint();
            const Vec2D  diff_x      = all_slots_boundary_segments_2[slot_idx]->getStartPoint() - all_slots_boundary_segments_2[slot_idx]->getEndPoint();
            const Vec2D  diff_y      = all_slots_boundary_segments_3[slot_idx]->getEndPoint()   - all_slots_boundary_segments_3[slot_idx]->getStartPoint();

            // Prepare the arguments for the integral in local x-direction
            params_x[0] = reinterpret_cast<const double*>(&integral_y);
            params_x[1] = reinterpret_cast<const double*>(&slot_idx);

            // Prepare the arguments for the integrals in local y-direction
            params_y[1] = reinterpret_cast<const double*>(&origin);
            params_y[2] = reinterpret_cast<const double*>(&diff_x);
            params_y[3] = reinterpret_cast<const double*>(&diff_y);

            // Integrate over the rotor slot
            torques[slot_idx] = integral_x()*slot_length*slot_width*rotor_slots[slot_idx]->getJ();

            // Free memory
            delete[] params_x;
            delete[] params_y;
            gsl_integration_workspace_free(gsl_workspace_x);
            gsl_integration_workspace_free(gsl_workspace_y);
        };

        const auto task_cubature = [&torques,&all_slots_boundary_segments_2,&all_slots_boundary_segments_3,&rotor_slots,&eval_sol_slot](const uint slot_idx) {
            // Define some constants
            const double slot_length = (all_slots_boundary_segments_2[slot_idx]->getEndPoint()-all_slots_boundary_segments_2[slot_idx]->getStartPoint()).Norm2();
            const double slot_width  = (all_slots_boundary_segments_3[slot_idx]->getEndPoint()-all_slots_boundary_segments_3[slot_idx]->getStartPoint()).Norm2();
            const Vec2D  origin      = all_slots_boundary_segments_2[slot_idx]->getEndPoint();
            const Vec2D  diff_x      = all_slots_boundary_segments_2[slot_idx]->getStartPoint() - all_slots_boundary_segments_2[slot_idx]->getEndPoint();
            const Vec2D  diff_y      = all_slots_boundary_segments_3[slot_idx]->getEndPoint()   - all_slots_boundary_segments_3[slot_idx]->getStartPoint();

            // Allocate memory and define the integration boundaries
            double* xmin = new double[2]; xmin[0] = xmin[1] = 0.0;
            double* xmax = new double[2]; xmax[0] = xmax[1] = 1.0;
            double value, error;

            auto integrand = std::function<double(const double, const double)>([&diff_x,&diff_y,&eval_sol_slot,&slot_idx,&origin](const double tx, const double ty)->double {
                const Vec2D origin_y = origin + tx*diff_x;
                const Vec2D r        = origin_y + ty*diff_y;
                const Vec2D B        = eval_sol_slot(r,slot_idx);

                return r.dot(B);
            });

            const int ret = hcubature(1u /*fdim*/, cubature_integrand /*f*/, &integrand /*fdata*/,
                2 /*dim*/, xmin /*xmin*/, xmax /*xmax*/,
                0 /*maxEval*/, INTEGRAL_ABSOLUTE_ERROR_LIMIT /*reqAbsError*/, INTEGRAL_RELATIVE_ERROR_LIMIT /*reqRelError*/,
                ERROR_L1 /*norm*/,
                &value /*val*/, &error /*err*/);
            if (ret != 0) std::cout << "An integration error occurred." << std::endl;

            // Integrate over the rotor slot
            torques[slot_idx] = value*slot_length*slot_width*rotor_slots[slot_idx]->getJ();

            // Free memory
            delete[] xmin; delete xmax;
        };

        // Measure the time it takes to compute the torque
        const auto start_time = std::chrono::steady_clock::now();

        // Create worker threads
        vector<std::thread> worker_threads;
        for (uint slot_idx = 0; slot_idx < num_rotor_slots; ++slot_idx)
            //worker_threads.emplace_back(task,slot_idx);
            worker_threads.emplace_back(task_cubature,slot_idx);

        // Wait until all workers threads completed execution
        for (uint slot_idx = 0; slot_idx < num_rotor_slots; ++slot_idx)
            worker_threads[slot_idx].join();

        // Print the time it took to compute the torque
        std::cout << "It took " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-start_time).count()/1000.0 << "s to compute the torque." << std::endl;

        // Print information
        double torque = 0.0;
        for (uint slot_idx = 0; slot_idx < num_rotor_slots; ++slot_idx)
            torque += torques[slot_idx];
        std::cout << "The total torque produced by the machine is " << torque << "Nm/m." << std::endl;
    }

    // Compute the boundary integrals of the tangential derivatives for all domains
    if (compute_tang_ders) {
        std::cout << "The boundary integrals of the tangential derivative are:" << std::endl;
        uint dom_cnt = 0;
        for (const auto& dom_results : p.getTangentialDerivativesBoundaryIntegrals()) {
            if (std::get<1>(dom_results).size() != 0) std::cout << "Domain " << std::get<0>(dom_results) << ":" << std::endl;
            for (const double& r : std::get<1>(dom_results))
                std::cout << r << std::endl;
        }
    }

    //Plot the magnetic flux density */
    if (plot_field_data) {
        // Compute the field data
        //const auto field_data = p.getFieldData(2.0e6,load_field_data,store_field_data,"results/dc_motor");
        const auto field_data = p.getFieldData(2.0e6,load_field_data,store_field_data,"results/dc_motor_nabla");

        double min =  std::numeric_limits<double>::max();
        double max = -std::numeric_limits<double>::max();
        for (const auto& field_data : field_data)
            for (uint i = 0; i < field_data.x->rows(); ++i) {
                const double strength = Vec2D{(*field_data.u)(i),(*field_data.v)(i)}.Norm2();
                if (strength < min) min = strength;
                if (strength > max) max = strength;
            }
        for (const auto& field_data : field_data)
            sanji::quiver(*field_data.x,
                          *field_data.y,
                          *field_data.u,
                          //*field_data.v,{{"arrow_length",0.0006},{"colormap",TURBO},{"min",min},{"max",max}},{"use_colormap","center_arrows"});
                          *field_data.v,{{"arrow_length",0.0006},{"colormap",TURBO},{"min",min},{"max",max}},{"use_colormap","center_arrows","use_logscale"});

        /* Print statistics */
        std::cout << "The minimum is " << min << std::endl;
        std::cout << "The maximum is " << max << std::endl;
    }

    //// Compute the normal component of the B field along the inner boundary of the rotor
    //if (compute_bnd_sol) {
    //    // Control parameters
    //    const uint N  = 200u;
    //    const uint Ne = 10u;

    //    // Allocate memory for the results
    //    Eigen::ArrayXd B_norm_bnd = VectorXd::LinSpaced(N,0u,N-1u);
    //    vector<VectorXd> B_norm_int(Ne);
    //    for (uint ei = 0u; ei < Ne; ++ei) B_norm_int[ei] = VectorXd(N);

    //    // Find the correct directed boundary segment
    //    directed_boundary_segment_ptr dir_bnd_seg_ptr;
    //    for (const auto& dir_bnd_seg_ptrs_ : rotor->getDirectedBoundarySegmentPointers())
    //        for (const auto& dir_bnd_seg_ptr_ : dir_bnd_seg_ptrs_)
    //            if (rotor_outer_boundary_1.get() == &**dir_bnd_seg_ptr_)
    //                dir_bnd_seg_ptr = dir_bnd_seg_ptr_;

    //    // Compute the magnetic flux density near the boundary and at the boundary
    //    std::for_each(B_norm_bnd.cbegin(),B_norm_bnd.cend(),[N,&B_norm_bnd,&B_norm_int,&p,&rotor,&dir_bnd_seg_ptr](const int n) {
    //        const auto [loop_idx,dir_bnd_seg_idx] = rotor->getDirectedBoundarySegmentIndex((**dir_bnd_seg_ptr).getBoundarySegmentIndex());
    //        const auto [B,x,nu]                   = p.evaluateBoundarySolution(rotor,loop_idx,dir_bnd_seg_idx,static_cast<double>(n)/N);
    //        B_norm_bnd(n)                         = nu.dot(B);

    //        for (uint ei = 0; ei < Ne; ++ei) {
    //            const double t = static_cast<double>(ei)/(Ne-1);
    //            (*B_norm_int[ei])(n) = nu.dot(p.evaluateInteriorSolution(rotor,x-nu*std::pow(10,-(t*4+(1-t)*9))));
    //        }
    //    });


    //    // Plot the results
    //    sanji::figure("Magnetic flux in normal direction along boundary");
    //    for (uint ei = 0u; ei < Ne; ++ei) {
    //        sanji::plot(VectorXd::LinSpaced(N,0,N-1)*2.0*M_PI/N,*B_norm_int[ei],{{"color",BLACK}});
    //    }
    //    sanji::plot(VectorXd::LinSpaced(N,0,N-1)*2.0*M_PI/N,B_norm_bnd,{{"color",RED}});
    //    sanji::figure("Deviation from boundary value");
    //    sanji::plot(VectorXd::LinSpaced(Ne,9,4),error);
    //}
    //Eigen::VectorXd error(Ne);
    //for (uint ei = 0; ei < Ne; ++ei) {
    //    Eigen::ArrayXd diff = B_norm_bnd-B_norm_int[ei]->array();
    //    error(ei)           = (diff*diff).sqrt().mean();
    //}

    // Execute the application
    app.exec();
}
